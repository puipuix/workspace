using Godot;
using GodotUtils;
using System;
using System.Globalization;

[Tool]
public class MyCloud : Sprite
{
	private static PackedScene _packedScene = GD.Load<PackedScene>("res://Game/Common/Decorations/MyCloud.tscn");
	public static MyCloud Instance(Color color, int seed)
	{
		var cloud = (MyCloud)_packedScene.Instance();
		cloud.Color = color;
		cloud.Seed = seed;
		return cloud;
	}

	private NoiseTexture _noise;
	private ShaderMaterial _shader;

	[Export]
	public Color Color { get; set; } = Colors.White;

	private int _seed = 0;
	[Export]
	public int Seed { get => _seed; set { _seed = value; Flush(); } }

	public bool IsDisposed { get; private set; }

	public override void _Ready()
	{
		base._Ready();
		_shader = (ShaderMaterial)Material.Duplicate();
		Material = _shader;
		_noise = (NoiseTexture)Texture.Duplicate(true);
		Texture = _noise;
		Flush();
	}

	protected override void Dispose(bool disposing)
	{
		base.Dispose(disposing);
		if (disposing)
		{
			_noise = null;
			_shader = null;
			IsDisposed = true;
		}
	}

	private void Flush()
	{
		if (_noise != null && !IsDisposed)
		{
			_noise.Noise.Seed = Seed;
			_shader.SetShaderParam("Color", Color);

			Random r = new Random(Seed);
			_noise.Height = r.Next(256, 1024);
			_noise.Width = r.Next(_noise.Height, 2048);
			_noise.Noise.Period = r.NextFloat(30, 100);
			_shader.SetShaderParam("Fadeout", r.NextFloat(0.5f, 1.0f));
			_shader.SetShaderParam("Power", r.NextFloat(1f, 3f));
			_shader.SetShaderParam("Intensity", r.NextFloat(1.5f, 3f));
			_shader.SetShaderParam("MinAlpha", r.NextFloat(1f, 1.25f));
		}
	}
}
