using Godot;
using GodotUtils;
using System;

public class MyTree : Sprite
{
	private static PackedScene _packedScene = GD.Load<PackedScene>("res://Game/Common/Decorations/MyTree.tscn");
	private static Texture[] _textures =
	{
		GD.Load<Texture>("res://Content/Textures/tree1.png"),
		GD.Load<Texture>("res://Content/Textures/tree2.png"),
		GD.Load<Texture>("res://Content/Textures/tree3.png"),
		GD.Load<Texture>("res://Content/Textures/tree4.png"),
	};

	public static MyTree Instance(Random r)
	{
		var tree = (MyTree)_packedScene.Instance();
		tree.Texture = r.NextElement(_textures);
		var shader = (ShaderMaterial)tree.Material.Duplicate();
		shader.SetShaderParam("Offset", r.NextFloat(3));
		tree.Material = shader;
		return tree;
	}
}
