﻿using BPlan.Game.Common.Projectiles;
using BPlan.Game.Common.Vehicles.Planes;
using BPlan.Game.NetWork;
using Godot;
using GodotUtils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BPlan.Game.Common
{
    public struct MySpawnInfo
    {
        public string Name { get; }
        public Vector2 Origin { get; }
        public bool Reversed { get; }
        public int Direction => Reversed ? -1 : 1;
        public float Rotation => Reversed ? Mathf.Deg2Rad(195) : Mathf.Deg2Rad(-15);

        public MySpawnInfo(string name, Vector2 origin, bool reversed)
        {
            Name = name;
            Origin = origin;
            Reversed = reversed;
        }
    }

    public enum EMyScenarioRotationMode { Random, None, Forward, Backward }

    public static class EMyScenario
    {
        private static MyScenario[] _scenarios = {
        new MyPVPScenario(),
        new MyBiplanVsBiplanScenario(), new MyBiplanVsAttackerScenario(), new MyBiplanVsInterseptorScenario(), new MyBiplanVsBomberScenario(),
        new MyInterseptorVsInterseptorScenario(), new MyInterseptorVsAttackerScenario(), new MyInterseptorVsBomberScenario(),
        new MyAttackerVsAttackerScenario(), new MyAttackerVsBomberScenario(),
        new MyBomberVsBomberScenario()};

        public static MyScenario[] GetScenarios(string name = null)
        {
            return _scenarios.Where(s => name == null || s.Name.StartsWith(name)).ToArray();
        }
    }

    public abstract class MyScenario
    {
        private static readonly string[] _iaNames = new string[] {
            "Hipolito","Jeramy","Billy","Roscoe","Santo","Theron","Omer","Les","Lamar","Stephan","Brock","Tyson","Gus","Elden",
            "Garret","Walton","Kristopher","Dustin","Carmen","Judson","Fletcher","Davis","Timothy","Randall","Maximo","Pedro",
            "Quinton","Alton","Nestor","Nathaniel","Stefan","Kasey","Stevie","Odell","Lonnie","Oswaldo","Tyron","Vince","Ryan",
            "Micah","Rodolfo","Tad","Phillip","Denis","Cleveland","Shad","Raymon","Aron","Darin","Carol","Sherley","Shayna",
            "Julianne","Shellie","Rae","Celinda","Ellan","Janine","Akiko","Denyse","Lauralee","Sabine","Anika","Consuelo",
            "Tamatha","Jeanine","Tonie","Sharlene","Robbin","Mozell","Edna","Larue","Eden","Zella","Alica","Joella","Ruthann",
            "Mica","Celina","Kitty","Winifred","Lakia","Amiee","Kimberley","Loma","Annabel","Carole","Vertie","Brittani","Luz",
            "Christiane","Ozella","Judi","Abbey","Zandra","Keiko","Cristy","Paola","Somer" };

        public static string GenerateRandomName() => _iaNames[MyTools.RNG.Next(_iaNames.Length)];

        public Dictionary<int, MyIdentification>[] Team { get; private set; } = { };

        private int _botFreeId = 2;

        private Dictionary<int, MyIdentification>[] _botTeam = { };

        private float[] _spawnReversationUsed = { };

        private MySpawnInfo[] _spawnPoint = { };

        private int[] _playerAlives = { };

        public int GetPlayerAliveCount(int team) => _playerAlives[team];
        public IEnumerable<sbyte> GetTeamsAlives() => Enumerable.Range(0, _playerAlives.Length).Where(i => _playerAlives[i] > 0).Select(integer => (sbyte)integer);
        public string GetTeamName(int team) => _spawnPoint[team].Name;
        public MyScenarioData Data => new MyScenarioData(_spawnPoint.Select(s => s.Name).ToArray());

        public int TeamCount { get; private set; }
        protected int GenerateBotPlayerId() => _botFreeId++;

        protected void SetTeamCount(params MySpawnInfo[] spawnPoint)
        {
            TeamCount = spawnPoint.Length;
            _botFreeId = 2;
            Team = new Dictionary<int, MyIdentification>[TeamCount];
            _botTeam = new Dictionary<int, MyIdentification>[TeamCount];
            _spawnReversationUsed = new float[TeamCount];
            _playerAlives = new int[TeamCount];
            _spawnPoint = spawnPoint;

            for (int i = 0; i < TeamCount; i++)
            {
                Team[i] = new Dictionary<int, MyIdentification>();
                _botTeam[i] = new Dictionary<int, MyIdentification>();
                _spawnReversationUsed[i] = 0;
            }
        }

        protected void AddPlane(MyIdentification id, MyClientData client, EMyPlayerInputType input, Color color, MyBattle battle)
        {
            _playerAlives[id.TeamId]++;
            Team[id.TeamId].Add(id.ServerId, id);
            MySpawnInfo spawn = _spawnPoint[id.TeamId];
            float reservation = EMyPlanes.GetSpawnReservationFromId(id.ServerPlaneId) * spawn.Direction;
            float shift = _spawnReversationUsed[id.TeamId] + reservation * 0.5f;
            _spawnReversationUsed[id.TeamId] += reservation;
            Vector2 origin = new Vector2(spawn.Origin.x - shift, spawn.Origin.y - EMyPlanes.GetSpawnAltitudeFromId(id.ServerPlaneId));
            MyPlaneData data = new MyPlaneData(id.ServerPlaneId, id, input, color, new Transform2D(spawn.Rotation, origin), spawn.Reversed);
            battle.AddClientData(id, client);
            battle.AddPlane(data);
        }

        protected void AddBot(sbyte planeId, sbyte team, Color color, MyBattle battle, string name = null)
        {
            int index = GenerateBotPlayerId();
            MyClientData client = new MyClientData(1, name ?? GenerateRandomName(), planeId, EMyPlanes.GetRandomAmmoRacks(), color, true);
            MyIdentification id = new MyIdentification(index, team, planeId);
            _botTeam[team].Add(id.ServerId, id);
            AddPlane(id, client, EMyPlayerInputType.Ia, color, battle);
        }

        protected void RemoveBot(MyBattle battle, int team)
        {
            int botId = _botTeam[team].Keys.First();
            _playerAlives[team]--;
            _botTeam[team].Remove(botId);
            Team[team].Remove(botId);
            battle.RemovePlane(botId);
        }

        /// <summary>
        /// Return the team number or <see cref="MyIdentification.UnknowId"/> if no bot exist.
        /// </summary>
        /// <returns></returns>
        protected sbyte RemoveBotToReplaceByPlayer(MyBattle battle)
        {
            sbyte teamMax = MyIdentification.UnknowId;
            int ratioMax = 0;

            for (sbyte i = 0; i < _botTeam.Length; i++)
            {
                if (_botTeam[i].Count > 0)
                {
                    int ratio = 100 * _botTeam[i].Count / Team[i].Count;
                    if (ratio == ratioMax)
                    {
                        if (MyTools.RNG.NextBool())
                        {
                            teamMax = i;
                            ratioMax = ratio;
                        }
                    }
                    else if (ratio > ratioMax)
                    {
                        teamMax = i;
                        ratioMax = ratio;
                    }
                }
            }

            if (teamMax != MyIdentification.UnknowId)
            {
                RemoveBot(battle, teamMax);
            }

            return teamMax;
        }

        public virtual void RemovePlayer(MyClientData client, MyBattle battle)
        {
            var plane = battle.GetPlanes().Where(p => p.Identification.ServerId == client.NetworkId).FirstOrDefault();
            if (plane != null)
            {
                PlayerKilled(plane.Identification, battle);
            }
            battle.RemovePlane(client.NetworkId);
            for (int i = 0; i < Team.Length; i++)
            {
                Team[i].Remove(client.NetworkId);
            }
            MyLogText txt = new MyPlayerNameLogText(new MyIdentification(MyIdentification.ErrorId, 0, EMyPlanes.Biplan), client.UserName);
            battle.GlobalLog(txt + TranslationServer.Translate("LOG_LEAVE"));
        }

        public virtual void PlayerKilled(MyIdentification id, MyBattle battle)
        {
            _playerAlives[id.TeamId]--;
            if (GetPlayerAliveCount(id.TeamId) == 0)
            {
                PrintDefeat(id.TeamId, battle);
                var alives = GetTeamsAlives();
                int count = alives.Count();
                if (count <= 1)
                {
                    if (count == 0)
                    {
                        battle.LocalLog("LOG_NO_VIC");
                    }
                    else if (count == 1)
                    {
                        sbyte team = alives.First();
                        PrintVictory(team, battle);
                        battle.Rpc(battle.OnBattleEnd);
                    }
                }
            }
        }

        public virtual void Clear()
        {
            SetTeamCount();
        }

        protected void PrintDefeat(sbyte id, MyBattle battle)
        {
            battle.LocalLog(TranslationServer.Translate("LOG_TEAM") + new MyStringLogtext(GetTeamName(id), MyIdentification.GetIdentificationColor(new MyIdentification(MyIdentification.UnknowId, id, EMyPlanes.Biplan), battle.LocalIdentification)) + TranslationServer.Translate("LOG_LOS"));
        }

        protected void PrintVictory(sbyte id, MyBattle battle)
        {
            battle.LocalLog(TranslationServer.Translate("LOG_TEAM") + new MyStringLogtext(GetTeamName(id), MyIdentification.GetIdentificationColor(new MyIdentification(MyIdentification.UnknowId, id, EMyPlanes.Biplan), battle.LocalIdentification)) + TranslationServer.Translate("LOG_WIN"));
        }

        protected string ColorName(Color c)
        {
            return c.r == 0.99f && c.g == 0.99f && c.b == 0.5f ? "Puipuix" : null;
        }

        public abstract string Name { get; }
        public abstract void InitalizeBattle(MyBattle battle);
        public abstract void AddPlayer(MyClientData client, MyBattle battle);
    }

    public abstract class MyXVSYScenario : MyScenario
    {
        public override string Name => EMyPlanes.GetNameFromId(TeamPlaneId[0]) + " vs " + EMyPlanes.GetNameFromId(TeamPlaneId[1]);

        protected abstract sbyte[] TeamPlaneId { get; }

        protected MyDeferredCalls _botAdd;

        public override void AddPlayer(MyClientData client, MyBattle battle)
        {
            sbyte team = RemoveBotToReplaceByPlayer(battle);
            if (team == MyIdentification.UnknowId)
            {
                team = (sbyte)(Team[0].Count > Team[1].Count ? 1 : 0);
            }

            MyIdentification id = new MyIdentification(client.NetworkId, team, TeamPlaneId[team]);
            AddPlane(id, client, EMyPlayerInputType.Local, client.CustomColor, battle);

            MyLogText txt = new MyPlayerNameLogText(id);
            battle.GlobalLog(txt + (TranslationServer.Translate("LOG_JOIN") + GetTeamName(team) + "."));
            MyDebug.AddTab();
            MyDebug.Print("(id: " + client.NetworkId + ")");
            MyDebug.RemoveTab();
        }


        public override void InitalizeBattle(MyBattle battle)
        {
            SetTeamCount(
                new MySpawnInfo(EMyPlanes.GetNameFromId(TeamPlaneId[0]), new Vector2(-battle.MapLimit * 0.5f, -20), false),
                new MySpawnInfo(EMyPlanes.GetNameFromId(TeamPlaneId[1]), new Vector2(battle.MapLimit * 0.5f, -20), true));
            battle.GameRule = MyGameRule.Arcade;
            battle.ClearClientData();
            battle.BattleSeed = MyTools.RNG.Next();
            _botAdd = new MyDeferredCalls(battle.GetTree(), 0.1f)
                .PushFor(0, i => i < battle.IACount, i => i + 1, i =>
                {
                    if (Team[0].Count < battle.IACount)
                    {
                        Color c = MyTools.RNG.NextColor();
                        AddBot(TeamPlaneId[0], 0, c, battle, ColorName(c));
                    }
                    if (Team[1].Count < battle.IACount)
                    {
                        Color c = MyTools.RNG.NextColor();
                        AddBot(TeamPlaneId[1], 1, c, battle, ColorName(c));
                    }
                })
                .PushSingle(() => _botAdd = null);
            _botAdd.Start();
        }

        public override void Clear()
        {
            base.Clear();
            _botAdd?.Stop();
        }
    }

    public class MyBiplanVsBiplanScenario : MyXVSYScenario
    {
        protected override sbyte[] TeamPlaneId { get; } = { EMyPlanes.Biplan, EMyPlanes.Biplan };
    }

    public class MyBiplanVsInterseptorScenario : MyXVSYScenario
    {
        protected override sbyte[] TeamPlaneId { get; } = { EMyPlanes.Interseptor, EMyPlanes.Biplan };
    }

    public class MyBiplanVsAttackerScenario : MyXVSYScenario
    {
        protected override sbyte[] TeamPlaneId { get; } = { EMyPlanes.Biplan, EMyPlanes.Attacker };
    }

    public class MyBiplanVsBomberScenario : MyXVSYScenario
    {
        protected override sbyte[] TeamPlaneId { get; } = { EMyPlanes.Bomber, EMyPlanes.Biplan };
    }

    public class MyInterseptorVsInterseptorScenario : MyXVSYScenario
    {
        protected override sbyte[] TeamPlaneId { get; } = { EMyPlanes.Interseptor, EMyPlanes.Interseptor };
    }

    public class MyInterseptorVsAttackerScenario : MyXVSYScenario
    {
        protected override sbyte[] TeamPlaneId { get; } = { EMyPlanes.Attacker, EMyPlanes.Interseptor };
    }

    public class MyInterseptorVsBomberScenario : MyXVSYScenario
    {
        protected override sbyte[] TeamPlaneId { get; } = { EMyPlanes.Interseptor, EMyPlanes.Bomber };
    }

    public class MyAttackerVsAttackerScenario : MyXVSYScenario
    {
        protected override sbyte[] TeamPlaneId { get; } = { EMyPlanes.Attacker, EMyPlanes.Attacker };
    }

    public class MyAttackerVsBomberScenario : MyXVSYScenario
    {
        protected override sbyte[] TeamPlaneId { get; } = { EMyPlanes.Attacker, EMyPlanes.Bomber };
    }

    public class MyBomberVsBomberScenario : MyXVSYScenario
    {
        protected override sbyte[] TeamPlaneId { get; } = { EMyPlanes.Bomber, EMyPlanes.Bomber };
    }

    public class MyPVPScenario : MyScenario
    {
        public override string Name => "PVP";

        protected MyDeferredCalls _botAdd;

        public override void AddPlayer(MyClientData client, MyBattle battle)
        {
            sbyte team = RemoveBotToReplaceByPlayer(battle);
            if (team == MyIdentification.UnknowId)
            {
                team = (sbyte)(Team[0].Count > Team[1].Count ? 1 : 0);
            }

            MyIdentification id = new MyIdentification(client.NetworkId, team, client.CustomPlaneId);
            AddPlane(id, client, EMyPlayerInputType.Local, client.CustomColor, battle);

            MyLogText txt = new MyPlayerNameLogText(id);
            battle.GlobalLog(txt + (" Join the team " + GetTeamName(team) + "."));
            MyDebug.Print("(id: " + client.NetworkId + ")");
        }

        public override void InitalizeBattle(MyBattle battle)
        {
            SetTeamCount(
                new MySpawnInfo("Alpha", new Vector2(-battle.MapLimit * 0.5f, -20), false),
                new MySpawnInfo("Bravo", new Vector2(battle.MapLimit * 0.5f, -20), true));
            battle.GameRule = MyGameRule.Arcade;
            battle.ClearClientData();
            battle.BattleSeed = MyTools.RNG.Next();
            _botAdd = new MyDeferredCalls(battle.GetTree(), 0.1f)
               .PushFor(0, i => i < battle.IACount, i => i + 1, i =>
               {
                   if (Team[0].Count < battle.IACount)
                   {
                       Color c = MyTools.RNG.NextColor();
                       AddBot(EMyPlanes.GetRandomPlaneId(), 0, c, battle, ColorName(c));
                   }
                   if (Team[1].Count < battle.IACount)
                   {
                       Color c = MyTools.RNG.NextColor();
                       AddBot(EMyPlanes.GetRandomPlaneId(), 1, c, battle, ColorName(c));
                   }
               })
               .PushSingle(() => _botAdd = null);
            _botAdd.Start();
        }

        public override void Clear()
        {
            base.Clear();
            _botAdd?.Stop();
        }
    }
}
