using BPlan.Game;
using BPlan.Game.Common;
using BPlan.Game.Common.Projectiles;
using BPlan.Game.Common.Vehicles.Planes;
using BPlan.Game.NetWork;

using Godot;

using GodotUtils;

using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;

public class MyBattle : Node2D
{
    public const string SignalOnLocalIdChanged = "local_id_changed";
    public const int StartTime = 10;

    private static readonly DynamicFont BattleFont = GD.Load<DynamicFont>("res://Content/Fonts/LiberationFont.tres");
    private static readonly PackedScene _chatTextScene = GD.Load<PackedScene>("res://Game/Common/GUI/MyChatText.tscn");
    public static MyBattle CurrentBattle { get; private set; }

    #region Battle public data

    [Export]
    public int MaxTreeCount { get; set; } = 30;
    [Export]
    public int MinTreeCount { get; set; } = 10;
    [Export]
    public int TreeLoadCountPerTick { get; set; } = 10;
    [Export]
    public int MaxCloudCount { get; set; } = 30;
    [Export]
    public int MinCloudCount { get; set; } = 10;
    [Export]
    public int MaxCloudAltitude { get; set; } = 400;
    [Export]
    public int MinCloudAltitude { get; set; } = 200;
    [Export]
    public int CloudAltitudeVariation { get; set; } = 100;


    [Export]
    public float MapLimit { get; set; } = 8000f;
    public MyGameRule GameRule { get; set; }

    private bool _fightEnabled = false;
    public bool FightEnabled
    {
        get => _fightEnabled;
        set {
            if (IsInsideTree() && GetTree().HasNetworkPeer())
            {
                if (IsNetworkMaster())
                {
                    Rpc(nameof(SetFightEnabled), value);
                }
            }
            else
            {
                SetFightEnabled(value);
            }
        }
    }
    [PuppetSync]
    private void SetFightEnabled(bool value)
    {
        _fightEnabled = value;
        if (_fightEnabled)
        {
            _resultPanel.Hide();
        }
    }

    private object _focusedObject;
    private int _priority = 0;
    public bool UIFocused { get; private set; }

    public void UpdateUIFocused(object focused, bool value, int priority = 0)
    {
        if (value || (focused?.Equals(_focusedObject) ?? _focusedObject == null))
        {
            UIFocused = value;
            if (value)
            {
                if (_priority <= priority)
                {
                    _focusedObject = focused;
                    _priority = priority;
                }
            }
            else
            {
                _priority = -1;
                _focusedObject = null;
            }
        }
    }

    private MyIdentification? _localIdentification = null;
    public MyIdentification? LocalIdentification
    {
        get => _localIdentification;
        private set {
            _localIdentification = value;
            EmitSignal(SignalOnLocalIdChanged);
        }
    }

    public bool AutoStart { get; set; } = true;

    private int _battleSeed = 0;
    public int BattleSeed
    {
        get => _battleSeed;
        set {
            this.Rpc(RPCSetBattleSeed, value);
        }
    }

    public bool NetWorkEstablished { get; private set; }

    #endregion

    #region Nodes

    [MyOnReady("Loading")]
    public MyLoadingScreen Loading { get; private set; }

    [MyOnReady("MyBattleMenu", EMyNodeResearchMode.Descendant)]
    private MyBattleMenu _menu = null;

    [MyOnReady("GUI/MyTouchScreen")]
    public MyTouchScreen TouchScreen { get; private set; }

    [MyOnReady("BattleGround")]
    private Node2D _battleGround = null;

    [MyOnReady("ForeGround")]
    private Node2D _foreGround = null;

    [MyOnReady("BackGround")]
    private Node2D _backGround = null;

    [MyOnReady("CameraHolder")]
    public Node2D CameraHolder { get; private set; }

    [MyOnReady("CameraHolder/Camera")]
    private Camera2D _camera = null;

    [MyOnReady("Markers", EMyNodeResearchMode.Descendant)]
    private Control _markersZone = null;

    [MyOnReady("Chat", EMyNodeResearchMode.Descendant)]
    private VBoxContainer _chat = null;

    [MyOnReady("ChatVBox", EMyNodeResearchMode.Descendant)]
    private VBoxContainer _chatVBox = null;

    [MyOnReady("ChatHide", EMyNodeResearchMode.Descendant)]
    private TextureButton _chatHide = null;

    [MyOnReady("TeamDisplays", EMyNodeResearchMode.Descendant)]
    private HBoxContainer _teamDisplays = null;

    [MyOnReady("ResultDisplay", EMyNodeResearchMode.Descendant)]
    private HBoxContainer _resultDisplays = null;

    [MyOnReady("ResultPanel", EMyNodeResearchMode.Descendant)]
    private PanelContainer _resultPanel = null;

    [MyOnReady("ChatInput", EMyNodeResearchMode.Descendant)]
    private LineEdit _chatInput = null;

    [MyOnReady("ScrollChat", EMyNodeResearchMode.Descendant)]
    private ScrollContainer _scrollChat = null;

    [MyOnReady("PlaneDisplay", EMyNodeResearchMode.Descendant)]
    private Control _planeDisplay = null;

    [MyOnReady("PlaneDisplayFG", EMyNodeResearchMode.Descendant)]
    private Control _planeDisplayFG = null;

    [MyOnReady("PlaneDisplayBG", EMyNodeResearchMode.Descendant)]
    private Control _planeDisplayBG = null;

    [MyOnReady("MyPlaneStatDisplay", EMyNodeResearchMode.Descendant)]
    private MyPlaneStatDisplay _planeStat = null;

    [MyOnReady("AudioChase")]
    private AudioStreamPlayer _audioChase = null;

    [MyOnReady("AudioPrelude")]
    private AudioStreamPlayer _audioPrelude = null;

    [MyOnReady("AudioDevastation")]
    private AudioStreamPlayer _audioDevastation = null;

    private AudioStreamPlayer _currentPlaying = null;

    [MyOnReady("MyCountDownDIsplay", EMyNodeResearchMode.Descendant)]
    private MyCountDownDisplay _countDown = null;

    [MyOnReady("GrassFront")]
    private TextureRect _grassFront = null;

    [MyOnReady("DirtBack")]
    private ColorRect _dirtBack = null;

    [MyOnReady("GrassBack")]
    private ColorRect _grassBack = null;

    [MyOnReady("Ground/Shape")]
    private CollisionShape2D _ground = null;

    #endregion

    #region Scenario Var
    public List<MyScenario> ScenarioRotation { get; set; } = new List<MyScenario>() {
        new MyPVPScenario(),
    };

    public int CurrentScenarioIndex { get; private set; } = -1;
    public MyScenario Scenario { get; private set; }

    private MyScenarioData? _scenarioData;
    public MyScenarioData? ScenarioData
    {
        get => _scenarioData;
        private set {
            if (IsNetworkMaster())
            {
                var buffer = new byte[MyRPCConvert.GetNullableBufferLength(value)];
                int index = 0;
                index = MyRPCConvert.StoreNullable(value, buffer, index);
                this.Rpc(RPCSetScenarioData, buffer);
            }
        }
    }

    [PuppetSync]
    private void RPCSetScenarioData(byte[] buffer)
    {
        MyRPCConvert.UnStoreNullable(out _scenarioData, buffer, 0);
        RecreateTeamDisplays(_teamDisplays);
    }

    public EMyScenarioRotationMode ScenarioRotationMode { get; set; } = EMyScenarioRotationMode.Random;

    public int IACount { get; set; } = 5;

    #endregion

    private MyEntity _folowedEntity;
    private Dictionary<int, MyClientData> _playersData = new System.Collections.Generic.Dictionary<int, MyClientData>();
    private HashSet<int> _spectators = new HashSet<int>();
    private Dictionary<int, int> _killCount = new Dictionary<int, int>();
    private LinkedListNode<string> _chatInputSentNode;
    private LinkedList<string> _chatInputSent = new LinkedList<string>();
    private string _currentInputChat;
    private Dictionary<MyPlane, MyBattlePlaneMarker> _markers = new Dictionary<MyPlane, MyBattlePlaneMarker>();
    private SceneTreeTimer _startTimer;
    private MyDeferredCalls _decoration;

    public override void _Ready()
    {
        CurrentBattle = this;
        base._Ready();

        AddUserSignal(SignalOnLocalIdChanged);
        MyOnReadyAttribute.SetUp(this);
        var chatZone = new Control[] { _chatHide, _scrollChat };
        foreach (var item in chatZone)
        {
            item.Connect<object, bool, int>("mouse_entered", this, UpdateUIFocused, item, true, 0);
            item.Connect<object, bool, int>("mouse_exited", this, UpdateUIFocused, item, false, 0);
        }

        _chatInput.Connect<object, bool, int>("focus_entered", this, UpdateUIFocused, _chatInput, true, 10);
        _chatInput.Connect("focus_exited", this, ChatExited);

        (_audioChase.Stream as AudioStreamOGGVorbis).Loop = false;
        (_audioDevastation.Stream as AudioStreamOGGVorbis).Loop = false;
        (_audioPrelude.Stream as AudioStreamOGGVorbis).Loop = false;

        _camera.LimitLeft = (int)-MapLimit;
        _camera.LimitRight = (int)MapLimit;
        _grassFront.RectPosition = new Vector2(-MapLimit, _grassFront.RectPosition.y);
        _grassBack.RectPosition = new Vector2(-MapLimit, _grassBack.RectPosition.y);
        _dirtBack.RectPosition = new Vector2(-MapLimit, _dirtBack.RectPosition.y);
        _grassFront.RectSize = new Vector2(2 * MapLimit, _grassFront.RectSize.y);
        _grassBack.RectSize = new Vector2(2 * MapLimit, _grassBack.RectSize.y);
        _dirtBack.RectSize = new Vector2(2 * MapLimit, _dirtBack.RectSize.y);
        ((RectangleShape2D)_ground.Shape).Extents = new Vector2(MapLimit * 1.1f, 10);

        _chatHide.Connect<bool>("toggled", this, ChatHideToggled);
    }

    public void OnNetworkEstablished()
    {
        NetWorkEstablished = true;
        _menu.SetUpNetWork();
    }

    protected override void Dispose(bool disposing)
    {
        base.Dispose(disposing);
        if (disposing)
        {
            if (CurrentBattle == this)
            {
                CurrentBattle = null;
            }
            _decoration?.Stop();
            _decoration?.CallDeferred("free");
            _battleGround = _foreGround = _backGround = CameraHolder = null;
            _chat = null;
            _teamDisplays = null;
            _chatInput = null;
            _scrollChat = null;
            _planeDisplay = _planeDisplayFG = _planeDisplayBG = _markersZone = null;
            _planeStat = null;
            ScenarioRotation.Clear();
            ScenarioRotation = null;
            _scenarioData = null;
            _folowedEntity = null;
            _playersData.Clear();
            _playersData = null;
            _killCount.Clear();
            _killCount = null;
            _chatInputSentNode = null;
            _chatInputSent.Clear();
            _chatInputSent = null;
            _currentInputChat = null;
            _markers.Clear();
            _markers = null;
            _startTimer = null;
            _resultPanel = null;
            _resultDisplays = null;
        }
    }

    #region Client Methods

    public bool GetClientData(MyIdentification id, out MyClientData data)
    {
        return _playersData.TryGetValue(id.ServerId, out data);
    }

    public bool GetClientData(int serverId, out MyClientData data)
    {
        return _playersData.TryGetValue(serverId, out data);
    }

    public MyClientData? GetClientData(MyIdentification id)
    {
        return GetClientData(id.ServerId);
    }

    public MyClientData? GetClientData(int serverId)
    {
        if (_playersData.TryGetValue(serverId, out var data))
        {
            return data;
        }
        else
        {
            return null;
        }
    }

    public void ClearClientData()
    {
        if (IsNetworkMaster())
        {
            this.Rpc(RPCClearClientData);
        }
    }

    public void AddClientData(MyIdentification id, MyClientData data)
    {
        if (IsNetworkMaster())
        {
            this.Rpc(RPCAddClientData, id.ServerId, data.ToRPCValue());
        }
    }

    [PuppetSync]
    private void RPCClearClientData()
    {
        _playersData.Clear();
        _killCount.Clear();
    }

    [PuppetSync]
    private void RPCAddClientData(int id, byte[] buffer)
    {
        MyClientData data = new MyClientData(buffer);
        _playersData[id] = data;
        _killCount[id] = 0;
    }

    public void SpectateOn()
    {
        this.RpcId(1, SetSpectate, GetTree().GetNetworkUniqueId(), true);
    }

    public void SpectateOff()
    {
        this.RpcId(1, SetSpectate, GetTree().GetNetworkUniqueId(), false);
    }

    [MasterSync]
    private void SetSpectate(int id, bool value)
    {
        if (value)
        {
            if (!_spectators.Contains(id))
            {
                _spectators.Add(id);
            }
        }
        else
        {
            _spectators.Remove(id);
        }

    }

    #endregion

    #region Chat

    public void ClearChat()
    {
        _chat.RemoveAllChildren(n => n.QueueFree());
    }

    private void ChatExited()
    {
        UpdateUIFocused(_chatInput, false);
        _chatInputSentNode = null;
        _currentInputChat = null;
    }

    private void ChatHideToggled(bool pressed)
    {
        _chatVBox.Visible = !pressed;
    }

    private void UpdateChatInput()
    {
        if (_chatInput.HasFocus())
        {
            if (Input.IsActionJustPressed("ui_accept"))
            {
                if (!string.IsNullOrWhiteSpace(_chatInput.Text))
                {
                    if (_chatInput.Text[0] == '/')
                    {
                        LocalLog(_chatInput.Text);
                        MyBattleChatCommandManager.ParseAndExecute(this, _chatInput.Text);
                    }
                    else
                    {
                        GlobalLog((MyLogText)LocalIdentification.Value + (": " + _chatInput.Text));
                    }
                    _chatInputSent.AddLast(_chatInput.Text);
                }
                _chatInput.Text = "";
                _chatInput.ReleaseFocus();
            }
            else if (Input.IsActionJustPressed("ui_cancel"))
            {
                _chatInput.ReleaseFocus();
            }
            else if (Input.IsActionJustPressed("ui_up"))
            {
                if (_currentInputChat is null)
                {
                    _currentInputChat = _chatInput.Text ?? "";
                    _chatInputSentNode = _chatInputSent.Last;
                }
                else if (_chatInputSentNode != null && _chatInputSentNode.Previous != null)
                {
                    _chatInputSentNode = _chatInputSentNode.Previous;
                }

                if (_chatInputSentNode != null)
                {
                    _chatInput.Text = _chatInputSentNode.Value;
                }

            }
            else if (Input.IsActionJustPressed("ui_down"))
            {
                _chatInputSentNode = _chatInputSentNode?.Next;
                if (_chatInputSentNode != null)
                {
                    _chatInput.Text = _chatInputSentNode.Value;
                }
                else if (_currentInputChat != null)
                {
                    _chatInput.Text = _currentInputChat;
                    _currentInputChat = null;
                }
            }
        }
        else if (Input.IsActionJustPressed("ui_cancel"))
        {
            _menu.Visible = !_menu.Visible;
            UpdateUIFocused(_menu, _menu.Visible);
        }
        else if (Input.IsActionJustPressed("ui_accept"))
        {
            _chatInput.GrabFocus();
        }
    }

    public void LocalLog(MyLogText txt)
    {
        MyDebug.Print(txt.ToString());

        MyLogText dated = DateTime.Now.ToString("[HH:mm:ss] ") + txt;

        var margin = (MarginContainer)_chatTextScene.Instance();
        var lab = margin.GetNode<RichTextLabel>("RichTextLabel");
        lab.BbcodeText = dated.ToBBCode(LocalIdentification);
        float sizeY = BattleFont.GetWordwrapStringSize(lab.Text, 1200).y;
        lab.RectMinSize = new Vector2(600, sizeY);
        _chat.AddChild(margin);
        if (_chat.GetChildCount() > 20)
        {
            var child = _chat.GetChild(0);
            _chat.RemoveChild(child);
            child.QueueFree();
        }
        _scrollChat.ScrollVertical += 10000000;
    }
    [RemoteSync]
    private void RPCLocalLog(byte[] buffer)
    {
        LocalLog(MyLogText.FromRPCValue(buffer));
    }
    public void GlobalLog(MyLogText txt)
    {
        Rpc(nameof(RPCLocalLog), txt.ToRPCValue());
    }

    #endregion

    #region Controls

    private void FolowEntity(MyEntity entity)
    {
        _folowedEntity = entity;
        _planeDisplay.RemoveAllChildren(n => n.QueueFree());
        _planeDisplayFG.RemoveAllChildren(n => n.QueueFree());
        _planeDisplayBG.RemoveAllChildren(n => n.QueueFree());
        if (entity is MyPlane plane)
        {
            _planeStat.Plane = plane;
            foreach (var item in plane.GetChildren().OfType<MyComponent>())
            {
                var ds = MyComponentDisplay.Instance(item);
                ds.AddTo(_planeDisplayFG, _planeDisplay, _planeDisplayBG);
            }
        }
    }

    private void RefreshTeamDisplay(MyPlane plane)
    {
        foreach (var t in _teamDisplays.GetChildren().OfType<MyTeamDisplay>())
        {
            if (t.UpdatePlayer(plane))
            {
                break;
            }
        }
    }

    private void RecreateTeamDisplays(HBoxContainer container)
    {
        container.RemoveAllChildren(n => n.QueueFree());
        if (_scenarioData.HasValue)
        {
            for (int i = 0; i < _scenarioData.Value.TeamCount; i++)
            {
                var dis = MyTeamDisplay.Instance();
                container.AddChild(dis);
                dis.SetPlayers(this, i);
                if (i < _scenarioData.Value.TeamCount - 1)
                {
                    container.AddChild(new VSeparator());
                }
            }
        }
    }

    private void RefreshPlaneDisplay()
    {
        float scale = 3;
        _planeDisplayFG.RectScale = _planeDisplay.RectScale = _planeDisplayBG.RectScale = new Vector2(scale, scale);
        //PlaneDisplayRect.RectScale = new Vector2(scale, scale);
        //PlaneDisplayRect.RectPosition = new Vector2(GetViewport().Size.x - PlaneDisplayRect.RectSize.x * scale, GetViewport().Size.y - PlaneDisplayRect.RectSize.y * scale);
    }

    #endregion

    #region Scenario

    public void LoadScenario(int scenarioIndex)
    {
        if (IsNetworkMaster() && scenarioIndex >= 0)
        {
            _startTimer?.Disconnect("timeout", this, Start);
            FightEnabled = false;
            Rpc(nameof(Clear));
            Scenario?.Clear();
            CurrentScenarioIndex = scenarioIndex;
            if (CurrentScenarioIndex >= 0 && ScenarioRotation.Count > 0)
            {
                Scenario = ScenarioRotation[CurrentScenarioIndex % ScenarioRotation.Count];
                Scenario.InitalizeBattle(this);
                ScenarioData = Scenario.Data;
                foreach (var player in MyNetworkManager.CurrentInstance.PlayersData.Values.Where(c => !_spectators.Contains(c.NetworkId)))
                {
                    Scenario.AddPlayer(player, this);
                }

                GlobalLog(Tr("LOG_SCENARIO") + Scenario.Name + ".");
                if (AutoStart)
                {
                    GlobalLog(Tr("LOG_START") + StartTime + Tr("LOG_SECOND"));
                    _countDown.CountDown = StartTime;
                    _startTimer = GetTree().CreateTimer(StartTime);
                    _startTimer.Connect("timeout", this, Start);
                }
            }
            else
            {
                LocalLog("No scenario aviable.");
            }
        }
    }

    private void Start()
    {
        FightEnabled = true;
    }


    public void LoadNextScenario()
    {
        LoadScenario(CurrentScenarioIndex + 1);
    }

    public void RestartScenario()
    {
        LoadScenario(CurrentScenarioIndex);
    }

    public void LoadPreviousScenario()
    {
        if (CurrentScenarioIndex == 0)
        {
            LoadScenario(ScenarioRotation.Count - 1);
        }
        else
        {
            LoadScenario(CurrentScenarioIndex - 1);
        }
    }

    public void LoadRandomScenario()
    {
        LoadScenario(MyTools.RNG.Next(ScenarioRotation.Count));
    }

    public void AutoLoadScenario()
    {
        switch (ScenarioRotationMode)
        {
            case EMyScenarioRotationMode.Random: LoadRandomScenario(); break;
            case EMyScenarioRotationMode.None: RestartScenario(); break;
            case EMyScenarioRotationMode.Forward: LoadNextScenario(); break;
            case EMyScenarioRotationMode.Backward: LoadPreviousScenario(); break;
            default:
                throw new NotImplementedException("The rotation mode: " + ScenarioRotationMode + " isn't implemented.");
        }
    }

    [PuppetSync]
    public void OnBattleEnd()
    {
        RecreateTeamDisplays(_resultDisplays);
        _resultPanel.Show();
        if (IsNetworkMaster())
        {
            AutoLoadScenario();
        }
    }

    #endregion

    #region Battle Entity


    [PuppetSync]
    private void RPCSetBattleSeed(int seed)
    {
        _decoration?.Stop();
        _decoration?.CallDeferred("free");
        _battleSeed = seed;
        _foreGround.RemoveAllChildren(n => n.QueueFree());
        _backGround.RemoveAllChildren(n => n.QueueFree());
        Random r = new Random(_battleSeed);

        if (!(_currentPlaying?.Playing ?? false))
        {
            _currentPlaying = r.NextElement(_audioChase, _audioDevastation, _audioPrelude);
            _currentPlaying.Play();
        }

        int clouds = r.Next(MinCloudCount, MaxCloudCount);
        float cloudAltitude = r.NextFloat(MyConst.AltitudeToPx(MinCloudAltitude), MyConst.AltitudeToPx(MaxCloudAltitude));
        float cloudMin = cloudAltitude - MyConst.AltitudeToPx(CloudAltitudeVariation);
        float cloudMax = cloudAltitude + MyConst.AltitudeToPx(CloudAltitudeVariation);
        int trees = r.Next(MinTreeCount, MaxTreeCount);
        _decoration = new MyDeferredCalls(GetTree(), 0.16f)
            .PushWhile(() =>
            {
                var cloud = MyCloud.Instance(Colors.White, r.Next());
                _foreGround.AddChild(cloud);
                cloud.Position = new Vector2(r.NextFloat(-MapLimit, MapLimit), r.NextFloat(cloudMin, cloudMax));
                return clouds-- > 0;
            })
            .PushWhile(() =>
            {
                for (int i = 0; i < TreeLoadCountPerTick && trees > 0; trees--, i++)
                {
                    var tree = MyTree.Instance(r);
                    if (r.NextBool())
                    {
                        _backGround.AddChild(tree);
                    }
                    else
                    {
                        _foreGround.AddChild(tree);
                    }
                    tree.Position = new Vector2(r.NextFloat(-MapLimit, MapLimit), -tree.Texture.GetHeight());
                }
                return trees > 0;
            })
            .PushSingle(() => _decoration = null);
        _decoration.Start();
    }

    public IEnumerable<MyPlane> GetPlanes(int team = -1) => _battleGround.GetChildren().OfType<MyPlane>().Where(p => team < 0 || p.Identification.TeamId == team);
    public IEnumerable<MyProjectile> GetProjectiles() => _battleGround.GetChildren().OfType<MyProjectile>();


    private void RPCResultAddPlane(MyPlaneData data)
    {
        MyDebug.AddTab();
        MyDebug.Print(string.Concat("Adding new plane... (Server: ", data.Identification.ServerId, ", Client: ", data.Identification.GetClient(out MyClientData client) ? client.UserName : "Error", ")"));
        MyPlane plane = EMyPlanes.GetFromId(data.Identification.ServerPlaneId);

        _battleGround.AddChild(plane);
        plane.Initialize(data);

        var marker = MyBattlePlaneMarker.Instance(plane);
        _markersZone.AddChild(marker);
        _markers.Add(plane, marker);
        
        if (_folowedEntity == null || GetTree().GetNetworkUniqueId() == plane.Identification.ServerId)
        {
            MyDebug.AddTab();
            MyDebug.Print("Moving camera to new plane...");
            MyDebug.RemoveTab();
            FolowEntity(plane);
            if (GetTree().GetNetworkUniqueId() == plane.Identification.ServerId)
            {
                LocalIdentification = plane.Identification;
            }
        }
        MyDebug.RemoveTab();
        RecreateTeamDisplays(_teamDisplays);
    }
    [RemoteSync]
    private void RPCAddPlane(byte[] buffer)
    {
        MyPlaneData data = new MyPlaneData(buffer);
        RPCResultAddPlane(data);
    }
    public void AddPlane(MyPlaneData data)
    {
        if (IsNetworkMaster())
        {
            Rpc(nameof(RPCAddPlane), data.ToRPCValue());
        }
    }

    public void RemovePlane(int id)
    {
        if (IsNetworkMaster())
        {
            this.Rpc(RPCRemovePlane, id);
        }
    }
    [RemoteSync]
    private void RPCRemovePlane(int id)
    {
        MyDebug.Print("Removing plane... (Id: " + id + ")");
        MyPlane plane = _battleGround.GetNodeOrNull<MyPlane>(MyPlane.GenerateName(id));
        if (plane is MyPlane)
        {
            if (plane.Equals(_folowedEntity))
            {
                MyDebug.AddTab();
                MyDebug.Print("Moving camera to center...");
                MyDebug.RemoveTab();
                FolowEntity(null);
            }
            if (_markers.TryGetValue(plane, out var marker))
            {
                _markersZone.RemoveChild(marker);
                _markers.Remove(plane);
            }
            _battleGround.RemoveChild(plane);
            plane.QueueFree();
            if (_playersData.ContainsKey(id))
            {
                _playersData.Remove(id);
            }
            RecreateTeamDisplays(_teamDisplays);
        }
        else
        {
            MyDebug.AddTab();
            MyDebug.Print("Error Plane not found.");
            MyDebug.RemoveTab();
        }
    }


    [PuppetSync]
    public void Clear()
    {
        FolowEntity(null);
        _decoration?.Stop();
        _markers.Clear();
        _markersZone.RemoveAllChildren(n => n.QueueFree());
        _battleGround.RemoveAllChildren(n => n.QueueFree());
        _foreGround.RemoveAllChildren(n => n.QueueFree());
    }


    [Puppet]
    public void RPCRefreshClient(byte[] buffer)
    {
        MyDebug.Print("Data receive for refresh");
        Clear();
        int index = 0;
        index = MyRPCConvert.UnStore(out _fightEnabled, buffer, index);
        index = MyRPCConvert.UnStore(out int seed, buffer, index);
        RPCSetBattleSeed(seed);
        GameRule = new MyGameRule(buffer, ref index);
        index = MyRPCConvert.UnStoreNullable(out _scenarioData, buffer, index);
        RecreateTeamDisplays(_teamDisplays);
        _playersData.Clear();
        index = MyRPCConvert.UnStore(out (int id, MyClientData client, int kill)[] clients, (out (int id, MyClientData client, int kill) data, byte[] buf, int i) =>
        {
            i = MyRPCConvert.UnStore(out int key, buf, i);
            MyClientData client = new MyClientData(buf, ref i);
            i = MyRPCConvert.UnStore(out int kill, buf, i);
            data = (key, client, kill);
            return i;
        }, buffer, index);

        for (int i = 0; i < clients.Length; i++)
        {
            _playersData[clients[i].id] = clients[i].client;
            _killCount[clients[i].id] = clients[i].kill;
        }


        index = MyRPCConvert.UnStore(out MyPlaneData[] planes, (out MyPlaneData data, byte[] buf, int i) =>
        {
            data = new MyPlaneData(buf, ref i);
            return i;
        }, buffer, index);

        for (int i = 0; i < planes.Length; i++)
        {
            RPCResultAddPlane(planes[i]);
        }

        index = MyRPCConvert.UnStore(out MyProjectileData[] projectiles, (out MyProjectileData data, byte[] buf, int i) =>
        {
            data = new MyProjectileData(buf, ref i);
            return i;
        }, buffer, index);

        for (int i = 0; i < projectiles.Length; i++)
        {
            MyProjectile.AddProjectile(_battleGround, projectiles[i]);
        }

        Loading.Text = "LOAD_END";
        Loading.Progress = 1;
        Loading.FadeOut();
    }
    [Master]
    private void RefreshClient(int id)
    {
        if (IsNetworkMaster())
        {
            MyDebug.Print("Refresh Request from client: " + id);

            var planes = GetPlanes().Select(plane => new MyPlaneData(plane.PlaneId, plane.Identification, plane.PlayerInput.Type, plane.Color, plane.Transform, plane.Reversed)).ToArray();
            var projectiles = GetProjectiles().Select(projectile => new MyProjectileData(projectile.ProjectileId, projectile.Identification, projectile.Position, projectile.Velocity, Vector2.Zero, projectile.GunDamage, projectile.Name, projectile.Scale.x)).ToArray();
            var clients = _playersData.Select(k=> {
                if (!GetKillCount(k.Key, out int kill))
                {
                    kill = 0;
                }
                (int id, MyClientData client, int kill) tmp = (k.Key, k.Value, kill);
                return tmp;
            }).ToArray();
            int length = MyRPCConvert.GetBufferLength(_fightEnabled, _battleSeed, GameRule, planes, projectiles);
            length += MyRPCConvert.GetNullableBufferLength(_scenarioData);
            length += MyRPCConvert.GetArrayBufferLength(clients, t => MyRPCConvert.GetBufferLength(t.id, t.client, t.kill));

            byte[] buffer = new byte[length];

            int index = 0;
            index = MyRPCConvert.Store(_fightEnabled, buffer, index);
            index = MyRPCConvert.Store(_battleSeed, buffer, index);

            index = GameRule.ToRPCValue(buffer, index);
            index = MyRPCConvert.StoreNullable(_scenarioData, buffer, index);

            index = MyRPCConvert.Store(clients, ((int id, MyClientData client, int kill) client, byte[] buf, int i) =>
            {
                i = MyRPCConvert.Store(client.id, buf, i);
                i = client.client.ToRPCValue(buf, i);
                i = MyRPCConvert.Store(client.kill, buf, i);
                return i;
            }, buffer, index);

            index = MyRPCConvert.Store(planes, MyRPCConvert.Store, buffer, index);

            index = MyRPCConvert.Store(projectiles, MyRPCConvert.Store, buffer, index);

            this.RpcId(id, RPCRefreshClient, buffer);
        }
    }
    public void Refresh(float progressBar = 0)
    {
        if (!IsNetworkMaster())
        {
            MyDebug.Print("Sending Refresh Request");
            Loading.Text = "LOAD_REFRESH";
            Loading.Progress = progressBar;
            Loading.PopUp();
            this.RpcId(1, RefreshClient, GetTree().GetNetworkUniqueId());
        }
    }

    #endregion

    #region Kill

    public bool GetKillCount(int serverId, out int killCount)
    {
        return _killCount.TryGetValue(serverId, out killCount);
    }

    [PuppetSync]
    private void RPCNotifyPlaneKilledClient(int killedId, int killerId)
    {
        try
        {
            if (_killCount.TryGetValue(killerId, out int killCount))
            {
                _killCount[killerId] = killCount + 1;
            }

            int count = killerId == MyIdentification.UnknowId ? 1 : 0;
            foreach (var item in GetPlanes())
            {
                if (item.Identification.ServerId == killedId || (killerId != MyIdentification.UnknowId && item.Identification.ServerId == killerId))
                {
                    count++;
                    RefreshTeamDisplay(item);
                    if (count == 2)
                    {
                        break;
                    }
                }
            }
        }
        catch (Exception e)
        {
            MyDebug.PrintErr(e);
        }
    }

    public void NotifyPlaneKilled(MyPlane plane, int msgIndex, MyIdentification? origin)
    {
        if (IsNetworkMaster())
        {
            GlobalLog(EMyLogMessage.GetDeathMessage(plane.Identification, origin, msgIndex));
            Scenario?.PlayerKilled(plane.Identification, this);
            this.Rpc(RPCNotifyPlaneKilledClient, plane.Identification.ServerId, origin?.ServerId ?? MyIdentification.UnknowId);
        }
    }

    #endregion

    #region Ping

    [RemoteSync]
    private void RPCPingReply()
    {
        LocalLog("Ping: " + (DateTime.Now - _pingStart).TotalMilliseconds + "ms");
    }
    [RemoteSync]
    private void RPCPingRequest(int id)
    {
        RpcId(id, nameof(RPCPingReply));
    }
    private DateTime _pingStart;
    public void SendPingRequest()
    {
        _pingStart = DateTime.Now;
        RpcId(1, nameof(RPCPingRequest), GetTree().GetNetworkUniqueId());
    }
    #endregion

    private void UpdateCamera()
    {
        if (!UIFocused)
        {
            if (Input.IsActionJustPressed("plane_reverse"))
            {
                int current;
                if (_folowedEntity is null)
                {
                    current = 0;
                }
                else if (_folowedEntity is MyPlane plane && (!plane.IsAlive || plane.Identification != LocalIdentification))
                {
                    current = plane.Identification.ServerId;
                }
                else
                {
                    current = -1;
                }
                if (current > -1)
                {
                    IEnumerable<MyPlane> planeAlive = GetPlanes().Where(p => p.IsAlive);
                    IEnumerable<MyPlane> planes = planeAlive.Where(p => p.Identification.ServerId > current);
                    if (planes.IsEmptyEnumerable())
                    {
                        planes = planeAlive;
                    }
                    FolowEntity(planes.FindMin((a, b) => a.Identification.ServerId.CompareTo(b.Identification.ServerId)));
                }
            }
            else if (Input.IsActionJustPressed("plane_zoom_in") && _camera.Zoom.x > 0.25f)
            {
                _camera.Zoom *= 0.75f;
            }
            else if (Input.IsActionJustPressed("plane_zoom_out") && _camera.Zoom.x < 3f)
            {
                _camera.Zoom *= 1.25f;
            }
        }

        if (_folowedEntity.IsAccessible())
        {
            CameraHolder.GlobalPosition = _folowedEntity.GlobalPosition;

        }
        else
        {
            CameraHolder.Position = Vector2.Zero;
        }
    }

    public override void _Process(float delta)
    {
        base._Process(delta);
        UpdateCamera();
        UpdateChatInput();
        RefreshPlaneDisplay();
    }
}
