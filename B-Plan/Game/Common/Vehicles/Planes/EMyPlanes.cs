using BPlan.Game.Common.Projectiles;
using Godot;
using GodotUtils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BPlan.Game.Common.Vehicles.Planes
{
	public static class EMyPlanes
	{
		public const sbyte Biplan = 0, Interseptor = 1, Attacker = 2, Bomber = 3;
		private static PackedScene[] _planes;
		private static Texture[] _planeTextures;
		private static float[] _spawnAltitude;
		private static float[] _spawnReservation;
		private static string[] _planeNames;
		private static MyAmmoRack[][][] _aviableAmmo;

		public static void Preload()
		{
			_planes = new PackedScene[]{
				GD.Load<PackedScene>("res://Game/Common/Vehicles/Planes/MyBiplan.tscn"),
				GD.Load<PackedScene>("res://Game/Common/Vehicles/Planes/MyInterseptor.tscn"),
				GD.Load<PackedScene>("res://Game/Common/Vehicles/Planes/MyAttacker.tscn"),
				GD.Load<PackedScene>("res://Game/Common/Vehicles/Planes/MyBomber.tscn"),
			};
			_planeTextures = new Texture[] {
				GD.Load<Texture>("res://Content/Textures/Biplan.png"),
				GD.Load<Texture>("res://Content/Textures/Interseptor.png"),
				GD.Load<Texture>("res://Content/Textures/Attacker.png"),
				GD.Load<Texture>("res://Content/Textures/Bomber.png"),
			};
			_spawnAltitude = new float[] {
				0,
				0,
				0,
				20,
			}; _spawnReservation = new float[] {
				100,
				100,
				100,
				200,
			};
			_planeNames = new string[] {
				"BP-12",
				"XI-37",
				"IL-23",
				"GS-1237"
			};
		}

		public static void Load()
		{
			_aviableAmmo = new MyAmmoRack[][][] {
				new MyAmmoRack[][] { MyAmmoRack.MG12AmmoRacks, MyAmmoRack.MG12AmmoRacks, MyAmmoRack.NoneAmmoRacks, MyAmmoRack.NoneAmmoRacks },
				new MyAmmoRack[][] { MyAmmoRack.MG20AmmoRacks, MyAmmoRack.MG37AmmoRacks, MyAmmoRack.NoneAmmoRacks, MyAmmoRack.NoneAmmoRacks },
				new MyAmmoRack[][] { MyAmmoRack.MG23AmmoRacks, MyAmmoRack.MG12AmmoRacks, MyAmmoRack.RocketAmmoRacks, MyAmmoRack.NoneAmmoRacks },
				new MyAmmoRack[][] { MyAmmoRack.MG23AmmoRacks, MyAmmoRack.MG12AmmoRacks, MyAmmoRack.BombAmmoRacks, MyAmmoRack.NoneAmmoRacks },
			};
		}

		public static int PlaneCount => _planes.Length;
		

		public static MyAmmoRack[][] GetAviableAmmoRackFromId(int id)
		{
			return _aviableAmmo[id];
		}

		public static MyAmmoRack[] GetRandomAmmoRackFromId(int planeId)
		{
			var av = GetAviableAmmoRackFromId(planeId);
			return new MyAmmoRack[] { MyTools.RNG.NextElement(av[0]), MyTools.RNG.NextElement(av[1]), MyTools.RNG.NextElement(av[2]), MyTools.RNG.NextElement(av[3]) };
		}

		public static int[][][] GetRandomAmmoRacks()
		{
			var tab = new int[PlaneCount][][];
			for (int i = 0; i < PlaneCount; i++)
			{
				tab[i] = new int[4][];
				var ammo = GetRandomAmmoRackFromId(i);
				tab[i][0] = ammo[0];
				tab[i][1] = ammo[1];
				tab[i][2] = ammo[2];
				tab[i][3] = ammo[3];
			}
			return tab;
		}

		public static sbyte GetRandomPlaneId()
		{
			return (sbyte)MyTools.RNG.Next(_planes.Length);
		}

		public static MyPlane GetFromId(int id)
		{
			try
			{
				MyPlane p = (MyPlane)_planes[id].Instance();
				return p;
			}
			catch (Exception e)
			{
				MyDebug.PrintErr(new Exception("Error while loading plane: " + id + ".", e));
				return null;
			}
		}

		public static Texture GetPreviewFromId(int id)
		{
			try
			{
				return _planeTextures[id];
			}
			catch (Exception e)
			{
				MyDebug.PrintErr(new Exception("Error while loading texture: " + id + ".", e));
				return null;
			}
		}

		public static string GetNameFromId(int id)
		{
			try
			{
				return _planeNames[id];
			}
			catch (Exception e)
			{
				MyDebug.PrintErr(new Exception("Error while loading name: " + id + ".", e));
				return null;
			}
		}


		public static float GetSpawnReservationFromId(int id)
		{
			try
			{
				return _spawnReservation[id];
			}
			catch (Exception e)
			{
				MyDebug.PrintErr(new Exception("Error while loading spawn reservation: " + id + ".", e));
				return 0;
			}
		}


		public static float GetSpawnAltitudeFromId(int id)
		{
			try
			{
				return _spawnAltitude[id];
			}
			catch (Exception e)
			{
				MyDebug.PrintErr(new Exception("Error while loading altitude: " + id + ".", e));
				return 0;
			}
		}

	}
}
