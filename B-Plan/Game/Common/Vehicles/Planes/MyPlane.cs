using BPlan.Game;
using BPlan.Game.Common;
using BPlan.Game.Common.Components;
using BPlan.Game.Common.Projectiles;
using BPlan.Game.Common.Vehicles.Planes;
using BPlan.Game.Menu;
using BPlan.Game.NetWork;
using Godot;
using GodotUtils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//[Tool]
public class MyPlane : MyEntity, IMyDamageElement
{
    public static string GenerateName(int playerIndex) => "Pl" + playerIndex;

    public enum EMyEngineStatus { Start, Off, HighToLow, Low, High }

    #region Performances

    [Export]
    public float EngineThrust { get; set; } = 1f;

    [Export]
    public float AirResistance { get; set; } = 0.05f;

    [Export]
    public float WingLift { get; set; } = .02f;

    [Export]
    public float MaxWingLiftSpeed { get; set; } = 0.99f;

    [Export]
    public float ReversedWingLiftEfficiency { get; set; } = 0.5f;

    [Export]
    public float Manoeuvrability { get; set; } = 90;

    [Export]
    public float EnergyConservation { get; set; } = 0.95f;

    [Export]
    public float StallAngle { get; set; } = 50;

    [Export]
    public float MaxAltitude { get; set; } = 1000;

    [Export]
    public EmyGunType PrimaryGunType { get; set; } = EmyGunType.Gun;

    [Export]
    public EmyGunType SecondaryGunType { get; set; } = EmyGunType.None;

    [Export]
    public EmyGunType ThirdGunType { get; set; } = EmyGunType.None;

    [Export]
    public EmyGunType FourthGunType { get; set; } = EmyGunType.None;

    #endregion

    #region External Set

    public MyPlayerInput PlayerInput { get; set; } = new MyLocalPlayerInput();

    private Color _color = Colors.White;

    public Color Color
    {
        get => _color;
        set {
            _color = value;
            foreach (var item in GetChildren().OfType<MyComponent>())
            {
                item.Color = value;
            }
            (BotAim?.Material as ShaderMaterial)?.SetShaderParam("Albedo", Color);
        }
    }

    public string DebugInfo { get; set; } = "";

    #endregion


    #region Nodes

    [MyOnReady("StallEmitter")]
    private Particles2D _stallEmitter = null;

    [MyOnReady("NameDisplay")]
    private MyNameDisplay _nameDisplay = null;

    MyComponentGroup<MyComponent> _wings = new MyComponentGroup<MyComponent>(),
        _tails = new MyComponentGroup<MyComponent>(),
        _engines = new MyComponentGroup<MyComponent>(),
        _pilots = new MyComponentGroup<MyComponent>(),
        _bodies = new MyComponentGroup<MyComponent>();

    MyComponentGroup<MyGun> _primaryGuns = new MyComponentGroup<MyGun>();
    MyComponentGroup<MyGun> _fourthGuns = new MyComponentGroup<MyGun>();
    MyComponentGroup<MyGun> _thirdGuns = new MyComponentGroup<MyGun>();
    MyComponentGroup<MyGun> _secondaryGun = new MyComponentGroup<MyGun>();

    [MyOnReady("BotAim")]
    public RayCast2D BotAim { get; private set; }

    [MyOnReady("Velocity")]
    private Line2D _velocityLine = null;
    [MyOnReady("StallAngleMin")]
    private Line2D _minStallAngleLine = null;
    [MyOnReady("StallAngleMax")]
    private Line2D _maxStallAngleLine = null;

    [MyOnReady("EngineHigh")]
    private AudioStreamPlayer2D _engineHigh = null;

    [MyOnReady("EngineLow")]
    private AudioStreamPlayer2D _engineLow = null;

    [MyOnReady("EngineHighToLow")]
    private AudioStreamPlayer2D _engineHighToLow = null;

    [MyOnReady("EngineOff")]
    private AudioStreamPlayer2D _engineOff = null;

    [MyOnReady("EngineStart")]
    private AudioStreamPlayer2D _engineStart = null;

    private AudioStreamPlayer2D _engineSoundPlaying;

    #endregion

    public Vector2 Velocity { get; private set; }
    public float AngularVelocity { get; private set; }

    private float _stability = 1;

    /// <summary>
    /// The plane stall if < 0
    /// </summary>
    public float Stability
    {
        get => _stability;
        private set {
            _stability = value;
            bool stall = _stability < 0.7f && Velocity.LengthSquared() > 20;
            (_stallEmitter.ProcessMaterial as ParticlesMaterial).Scale = Mathf.Clamp((1 - Stability) / 0.7f, 0, 1) * 5;
            if (stall != _stallEmitter.Emitting)
            {
                _stallEmitter.Emitting = stall;
            }
        }
    }

    public bool IsStalling => Stability < 0;

    private bool _isAlive = true;
    public bool IsAlive
    {
        get => _isAlive;
        private set {
            if (IsNetworkMaster())
            {
                this.Rpc(RPCSetIsAlive, value);
            }
        }
    }

    public int PlaneId { get; private set; }

    [PuppetSync]
    private void RPCSetIsAlive(bool alive)
    {
        _isAlive = alive;
    }

    public float EngineThrottle { get; set; } = 0f;

    public int KillCount
    {
        get {
            if (Battle.GetKillCount(Identification.ServerId, out int kill))
            {
                return kill;
            }
            else
            {
                return 0;
            }
        }
    }

    private MyIdentification? _lastHit;
    private int? _lastHitMsg;

    private EMyEngineStatus _engineStatus = EMyEngineStatus.Start;

    public override void _Ready()
    {
        base._Ready();
        if (!Engine.EditorHint)
        {
            MyOnReadyAttribute.SetUp(this);
            _wings.SetUp(this, EMyComponentType.Wing);
            _tails.SetUp(this, EMyComponentType.Tail);
            _engines.SetUp(this, EMyComponentType.Engine);
            _pilots.SetUp(this, EMyComponentType.Pilot);
            _bodies.SetUp(this, EMyComponentType.Body);
            _primaryGuns.SetUp(this, EMyComponentType.PrimaryGun);
            _secondaryGun.SetUp(this, EMyComponentType.SecondaryGun);
            _fourthGuns.SetUp(this, EMyComponentType.FourthGun);
            _thirdGuns.SetUp(this, EMyComponentType.ThirdGun);

            _engineHigh.Stream = (AudioStream)_engineHigh.Stream.Duplicate(true);
            _engineStart.Stream = (AudioStream)_engineStart.Stream.Duplicate(true);
            _engineLow.Stream = (AudioStream)_engineLow.Stream.Duplicate(true);
            _engineHighToLow.Stream = (AudioStream)_engineHighToLow.Stream.Duplicate(true);
            _engineOff.Stream = (AudioStream)_engineOff.Stream.Duplicate(true);

            (_engineOff.Stream as AudioStreamOGGVorbis).Loop = false;
            (_engineStart.Stream as AudioStreamOGGVorbis).Loop = false;
            (_engineHighToLow.Stream as AudioStreamOGGVorbis).Loop = false;
            (_engineHigh.Stream as AudioStreamOGGVorbis).LoopOffset = 3;

            var shader = (ShaderMaterial)BotAim.Material.Duplicate();
            shader.SetShaderParam("Albedo", Color);
            BotAim.Material = shader;

            var timer = GetNode<Timer>("Timer");
            timer.WaitTime = MyTools.RNG.NextFloat() * 3;
            timer.Connect("timeout", this, OnTimerOut);
            timer.Start();

            _stallEmitter.ProcessMaterial = (Material)_stallEmitter.ProcessMaterial.Duplicate();
            _stallEmitter.Amount = (int)Mathf.Lerp(6, 32, (float)MyGameSettings.Particle / 100);
        }
    }

    protected override void Dispose(bool disposing)
    {
        base.Dispose(disposing);
        if (disposing)
        {
            PlayerInput?.Dispose();
            PlayerInput = null;
            _wings.Dispose();
            _tails.Dispose();
            _engines.Dispose();
            _pilots.Dispose();
            _bodies.Dispose();
            _wings = _tails = _engines = _pilots = _bodies = null;
            _primaryGuns.Dispose();
            _secondaryGun.Dispose();
            _thirdGuns.Dispose();
            _fourthGuns.Dispose();
            _primaryGuns = _fourthGuns = _secondaryGun = _thirdGuns = null;
            BotAim = null;
            _velocityLine = _minStallAngleLine = _maxStallAngleLine = null;
            _engineHigh = _engineLow = _engineHighToLow = _engineOff = _engineSoundPlaying = null;
        }
    }

    private void OnTimerOut()
    {
        _engineStart.Play();
    }

    public void Initialize(MyPlaneData data)
    {
        Identification = data.Identification;
        PlayerInput = MyPlayerInput.NewFromType(data.InputType);
        AddChild(PlayerInput);
        Transform = data.Transform;
        MyRotation = data.Transform.Rotation;
        Reversed = data.Reversed;
        Color = data.Color;
        PlaneId = data.PlaneId;
        foreach (var item in _primaryGuns)
        {
            item.AmmoRack = Identification.GetClient()?.GetPrimaryAmmo(PlaneId) ?? MyAmmoRack.GetFromId(MyAmmoRack.None);
        }
        foreach (var item in _secondaryGun)
        {
            item.AmmoRack = Identification.GetClient()?.GetSecondaryAmmo(PlaneId) ?? MyAmmoRack.GetFromId(MyAmmoRack.None);
        }
        foreach (var item in _thirdGuns)
        {
            item.AmmoRack = Identification.GetClient()?.GetThirdAmmo(PlaneId) ?? MyAmmoRack.GetFromId(MyAmmoRack.None);
        }
        foreach (var item in _fourthGuns)
        {
            item.AmmoRack = Identification.GetClient()?.GetFourthAmmo(PlaneId) ?? MyAmmoRack.GetFromId(MyAmmoRack.None);
        }

        Battle.Connect(MyBattle.SignalOnLocalIdChanged, this, nameof(UpdateName));
        SetNetworkMaster(1);
        UpdateName();
        if (IsNetworkMaster())
        {
            UpdateIsAlive();
        }
        else
        {
            GetChildren().OfType<MyComponent>().ForEach(c => c.AskUpdateState());
        }
    }

    public void Kill()
    {
        if (IsNetworkMaster())
        {
            this.Rpc(RPCKill);
        }
    }

    [PuppetSync]
    private void RPCKill()
    {
        foreach (var item in _pilots)
        {
            item.Damage(short.MaxValue, EMyLogMessage.Crash, null);
        }
    }

    private void UpdateName()
    {
        Name = GenerateName(Identification.ServerId);
        if (_nameDisplay != null)
        {
            _nameDisplay.Text = Identification.GetClient()?.UserName ?? "Error";
            _nameDisplay.Color = MyIdentification.GetIdentificationColor(Identification, Battle.LocalIdentification);
        }
    }

    /// <summary>
    /// Return if alive has changed
    /// </summary>
    /// <returns></returns>
    private bool UpdateIsAlive()
    {
        bool was = IsAlive;
        IsAlive = !(_engines.AreDestroyed || _tails.AreDestroyed || _pilots.AreDestroyed || _wings.AreDestroyed);
        if (was && !IsAlive)
        {
            if (_lastHitMsg == EMyLogMessage.SelfExplosion && _lastHit.HasValue)
            {
                Battle.NotifyPlaneKilled(this, EMyLogMessage.Explosion, _lastHit);
            }
            else if (_lastHitMsg == EMyLogMessage.Crash && _lastHit.HasValue)
            {
                Battle.NotifyPlaneKilled(this, EMyLogMessage.CauseCrash, _lastHit);
            }
            else
            {
                Battle.NotifyPlaneKilled(this, _lastHitMsg ?? EMyLogMessage.Crash, _lastHit);
            }
        }
        return was != IsAlive;
    }

    private void UpdateInput(float delta, out bool engineChanged, out float rotation, out bool pilotDestroyed)
    {
        PlayerInput.Update(delta, this);
        rotation = 0f;
        pilotDestroyed = _pilots.AreDestroyed;
        engineChanged = false;
        if (IsAlive)
        {
            rotation -= PlayerInput.PlaneRotatePressed();
            rotation += PlayerInput.PlaneRotateInvPressed();
            if (PlayerInput.PlaneIncreaseThrustPressed())
            {
                if (EngineThrottle + delta <= 1)
                {
                    engineChanged = true;
                    EngineThrottle += delta;
                }
            }
            if (PlayerInput.PlaneDecreaseThrustPressed())
            {
                if (EngineThrottle - delta >= 0)
                {
                    engineChanged = true;
                    EngineThrottle -= delta;
                }
            }

            if (PlayerInput.PlaneReverseJustPressed())
            {
                Reversed = !Reversed;
            }

            if (PlayerInput.PlanePrimaryGunPressed())
            {
                foreach (var item in _primaryGuns)
                {
                    item.Shot();
                }
            }

            if (PlayerInput.PlaneSecondaryGunPressed())
            {
                foreach (var item in _secondaryGun)
                {
                    item.Shot();
                }
            }

            if (PlayerInput.PlaneThirdGunPressed())
            {
                foreach (var item in _thirdGuns)
                {
                    item.Shot();
                }
            }

            if (PlayerInput.PlaneFourthGunPressed())
            {
                foreach (var item in _fourthGuns)
                {
                    item.Shot();
                }
            }
        }
    }

    private void PlayEngine(AudioStreamPlayer2D toplay, EMyEngineStatus status)
    {
        _engineSoundPlaying?.Stop();
        toplay.Play();
        _engineSoundPlaying = toplay;
        _engineStatus = status;
    }

    [Puppet]
    private void RPCUpdateVelocity(Vector2 velocity, float angularVelocity)
    {
        Velocity = velocity;
        AngularVelocity = angularVelocity;
        if (!IsNetworkMaster())
        {
            Stability = 1 - Mathf.Pow(Velocity.Normalized().AngleTo(ForwardVector) / Mathf.Deg2Rad(StallAngle), 2);
        }
    }

    [PuppetSync]
    private void RPCUpdateAudio(float enginePower, bool isAlive)
    {
        if (isAlive)
        {
            if (_engineStatus == EMyEngineStatus.Start) // wait signal
            {
                if (_engineLow.Playing)
                {
                    _engineStatus = EMyEngineStatus.Low;
                    _engineSoundPlaying = _engineLow;
                }
            }
            else if (_engineStatus == EMyEngineStatus.HighToLow) // wait signal
            {
                if (_engineLow.Playing)
                {
                    _engineStatus = EMyEngineStatus.Low;
                    _engineSoundPlaying = _engineLow;
                }
            }
            else
            {
                if (enginePower < 0.5f && _engineStatus == EMyEngineStatus.High)
                {
                    PlayEngine(_engineHighToLow, EMyEngineStatus.HighToLow);
                }
                else if (enginePower >= 0.5f && _engineStatus == EMyEngineStatus.Low)
                {
                    PlayEngine(_engineHigh, EMyEngineStatus.High);
                }
            }
        }
        else if (_engineStatus != EMyEngineStatus.Off)
        {
            PlayEngine(_engineOff, EMyEngineStatus.Off);
        }
    }

    public override void _Process(float delta)
    {
        base._Process(delta);

        if (Battle.FightEnabled)
        {
            var nw = Identification.GetClient()?.NetworkId ?? 1;
            if (IsNetworkMaster() || nw == GetTree().GetNetworkUniqueId())
            {
                UpdateInput(delta, out bool engineChanged, out float rotation, out bool pilotDestroyed);

                // Air resistance
                // V * V.abs() to get the opposite square vector
                Velocity -= Velocity * Velocity.Abs() * AirResistance * 0.01f * (IsStalling ? 5 : 1) * delta;

                // Engine thrust
                Velocity += ForwardVector * EngineThrottle * MyConst.MToPx(EngineThrust) * _engines.TotalEffectiveness * delta;

                // Gravity
                float grav = MyConst.Gravity * delta;
                Velocity += Vector2.Down * grav;

                // Wing lift
                float speedX = Math.Abs(Velocity.x);
                float airDensity = Math.Min((MaxAltitude - MyConst.PxToAltitude(Position.y)) / 500, 1);
                airDensity = Mathf.Pow(airDensity, 5);
                float effectiveness = _wings.TotalEffectiveness;
                float liftFromRotation = UpVector.y * UpVector.y;
                if ((!Reversed && UpVector.y > 0) || (Reversed && UpVector.y < 0))
                {
                    liftFromRotation *= ReversedWingLiftEfficiency;
                }
                // Can't be higher of a % of the gravity
                float lift = airDensity * liftFromRotation * WingLift * effectiveness * Mathf.Pow(speedX, 1.2f);
                lift = Math.Min(MyConst.Gravity * MaxWingLiftSpeed, lift);
                Velocity += Vector2.Up * lift * delta;

                // Rotation
                rotation *= Mathf.Deg2Rad(Manoeuvrability) * _pilots.TotalEffectiveness * _tails.TotalEffectiveness;
                rotation *= 1 - Math.Min(speedX * 0.0005f, 0.9f);

                AngularVelocity = rotation;
                rotation *= delta;
                MyRotate(rotation);

                // Checking stall to prevent rotate the velocity
                Stability = 1 - Mathf.Pow(Velocity.Normalized().AngleTo(ForwardVector) / Mathf.Deg2Rad(StallAngle), 2);
                if (!IsStalling)
                {
                    Velocity = Velocity.Rotated(rotation * EnergyConservation * Stability);
                }

                // Debug
                if (_velocityLine.Visible)
                {
                    _velocityLine.GlobalRotation = _minStallAngleLine.GlobalRotation = _maxStallAngleLine.GlobalRotation = 0;
                    _velocityLine.Scale = _minStallAngleLine.Scale = _maxStallAngleLine.Scale = Scale;
                    _velocityLine.SetPointPosition(1, Velocity.Normalized() * 120);
                    _minStallAngleLine.SetPointPosition(1, ForwardVector.Rotated(Mathf.Deg2Rad(-StallAngle)) * 100);
                    _maxStallAngleLine.SetPointPosition(1, ForwardVector.Rotated(Mathf.Deg2Rad(StallAngle)) * 100);
                }

                bool tailsDestroyed = _tails.AreDestroyed;

                float rotationCoef = tailsDestroyed ? 0.005f : 0;
                rotationCoef += pilotDestroyed ? 0.001f : 0;

                MyRotate(Math.Max(0, Velocity.Length() - 200) * delta * rotationCoef * (Reversed ? 1 : -1));
                Velocity = MoveAndSlide(Velocity);

                if (GetSlideCount() != 0)
                {
                    Velocity -= Velocity * (EngineThrottle > 0.5f ? 0.01f : 0.1f) * delta;
                }

                if (IsNetworkMaster())
                {
                    if (engineChanged || UpdateIsAlive())
                    {
                        this.Rpc(RPCUpdateAudio, EngineThrottle, IsAlive);
                    }
                    this.Rpc(RPCUpdateVelocity, Velocity, AngularVelocity);
                }
            }
            else
            {
                MoveAndSlide(Velocity);
                MyRotate(AngularVelocity * delta);
            }

        }
        if (!IsAlive)
        {
            _nameDisplay.Color = MyIdentification.GetIdentificationColor(Identification, Battle.LocalIdentification).Darkened(0.75f);
        }
    }


    public void OnComponentDestroyed(int msg, MyIdentification? origin)
    {
        if (IsNetworkMaster() && IsAlive)
        {
            if (origin.HasValue)
            {
                _lastHit = origin;
            }
            _lastHitMsg = msg;
            UpdateIsAlive();
        }
    }


    public void OnComponentDamaged(MyIdentification? origin)
    {
        if (IsNetworkMaster() && origin.HasValue)
        {
            _lastHit = origin.Value;
        }
    }

    public void DamageComponent(MyComponent componant)
    {
        if (IsNetworkMaster() && Battle.GameRule.IsRamDamageValid(Identification, componant.Plane.Identification))
        {
            componant.Damage(short.MaxValue, EMyLogMessage.Ram, Identification);
        }
    }
}
