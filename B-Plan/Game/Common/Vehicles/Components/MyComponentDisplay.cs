using BPlan.Game.Common.Components;
using Godot;
using GodotUtils;
using System;

public class MyComponentDisplay : CenterContainer
{
	private const float EfficiencyForRed = 0.25f;
	private const float AlphaHidden = 0.3f;
	private const float AlphaScratch = 0.6f;
	private const float AlphaBody = 0.6f;
	private const float AlphaFlightSystem = 0.8f;
	private const float AlphaMainSystem= 1f;
	private static PackedScene _packedScene = GD.Load<PackedScene>("res://Game/Common/Vehicles/Components/MyComponentDisplay.tscn");

	public static MyComponentDisplay Instance(MyComponent toDisplay)
	{
		var tmp = (MyComponentDisplay)_packedScene.Instance();
		tmp.ToDisplay = toDisplay;
		return tmp;
	}
	public MyComponent ToDisplay { get; set; }

	private ShaderMaterial _shader;
	private TextureRect _render;
	private float _alpha;
	private Control _bg;

	public void AddTo(Control fg, Control mid, Control bg)
	{
		_bg = bg;
		switch (ToDisplay.ComponentType)
		{
			case EMyComponentType.Wing:
			case EMyComponentType.Tail:
				mid.AddChild(this);
				break;
			case EMyComponentType.Body:
				bg.AddChild(this);
				break;
			default:
				fg.AddChild(this);
				break;
		}
	}

	public override void _Ready()
	{
		base._Ready();
		_render = GetNode<TextureRect>("Render");
		_shader = (ShaderMaterial)_render.Material.Duplicate();
		_render.Texture = ToDisplay.Sprite.Texture;
		_shader.SetShaderParam("Texture", ToDisplay.BaseTexture);
		_shader.SetShaderParam("Tolerance", 1);
		_render.Material = _shader;
		switch (ToDisplay.ComponentType)
		{
			case EMyComponentType.Wing:
			case EMyComponentType.Tail:
				_alpha = AlphaFlightSystem;
				break;
			case EMyComponentType.Body:
				_alpha = AlphaBody;
				break;
			default:
				_alpha = AlphaMainSystem;
				break;
		}
	}

	protected override void Dispose(bool disposing)
	{
		base.Dispose(disposing);
		if (disposing)
		{
			ToDisplay = null;
			_shader = null;
			_render = null;
			_bg = null;
		}
	}

	public override void _Process(float delta)
	{
		base._Process(delta);
		Color color;
		RectPosition = ToDisplay.Position;
		RectRotation = ToDisplay.RotationDegrees;
		_render.FlipH = ToDisplay.Sprite.FlipH;
		_render.FlipV = ToDisplay.Sprite.FlipV;
		if (ToDisplay.IsDestroyed)
		{
			color = new Color(0.1f, 0, 0, _alpha);
			if (!GetParent().Equals(_bg))
			{
				GetParent().RemoveChild(this);
				_bg.AddChild(this);
			}
		}
		else if (ToDisplay.IsDamaged)
		{
			var g = ToDisplay.Effectiveness - EfficiencyForRed;
			color = new Color(1, g > 0 ? g / (1- EfficiencyForRed) : 0, 0, _alpha);
		} else if (ToDisplay.HasScratch)
		{
			color = new Color(1, 1, 0, AlphaScratch);
		}
		else
		{
			color = new Color(1, 1, 1, AlphaHidden);
		}
		_shader.SetShaderParam("ColorAlpha", color);
	}
}
