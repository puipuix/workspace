﻿using BPlan.Game.Common.Projectiles;
using BPlan.Game.Common.Vehicles.Planes;
using BPlan.Game.NetWork;
using Godot;
using Godot.Collections;
using GodotUtils;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;

namespace BPlan.Game.Common
{
    public struct MyIdentification : IMyRPCData
    {
        public int ServerId { get; private set; }
        public sbyte TeamId { get; private set; }
        public sbyte ServerPlaneId { get; private set; }

        public bool IsValid => ServerId != ErrorId;
        
        public int Construct(byte[] buffer, int index)
        {
            index = MyRPCConvert.UnStore(out int s, buffer, index);
            ServerId = s;
            index = MyRPCConvert.UnStore(out sbyte t, buffer, index);
            TeamId = t;
            index = MyRPCConvert.UnStore(out sbyte p, buffer, index);
            ServerPlaneId = p;
            return index;
        }

        public MyIdentification(byte[] buffer, ref int index) : this()
        {
            index = Construct(buffer, index);
        }

        public MyIdentification(byte[] buffer) : this()
        {
            Construct(buffer, 0);
        }

        public MyIdentification(int id, sbyte team, sbyte serverPlaneId)
        {
            ServerId = id;
            TeamId = team;
            ServerPlaneId = serverPlaneId;
        }

        public MyClientData? GetClient()
        {
            return MyBattle.CurrentBattle?.GetClientData(ServerId);
        }

        public bool GetClient(out MyClientData client)
        {
            client = new MyClientData(ErrorId, "Error", EMyPlanes.Biplan, MyAmmoRack.NoneAmmoRacksAllPlane, Colors.Black);
            return MyBattle.CurrentBattle?.GetClientData(ServerId, out client) ?? false;
        }

        public int ToRPCValue(byte[] buffer, int index)
        {
            index = MyRPCConvert.Store(ServerId, buffer, index);
            index = MyRPCConvert.Store(TeamId, buffer, index);
            index = MyRPCConvert.Store(ServerPlaneId, buffer, index);
            return index;
        }

        public int GetRPCLength()
        {
            return 6;
        }

        public override string ToString()
        {
            return string.Concat("{ \"ServerId\": ", ServerId, ", \"TeamId\": ", TeamId, ", \"ServerPlaneId\": ", ServerPlaneId, " }");
        }

        public bool IsFriendly(MyIdentification other)
        {
            return TeamId != ErrorId && TeamId == other.TeamId;
        }

        public bool IsSelf(MyIdentification other)
        {
            return ServerId != ErrorId && ServerId == other.ServerId;
        }

        public override bool Equals(object obj)
        {
            return obj is MyIdentification id && id.ServerId == ServerId;
        }

        public override int GetHashCode()
        {
            return ServerId.GetHashCode();
        }

        public static bool operator ==(MyIdentification a, MyIdentification b)
        {
            return a.ServerId == b.ServerId;
        }

        public static bool operator !=(MyIdentification a, MyIdentification b)
        {
            return a.ServerId != b.ServerId;
        }

        [JsonIgnore]
        public static readonly sbyte UnknowId = -1;
        [JsonIgnore]
        public static readonly sbyte ErrorId = -2;

        public static Color GetIdentificationColor(MyIdentification target, MyIdentification? local)
        {
            if (local is null)
            {
                return Colors.Gray;
            }
            else if (local.Value.IsSelf(target))
            {
                return Colors.DarkGreen;
            }
            else if (local.Value.IsFriendly(target))
            {
                return Colors.Blue;
            }
            else
            {
                return Colors.Red;
            }
        }

    }
}
