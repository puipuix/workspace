using BPlan.Game.Common;
using BPlan.Game.NetWork;
using Godot;
using GodotUtils;
using System;
using System.Linq;

public class MyBattleMenu : TabContainer
{
    #region Nodes
    [MyOnReady("", EMyNodeResearchMode.Ancestor)]
    private MyBattle _battle = null;

    [MyOnReady("Exit", EMyNodeResearchMode.Descendant)]
    private Button _exit = null;

    [MyOnReady("Close", EMyNodeResearchMode.Descendant)]
    private Button _close = null;

    [MyOnReady("Pause", EMyNodeResearchMode.Descendant)]
    private Button _pause = null;

    [MyOnReady("Resume", EMyNodeResearchMode.Descendant)]
    private Button _resume = null;

    [MyOnReady("SpectateOn", EMyNodeResearchMode.Descendant)]
    private Button _spectate = null;

    [MyOnReady("SpectateOff", EMyNodeResearchMode.Descendant)]
    private Button _spectateOff = null;

    [MyOnReady("AddContainer", EMyNodeResearchMode.Descendant)]
    private VBoxContainer _addContainer = null;

    [MyOnReady("RemoveContainer", EMyNodeResearchMode.Descendant)]
    private VBoxContainer _removeContainer = null;

    [MyOnReady("Next", EMyNodeResearchMode.Descendant)]
    private Button _next = null;

    [MyOnReady("RotationMode", EMyNodeResearchMode.Descendant)]
    private OptionButton _rotationMode = null;

    [MyOnReady("IACount", EMyNodeResearchMode.Descendant)]
    private SpinBox _iACount = null;

    [MyOnReady("Players", EMyNodeResearchMode.Descendant)]
    private VBoxContainer _players = null;

    #endregion


    public override void _Ready()
    {
        base._Ready();
        MyOnReadyAttribute.SetUp(this);
        _exit.Connect("pressed", this, Exit);
        _close.Connect("pressed", this, MyHide);
        _pause.Connect<bool>("pressed", this, SetFightEnabled, false);
        _resume.Connect<bool>("pressed", this, SetFightEnabled, true);
        _next.Connect("pressed", _battle, _battle.AutoLoadScenario);
        _spectate.Connect("pressed", this, SpectateOn);
        _spectateOff.Connect("pressed", this, SpectateOff);

        _pause.Hide();
        _resume.Hide();
        _spectateOff.Hide();


        foreach (var item in Enum.GetNames(typeof(EMyScenarioRotationMode)))
        {
            _rotationMode.AddItem(item);
        }
        _rotationMode.Select((int)_battle.ScenarioRotationMode);
        _rotationMode.Connect<int>("item_selected", this, OnRotationSelected);

        _iACount.Value = _battle.IACount;
        _iACount.Connect<float>("value_changed", this, IACountChanged);

        SetTabDisabled(2, true);
    }

    public void SetUpNetWork()
    {
        SetTabDisabled(2, !_battle.IsNetworkMaster());
        GetTree().Connect("network_peer_connected", this, nameof(OnPlayerConnected));
        GetTree().Connect("network_peer_disconnected", this, nameof(OnPlayerDisconnected));
        foreach (var item in EMyScenario.GetScenarios())
        {
            _addContainer.AddChild(MyScenarioDisplay.InstanceForAdd(item, _removeContainer));
        }
        for (int i = 0; i < _battle.ScenarioRotation.Count; i++)
        {
            _removeContainer.AddChild(MyScenarioDisplay.InstanceForRemove(_battle.ScenarioRotation[i], i));
        }
    }

    private void OnPlayerConnected(int id)
    {
        this.CallDeferred(AddClientFromId, id);
    }

    private void AddClientFromId(int networkId)
    {
        if (MyNetworkManager.CurrentInstance.PlayersData.TryGetValue(networkId, out var data))
        {
            AddClient(data);
        }
        else
        {
            MyDebug.PrintErr("Unknow client id: " + networkId);
        }
    }

    private void IACountChanged(float value)
    {
        _battle.IACount = Math.Max(1, Mathf.RoundToInt(value));
    }

    private void OnPlayerDisconnected(int networkId)
    {
        var ch = _players.GetChildren().OfType<MyPlayerDisplay>().Where(p => p.Id == networkId).FirstOrDefault();
        if (ch != null)
        {
            _players.RemoveChild(ch);
            ch.QueueFree();
        }
        else
        {
            MyDebug.PrintErr("Unknow client id: " + networkId);
        }
    }

    private void AddClient(MyClientData data)
    {
        if (!data.IsNPC)
        {
            var p = MyPlayerDisplay.Instance(data.UserName, data.NetworkId, _battle.IsNetworkMaster());
            _players.AddChild(p);
        }
    }

    private void OnRotationSelected(int index)
    {
        _battle.ScenarioRotationMode = (EMyScenarioRotationMode)index;
    }

    private void SetFightEnabled(bool value)
    {
        if (_battle.IsNetworkMaster())
        {
            _battle.FightEnabled = value;
        }
    }

    private void MyHide()
    {
        Hide();
    }

    private void Exit()
    {
        MyNetworkManager.CurrentInstance.CallDeferred(MyNetworkManager.CurrentInstance.StopNetworking, "BATTLE_LEAVED");
    }

    private void SpectateOn()
    {
        _spectate.Hide();
        _spectateOff.Show();
        _battle.SpectateOn();
    }

    private void SpectateOff()
    {
        _spectateOff.Hide();
        _spectate.Show();
        _battle.SpectateOff();
    }


    public override void _Process(float delta)
    {
        base._Process(delta);
        if (_battle.NetWorkEstablished && _battle.IsNetworkMaster())
        {
            if (_resume.Visible != !_battle.FightEnabled)
            {
                _resume.Visible = !_battle.FightEnabled;
            }

            if (_pause.Visible != _battle.FightEnabled)
            {
                _pause.Visible = _battle.FightEnabled;
            }
        }
    }

}
