using BPlan.Game.Common;
using Godot;
using GodotUtils;
using System;

public class MyBattlePlaneMarker : CenterContainer
{
	private static readonly PackedScene _packedScene = GD.Load<PackedScene>("res://Game/Common/GUI/MyBattlePlaneMarker.tscn");

	public static MyBattlePlaneMarker Instance(MyPlane entity)
	{
		var m = (MyBattlePlaneMarker)_packedScene.Instance();
		m.ToFolow = entity;
		return m;
	}

	[MyOnReady("Texture")]
	private TextureRect _texture = null;

	private ShaderMaterial _shader; 

	public MyPlane ToFolow { get; set; }
	protected override void Dispose(bool disposing)
	{
		base.Dispose(disposing);
		if (disposing)
		{
			_texture = null;
			_shader = null;
			ToFolow = null;
		}
	}

	public override void _Ready()
	{
		base._Ready();
		MyOnReadyAttribute.SetUp(this);
		_shader = (ShaderMaterial)_texture.Material.Duplicate();
		_texture.Material = _shader;
	}

	public override void _Process(float delta)
	{
		base._Process(delta);
		if (ToFolow.IsAccessible())
		{
			float ratio = (ToFolow.Position.x + ToFolow.Battle.MapLimit) / (ToFolow.Battle.MapLimit * 2);
			RectPosition = new Vector2(GetParentControl().RectSize.x * ratio, 0);
			_shader.SetShaderParam("Albedo", MyIdentification.GetIdentificationColor(ToFolow.Identification, ToFolow.Battle.LocalIdentification));
			Visible = ToFolow.IsAlive;
		}
	}
}
