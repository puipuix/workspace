using BPlan.Game;
using BPlan.Game.Common.Vehicles.Planes;
using Godot;
using GodotUtils;
using System;
using System.Linq;

public class MyPlaneStatDisplay : VBoxContainer
{

    [MyOnReady("PlaneName", EMyNodeResearchMode.Descendant)]
    private Label _name = null;

    [MyOnReady("Speed", EMyNodeResearchMode.Descendant)]
    private Label _speed = null;

    [MyOnReady("Altitude", EMyNodeResearchMode.Descendant)]
    private Label _altitude = null;

    [MyOnReady("Thust", EMyNodeResearchMode.Descendant)]
    private Label _thrust = null;

    [MyOnReady("Stalling", EMyNodeResearchMode.Descendant)]
    private Label _stalling = null;

    [MyOnReady("Debug", EMyNodeResearchMode.Descendant)]
    private Label _debugLabel = null;

    [MyOnReady("GunInfos", EMyNodeResearchMode.Descendant)]
    private VBoxContainer _gunInfos = null;

    [MyOnReady("DebugContainer", EMyNodeResearchMode.Descendant)]

#pragma warning disable CS0414, IDE0051 // not used
    private Container _debugContainer = null;
#pragma warning restore CS0414, IDE0051

    private MyPlane _plane;
    public MyPlane Plane
    {
        get => _plane;
        set {
            _plane = value;
            SetUp();
        }
    }

    public override void _Ready()
    {
        base._Ready();
        MyOnReadyAttribute.SetUp(this);
    }

    protected override void Dispose(bool disposing)
    {
        base.Dispose(disposing);
        if (disposing)
        {
            _name = _speed = _altitude = _thrust = _stalling = null;
            _gunInfos = null;
            _plane = null;
        }
    }

    private void SetUp()
    {
        _name.Text = _plane == null ? "-" : EMyPlanes.GetNameFromId(_plane.Identification.ServerPlaneId);
        _gunInfos.RemoveAllChildren(n => n.QueueFree());
        if (_plane.IsAccessible())
        {
            foreach (var gun in _plane.GetChildren().OfType<MyGun>())
            {
                MyGunStatDisplay ds = MyGunStatDisplay.Instance();
                _gunInfos.AddChild(ds);
                ds.Gun = gun;
            }
        }

    }

    public override void _Process(float delta)
    {
        base._Process(delta);

        if (_plane != null && IsInstanceValid(_plane))
        {
            _altitude.Text = (int)MyConst.PxToAltitude(_plane.Position.y) + "m";
            _speed.Text = (int)MyConst.PxsToKmh(_plane.Velocity.Length()) + "km/h";
            _thrust.Text = Mathf.RoundToInt(_plane.EngineThrottle * 100) + "%";
#if DEBUG
            _stalling.Text = _plane.IsStalling.ToString();
            _debugLabel.Text = _plane.DebugInfo;
            //_debugContainer.Show();
#endif
        }
        else
        {
            _plane = null;
            _altitude.Text = "-";
            _speed.Text = "-";
            _thrust.Text = "0%";
            _stalling.Text = "False";
        }
    }
}
