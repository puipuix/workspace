using Godot;
using GodotUtils;
using System;

public class MyLoadingScreen : CanvasLayer
{
    private static PackedScene _packedScene = GD.Load<PackedScene>("res://Game/Common/GUI/MyLoadingScreen.tscn");

    public static MyLoadingScreen Instance()
    {
        return (MyLoadingScreen)_packedScene.Instance();
    }


    [MyOnReady("Progress", EMyNodeResearchMode.Descendant)]
    private ProgressBar _progress = null;
    [MyOnReady("Text", EMyNodeResearchMode.Descendant)]
    private Label _text = null;
    [MyOnReady("BackGround")]
    private Control _bg = null;


    private string _txt = "LOAD_LOADING";
    public string Text
    {
        get => _txt;
        set {
            _txt = value;
            if (_text.IsAccessible())
            {
                _text.Text = _txt;
            }
        }
    }

    private MyDeferredCalls _call = null;

    public float Progress
    {
        get => (float)_progress.Value * 0.01f;
        set => _progress.Value = value * 100;
    }

    public override void _Ready()
    {
        base._Ready();
        MyOnReadyAttribute.SetUp(this);
        _text.Text = _txt;
    }

    public void FadeOut()
    {
        if (_bg.Visible)
        {
            _call = new MyDeferredCalls(GetTree(), 0.01f)
           .PushFor(0, i => i < 100, i => i + 3, i => _bg.Modulate = new Color(1, 1, 1, 1 - i * 0.01f))
           .PushSingle(() => _bg.Visible = false);
            _call.Start();
        }       
    }

    public void PopUp()
    {
        _bg.Modulate = new Color(1, 1, 1, 1);
        _bg.Visible = true;
    }

    protected override void Dispose(bool disposing)
    {
        base.Dispose(disposing);
        if (_call.IsAccessible())
        {
            _call?.Stop();
            _call?.CallDeferred("free");
        }
    }
}
