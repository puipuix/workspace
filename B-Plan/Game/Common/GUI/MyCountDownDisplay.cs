using Godot;
using System;

[Tool]
public class MyCountDownDisplay : Label
{
	[Export]
	public float CountDown { get; set; } = 10f;



	public override void _Process(float delta)
	{
		base._Process(delta);
		if (CountDown > 0)
		{
			CountDown -= delta;
			Text = Math.Max(0, (int)CountDown).ToString();
		}
		else
		{
			Text = "";
		}
	}
}
