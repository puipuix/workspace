using BPlan.Game.Common;
using Godot;
using GodotUtils;
using System;
using System.Collections.Generic;
using System.Linq;

public class MyScenarioDisplay : HBoxContainer
{
    private static PackedScene _packedScene = GD.Load<PackedScene>("res://Game/Common/GUI/MyScenarioDisplay.tscn");

    private static MyScenarioDisplay Instance(int index, EMyMode mode, MyScenario scenario, Control removeContainer)
    {
        var sc = (MyScenarioDisplay)_packedScene.Instance();
        sc._index = index;
        sc._mode = mode;
        sc._scenario = scenario;
        sc._removeContainer = removeContainer;
        return sc;
    }

    public static MyScenarioDisplay InstanceForAdd(MyScenario scenario, Control removeContainer)
    {
        return Instance(-1, EMyMode.Add, scenario, removeContainer);
    }

    public static MyScenarioDisplay InstanceForRemove(MyScenario scenario, int index)
    {
        return Instance(index, EMyMode.Remove, scenario, null);
    }

    [MyOnReady("Add")]
    private TextureButton _add = null;

    [MyOnReady("Remove")]
    private TextureButton _remove = null;
    
    [MyOnReady("Name")]
    private Label _name = null;

    [MyOnReady("", EMyNodeResearchMode.Ancestor)]
    private MyBattle _battle = null;

    public enum EMyMode { Add, Remove }
    private EMyMode _mode;
    public MyScenario _scenario;
    public int _index = -1;
    private Control _removeContainer = null;
    public override void _Ready()
    {
        base._Ready();
        MyOnReadyAttribute.SetUp(this);

        _add.Visible = _mode == EMyMode.Add;
        _remove.Visible = _mode == EMyMode.Remove;

        _add.Connect("pressed", this, Add);
        _remove.Connect("pressed", this, Remove);

        _name.Text = _scenario.Name;
    }

    private void Add()
    {
        if (_mode == EMyMode.Add)
        {
            _battle.ScenarioRotation.Add(_scenario);
            var ds = InstanceForRemove(_scenario, _battle.ScenarioRotation.Count - 1);
            _removeContainer.CallDeferred("add_child", ds);
        }
    }

    private void Remove()
    {
        if (_mode == EMyMode.Remove && _battle.ScenarioRotation.Count > 1)
        {
            _battle.ScenarioRotation.RemoveAt(_index);
            this.CallDeferred(RemoveMe);
        }
    }

    private void RemoveMe()
    {
        if (GetParent() != null)
        {
            foreach (var item in GetParent().GetChildren().OfType<MyScenarioDisplay>())
            {
                if (item._index > _index)
                {
                    item._index--;
                }
            }
            GetParent().RemoveChild(this);
            QueueFree();
        }
    }
}
