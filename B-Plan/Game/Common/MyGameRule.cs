﻿using Godot;
using Godot.Collections;
using GodotUtils;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;

namespace BPlan.Game.Common
{
    public struct MyGameRule : IMyRPCData
    {
        public bool EnemyFireEnabled { get; private set; }
        public bool SelfFireEnabled { get; private set; }
        public bool FriendlyFireEnabled { get; private set; }
        public bool FriendlyRammingEnabled { get; private set; }
        public bool RammingEnabled { get; private set; }
        
        public MyGameRule(bool enemyFireEnabled, bool selfFireEnabled, bool friendlyFireEnabled, bool friendlyRammingEnabled, bool rammingEnabled)
        {
            EnemyFireEnabled = enemyFireEnabled;
            SelfFireEnabled = selfFireEnabled;
            FriendlyFireEnabled = friendlyFireEnabled;
            FriendlyRammingEnabled = friendlyRammingEnabled;
            RammingEnabled = rammingEnabled;
        }

        public int Construct(byte[] buffer, int index)
        {
            index = MyRPCConvert.UnStore(out bool b, buffer, index);
            EnemyFireEnabled = b;
            index = MyRPCConvert.UnStore(out b, buffer, index);
            SelfFireEnabled = b;
            index = MyRPCConvert.UnStore(out b, buffer, index);
            FriendlyFireEnabled = b;
            index = MyRPCConvert.UnStore(out b, buffer, index);
            FriendlyRammingEnabled = b;
            index = MyRPCConvert.UnStore(out b, buffer, index);
            RammingEnabled = b;
            return index;
        }

        public MyGameRule(byte[] buffer, ref int index) : this()
        {
            index = Construct(buffer, index);
        }
        public MyGameRule(byte[] buffer) : this()
        {
            Construct(buffer, 0);
        }

        public bool IsRamDamageValid(MyIdentification a, MyIdentification b)
        {
            return !a.IsSelf(b) && (a.IsFriendly(b) ? FriendlyRammingEnabled : RammingEnabled);
        }

        public bool IsProjectileDamageValid(MyIdentification a, MyIdentification b, bool selfHitProtection)
        {
            return a.IsSelf(b) ? (SelfFireEnabled && !selfHitProtection) : (a.IsFriendly(b) ? FriendlyFireEnabled : EnemyFireEnabled);
        }

        public int GetRPCLength()
        {
            return 5;
        }

        public int ToRPCValue(byte[] buffer, int index)
        {
            index = MyRPCConvert.Store(EnemyFireEnabled, buffer, index);
            index = MyRPCConvert.Store(SelfFireEnabled, buffer, index);
            index = MyRPCConvert.Store(FriendlyFireEnabled, buffer, index);
            index = MyRPCConvert.Store(FriendlyRammingEnabled, buffer, index);
            index = MyRPCConvert.Store(RammingEnabled, buffer, index);
            return index;
        }

        [JsonIgnore]
        public static readonly MyGameRule Arcade = new MyGameRule(true, false, false, false, false);
        [JsonIgnore]
        public static readonly MyGameRule Realistic = new MyGameRule(true, false, true, false, true);
        [JsonIgnore]
        public static readonly MyGameRule Simulator = new MyGameRule(true, true, true, true, true);
    }
}
