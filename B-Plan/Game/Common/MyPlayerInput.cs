﻿using Godot;
using GodotUtils;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BPlan.Game.Common
{
    public enum EMyPlayerInputType : byte { None, Local, Ia }

    public abstract class MyPlayerInput : Node
    {
        public abstract EMyPlayerInputType Type { get; }
        public abstract float PlaneRotatePressed();
        public abstract float PlaneRotateInvPressed();
        public abstract bool PlaneReverseJustPressed();
        public abstract bool PlaneIncreaseThrustPressed();
        public abstract bool PlaneDecreaseThrustPressed();
        public abstract bool PlanePrimaryGunPressed();
        public abstract bool PlaneThirdGunPressed();
        public abstract bool PlaneFourthGunPressed();
        public abstract bool PlaneSecondaryGunPressed();

        public abstract Vector2? PlaneTurretGlobalTarget();

        protected MyPlane _plane;

        public override void _Ready()
        {
            base._Ready();
            Name = "Player Input";
        }

        public virtual void Update(float delta, MyPlane plane)
        {
            _plane = plane;
        }

        public static MyPlayerInput NewFromType(EMyPlayerInputType type)
        {
            switch (type)
            {
                case EMyPlayerInputType.Local:
                    return new MyLocalPlayerInput();
                case EMyPlayerInputType.Ia:
                    return new MyIaPlayerInput();
                default:
                    return new MyNonePlayerInput();
            }
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            if (disposing)
            {
                _plane = null;
            }
        }
    }

    public class MyNonePlayerInput : MyPlayerInput
    {
        public override EMyPlayerInputType Type => EMyPlayerInputType.None;

        public override bool PlaneThirdGunPressed()
        {
            return false;
        }

        public override bool PlaneDecreaseThrustPressed()
        {
            return false;
        }

        public override bool PlaneIncreaseThrustPressed()
        {
            return false;
        }

        public override bool PlaneReverseJustPressed()
        {
            return false;
        }

        public override bool PlaneFourthGunPressed()
        {
            return false;
        }

        public override float PlaneRotateInvPressed()
        {
            return 0;
        }

        public override float PlaneRotatePressed()
        {
            return 0;
        }

        public override bool PlanePrimaryGunPressed()
        {
            return false;
        }

        public override bool PlaneSecondaryGunPressed()
        {
            return false;
        }

        public override Vector2? PlaneTurretGlobalTarget()
        {
            return null;
        }
    }

    public class MyLocalPlayerInput : MyPlayerInput
    {
        public override EMyPlayerInputType Type => EMyPlayerInputType.Local;

        [Master]
        private bool _reverse = false;
        [Master]
        private bool _decrease = false;
        [Master]
        private bool _increase = false;
        [Master]
        private float _rotate = 0;
        [Master]
        private float _rotateInv = 0;
        [Master]
        private bool _gun1 = false;
        [Master]
        private bool _gun2 = false;
        [Master]
        private bool _gun3 = false;
        [Master]
        private bool _gun4 = false;
        
        private Vector2? _turret = null;

        [Master]
        private void RSetTurret(Vector2 vector, bool isNull)
        {
            _turret = isNull ? (Vector2?)null : vector;
        }


        public override void Update(float delta, MyPlane plane)
        {
            base.Update(delta, plane);
            var nw = plane.Identification.GetClient()?.NetworkId;
            if (nw.HasValue)
            {
                if (nw.Value == GetTree().GetNetworkUniqueId())
                {
                    if (_plane.Battle.UIFocused)
                    {
                        _reverse =_decrease = _increase = _gun1 = _gun2 = _gun3 = _gun4 = false;
                        _rotate = _rotateInv = 0;
                        _turret = null;
                    }
                    else
                    {
                        _reverse = plane.Battle.TouchScreen.FlushReverseJustPressed() || Input.IsActionJustPressed("plane_reverse");
                        _decrease = _plane.Battle.TouchScreen.DecreaseThrustPressed || Input.IsActionPressed("plane_decrease_thrust");
                        _increase = _plane.Battle.TouchScreen.IncreaseThrustPressed || Input.IsActionPressed("plane_increase_thrust");
                        _rotate = Math.Max(_plane.Battle.TouchScreen.TurnPressed, Input.GetActionStrength("plane_rotate"));
                        _rotateInv = Math.Max(_plane.Battle.TouchScreen.TurnInvPressed, Input.GetActionStrength("plane_rotate_inv"));

                        _gun1 = (_plane.Battle.TouchScreen.TurretPressed && _plane.PrimaryGunType == Components.EmyGunType.Turret) 
                            || _plane.Battle.TouchScreen.GunPressed || Input.IsActionPressed("plane_primary_gun");
                        _gun2 = (_plane.Battle.TouchScreen.TurretPressed && _plane.SecondaryGunType == Components.EmyGunType.Turret)
                            || _plane.Battle.TouchScreen.GunPressed || Input.IsActionPressed("plane_secondary_gun");
                        _gun3 = (_plane.Battle.TouchScreen.TurretPressed && _plane.ThirdGunType == Components.EmyGunType.Turret)
                            || _plane.Battle.TouchScreen.GunPressed || Input.IsActionPressed("plane_third_gun");
                        _gun4 = (_plane.Battle.TouchScreen.TurretPressed && _plane.FourthGunType == Components.EmyGunType.Turret)
                            || _plane.Battle.TouchScreen.GunPressed || Input.IsActionPressed("plane_fourth_gun");

                        if (_plane.IsAccessible())
                        {
                            if (_plane.Battle.TouchScreen.Enabled)
                            {
                                _turret = _plane.GlobalPosition + _plane.Battle.TouchScreen.TurretDirection * 1000;
                            }
                            else
                            {
                                _turret = _plane.GetGlobalMousePosition();
                            }
                        }
                        else
                        {
                            _turret = null;
                        }
                        
                    }

                    if (!IsNetworkMaster())
                    {
                        RsetId(1, nameof(_reverse), _reverse);
                        RsetId(1, nameof(_decrease), _decrease);
                        RsetId(1, nameof(_increase), _increase);
                        RsetId(1, nameof(_gun1), _gun1);
                        RsetId(1, nameof(_gun2), _gun2);
                        RsetId(1, nameof(_gun3), _gun3);
                        RsetId(1, nameof(_gun4), _gun4);
                        RsetId(1, nameof(_rotate), _rotate);
                        RsetId(1, nameof(_rotateInv), _rotateInv);
                        RpcId(1, nameof(RSetTurret), _turret ?? Vector2.Zero, _turret is null);
                    }
                }
            }
        }

        public override bool PlaneDecreaseThrustPressed()
        {
            return _decrease;
        }

        public override bool PlaneIncreaseThrustPressed()
        {
            return _increase;
        }

        public override bool PlaneReverseJustPressed()
        {
            return _reverse;
        }

        public override float PlaneRotateInvPressed()
        {
            return _rotate;
        }

        public override float PlaneRotatePressed()
        {
            return _rotateInv;
        }

        public override bool PlanePrimaryGunPressed()
        {
            return _gun1;
        }

        public override bool PlaneSecondaryGunPressed()
        {
            return _gun2;
        }
        public override bool PlaneThirdGunPressed()
        {
            return _gun3;
        }

        public override bool PlaneFourthGunPressed()
        {
            return _gun4;
        }

        public override Vector2? PlaneTurretGlobalTarget()
        {
            return _turret;
        }
    }

    public class MyIaPlayerInput : MyPlayerInput
    {
        private const float TargetSelectionSpeedUp = 3;

        private float ClimbAngle = 15;
        private float ShotAngle = 10;
        private float ShotRange = 800;
        private float TurretShotRange = 1000;
        private float CruiseAltitude = 1500;
        private float MinAltitude = 300;
        private float TargetError = 100;

        public override EMyPlayerInputType Type => EMyPlayerInputType.Ia;

        private MyPlane _target;

        private Vector2 _objective;

        private int _rotate = 0;
        private bool _shot = false;
        private bool _turret = false;
        private bool _rocket = false;
        private bool _bomb = false;

        private float _targetReset = 20f * TargetSelectionSpeedUp;
        private float _targetErrorReset = 1f;
        private bool _reverse = false;
        private Vector2 _targetError = Vector2.Zero;

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            if (disposing)
            {
                _target = null;
            }
        }

        private void Randomize(MyPlane plane)
        {
            ClimbAngle = MyTools.RNG.Next(10, 45);
            ShotAngle = MyTools.RNG.Next(5, 20);
            ShotRange = MyTools.RNG.Next(1000, 2000);
            TurretShotRange = MyTools.RNG.Next(1000, 2000);
            int max = -(int)(MyConst.AltitudeToPx(plane.MaxAltitude) * 0.75f);
            int min = Math.Min(max, 500 - (int)(plane.Manoeuvrability * plane.EngineThrust));
            int avg = min + (int)((max - min) * 0.5f);
            CruiseAltitude = MyTools.RNG.Next(avg, max);
            MinAltitude = MyTools.RNG.Next(min, avg);
            TargetError = MyTools.RNG.Next(100, 300);
        }

        private float GetClimbAngle(float altitude)
        {
            if (altitude < MinAltitude)
            {
                return Mathf.Deg2Rad(5 + (ClimbAngle - 5) * Mathf.Pow(altitude / MinAltitude, 0.5f));
            }
            else
            {
                return Mathf.Deg2Rad(ClimbAngle);
            }
        }

        public override void Update(float delta, MyPlane plane)
        {
            if (_plane == null)
            {
                Randomize(plane);
            }
            base.Update(delta, plane);
            if (_plane.IsAlive)
            {
                _targetReset -= delta;
                _targetErrorReset -= delta;
                _reverse = false;

                // when down vector is negative, reversed should be true
                // so if not we reverse
                _reverse = _plane.DownVector.y < 0 ^ _plane.Reversed;

                if (_targetErrorReset < 0)
                {
                    _targetErrorReset = MyTools.RNG.NextFloat(0.1f, 2);
                    _targetError = (MyTools.RNG.NextVector2() * 2 - Vector2.One) * TargetError;
                }

                // look for the closest target
                if (_targetReset < 0)
                {
                    _targetReset = MyTools.RNG.NextFloat(3, 5);
                    _target = _plane.Battle.GetPlanes()
                        .Where(p => !p.Identification.IsFriendly(_plane.Identification) && p.IsAlive) // LF alive enemies
                        .Select(p => (p, p.Position.DistanceSquaredTo(plane.Position))) // calculate distance
                        .FindMin((a, b) => a.Item2.CompareTo(b.Item2)).p; // select closest one
                    Randomize(_plane);
                }

                float climbAngle = GetClimbAngle(-_plane.Position.y);
                // tell if the angle of the plane is low enought to prevent been killed by the ground
                bool flatFlight = Math.Abs(_plane.ForwardVector.y) < Mathf.Sin(climbAngle);
                if ((-_plane.Position.y < MinAltitude && !flatFlight) || !_target.IsAccessible() || !_target.IsAlive)
                {
                    _targetReset -= delta * (TargetSelectionSpeedUp - 1); // speed up the target selection, -1 because we already add delta once

                    // here we calculate a position at the cruise altitude to make the plane clibe at ClimbAngle.
                    Vector2 leftRight = new Vector2(_plane.ForwardVector.x < 0 ? -1 : 1, 0);
                    if (-_plane.Position.y < CruiseAltitude)
                    {
                        float dist = Math.Abs(CruiseAltitude + _plane.Position.y) * Mathf.Tan(Mathf.Pi * 0.5f - climbAngle);
                        _objective = new Vector2(_plane.Position.x, -CruiseAltitude) + leftRight * dist;
                    }
                    else
                    {
                        _objective = _plane.Position + leftRight * 100;
                    }
                    _shot = false;
                    if (_target.IsAccessible())
                    {
                        _turret = (_plane.Position - _target.Position).Length() < TurretShotRange;
                    }
                    else
                    {
                        _turret = false;
                    }
                }
                else
                {
                    // The distance vector and value from target to me
                    Vector2 targToPlane = _plane.Position - _target.Position;
                    float dist = targToPlane.Length();
                    _turret = dist < TurretShotRange;
                    // here _target has a value so we look if he's range here.
                    _shot = dist < ShotRange;

                    _rocket = _shot && _target.Velocity.Length() < 500 && -_target.Position.y < 150;
                    _bomb = _rocket && Math.Abs(targToPlane.x) < 300;

                    // The difference between the bullet velocity and the target velocity.
                    float bulletVelosDiffWithTarget = (_plane.Velocity - _target.Velocity).Length() + 1550;

                    // We take the time to reach the target position
                    float time = dist / bulletVelosDiffWithTarget;
                    if (Math.Abs(_target.AngularVelocity) > 0.1f)
                    {
                        Vector2 ttl = Vector2.Zero;
                        int theoCount = (int)(time / delta);
                        int count = Math.Min(theoCount, 50); // Limit to 50 tick
                        float scale = (float)theoCount / count;
                        // We need to define the scale of the new delta, in case theoCount is greater than 50
                        // (We need to take all the travel time)
                        Vector2 linSpeed = _target.Velocity * delta * scale;
                        float angSpeed = _target.AngularVelocity * delta * scale;
                        while (count > 0)
                        {
                            count--;
                            ttl = (ttl + linSpeed).Rotated(angSpeed);
                        }
                        _objective = _target.Position + ttl - _plane.Velocity * time;
                    }
                    else
                    {
                        _objective = _target.Position + (_target.Velocity - _plane.Velocity) * time;
                    }

                    _objective += _targetError * Math.Min(dist / TargetError, 1);

                    if (targToPlane.x < -_plane.Battle.MapLimit || _plane.Battle.MapLimit < targToPlane.x)
                    {
                        _objective += new Vector2(2 * _plane.Battle.MapLimit * (_objective.x < 0 ? 1 : -1), 0);
                    }
                }

                Vector2 objectiveDirection = _objective - _plane.Position;

                float angle = Mathf.Rad2Deg(_plane.ForwardVector.AngleTo(objectiveDirection));
                float absAngle = Math.Abs(angle);
                if (-_plane.Position.y < MinAltitude && absAngle > 90)
                {
                    _rotate = _plane.ForwardVector.x > 0 ? 1 : -1;
                    _shot = false;
                    _rocket = false;
                }
                else
                {
                    if (_shot)
                    {
                        Vector2 targetDirection = _target.Position - _plane.Position;
                        // use to check if the plane isn't behind us
                        float verificationAngle = Math.Abs(Mathf.Rad2Deg(_plane.ForwardVector.AngleTo(targetDirection)));
                        _shot = absAngle < ShotAngle && verificationAngle < 90;
                        _rocket = _rocket && _shot;
                    }

                    _rotate = (int)-angle;
                }

                _plane.BotAim.CastTo = objectiveDirection.Rotated(-_plane.Rotation);
            }
            else
            {
                _plane.BotAim.Hide();
                _shot = false;
                _turret = false;
                _rocket = false;
                _bomb = false;
                _rotate = 0;
                _reverse = false;
            }
        }

        public override bool PlaneDecreaseThrustPressed()
        {
            return false;
        }

        public override bool PlaneIncreaseThrustPressed()
        {
            return true;
        }

        public override bool PlaneReverseJustPressed()
        {
            return _reverse;
        }

        public override float PlaneRotateInvPressed()
        {
            return _rotate < 0 ? 1 : 0;
        }

        public override float PlaneRotatePressed()
        {
            return _rotate > 0 ? 1 : 0;
        }

        public override bool PlanePrimaryGunPressed()
        {
            switch (_plane.PrimaryGunType)
            {
                case Components.EmyGunType.Gun:
                    return _shot;
                case Components.EmyGunType.Turret:
                    return _turret;
                case Components.EmyGunType.Bomb:
                    return _bomb;
                case Components.EmyGunType.Rocket:
                    return _rocket;
                default:
                    return false;
            }
        }
        public override bool PlaneSecondaryGunPressed()
        {
            switch (_plane.SecondaryGunType)
            {
                case Components.EmyGunType.Gun:
                    return _shot;
                case Components.EmyGunType.Turret:
                    return _turret;
                case Components.EmyGunType.Bomb:
                    return _bomb;
                case Components.EmyGunType.Rocket:
                    return _rocket;
                default:
                    return false;
            }
        }

        public override bool PlaneThirdGunPressed()
        {
            switch (_plane.ThirdGunType)
            {
                case Components.EmyGunType.Gun:
                    return _shot;
                case Components.EmyGunType.Turret:
                    return _turret;
                case Components.EmyGunType.Bomb:
                    return _bomb;
                case Components.EmyGunType.Rocket:
                    return _rocket;
                default:
                    return false;
            }
        }

        public override bool PlaneFourthGunPressed()
        {
            switch (_plane.FourthGunType)
            {
                case Components.EmyGunType.Gun:
                    return _shot;
                case Components.EmyGunType.Turret:
                    return _turret;
                case Components.EmyGunType.Bomb:
                    return _bomb;
                case Components.EmyGunType.Rocket:
                    return _rocket;
                default:
                    return false;
            }
        }

        public override Vector2? PlaneTurretGlobalTarget()
        {
            return _target.IsAccessible() ? _target.GlobalPosition : (Vector2?)null;
        }
    }
}
