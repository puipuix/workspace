using BPlan.Game.Common;
using BPlan.Game.Menu;
using Godot;
using GodotUtils;
using System;

public class MyExplosion : StaticBody2D, IMyDamageElement
{
    private static readonly PackedScene _packedScene = GD.Load<PackedScene>("res://Game/Common/Projectiles/MyExplosion.tscn");

    public static MyExplosion Instance(int damageRadius, MyIdentification identification, MyIdentification? origin, int damage = 1000, int particuleCount = 10, float particuleRadius = 15, float burnChance = 1, float burnDuration = 10)
    {
        var expl = (MyExplosion)_packedScene.Instance();
        expl.DamageRadius = damageRadius;
        expl.ParticuleRadius = particuleRadius;
        expl.ParticuleCount = particuleCount;
        expl.Origin = origin;
        expl.Identification = identification;
        expl.Damage = damage;
        expl.BurnChance = burnChance;
        expl.BurnDuration = burnDuration;
        return expl;
    }

    public float BurnChance { get; set; }
    public float BurnDuration { get; set; }
    public int Damage { get; set; }

    public int ParticuleCount { get; set; }
    public int DamageRadius { get; set; }
    public float ParticuleRadius { get; set; }

    public MyIdentification? Origin { get; set; }

    public MyIdentification Identification { get; set; }

    [MyOnReady("CollisionShape2D")]
    private CollisionShape2D _collision = null;
    [MyOnReady("Particles2D")]
    private Particles2D _particle = null;
    [MyOnReady("Audio")]
    private AudioStreamPlayer2D _audio = null;

    private CircleShape2D _shape;
    private ParticlesMaterial _material;

    public override void _Ready()
    {
        base._Ready();
        MyOnReadyAttribute.SetUp(this);
        _shape = (CircleShape2D)_collision.Shape.Duplicate();
        _collision.Shape = _shape;
        _material = (ParticlesMaterial)_particle.ProcessMaterial;
        _particle.ProcessMaterial = _material;
    }


    public void Explode(Vector2 globalPosition)
    {
        GlobalPosition = globalPosition;
        _audio.VolumeDb = -15 + DamageRadius / 5;
        _shape.Radius = DamageRadius;
        _material.EmissionSphereRadius = DamageRadius;
        _material.InitialVelocity = DamageRadius * 4;
        _material.LinearAccel = -DamageRadius * 8;
        _particle.Amount = Math.Max(ParticuleCount * MyGameSettings.Particle / 100, 1);
        _material.Scale = ParticuleRadius;
        _collision.Disabled = false;
        _particle.Emitting = true;
        _audio.Play();
        var timer = GetTree().CreateTimer(0.25f);
        timer.Connect("timeout", this, DisableHitbox);
        timer = GetTree().CreateTimer(_particle.Lifetime + 1);
        timer.Connect("timeout", this, Remove);
    }

    private void DisableHitbox()
    {
        _collision.Disabled = true;
    }

    public void Remove()
    {
        GetParent().RemoveChild(this);
        QueueFree();
    }

    public void DamageComponent(MyComponent componant)
    {
        var rule = componant.Plane.Battle.GameRule;
        var id = Origin ?? Identification;
        if (rule.IsProjectileDamageValid(id, componant.Plane.Identification, false))
        {
            componant.Damage(Damage, Origin.HasValue ? EMyLogMessage.Explosion : EMyLogMessage.SelfExplosion, Origin);
            if (MyTools.RNG.NextBool(BurnChance))
            {
                componant.Burn(BurnDuration, Origin);
            }
        }
    }
}
