﻿using System;
using System.Linq;
using BPlan.Game.Common.Vehicles.Planes;
using Godot;
using GodotUtils;

namespace BPlan.Game.Common.Projectiles
{
    public static class EMyProjectiles
    {
        public const int Bullet = 0, TracerBullet = 1, APBullet = 2, APTBullet = 3, APITBullet = 4,
            APIBullet = 5, IBullet = 6, ITBullet = 7, IT37Bullet = 8, APT37Bullet = 9,
            APHET20mm = 10, HET20mm = 11, RocketHE = 12, Bomb = 13;
        private static PackedScene[] _projectiles;

        public static void Preload()
        {
            _projectiles = new PackedScene[] {
                GD.Load<PackedScene>("res://Game/Common/Projectiles/Bullets/MyBulletProjectile.tscn"),
                GD.Load<PackedScene>("res://Game/Common/Projectiles/Bullets/MyTracerBulletProjectile.tscn"),
                GD.Load<PackedScene>("res://Game/Common/Projectiles/Bullets/MyAPBulletProjectile.tscn"),
                GD.Load<PackedScene>("res://Game/Common/Projectiles/Bullets/MyAPTBulletProjectile.tscn"),
                GD.Load<PackedScene>("res://Game/Common/Projectiles/Bullets/MyAPITBulletProjectile.tscn"),
                GD.Load<PackedScene>("res://Game/Common/Projectiles/Bullets/MyAPIBulletProjectile.tscn"),
                GD.Load<PackedScene>("res://Game/Common/Projectiles/Bullets/MyIBulletProjectile.tscn"),
                GD.Load<PackedScene>("res://Game/Common/Projectiles/Bullets/MyITBulletProjectile.tscn"),
                GD.Load<PackedScene>("res://Game/Common/Projectiles/Bullets/My37mmITBulletProjectile.tscn"),
                GD.Load<PackedScene>("res://Game/Common/Projectiles/Bullets/My37mmAPTBulletProjectile.tscn"),
                GD.Load<PackedScene>("res://Game/Common/Projectiles/Bullets/My20mmAPHETBulletProjectile.tscn"),
                GD.Load<PackedScene>("res://Game/Common/Projectiles/Bullets/My20mmHETBulletProjectile.tscn"),
                GD.Load<PackedScene>("res://Game/Common/Projectiles/Rockets/MyRocketProjectile.tscn"),
                GD.Load<PackedScene>("res://Game/Common/Projectiles/Bombs/MyBombProjectile.tscn"),
            };
        }

        public static MyProjectile GetFromId(int id)
        {
            try
            {
                MyProjectile p = (MyProjectile)_projectiles[id].Instance();
                p.ProjectileId = id;
                return p;
            }
            catch (Exception e)
            {
                MyDebug.PrintErr(new Exception("Error while loading projectile: " + id + ".", e));
                return null;
            }
        }
    }


    public struct MyAmmoRack
    {
        public string Name { get; }
        public int Id { get; }
        public int[] Projectiles { get; }

        public MyAmmoRack(string name, int id, int[] projectiles)
        {
            Name = name;
            this.Id = id;
            Projectiles = projectiles;
        }

        public const int None = 0, Tracer = 1, Bullet = 2, APIT = 3, IncendaryStealth = 4, Incendary = 5, Stealth = 6;
        public const int AirTarget = 7, GroundTarget = 8, AntiArmor = 9, AntiArmorStealth = 10, Universal = 11;

        public const int AirTarget37 = 12, GroundTarget37 = 13, Universal37 = 14;

        public const int AirTarget20 = 15, Ground20 = 16, Universal20 = 17, Stealth20 = 18;

        public const int Rocket = 19;

        public const int Bomb = 20;

        private static MyAmmoRack[] _racks;

        public static MyAmmoRack[] MG12AmmoRacks { get; private set; }
        public static MyAmmoRack[] MG20AmmoRacks { get; private set; }
        public static MyAmmoRack[] MG37AmmoRacks { get; private set; }
        public static MyAmmoRack[] MG23AmmoRacks { get; private set; }
        public static MyAmmoRack[] RocketAmmoRacks { get; private set; }
        public static MyAmmoRack[] NoneAmmoRacks { get; private set; }
        public static MyAmmoRack[] BombAmmoRacks { get; private set; }
        public static int[][][] NoneAmmoRacksAllPlane { get; private set; }

        public static void Preload()
        {
            _racks = new MyAmmoRack[] {
                new MyAmmoRack("None", None, new int[] { }),
                new MyAmmoRack("Tracer", Tracer, new int[] { EMyProjectiles.TracerBullet }),
                new MyAmmoRack("Bullet", Bullet, new int[] { EMyProjectiles.Bullet }),
                new MyAmmoRack("APIT", APIT, new int[] { EMyProjectiles.APITBullet }),
                new MyAmmoRack("Incendary Stealth", IncendaryStealth, new int[] { EMyProjectiles.IBullet, EMyProjectiles.APIBullet }),
                new MyAmmoRack("Incendary", Incendary, new int[] { EMyProjectiles.ITBullet, EMyProjectiles.APITBullet }),
                new MyAmmoRack("Stealth", Stealth, new int[] { EMyProjectiles.IBullet, EMyProjectiles.APBullet, }),
                new MyAmmoRack("Air Target", AirTarget, new int[] { EMyProjectiles.APBullet, EMyProjectiles.APBullet, EMyProjectiles.APITBullet, EMyProjectiles.IBullet, EMyProjectiles.IBullet, EMyProjectiles.APITBullet }),
                new MyAmmoRack("Ground Target", GroundTarget, new int[] { EMyProjectiles.APTBullet, EMyProjectiles.APTBullet, EMyProjectiles.APITBullet }),
                new MyAmmoRack("Anti Armor", AntiArmor, new int[] { EMyProjectiles.APTBullet }),
                new MyAmmoRack("Anti Armor Stealth", AntiArmorStealth, new int[] { EMyProjectiles.APBullet }),
                new MyAmmoRack("Universal", Universal, new int[] { EMyProjectiles.IBullet, EMyProjectiles.APTBullet, EMyProjectiles.Bullet, EMyProjectiles.ITBullet, EMyProjectiles.APBullet, EMyProjectiles.APITBullet }),
                new MyAmmoRack("Air Target", AirTarget37, new int[] { EMyProjectiles.IT37Bullet }),
                new MyAmmoRack("Ground Target", GroundTarget37, new int[] { EMyProjectiles.APT37Bullet }),
                new MyAmmoRack("Universal", Universal37, new int[] { EMyProjectiles.APT37Bullet, EMyProjectiles.IT37Bullet }),
                new MyAmmoRack("Air Target", AirTarget20, new int[] { EMyProjectiles.HET20mm, EMyProjectiles.APHET20mm, }),
                new MyAmmoRack("Ground Target", Ground20, new int[] { EMyProjectiles.APTBullet, EMyProjectiles.APTBullet, EMyProjectiles.APHET20mm, }),
                new MyAmmoRack("Universal", Universal20, new int[] { EMyProjectiles.APBullet, EMyProjectiles.HET20mm, EMyProjectiles.APHET20mm, EMyProjectiles.IBullet, }),
                new MyAmmoRack("Stealth", Stealth20, new int[] { EMyProjectiles.APBullet, EMyProjectiles.IBullet, }),
                new MyAmmoRack("Rocket", Rocket, new int[]{ EMyProjectiles.RocketHE }),
                new MyAmmoRack("250lb Bombs", Bomb, new int[]{ EMyProjectiles.Bomb }),
            };

            MG12AmmoRacks = _racks.SelectRange(Tracer, Universal + 1).ToArray();
            MG20AmmoRacks = _racks.SelectRange(AirTarget20, Stealth20 + 1).ToArray();
            MG37AmmoRacks = _racks.SelectRange(AirTarget37, Universal37 + 1).ToArray();
            RocketAmmoRacks = new MyAmmoRack[] { _racks[Rocket] };
            BombAmmoRacks = new MyAmmoRack[] { _racks[Bomb] };
            MG23AmmoRacks = new MyAmmoRack[] { _racks[AntiArmor], _racks[GroundTarget], _racks[AirTarget], _racks[APIT] };
            NoneAmmoRacks = new MyAmmoRack[] { _racks[None] };

        }

        public static void Load()
        {

            NoneAmmoRacksAllPlane = new int[EMyPlanes.PlaneCount][][];

            for (int i = 0; i < EMyPlanes.PlaneCount; i++)
            {
                NoneAmmoRacksAllPlane[i] = new int[4][];
                for (int y = 0; y < 4; y++)
                {
                    NoneAmmoRacksAllPlane[i][y] = _racks[None];
                }
            }
        }

        public static MyAmmoRack GetFromId(int id)
        {
            return _racks[id];
        }

        public static implicit operator int(MyAmmoRack rack)
        {
            return rack.Id;
        }

        public static implicit operator int[](MyAmmoRack rack)
        {
            return rack.Projectiles;
        }
    }

}
