using BPlan.Game.Common;
using BPlan.Game.Common.Projectiles;
using BPlan.Game.NetWork;
using Godot;
using GodotUtils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//[Tool]
public class MyBulletProjectile : MyProjectile
{
	[Export]
	public int Damage { get; set; } = 100;

	[Export]
	public int ArmorPiercing { get; set; } = 0;

	[Export]
	public float IncendaryDuration { get; set; } = 0;

	[Export]
	public float IncendaryProbability { get; set; } = -1;

	[Export]
	public float DamageDegradation { get; set; } = 30;

	[Export]
	public float ArmorPiercingDegradation { get; set; } = 1;


	private int _armorPiercingLeft;
	private int _currentDamage => Math.Max(0, GunDamage + Damage - Mathf.RoundToInt(DamageDegradation * _flyTime));
	private int _currentArmorPiercing => Math.Max(0, _armorPiercingLeft - Mathf.RoundToInt(ArmorPiercingDegradation * _flyTime));

	public override void _Ready()
	{
		base._Ready();
		Visible = false;
		_armorPiercingLeft = ArmorPiercing;
	}

	public override void DamageComponent(MyComponent component)
	{
		if (Battle.GameRule.IsProjectileDamageValid(Identification, component.Plane.Identification, SelfHitProtection))
		{
			component.Damage(_currentDamage, EMyLogMessage.ShotDown, Identification);
			if (MyTools.RNG.NextBool(IncendaryProbability) && component.CanBurn)
			{
				component.Burn(IncendaryDuration, Identification);
			}

			if (_currentArmorPiercing > 0)
			{
				_armorPiercingLeft -= 1;
			}
			else
			{
				OnEndOfLife();
			}
		}
	}

	public override void _PhysicsProcess(float delta)
	{
		base._PhysicsProcess(delta);
		Visible = _flyTime > 0.020;
		if (!Engine.EditorHint && Battle.FightEnabled)
		{
			if (!IsEndOfLife && _currentDamage == 0 && IsNetworkMaster())
			{
				OnEndOfLife();
			}
		}
	}
}

