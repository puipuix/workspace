using BPlan.Game.Common.Projectiles;
using Godot;
using GodotUtils;
using System;

public class MyRocketProjectile : MyBombProjectile
{
    [Export]
    public int ThrustForce { get; set; } = 1;

    [Export]
    public float ThrustTime { get; set; } = 1;

    public override void _PhysicsProcess(float delta)
    {
        if (!Engine.EditorHint && _flyTime < ThrustTime && Battle.FightEnabled)
        {
            Velocity += Transform.x * ThrustForce * delta;
        }
        base._PhysicsProcess(delta);
    }
}
