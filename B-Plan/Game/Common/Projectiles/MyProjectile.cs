﻿using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using BPlan.Game.Common;
using BPlan.Game.Common.Vehicles.Planes;
using BPlan.Game.NetWork;
using Godot;
using GodotUtils;

namespace BPlan.Game.Common.Projectiles
{
    public abstract class MyProjectile : MyEntity, IMyDamageElement
    {
        public int GunDamage { get; set; }
        public int ProjectileId { get; internal set; } = -1;
        public Vector2 Velocity { get; protected set; }

        public bool SelfHitProtection => _flyTime < 0.1f;

        public float MaxFlyTime { get; set; } = 10;
        [Export]
        public float InitialSpeed { get; set; } = 0;

        [Export]
        public float AlignWithVelocityEffectiveness { get; set; } = 1;

        protected float _flyTime = 0f;

        public bool IsEndOfLife { get; protected set; }

        public override void _Ready()
        {
            base._Ready();
            IsRemoteSynchronized = false;
        }

        private void AlignWithVelocity()
        {
            var angle = GetAngleTo(GlobalPosition + Velocity);
            Rotate(angle * AlignWithVelocityEffectiveness);
            //LookAt(GlobalPosition + Velocity);
        }


        public virtual void Initialize(MyProjectileData data)
        {
            ProjectileId = data.ProjectileID;
            Identification = data.Identification;
            Position = data.Position;
            Name = data.Name;
            GunDamage = data.GunDamage;
            Velocity = data.Velocity + data.Direction * InitialSpeed;
            Scale = Vector2.One * data.Scale;
            LookAt(GlobalPosition + data.Direction);
            SetNetworkMaster(1);
        }

        public override void _Process(float delta)
        {
            if (!Engine.EditorHint && Battle.FightEnabled)
            {
                _flyTime += delta;

                Velocity += Vector2.Down * MyConst.Gravity * delta;
                MoveAndSlide(Velocity);
                AlignWithVelocity();
                
                if ((Position.y > 0 || _flyTime > MaxFlyTime) && IsNetworkMaster())
                {
                    OnEndOfLife();
                }
                else
                {
                    base._Process(delta);
                }
            }
        }

        public abstract void DamageComponent(MyComponent component);

        public virtual void OnEndOfLife()
        {
            IsEndOfLife = true;
            Rpc(nameof(RPCRemove));
        }

        [RemoteSync]
        protected virtual void RPCRemove()
        {
            IsEndOfLife = true;
            GetParent().RemoveChild(this);
            QueueFree();
        }

        public static void AddProjectile(Node world, MyProjectileData data)
        {
            MyProjectile projectile = EMyProjectiles.GetFromId(data.ProjectileID);
            if (projectile != null)
            {
                world.AddChild(projectile);
                projectile.Initialize(data);
            }
        }
    }
}
