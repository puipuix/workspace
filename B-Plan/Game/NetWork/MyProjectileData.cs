﻿using BPlan.Game.Common;
using Godot;
using Godot.Collections;
using GodotUtils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Metadata.W3cXsd2001;
using System.Text;
using System.Threading.Tasks;

namespace BPlan.Game.NetWork
{
    public struct MyProjectileData : IMyRPCData
    {
        public int ProjectileID { get; set; }
        public MyIdentification Identification { get; set; }
        public Vector2 Position { get; set; }
        public Vector2 Velocity { get; set; }
        public Vector2 Direction { get; set; }
        public int GunDamage { get; set; }
        public string Name { get; set; }
        public float Scale { get; set; }

        public MyProjectileData(int projectileID, MyIdentification identification, Vector2 position, Vector2 velocity, Vector2 direction, int gundmg, string name, float scale)
        {
            ProjectileID = projectileID;
            Identification = identification;
            Position = position;
            Velocity = velocity;
            Direction = direction;
            GunDamage = gundmg;
            Name = name;
            Scale = scale;
        }
        public int Construct(byte[] buffer, int index)
        {
            index = MyRPCConvert.UnStore(out int pro, buffer, index);
            ProjectileID = pro;
            index = MyRPCConvert.UnStore(out MyIdentification i, buffer, index);
            Identification = i;
            index = MyRPCConvert.UnStore(out Vector2 po, buffer, index);
            Position = po;
            index = MyRPCConvert.UnStore(out Vector2 v, buffer, index);
            Velocity = v;
            index = MyRPCConvert.UnStore(out Vector2 d, buffer, index);
            Direction = d;
            index = MyRPCConvert.UnStore(out int g, buffer, index);
            GunDamage = g;
            index = MyRPCConvert.UnStore(out string n, buffer, index);
            Name = n;
            index = MyRPCConvert.UnStore(out float s, buffer, index);
            Scale = s;
            return index;
        }

        public MyProjectileData(byte[] buffer, ref int index) : this()
        {
            index = Construct(buffer, index);
        }

        public MyProjectileData(byte[] buffer) : this()
        {
            Construct(buffer, 0);
        }

        public int GetRPCLength()
        {
            return MyRPCConvert.GetBufferLength(ProjectileID, Identification, Position, Velocity, Direction, GunDamage, Name, Scale);
        }

        public int ToRPCValue(byte[] buffer, int index)
        {
            index = MyRPCConvert.Store(ProjectileID, buffer, index);
            index = MyRPCConvert.Store(Identification, buffer, index);
            index = MyRPCConvert.Store(Position, buffer, index);
            index = MyRPCConvert.Store(Velocity, buffer, index);
            index = MyRPCConvert.Store(Direction, buffer, index);
            index = MyRPCConvert.Store(GunDamage, buffer, index);
            index = MyRPCConvert.Store(Name, buffer, index);
            index = MyRPCConvert.Store(Scale, buffer, index);
            return index;
        }
        
    }
}
