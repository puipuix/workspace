﻿using Godot.Collections;
using GodotUtils;
using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BPlan.Game.NetWork
{
    public struct MyScenarioData : IMyRPCData
    {
        public string[] TeamNames { get; private set; }

        public int TeamCount => TeamNames.Length;

        public int Construct(byte[] buffer, int index)
        {
            index = MyRPCConvert.UnStore(out string[] names, MyRPCConvert.UnStore, buffer, index);
            TeamNames = names;
            return index;
        }

        public MyScenarioData(string[] teamNames)
        {
            TeamNames = teamNames;
        }

        public MyScenarioData(byte[] buffer, ref int index) : this()
        {
            index = Construct(buffer, index);
        }
        public MyScenarioData(byte[] buffer) : this()
        {
            Construct(buffer, 0);
        }

        public int ToRPCValue(byte[] buffer, int index)
        {
            return MyRPCConvert.Store(TeamNames, MyRPCConvert.Store, buffer, index);
        }
        
        public int GetRPCLength()
        {
            return MyRPCConvert.GetArrayBufferLength(TeamNames);
        }
    }
}
