﻿using BPlan.Game.Common;
using Godot;
using Godot.Collections;
using GodotUtils;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Array = Godot.Collections.Array;

namespace BPlan.Game.NetWork
{
    public struct MyClientData : IMyRPCData
    {
        public int NetworkId { get; private set; }
        public string UserName { get; private set; }
        public bool IsNPC { get; private set; }

        public int[][][] Ammos { get; private set; }
        public sbyte CustomPlaneId { get; private set; }
        public Color CustomColor { get; private set; }

        public MyClientData(int networkId, string userName, sbyte customPlaneId, int[][][] ammos, Color custom, bool isNPC = false)
        {
            NetworkId = networkId;
            UserName = userName;
            IsNPC = isNPC;
            CustomPlaneId = customPlaneId;
            CustomColor = custom;
            Ammos = ammos;
        }

        public int Construct(byte[] buffer, int index)
        {
            index = MyRPCConvert.UnStore(out int n, buffer, index);
            NetworkId = n;
            index = MyRPCConvert.UnStore(out string na, buffer, index);
            UserName = na;
            index = MyRPCConvert.UnStore(out bool bo, buffer, index);
            IsNPC = bo;
            index = MyRPCConvert.UnStore(out sbyte p, buffer, index);
            CustomPlaneId = p;
            index = MyRPCConvert.UnStore(out Color c, buffer, index);
            CustomColor = c;
            index = MyRPCConvert.UnStore<int[][]>(out int[][][] tabtabtab, (out int[][] tabtab, byte[] buf, int i) =>
            {
                return MyRPCConvert.UnStore<int[]>(out tabtab, (out int[] tab, byte[] b, int j) =>
                {
                    return MyRPCConvert.UnStore<int>(out tab, MyRPCConvert.UnStore, b, j);
                }, buf, i);
            }, buffer, index);
            Ammos = tabtabtab;
            return index;
        }

        public MyClientData(byte[] buffer, ref int index) : this()
        {
            index = Construct(buffer, index);
        }

        public MyClientData(byte[] buffer) : this()
        {
            Construct(buffer, 0);
        }

        public int[] GetPrimaryAmmo(int planeId) => Ammos[planeId][0];
        public int[] GetSecondaryAmmo(int planeId) => Ammos[planeId][1];
        public int[] GetThirdAmmo(int planeId) => Ammos[planeId][2];
        public int[] GetFourthAmmo(int planeId) => Ammos[planeId][3];

        public int GetRPCLength()
        {
            return MyRPCConvert.GetBufferLength(NetworkId, UserName, IsNPC, Ammos, CustomPlaneId, CustomColor);
        }

        public int ToRPCValue(byte[] buffer, int index)
        {
            index = MyRPCConvert.Store(NetworkId, buffer, index);
            index = MyRPCConvert.Store(UserName, buffer, index);
            index = MyRPCConvert.Store(IsNPC, buffer, index);
            index = MyRPCConvert.Store(CustomPlaneId, buffer, index);
            index = MyRPCConvert.Store(CustomColor, buffer, index);
            index = MyRPCConvert.Store<int[][]>(Ammos, (tabtab, buf, i) =>
            {
                return MyRPCConvert.Store<int[]>(tabtab, (tab, b, j) =>
                {
                    return MyRPCConvert.Store<int>(tab, MyRPCConvert.Store, b, j);
                }, buf, i);
            }, buffer, index);
            return index;
        }
    }
}
