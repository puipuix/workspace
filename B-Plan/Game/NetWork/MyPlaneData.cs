﻿using BPlan.Game.Common;
using Godot;
using Godot.Collections;
using GodotUtils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BPlan.Game.NetWork
{
    public struct MyPlaneData : IMyRPCData
    {
        public int PlaneId { get; private set; }
        public MyIdentification Identification { get; private set; }
        public EMyPlayerInputType InputType { get; private set; }
        public Color Color { get; private set; }
        public Transform2D Transform { get; private set; }
        public bool Reversed { get; private set; }

        public MyPlaneData(int planeId, MyIdentification identification, EMyPlayerInputType inputType, Color color, Transform2D transform, bool reversed)
        {
            PlaneId = planeId;
            Identification = identification;
            InputType = inputType;
            Color = color;
            Transform = transform;
            Reversed = reversed;
        }

        public int Construct(byte[] buffer, int index)
        {
            index = MyRPCConvert.UnStore(out int p, buffer, index);
            PlaneId = p;
            index = MyRPCConvert.UnStore(out MyIdentification i, buffer, index);
            Identification = i;
            index = MyRPCConvert.UnStore(out byte b, buffer, index);
            InputType = (EMyPlayerInputType)b;
            index = MyRPCConvert.UnStore(out Color c, buffer, index);
            Color = c;
            index = MyRPCConvert.UnStore(out Transform2D t, buffer, index);
            Transform = t;
            index = MyRPCConvert.UnStore(out bool r, buffer, index);
            Reversed = r;
            return index;
        }

        public MyPlaneData(byte[] buffer, ref int index) : this()
        {
            index = Construct(buffer, index);
        }

        public MyPlaneData(byte[] buffer) : this()
        {
            Construct(buffer, 0);
        }

        public int GetRPCLength()
        {
            return MyRPCConvert.GetBufferLength(PlaneId, Identification, Color, Transform, Reversed) + 1;
        }

        public int ToRPCValue(byte[] buffer, int index)
        {
            index = MyRPCConvert.Store(PlaneId, buffer, index);
            index = MyRPCConvert.Store(Identification, buffer, index);
            index = MyRPCConvert.Store((byte)InputType, buffer, index);
            index = MyRPCConvert.Store(Color, buffer, index);
            index = MyRPCConvert.Store(Transform, buffer, index);
            index = MyRPCConvert.Store(Reversed, buffer, index);
            return index;
        }
    }
}
