﻿using Godot;
using Godot.Collections;
using GodotUtils;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BPlan.Game.NetWork
{
    public class MyServerManager : MyNetworkManager
    {
        public int Port { get; set; } = 59138;
        public bool AllowConnections { get; set; } = true;
        private MyServerManager() { }

        public static MyServerManager Instance(int requestedPort, bool allowConnections)
        {
            var serv = new MyServerManager() { Port = requestedPort };
            serv.AllowConnections = allowConnections;
            return serv;
        }

        public Error CreateServer()
        {
            CurrentInstance = this;
            Battle.Loading.PopUp();
            Battle.Loading.Text = "LOAD_CREATE_SERVER";
            Battle.Loading.Progress = 0;
            Peer = new NetworkedMultiplayerENet();
            Error err = Peer.CreateServer(Port);
            if (err == Error.Ok)
            {
                Battle.Loading.Text = "LOAD_SETUP";
                Battle.Loading.Progress = 0.5f;
                Peer.RefuseNewConnections = !AllowConnections;
                GetTree().NetworkPeer = Peer;
                Battle.LoadRandomScenario();
                StartNetworking();
            }
            return err;
        }

        [RemoteSync]
        protected override void RPCRegisterNewPlayerInfo(byte[] buffer)
        {
            base.RPCRegisterNewPlayerInfo(buffer); 
            MyClientData data = new MyClientData(buffer);
            if (data.NetworkId > 0)
            {
                Battle.Scenario?.AddPlayer(data, Battle);
            }
            MyDebug.ClearTab();
        }

        protected override void OnPlayerDisconnected(int id)
        {
            if (PlayersData.TryGetValue(id, out MyClientData data))
            {
                Battle.Scenario?.RemovePlayer(data, Battle);
            }
            base.OnPlayerDisconnected(id);
        }

        public override void StartNetworking()
        {
            base.StartNetworking();
            Battle.Loading.Progress = 1;
            Battle.Loading.Text = "LOAD_END";
            Battle.Loading.FadeOut();
        }
    }
}
