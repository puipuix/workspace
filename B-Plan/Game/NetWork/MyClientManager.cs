﻿using Godot;
using GodotUtils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BPlan.Game.NetWork
{
    public class MyClientManager : MyNetworkManager
    {
        public string Ip { get; set; }
        public int Port { get; set; } = 59138;

        public Error ConnectToServer()
        {
            CurrentInstance = this;
            Battle.Loading.PopUp();
            Battle.Loading.Text = "LOAD_CREATE_CLIENT";
            Battle.Loading.Progress = 0;
            GetTree().Connect("connected_to_server", this, nameof(OnConnectionToServer));
            GetTree().Connect("connection_failed", this, nameof(OnConnectionFailed));
            GetTree().Connect("server_disconnected", this, nameof(OnServerDisconnected));
            Peer = new NetworkedMultiplayerENet();
            Error err = Peer.CreateClient(Ip, Port);
            if (err == Error.Ok)
            {
                Battle.Loading.Text = "LOAD_CONNECTING";
                Battle.Loading.Progress = 0.1f;
                GetTree().NetworkPeer = Peer;
            }
            return err;
        }
        protected void OnConnectionToServer()
        {
            MyDebug.Print("Server connected");
            StartNetworking();
        }

        public override void StartNetworking()
        {
            base.StartNetworking();
            Battle.Refresh(0.25f);
        }

        protected void OnConnectionFailed()
        {
            MyDebug.Print("Connection failed");
            StopNetworking("ERR_CON_FAILED");
        }

        protected void OnServerDisconnected()
        {
            MyDebug.Print("Server disconnected");
            StopNetworking("ERR_DISCONNECTED");
        }
    }
}
