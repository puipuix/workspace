﻿using BPlan.Game.Common.Projectiles;
using BPlan.Game.Common.Vehicles.Planes;
using BPlan.Game.Menu;
using Godot;
using GodotUtils;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GDictionary = Godot.Collections.Dictionary;

namespace BPlan.Game.NetWork
{
    public abstract class MyNetworkManager : Node2D
    {
        public static MyNetworkManager CurrentInstance { get; protected set; }
        public MyClientData LocalData { get; protected set; }
        public NetworkedMultiplayerENet Peer { get; protected set; }
        public Dictionary<int, MyClientData> PlayersData { get; } = new Dictionary<int, MyClientData>();

        public MyBattle Battle { get; set; }
               
        protected MyNetworkManager()
        {
            Name = "NetworkManager";
        }

        public override void _Ready()
        {
            base._Ready();
            Battle = (MyBattle)GD.Load<PackedScene>("res://Game/Common/MyBattle.tscn").Instance();
            AddChild(Battle);
        }

        public virtual void StartNetworking()
        {
            MyDebug.Print("Starting Networking...(" + GetTree().GetNetworkUniqueId() + ")");
            GetTree().Connect("network_peer_connected", this, nameof(OnPlayerConnected));
            GetTree().Connect("network_peer_disconnected", this, nameof(OnPlayerDisconnected));
            Battle.OnNetworkEstablished();
            LocalData = new MyClientData(GetTree().GetNetworkUniqueId(), MyGameSettings.PlayerName, MyGameSettings.CustomPlaneId, MyGameSettings.Ammos, MyGameSettings.PlaneColor);
            Rpc(nameof(RPCRegisterNewPlayerInfo), LocalData.ToRPCValue());
        }

        public virtual bool StopNetworking(string reason)
        {
            if (CurrentInstance == this)
            {
                CurrentInstance = null;
                Peer.RefuseNewConnections = true;
                Peer.CloseConnection();
                GetTree().NetworkPeer = null;
                if (GetTree().IsConnected<int>("network_peer_connected", this, OnPlayerConnected))
                {
                    GetTree().Disconnect("network_peer_connected", this, nameof(OnPlayerConnected));
                    GetTree().Disconnect("network_peer_disconnected", this, nameof(OnPlayerDisconnected));
                }
                GetParent().AddChild(MyStartMenu.Instance(reason));
                GetParent().RemoveChild(this);
                QueueFree();
                MyDebug.Print("Networking stoped.");
                return true;
            }
            else
            {
                return false;
            }
        }

        protected virtual void OnPlayerConnected(int id)
        {
            MyDebug.Print("New player connected: " + id + ", sending local data...");
            MyDebug.AddTab();
            RpcId(id, nameof(RPCRegisterNewPlayerInfo), LocalData.ToRPCValue());
        }

        protected virtual void OnPlayerDisconnected(int id)
        {
            MyDebug.ClearTab();
            MyDebug.Print("Player disconnected: " + id);
            PlayersData.Remove(id);
        }

        [RemoteSync]
        protected virtual void RPCRegisterNewPlayerInfo(byte[] buffer)
        {
            MyDebug.ClearTab();
            MyDebug.Print("Registering player info...");
            MyClientData data = new MyClientData(buffer);
            MyDebug.Print("\t" + data.UserName + " (id: " + data.NetworkId + ")");
            if (data.NetworkId <= 0)
            {
                MyDebug.Print(data.NetworkId + " isn't a valid id (id <= 0)");
            }
            else
            {
                PlayersData.Add(data.NetworkId, data);
            }
        }


        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            Peer = null;
        }
    }
}
