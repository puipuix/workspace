﻿using BPlan.Game.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GodotUtils;
using BPlan.Game.NetWork;
using Godot;
using BPlan.Game.Common.Vehicles.Planes;

namespace BPlan.Game.Tests
{
    public static class MyTestRPC
    {
        public static void Test()
        {
            new MyGameRule(new MyGameRule(true, true, true, true, true).ToRPCValue());
            MyIdentification id = new MyIdentification(new MyIdentification(32, 1, 0).ToRPCValue());
            new MyPlaneData(new MyPlaneData(id.ServerPlaneId, id, EMyPlayerInputType.Local, Colors.RebeccaPurple, new Transform2D(), false).ToRPCValue());
            new MyProjectileData(new MyProjectileData(0, id, new Vector2(), new Vector2(), new Vector2(), 0, "ammo test", 1.1f).ToRPCValue());
            new MyScenarioData(new MyScenarioData(new string[] { "Alpha", "Bravo" }).ToRPCValue());
            new MyClientData(new MyClientData(1,"user test", 0, EMyPlanes.GetRandomAmmoRacks(), Colors.Red).ToRPCValue());
            var log = new MyPlayerNameLogText(id, "test") + " bla";
            MyLogText.FromRPCValue(log.ToRPCValue());
        }
    }
}
