shader_type canvas_item;

uniform sampler2D Texture : hint_albedo;
uniform float Tolerance;
uniform vec4 ColorAlpha : hint_color;

void fragment() {
	vec4 textColor = texture(Texture, UV);
	if (textColor.b >= Tolerance){
		COLOR.rgb = textColor.rgb;
	} else {
		COLOR.rgb = ColorAlpha.rgb * textColor.r;
	}
	//COLOR.rgb = n_out9p0;
	COLOR.a = textColor.a * ColorAlpha.a;
}