shader_type canvas_item;

uniform float Wind;
uniform float MaxShift;
uniform float Offset;

void fragment() {
	float shiftPx = (1.0-UV.y) * MaxShift * sin(Wind * (Offset + TIME));
	float shiftUV = UV.x + shiftPx / vec2(textureSize(TEXTURE, 0)).x;
	COLOR.rgba = texture(TEXTURE, vec2(shiftUV , UV.y));
}