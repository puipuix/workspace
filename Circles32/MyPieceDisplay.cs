using Godot;
using GodotUtils;
using System;

public class MyPieceDisplay : GridContainer
{
    public void Show(bool[,] piece, int owner, int localId, int[] players)
    {
        this.RemoveAllChildren();
        Columns = piece.GetLength(0);
        for (int j = 0; j < piece.GetLength(1); j++)
        {
            for (int i = 0; i < piece.GetLength(0); i++)
            {
                var circle = MyCircle.Instance(i, j);
                AddChild(circle);
                circle.SetValue(piece[i,j] ? owner : -1, localId, players);
                circle.RectMinSize = new Vector2(120, 120);
            }
        }
    }
}
