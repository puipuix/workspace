using Circles32;
using Godot;
using GodotUtils;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Security;
using System.Threading;

public class MyClient : Control, IMyGrid
{
    [MyOnReady("Grid", EMyNodeResearchMode.Descendant)]
    private GridContainer _grid = null;

    [MyOnReady("Turn", EMyNodeResearchMode.Descendant)]
    private Label _turn = null;

    [MyOnReady("You", EMyNodeResearchMode.Descendant)]
    private Label _you = null;

    [MyOnReady("MyPieceDisplay", EMyNodeResearchMode.Descendant)]
    private MyPieceDisplay _pieceDisplay = null;

    private bool _spectator;

    private int _currentPlayer;
    private int[] _players = new int[0];
    private string[] _playersName = new string[0];

    private int _r;
    private bool[,] _currentPiece = new bool[0, 0];
    private string _name;
    private Dictionary<int, string> _spectators = new Dictionary<int, string>();

    private void Print(object what, [CallerFilePath] string memberName = "", [CallerLineNumber] int sourceLineNumber = -1)
    {
        MyDebug.Print(what, "Client_" + (CustomMultiplayer?.GetNetworkUniqueId() ?? 0).ToString(), memberName, sourceLineNumber);
    }

    public override void _Ready()
    {
        base._Ready();
        MyOnReadyAttribute.SetUp(this);
    }

    public void ConnectToServer(string ip, int port, bool spectator, string name = null)
    {
        Print("Connecting to '" + ip + ":" + port + "' as " + (spectator ? "spectator..." : "player..."));
        _spectator = spectator;
        _name = name;
        var peer = new NetworkedMultiplayerENet();
        peer.CreateClient(ip, port);
        CustomMultiplayer = new MultiplayerAPI();
        CustomMultiplayer.SetRootNode(this);
        CustomMultiplayer.Connect("connected_to_server", this, OnConnectedToServer);
        CustomMultiplayer.Connect("connection_failed", this, OnConnectionFailed);
        CustomMultiplayer.Connect("server_disconnected", this, OnDisconnected);
        CustomMultiplayer.NetworkPeer = peer;
        Print("\tDone.");
    }

    private void OnConnectedToServer()
    {
        Print("Connected.");
        if (_spectator)
        {
            this.Rpc(JoinAsSpectator, _name ?? ("Spectator_" + CustomMultiplayer.GetNetworkUniqueId()));
        }
        else
        {
            this.Rpc(JoinAsPlayer, _name ?? ("Player_" + CustomMultiplayer.GetNetworkUniqueId()));
        }
    }

    private void OnConnectionFailed()
    {
        Print("Connection failed.");
    }

    private void OnDisconnected()
    {
        Print("Disonnected.");
    }

    public override void _Process(float delta)
    {
        base._Process(delta);
        if (CustomMultiplayer != null)
        {
            CustomMultiplayer.Poll();
        }
    }

    [Master]
    public void AskSendData()
    {

    }

    [Master]
    public void JoinAsPlayer(string name)
    {

    }

    [Master]
    public void JoinAsSpectator(string name)
    {

    }

    [Master]
    public void Play(int i, int j, int r)
    {

    }

    [Puppet]
    public void PrintWinner(int index, int[] scores)
    {
        Print((CustomMultiplayer.GetNetworkUniqueId() == _players[index] ? "win" : "lost"));
    }

    private void SetCurrentPieceImp(bool[,] piece)
    {
        _currentPiece = piece;
        _pieceDisplay.Show(_currentPiece, _currentPlayer, CustomMultiplayer.GetNetworkUniqueId(), _players);
    }

    [PuppetSync]
    public void SetCurrentPiece(byte[] buffer)
    {
        SetCurrentPieceImp(new MyPieceData(buffer).Piece);
    }

    [PuppetSync]
    public void SetCurrentPlayer(int playerId)
    {
        Print("Next player: " + playerId + ", me?: " + (playerId == CustomMultiplayer.GetNetworkUniqueId()));
        _currentPlayer = playerId;
        _pieceDisplay.Show(_currentPiece, _currentPlayer, CustomMultiplayer.GetNetworkUniqueId(), _players);
        _turn.Text = "Player_" + playerId;
        for (int i = 0; i < _players.Length; i++)
        {
            if (_players[i] == playerId)
            {
                if (_playersName[i] != null)
                {
                    _turn.Text = _playersName[i];
                }
                break;
            }
        }
        _you.Visible = playerId == CustomMultiplayer.GetNetworkUniqueId();
    }

    private void SetUpGrid(int[,] circles)
    {
        _grid.RemoveAllChildren(n => n.QueueFree());
        _grid.Columns = circles.GetLength(0);
        for (int j = 0; j < circles.GetLength(1); j++)
        {
            for (int i = 0; i < circles.GetLength(0); i++)
            {
                var circle = MyCircle.Instance(i, j);
                _grid.AddChild(circle);
                circle.SetValue(circles[i, j], CustomMultiplayer.GetNetworkUniqueId(), _players);
                if (!_spectator)
                {
                    circle.Connect<InputEvent, int, int>("gui_input", this, OnCircleClicked, i, j);
                }
            }
        }
    }

    private void OnCircleClicked(InputEvent @event, int i, int j)
    {
        if (@event is InputEventMouseButton button && button.ButtonIndex == (int)ButtonList.Left)
        {
            Print("click");
            this.RpcId(1, Play, i, j, _r);
        }
    }

    [Puppet]
    public void SetData(byte[] buffer)
    {
        Print("Receiving data...");
        MyDebug.AddTab();
        var data = new MyGridData(buffer);
        _players = data.Players;
        _playersName = data.PlayersName;
        SetUpGrid(data.Circles);
        SetCurrentPieceImp(data.CurrentPiece);
        SetCurrentPlayer(data.CurrentPlayer);
        Print("Done.");
        MyDebug.RemoveTab();
    }

    [Puppet]
    public void SetGridValue(int i, int j, int value)
    {
        _grid.GetNode<MyCircle>(MyCircle.GetName(i, j)).SetValue(value, CustomMultiplayer.GetNetworkUniqueId(), _players);
    }

    [Puppet]
    public void PlayerAdded(int index, int value, string name)
    {
        Print("New player: " + value + " at index: " + index + ".");
        _players[index] = value;
        _playersName[index] = name;
    }

    [Puppet]
    public void SpectatorAdded(int peer, string name)
    {
        _spectators[peer] = name;
    }
}
