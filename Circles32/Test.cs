using Circles32;
using Godot;
using GodotUtils;
using System;

public class Test : Node2D
{

    private void Print(bool[,] piece)
    {
        for (int j = 0; j < piece.GetLength(1); j++)
        {
            var str = "";
            for (int i = 0; i < piece.GetLength(0); i++)
            {
                str += (piece[i, j] ? 1 : 0);
            }
            GD.Print(str);
        }
    }

    public override void _Ready()
    {
        GD.Print("Test:");
        for (int i = 0; i < 100; i++)
        {
            var piece = EMyPieces.RandomPiece();
            var rotation = MyTools.RNG.Next(0, 5);
            var rotated = EMyPieces.Rotate(piece, rotation);
            GD.Print("====");
            Print(piece);
            GD.Print("rotation: " + rotation);
            Print(rotated);
        }
    }
}
