﻿using GodotUtils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Circles32
{
    public struct MyGridData : IMyRPCData
    {
        public int CurrentPlayer { get; private set; }
        public int[] Players { get; private set; }
        public bool[,] CurrentPiece { get; private set; }
        public int[,] Circles { get; private set; }
        public string[] PlayersName { get; private set; }

        public MyGridData(int currentPlayer, int[] players, string[] playersName, bool[,] currentPiece, int[,] circles)
        {
            CurrentPlayer = currentPlayer;
            Players = players;
            PlayersName = playersName;
            for (int i = 0; i < PlayersName.Length; i++)
            {
                PlayersName[i] = PlayersName[i] ?? "";
            }
            CurrentPiece = currentPiece;
            Circles = circles;
        }

        public MyGridData(byte[] buffer, ref int index) : this()
        {
            index = Construct(buffer, index);
        }

        public MyGridData(byte[] buffer) : this()
        {
            Construct(buffer, 0);
        }

        public int Construct(byte[] buffer, int index)
        {
            int i = MyRPCConvert.UnStore(out int currentPlayer, buffer, index);
            i = MyRPCConvert.UnStore(out int[] players, MyRPCConvert.UnStore, buffer, i);
            i = MyRPCConvert.UnStore(out string[] names, MyRPCConvert.UnStore, buffer, i);
            i = MyRPCConvert.UnStore(out int sizePiece, buffer, i);
            i = MyRPCConvert.UnStore(out bool[] piece, MyRPCConvert.UnStore, buffer, i);
            i = MyRPCConvert.UnStore(out int circleSize, buffer, i);
            i = MyRPCConvert.UnStore(out int[] circles, MyRPCConvert.UnStore, buffer, i);
            CurrentPlayer = currentPlayer;
            Players = players;
            PlayersName = names;
            CurrentPiece = EMyPieces.Cast(sizePiece, piece);
            Circles = circles.From1dTo2d(circleSize);
            return i;
        }
        public int ToRPCValue(byte[] buffer, int index)
        {
            (int sizePiece, bool[] piece) = EMyPieces.Cast(CurrentPiece);
            int i = MyRPCConvert.Store(CurrentPlayer, buffer, index);
            i = MyRPCConvert.Store(Players, MyRPCConvert.Store, buffer, i);
            i = MyRPCConvert.Store(PlayersName, MyRPCConvert.Store, buffer, i);
            i = MyRPCConvert.Store(sizePiece, buffer, i);
            i = MyRPCConvert.Store(piece, MyRPCConvert.Store, buffer, i);
            i = MyRPCConvert.Store(Circles.GetLength(0), buffer, i);
            i = MyRPCConvert.Store(Circles.From2dTo1d(), MyRPCConvert.Store, buffer, i);
            return i;
        }


        public int GetRPCLength()
        {
            return 4                        // CurrentPlayer
                + 4 + Players.Length * 4    // Players
                + MyRPCConvert.GetArrayBufferLength(PlayersName)
                + 4                         // Piece size
                + 4 + CurrentPiece.Length   // Piece
                + 4                         // Grid Size
                + 4 + Circles.Length * 4;   // Circles
        }

    }
}
