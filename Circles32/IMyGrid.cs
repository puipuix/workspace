﻿using Godot.Collections;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Circles32
{
    interface IMyGrid
    {
        /// <summary>
        /// Server
        /// </summary>
        void JoinAsPlayer(string name);

        /// <summary>
        /// Server
        /// </summary>
        void JoinAsSpectator(string name);

        /// <summary>
        /// Client
        /// </summary>
        /// <param name="i"></param>
        /// <param name="j"></param>
        /// <param name="value"></param>
        void SetGridValue(int i, int j, int value);

        /// <summary>
        /// Server
        /// </summary>
        /// <param name="i"></param>
        /// <param name="j"></param>
        /// <param name="r"></param>
        void Play(int i, int j, int r);

        /// <summary>
        /// Client
        /// </summary>
        /// <param name="playerId"></param>
        void SetCurrentPlayer(int playerId);

        /// <summary>
        /// Client
        /// </summary>
        void SetCurrentPiece(byte[] buffer);

        /// <summary>
        /// Client
        /// </summary>
        /// <param name="peer"></param>
        void PrintWinner(int index, int[] scores);

        /// <summary>
        /// Server
        /// </summary>
        void AskSendData();

        /// <summary>
        /// Client
        /// </summary>
        /// <param name="currentPlayerId"></param>
        /// <param name="gridSize"></param>
        /// <param name="grid"></param>
        void SetData(byte[] buffer);

        /// <summary>
        /// Client
        /// </summary>
        /// <param name="index"></param>
        /// <param name="value"></param>
        void PlayerAdded(int index, int value, string name);

        /// <summary>
        /// Client
        /// </summary>
        /// <param name="peer"></param>
        /// <param name="name"></param>
        void SpectatorAdded(int peer, string name);
    }
}
