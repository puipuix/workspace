shader_type canvas_item;

uniform vec4 Albedo : hint_color;

void fragment() {
	vec4 textColor = texture(TEXTURE, UV);
	COLOR.rgba = Albedo.rgba * textColor.rgba;
}