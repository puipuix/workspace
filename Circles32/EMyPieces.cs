﻿using Godot;
using GodotUtils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Circles32
{
    public static class EMyPieces
    {
        private static bool[,] b(int[,] piece)
        {
            var li = piece.GetLength(0);
            var lj = piece.GetLength(1);
            // I AND J ARE SWITCHED
            var asbool = new bool[lj, li];
            for (int i = 0; i < li; i++)
            {
                for (int j = 0; j < lj; j++)
                {
                    asbool[j, i] = piece[i, j] != 0;
                }
            }
            return asbool;
        }
        
        private static int[][,] _piecesAsInt;
        private static bool[][,] _pieces;

        static EMyPieces()
        {
            // I AND J ARE SWITCHED
            _piecesAsInt = new int[][,] 
            {
                new int[,]{ { 1, 1, 1, 1} },

                new int[,]{ { 1, 1, 1 },
                            { 1, 0, 0 } },

                new int[,]{ { 1, 1, 1 },
                            { 0, 0, 1 } },

                new int[,]{ { 1, 1 },
                            { 1, 1 } },

                new int[,]{ { 1, 1, 1 },
                            { 0, 1, 0 } },

                new int[,]{ { 1, 1, 0 },
                            { 0, 1, 1 } },

                new int[,]{ { 0, 1, 1 },
                            { 1, 1, 0 } },
            };

            _pieces = new bool[_piecesAsInt.Length][,];
            for (int i = 0; i < _pieces.Length; i++)
            {
                _pieces[i] = b(_piecesAsInt[i]);
            }
        }

        public static bool[,] RandomPiece()
        {
            return MyTools.RNG.NextElement(_pieces);
        }

        private static bool[,] Rotate(bool[,] piece)
        {
            var li = piece.GetLength(0);
            var lj = piece.GetLength(1);
            var rotated = new bool[lj, li];
            for (int i = 0; i < li; i++)
            {
                for (int j = 0; j < lj; j++)
                {
                    rotated[j, li - i - 1] = piece[i, j];
                }
            }
            return rotated;
        }

        public static bool[,] Rotate(bool[,] piece, int amount = 1)
        {
            while(amount-- > 0)
            {
                piece = Rotate(piece);
            }
            return piece;
        }

        public static (int i, bool[]) Cast(bool[,] piece)
        {
            return (piece.GetLength(0), piece.From2dTo1d());
        }

        public static bool[,] Cast(int i, bool[] piece)
        {
            if (i == 0)
            {
                return new bool[0, 0];
            } else
            {
                return piece.From1dTo2d(i);
            }
        }
    }

    public struct MyPieceData : IMyRPCData
    {
        public bool[,] Piece { get; private set; }

        public MyPieceData(bool[,] piece)
        {
            Piece = piece;
        }
        public MyPieceData(byte[] buffer, ref int index) : this()
        {
            index = Construct(buffer, index);
        }
        public MyPieceData(byte[] buffer) : this()
        {
            Construct(buffer, 0);
        }

        public int Construct(byte[] buffer, int index)
        {
            int i = MyRPCConvert.UnStore(out int sizeI, buffer, index);
            i = MyRPCConvert.UnStore(out bool[] array, MyRPCConvert.UnStore, buffer, i);
            Piece = EMyPieces.Cast(sizeI, array);
            return i;
        }

        public int GetRPCLength()
        {
            return 4 + 4 + Piece.Length;
        }

        public int ToRPCValue(byte[] buffer, int index)
        {
            (int sizeI, bool[] array) = EMyPieces.Cast(Piece);
            int i = MyRPCConvert.Store(sizeI, buffer, index);
            i = MyRPCConvert.Store(array, MyRPCConvert.Store, buffer, i);
            return i;
        }
    }
}
