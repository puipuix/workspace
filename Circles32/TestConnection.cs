using Godot;
using GodotUtils;
using System;

public class TestConnection : CanvasLayer
{
    [MyOnReady("Player1")]
    private Label _player1lab = null;
    
    [MyOnReady("Player2")]
    private Label _player2lab = null;
    
    [MyOnReady("Spectator")]
    private Label _speclab = null;

    [MyOnReady("Player1/Grid")]
    private MyClient _player1 = null;

    [MyOnReady("Player2/Grid")]
    private MyClient _player2 = null;

    [MyOnReady("Spectator/Grid")]
    private MyClient _spectator = null;

    [MyOnReady("Server/Grid")]
    private MyServer _server = null;

    public override void _Ready()
    {
        base._Ready();
        MyDebug.PrintCustomText = true;
        MyDebug.CustomTextSizeLimit = 15;
        MyOnReadyAttribute.SetUp(this);
        _server.StartServer(59130, 2);
        _player1.ConnectToServer("localhost", 59130, false);
        _player2.ConnectToServer("localhost", 59130, false);
        _spectator.ConnectToServer("localhost", 59130, true);
    }

    public override void _Process(float delta)
    {
        base._Process(delta);
        if (Input.IsActionJustPressed("ui_select"))
        {
            if (_player1lab.Visible)
            {
                _player1lab.Visible = false;
                _player2lab.Visible = true;
            } else if (_player2lab.Visible)
            {
                _player2lab.Visible = false;
                _speclab.Visible = true;
            } else
            {
                _speclab.Visible = false;
                _player1lab.Visible = true;
            }
        }
    }
}
