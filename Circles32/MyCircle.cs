using Godot;
using System;

public class MyCircle : TextureRect
{
    private static Color DefaultColor = Colors.Black;
    private static PackedScene _packedScene = GD.Load<PackedScene>("res://MyCircle.tscn");
    public static MyCircle Instance(int i, int j)
    {
        var circle = (MyCircle)_packedScene.Instance();
        circle.Name = GetName(i, j);
        return circle;
    }

    public static string GetName(int i, int j)
    {
        return "[" + i + ";" + j + "]";
    }

    private static readonly Color[] _colors = { Colors.Red, Colors.Orange, Colors.Yellow, Colors.LawnGreen, Colors.Pink, Colors.FloralWhite };

    private ShaderMaterial _shader;

    public override void _Ready()
    {
        base._Ready();
        _shader = (ShaderMaterial)Material.Duplicate();
        Material = _shader;
        _shader.SetShaderParam("Albedo", DefaultColor);
    }

    public void SetValue(int value, int localId, int[] players)
    {
        if (value == -1)
        {
            _shader.SetShaderParam("Albedo", Colors.Transparent);
        }
        else if (value == 0)
        {
            _shader.SetShaderParam("Albedo", DefaultColor);
        }
        else if (value == localId)
        {
            _shader.SetShaderParam("Albedo", Colors.Blue);
        }
        else
        {
            for (int i = 0; i < players.Length; i++)
            {
                if (players[i] == value)
                {
                    _shader.SetShaderParam("Albedo", _colors[i % _colors.Length]);
                }
            }
        }
    }
}
