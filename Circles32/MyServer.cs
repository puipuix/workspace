using Circles32;
using Godot;
using GodotUtils;
using System.Linq;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using System;
using System.Runtime.CompilerServices;

public class MyServer : Node, IMyGrid
{
    private int[,] _circles = new int[32, 32];

    private int[] _players;
    private string[] _playersName;
    private int _playerIndex = -1;
    private int _currentPlayerNetworkId => _playerIndex < 0 ? 0 : _players[_playerIndex % _players.Length];
    private bool[,] _currentPiece = new bool[0, 0];

    private float _time;
    private bool _started;
    private void Print(object what, [CallerFilePath] string memberName = "", [CallerLineNumber] int sourceLineNumber = -1)
    {
        MyDebug.Print(what.SafeToString(), "Server", memberName, sourceLineNumber);
    }

    public void StartServer(int port, int playerCount)
    {
        Print("Starting server on port " + port + ", " + playerCount + " players...");
        _players = new int[playerCount];
        _playersName = new string[playerCount];
        var peer = new NetworkedMultiplayerENet();
        peer.CreateServer(port);
        CustomMultiplayer = new MultiplayerAPI();
        CustomMultiplayer.SetRootNode(this);
        CustomMultiplayer.Connect<int>("network_peer_connected", this, OnPeerConnected);
        CustomMultiplayer.Connect<int>("network_peer_disconnected", this, OnPeerDisconnected);
        CustomMultiplayer.NetworkPeer = peer;
        Print("\tDone.");
    }

    public void StopServer()
    {
        Print("Stopping server...");
        CustomMultiplayer.RefuseNewNetworkConnections = true;
        var peer = CustomMultiplayer.NetworkPeer as NetworkedMultiplayerENet;
        CustomMultiplayer.NetworkPeer = null;
        peer.CloseConnection();
        Print("\tDone.");
    }

    private void OnPeerConnected(int peer)
    {
        Print("New peer connected...");
        MyDebug.AddTab();
        AskSendData(peer);
        Print("Done.");
        MyDebug.RemoveTab();
    }

    private void OnPeerDisconnected(int peer)
    {

    }


    [Master]
    public void JoinAsSpectator(string name)
    {
        var peer = CustomMultiplayer.GetRpcSenderId();
        Print("Peer: " + peer + " joins as spectator.");
        this.Rpc(SpectatorAdded, peer, name);
    }

    [Master]
    public void JoinAsPlayer(string name)
    {
        var peer = CustomMultiplayer.GetRpcSenderId();
        Print("Peer: " + peer + " joins as player...");
        MyDebug.AddTab();
        Print("Looking for index...");
        MyDebug.AddTab();
        bool notFound = true;
        for (int i = 0; peer != 0 && notFound && i < _players.Length; i++)
        {
            if (_players[i] == 0)
            {
                Print("Index aviable: " + i);
                _players[i] = peer;
                _playersName[i] = name;
                this.Rpc(PlayerAdded, i, peer, name);
                if (i == _players.Length - 1)
                {
                    StartBattle();
                }
                Print("End.");
                notFound = false;
            }
        }
        if (notFound)
        {
            Print("Max players reached.");
        }
        MyDebug.RemoveTab();
        MyDebug.RemoveTab();
        Print("\tEnd.");
    }


    private void StopBattle()
    {
        _started = false;
        _playerIndex = -1;
        Print("Battle stopped.");
    }

    private void StartBattle()
    {
        Print("Starting battle....");
        _started = true;
        MyDebug.AddTab();
        NextPlayer();
        Print("Done.");
        MyDebug.RemoveTab();
    }

    private void NextPlayer()
    {
        Print("Selecting next player...");
        MyDebug.AddTab();
        _playerIndex++;
        var data = new MyPieceData(EMyPieces.RandomPiece());
        this.Rpc(SetCurrentPiece, data.ToRPCValue());
        this.Rpc(SetCurrentPlayer, _currentPlayerNetworkId);
        _time = 0;
        Print("Done.");
        MyDebug.RemoveTab();
    }

    public override void _Process(float delta)
    {
        base._Process(delta);
        if (CustomMultiplayer != null)
        {
            CustomMultiplayer.Poll();
        }
        if (_started)
        {
            if (_time > 30)
            {
                NextPlayer();
            }
            else
            {
                _time += delta;
            }
        }
    }

    [PuppetSync]
    public void SetGridValue(int i, int j, int value)
    {
        try
        {
            _circles[i, j] = value;
        }
        catch (IndexOutOfRangeException e)
        {

            throw new IndexOutOfRangeException("i:" + i + ", j:" + j, e);
        }
    }

    private void SetCircles(int i, int j, int value)
    {
        Rpc("SetGridValue", i, j, value);
    }

    private void CaptureLine(int i, int value)
    {
        for (int j = 0; j < 32; j++)
        {
            SetCircles(i, j, value);
        }
        Print("Line " + i + "Captured.");
    }

    private void CaptureColumn(int j, int value)
    {
        for (int i = 0; i < 32; i++)
        {
            SetCircles(i, j, value);
        }
        Print("Column " + j + "Captured.");
    }

    private bool IsLineComplete(int i)
    {
        for (int j = 0; j < 32; j++)
        {
            if (_circles[i, j] == 0) return false;
        }
        return true;
    }

    private bool IsColumnComplete(int j)
    {
        for (int i = 0; i < 32; i++)
        {
            if (_circles[i, j] == 0) return false;
        }
        return true;
    }

    private bool CanPlacePiece(int i, int j, bool[,] piece)
    {
        // if is inside the grid, -1 because we start at i
        // (if we click on 30 out of 32 with a size of 2, we will place the piece from 30 to 31 and not from 30 to 32)
        if (i >= 0 && j >= 0 && i + piece.GetLength(0) - 1 < _circles.GetLength(0) && j + piece.GetLength(1) - 1 < _circles.GetLength(1))
        {
            for (int i2 = 0; i2 < piece.GetLength(0); i2++)
            {
                for (int j2 = 0; j2 < piece.GetLength(1); j2++)
                {
                    if (piece[i2, j2] && _circles[i + i2, j + j2] > 0) return false;
                }
            }
            return true;
        }
        else
        {
            return false;
        }
    }

    private HashSet<int> _lineModifiedCache = new HashSet<int>();
    private HashSet<int> _columModifiedCache = new HashSet<int>();

    private void PlacePiece(int i, int j, bool[,] piece, int player)
    {
        Print("Placing piece at [" + i + ";" + j + "].");
        MyDebug.AddTab();
        for (int i2 = 0; i2 < piece.GetLength(0); i2++)
        {
            for (int j2 = 0; j2 < piece.GetLength(1); j2++)
            {
                if (piece[i2, j2])
                {
                    int l = i + i2, c = j + j2;
                    SetCircles(l, c, player);
                    if (!_columModifiedCache.Contains(c)) _columModifiedCache.Add(c);
                    if (!_lineModifiedCache.Contains(l)) _lineModifiedCache.Add(l);
                }
            }
        }
        foreach (var c in _columModifiedCache)
        {
            if (IsColumnComplete(c))
            {
                CaptureColumn(c, player);
            }
        }

        foreach (var l in _lineModifiedCache)
        {
            if (IsLineComplete(l))
            {
                CaptureLine(l, player);
            }
        }
        _columModifiedCache.Clear();
        _lineModifiedCache.Clear();
        MyDebug.RemoveTab();
    }

    private bool GridIsFull()
    {
        return _currentPlayerNetworkId == _players[_players.Length - 1] && _circles.OfType<int>().Select(i => i > 0 ? 1 : 0).Sum() > _circles.Length / 2;
    }

    [Master]
    public void Play(int i, int j, int r)
    {

        int player = CustomMultiplayer.GetRpcSenderId();
        Print("Peer: " + player + " want to play at [" + i + ";" + j + "] with " + r + " rotation...");
        MyDebug.AddTab();
        if (_started)
        {
            if (_currentPlayerNetworkId == player)
            {
                var piece = EMyPieces.Rotate(_currentPiece, r);
                if (CanPlacePiece(i, j, piece))
                {
                    PlacePiece(i, j, piece, player);
                    if (GridIsFull())
                    {
                        Print("Grid is full !");
                        MyDebug.AddTab();
                        var points = new int[_players.Length];
                        foreach (var circle in _circles)
                        {
                            if (circle != 0)
                            {
                                for (int p = 0; p < points.GetLength(0); p++)
                                {
                                    if (_players[p] == circle)
                                    {
                                        points[p]++;
                                        break;
                                    }
                                }
                            }
                        }
                        var winner = Enumerable.Range(0, points.Length).FindMax((a, b) => points[a].CompareTo(points[b]));
                        this.Rpc(PrintWinner, winner, points);
                        StopBattle();
                        MyDebug.RemoveTab();
                    }
                    else
                    {
                        NextPlayer();
                    }
                }
                else
                {
                    Print("Impossible to place the piece here.");
                }
            }
            else
            {
                Print("Not his turn.");
            }
        }
        else
        {
            Print("Battle not started.");
        }
        MyDebug.RemoveTab();
    }

    /// <summary>
    /// No effect
    /// </summary>
    /// <param name="playerId"></param>
    [PuppetSync]
    public void SetCurrentPlayer(int playerId)
    {
        Print("Next player: " + playerId);
    }

    [PuppetSync]
    public void SetCurrentPiece(byte[] buffer)
    {
        var data = new MyPieceData(buffer);
        _currentPiece = data.Piece;
    }

    /// <summary>
    /// No effect, for client
    /// </summary>
    /// <param name="currentPlayerId"></param>
    /// <param name="gridSize"></param>
    /// <param name="grid"></param>
    [Puppet]
    public void SetData(byte[] buffer)
    {

    }

    private void AskSendData(int id)
    {
        Print("Sending data to " + id + "...");
        var data = new MyGridData(_currentPlayerNetworkId, _players, _playersName, _currentPiece, _circles);
        this.RpcId(id, SetData, data.ToRPCValue());
        Print("\tDone.");
    }

    [Master]
    public void AskSendData()
    {
        Print("Data request...");
        MyDebug.AddTab();
        AskSendData(CustomMultiplayer.GetRpcSenderId());
        MyDebug.RemoveTab();
        Print("\tDone.");
    }


    /// <summary>
    /// No effect, client only
    /// </summary>
    /// <param name="peer"></param>
    [PuppetSync]
    public void PrintWinner(int index, int[] scores)
    {
        Print("Scores:");
        for (int i = 0; i < scores.Length; i++)
        {
            Print("\t" + _players[i] + ": " + scores[i]);
        }
        Print("\tWinner is : " + _players[index] + " with " + scores[index] + " points.");
    }

    /// <summary>
    /// No effect
    /// </summary>
    /// <param name="index"></param>
    /// <param name="value"></param>
    [Puppet]
    public void PlayerAdded(int index, int value, string name)
    {

    }


    /// <summary>
    /// No effect
    /// </summary>
    /// <param name="index"></param>
    /// <param name="value"></param>
    [Puppet]
    public void SpectatorAdded(int peer, string name)
    {

    }
}
