using Bricksivers.Game.Utils;
using Godot;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bricksivers.Game.Physics
{

    public interface IMyCustomPhysicData
    {
        float Mass { get; set; }

        Vector3 LinearAccel { get; }

        Vector3 LinearSpeed { get; set; }

        Vector3 LinearDecel { get; set; }

        Vector3 MaxLinearSpeed { get; set; }

        Vector3 AngularAccel { get; }

        Vector3 AngularSpeed { get; set; }

        Vector3 AngularDecel { get; set; }

        Vector3 MaxAngularSpeed { get; set; }

        Vector3 Momentum { get; set; }

        float KineticEnergy { get; }

        MyMovementFunction MovementFunction { get; set; }

        MyMovementMode MovementMode { get; set; }
    }

    public enum MyMovementMode
    {
        /// <summary>
        /// No force will be applied.
        /// </summary>
        STATIC,

        /// <summary>
        /// All force will be applied.
        /// </summary>
        RIGID,

        /// <summary>
        /// Angular force can only be applied manualy with <see cref="MyRigidKinematicBodyState.AddTorqueAccel"/> or <see cref="MyRigidKinematicBodyState.AddTorqueForce"/> or by setting the value of <see cref="MyRigidKinematicBodyState.ang"/>.
        /// </summary>
        CHARACTER
    };

    public enum MyMovementFunction
    {
        /// <summary>
        /// MoveAndSlide.
        /// </summary>
        SLIDE,

        /// <summary>
        /// MoveAndCollide.
        /// </summary>
        COLLIDE_AND_STOP,

        /// <summary>
        /// MoveAndCollide and bounce on/push object
        /// </summary>
        COLLIDE_AND_BOUNCE,
    };

    [Tool]
    public class MyRigidKinematicBodyState : Godot.Object, IMyCustomPhysicData
    {
        const int COLLISION_SAVE_DURATION_TICK = 2;

        private int _tick;

        // for lock
        private object _locker = new object();

        private float _mass = 1;
        private Vector3 _linearAccel, _linearSpeed, _linearDecel, _maxLinearSpeed = Vector3.One * short.MaxValue;
        private Vector3 _angularAccel, _angularSpeed, _angularDecel, _maxAngularSpeed = Vector3.One * short.MaxValue;

        private MyMovementFunction _movementFunc = MyMovementFunction.COLLIDE_AND_STOP;
        private MyMovementMode _movementMode = MyMovementMode.STATIC;

        private Dictionary<object, int> _collideWith = new Dictionary<object, int>();

        [Export]
        public float Mass
        {
            get {
                var tmp = 0f;
                lock (_locker)
                {
                    tmp = _mass;
                }
                return tmp;
            }
            set {
                lock (_locker)
                {
                    _mass = value;
                }
            }
        }

        public Vector3 LinearAccel
        {
            get {
                var tmp = Vector3.Zero;
                lock (_locker)
                {
                    tmp = _linearAccel;
                }
                return tmp;
            }
        }
        [Export]
        public Vector3 LinearSpeed
        {
            get {
                var tmp = Vector3.Zero;
                lock (_locker)
                {
                    tmp = _linearSpeed;
                }
                return tmp;
            }
            set {
                lock (_locker)
                {
                    _linearSpeed = value;
                }
            }
        }
        [Export]
        public Vector3 LinearDecel
        {
            get {
                var tmp = Vector3.Zero;
                lock (_locker)
                {
                    tmp = _linearDecel;
                }
                return tmp;
            }
            set {
                lock (_locker)
                {
                    _linearDecel = value;
                }
            }
        }
        [Export]
        public Vector3 MaxLinearSpeed
        {
            get {
                var tmp = Vector3.Zero;
                lock (_locker)
                {
                    tmp = _maxLinearSpeed;
                }
                return tmp;
            }
            set {
                lock (_locker)
                {
                    _maxLinearSpeed = value;
                }
            }
        }

        public Vector3 AngularAccel
        {
            get {
                var tmp = Vector3.Zero;
                lock (_locker)
                {
                    tmp = _angularAccel;
                }
                return tmp;
            }
        }
        [Export]
        public Vector3 AngularSpeed
        {
            get {
                var tmp = Vector3.Zero;
                lock (_locker)
                {
                    tmp = _angularSpeed;
                }
                return tmp;
            }
            set {
                lock (_locker)
                {
                    _angularSpeed = value;
                }
            }
        }
        [Export]
        public Vector3 AngularDecel
        {
            get {
                var tmp = Vector3.Zero;
                lock (_locker)
                {
                    tmp = _angularDecel;
                }
                return tmp;
            }
            set {
                lock (_locker)
                {
                    _angularDecel = value;
                }
            }
        }
        [Export]
        public Vector3 MaxAngularSpeed
        {
            get {
                var tmp = Vector3.Zero;
                lock (_locker)
                {
                    tmp = _maxAngularSpeed;
                }
                return tmp;
            }
            set {
                lock (_locker)
                {
                    _maxAngularSpeed = value;
                }
            }
        }

        [Export]
        public Vector3 Momentum
        {
            get {
                var tmp = Vector3.Zero;
                lock (_locker)
                {
                    tmp = _linearSpeed * _mass;
                }
                return tmp;
            }
            set {
                lock (_locker)
                {
                    _linearSpeed = value / _mass;
                }
            }
        }

        public float KineticEnergy
        {
            get {
                var tmp = 0f;
                lock (_locker)
                {
                    tmp = 0.5f * _mass * _linearSpeed.LengthSquared();
                }
                return tmp;
            }
        }

        [Export(PropertyHint.Enum)]
        public MyMovementFunction MovementFunction
        {
            get {
                var tmp = MyMovementFunction.COLLIDE_AND_STOP;
                lock (_locker)
                {
                    tmp = _movementFunc;
                }
                return tmp;
            }
            set {
                lock (_locker)
                {
                    _movementFunc = value;
                }
            }
        }

        [Export(PropertyHint.Enum)]
        public MyMovementMode MovementMode
        {
            get {
                var tmp = MyMovementMode.STATIC;
                lock (_locker)
                {
                    tmp = _movementMode;
                }
                return tmp;
            }
            set {
                lock (_locker)
                {
                    _movementMode = value;
                }
            }
        }

        public Spatial Owner { get; set; }


        public bool HasCollideWith(object collider)
        {
            bool tmp = false;
            lock (_locker)
            {
                tmp = _collideWith.ContainsKey(collider);
            }
            return tmp;
        }

        public void SaveCollision(object collider)
        {
            lock (_locker)
            {
                _collideWith.Add(collider, _tick + COLLISION_SAVE_DURATION_TICK);
            }
        }

        public void AddCentralAccel(Vector3 accel)
        {
            lock (_locker)
            {
                _linearAccel += accel;
            }
        }

        public void AddCentralForce(Vector3 force)
        {
            lock (_locker)
            {
                _linearAccel += force / _mass;
            }
        }
        public void AddCentralInpulse(Vector3 force, float delta)
        {
            AddCentralForce(force / delta);
        }

        public void AddTorqueAccel(Vector3 accel)
        {
            lock (_locker)
            {
                _angularAccel += accel;
            }
        }

        public void AddTorqueForce(Vector3 force)
        {
            lock (_locker)
            {
                _angularAccel += force / _mass;
            }
        }

        public void AddAccel(Vector3 accel, Vector3 localPoint)
        {
            float angle = Math.Min(localPoint.AngleTo(-accel), localPoint.AngleTo(accel));
            
            float distance = Mathf.Sin(angle) * localPoint.Length();
            Vector3 cross = localPoint.Cross(localPoint + accel);
            Vector3 rotAxis;
            if (cross != Vector3.Zero)
            {
                rotAxis = cross.Normalized();
            } else
            {
                rotAxis = Vector3.Zero;
                distance = 0;
            }

            lock (_locker)
            {
                _angularAccel += rotAxis * distance * accel.Length();
                _linearAccel += accel / (1 + distance);
            }
        }

        public void AddForce(Vector3 force, Vector3 localPoint)
        {
            lock (_locker)
            {
                Vector3 accel = force / _mass;
                AddAccel(accel, localPoint);
            }
        }

        public void AddInpulse(Vector3 force, Vector3 localPoint, float delta)
        {
            AddForce(force / delta, localPoint);
        }

        public void FlushForces(float delta)
        {
            lock (_locker)
            {
                _linearSpeed = MyMath.Clamp(_linearSpeed + _linearAccel * delta, -_maxLinearSpeed, _maxLinearSpeed);
                _angularSpeed = MyMath.Clamp(_angularSpeed + _angularAccel * delta, -_maxAngularSpeed, _maxAngularSpeed);

                _linearAccel = _angularAccel = Vector3.Zero;
                _tick++;
                _collideWith.RemoveIf(kvp => kvp.Value < _tick);
            }
        }

        private void Collide(Vector3 momentumTransfer, Vector3 localPoint, float delta)
        {
            if (MovementMode == MyMovementMode.RIGID)
            {
                AddInpulse(momentumTransfer, localPoint, delta);
            } else if (MovementMode == MyMovementMode.CHARACTER)
            {
                AddCentralInpulse(momentumTransfer, delta);
            }
        }

        public struct MyColliderInfo
        {
            public object Collider { get; }
            public Vector3 AngularSpeed { get; }
            public Vector3 LinearSpeed { get; }
            public float Mass { get; }

            public MyColliderInfo(object collider, Vector3 angularSpeed, Vector3 linearSpeed, float mass)
            {
                Collider = collider;
                AngularSpeed = angularSpeed;
                LinearSpeed = linearSpeed;
                Mass = mass;
            }
        }

        public void CollideWithUnknow(object collider, Vector3 globalPoint, Vector3 normal, float delta, float energyConservation = 1f)
        {
            CollideWithInfo(new MyColliderInfo(collider, Vector3.Zero, Vector3.Zero, Mass), globalPoint, normal, delta, energyConservation);
        }

        public void CollideWithRigidKine(MyRigidKinematicBodyState colliderState, Vector3 globalPoint, Vector3 normal, float delta, float energyConservation = 1f)
        {
            CollideWithInfo(new MyColliderInfo(colliderState.Owner, colliderState.AngularSpeed, colliderState.LinearSpeed, colliderState.Mass), globalPoint, normal, delta, energyConservation);
        }

        public void CollideWithRigid(RigidBody collider, Vector3 globalPoint, Vector3 normal, float delta, float energyConservation = 1f)
        {
            CollideWithInfo(new MyColliderInfo(collider, Vector3.Zero, Vector3.Zero, 0), globalPoint, normal, delta, energyConservation);
        }

        public static void Collide2RigidKine(MyRigidKinematicBodyState a, MyRigidKinematicBodyState b, Vector3 globalPoint, Vector3 normal, float delta, float energyConservation = 1f)
        {
            a.CollideWithRigidKine(b, globalPoint, normal, delta, energyConservation);
            b.CollideWithRigidKine(a, globalPoint, -normal, delta, energyConservation);
        }

        public static void CollideRigidKineRigid(MyRigidKinematicBodyState a, RigidBody b, Vector3 globalPoint, Vector3 normal, float delta, float energyConservation = 1f)
        {
            a.CollideWithRigid(b, globalPoint, normal, delta, energyConservation);
        }

        public static void CollideRigidKineUnknow(MyRigidKinematicBodyState a, object b, Vector3 globalPoint, Vector3 normal, float delta, float energyConservation = 1f)
        {
            a.CollideWithUnknow(b, globalPoint, normal, delta, energyConservation);
        }

        public void CollideWithInfo(MyColliderInfo colliderInfo, Vector3 globalPoint, Vector3 normal, float delta, float energyConservation = 1f)
        {
            if (!HasCollideWith(colliderInfo.Collider))
            {
                Vector3 localPoint = Owner.ToLocalUnRotated(globalPoint);
                Vector3 colliderLocalPoint = (colliderInfo.Collider as Spatial)?.ToLocalUnRotated(globalPoint) ?? Vector3.Zero;
               
                Vector3 colliderSpeed = colliderInfo.LinearSpeed;
                Vector3 mySpeed = LinearSpeed;

                if (colliderInfo.AngularSpeed != Vector3.Zero)
                {
                    Vector3 colliderCross = colliderInfo.AngularSpeed.Cross(colliderLocalPoint);
                    if (colliderCross != Vector3.Zero)
                    {
                        Vector3 colliderRotSpeed = (colliderCross != Vector3.Zero ? colliderCross.Normalized() : Vector3.Zero) * colliderInfo.AngularSpeed.Length() * ((Vector3.One - colliderInfo.AngularSpeed.Normalized()) * colliderLocalPoint).Length();
                        //colliderSpeed += colliderRotSpeed;
                    }
                }

                if (AngularSpeed != Vector3.Zero)
                {
                    Vector3 cross = AngularSpeed.Cross(localPoint);
                    if (cross != Vector3.Zero)
                    {
                        Vector3 rotSpeed = (cross != Vector3.Zero ? cross.Normalized() : Vector3.Zero) * AngularSpeed.Length() * ((Vector3.One - AngularSpeed.Normalized()) * localPoint).Length();
                        //mySpeed += rotSpeed;
                    }

                }
                GD.Print(globalPoint);
                float invMass = 1f / (Mass + colliderInfo.Mass);
                Vector3 speed = (Mass - colliderInfo.Mass) * invMass * mySpeed + 2 * colliderInfo.Mass * invMass * colliderSpeed;
                Vector3 deltaSpeed = speed - LinearSpeed;
                Collide(deltaSpeed.Abs() * normal * energyConservation, localPoint, delta);
                SaveCollision(colliderInfo.Collider);
            }
        }

        private float Damp(float speed, float decel, float delta)
        {
            if (speed > 0)
            {
                return Math.Max(0, speed - decel * delta);
            } else
            {
                return Math.Min(0, speed + decel * delta);
            }
        }

        private Vector3 Damp(Vector3 speed, Vector3 decel, float delta)
        {
            return new Vector3(Damp(speed.x, decel.x, delta), Damp(speed.y, decel.y, delta), Damp(speed.z, decel.z, delta));
        }

        public void DampSpeeds(float delta)
        {
            lock (_locker)
            {
                _linearSpeed = Damp(_linearSpeed, _linearDecel, delta);
                _angularSpeed = Damp(_angularSpeed, _angularDecel, delta);
            }
        }
    }
}
