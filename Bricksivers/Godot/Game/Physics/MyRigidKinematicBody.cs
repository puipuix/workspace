using Bricksivers.Game.Physics;
using Bricksivers.Game.Utils;
using Godot;
using Godot.Collections;
using System.Linq;

[Tool]
public class MyRigidKinematicBody : KinematicBody, IMyCustomPhysicData
{
	#region State
	public MyRigidKinematicBodyState State { get; private set; } = new MyRigidKinematicBodyState();

	public Vector3 LinearAccel => State.LinearAccel;

	[MyExport("Linear/LinearSpeed", Variant.Type.Vector3)]
	public Vector3 LinearSpeed { get => State.LinearSpeed; set => State.LinearSpeed = value; }

	[MyExport("Linear/LinearDecel", Variant.Type.Vector3)]
	public Vector3 LinearDecel { get => State.LinearDecel; set => State.LinearDecel = value; }

	[MyExport("Linear/MaxLinearSpeed", Variant.Type.Vector3)]
	public Vector3 MaxLinearSpeed { get => State.MaxLinearSpeed; set => State.MaxLinearSpeed = value; }


	public Vector3 AngularAccel => State.AngularAccel;

	[MyExport("Angular/AngularSpeed", Variant.Type.Vector3)]
	public Vector3 AngularSpeed { get => State.AngularSpeed; set => State.AngularSpeed = value; }

	[MyExport("Angular/AngularDecel", Variant.Type.Vector3)]
	public Vector3 AngularDecel { get => State.AngularDecel; set => State.AngularDecel = value; }

	[MyExport("Angular/MaxAngularSpeed", Variant.Type.Vector3)]
	public Vector3 MaxAngularSpeed { get => State.MaxAngularSpeed; set => State.MaxAngularSpeed = value; }

	public Vector3 Momentum { get => State.Momentum; set => State.Momentum = value; }

	public float KineticEnergy => State.KineticEnergy;


	[MyExport("Data/Mass", Variant.Type.Real)]
	public float Mass { get => State.Mass; set => State.Mass = value; }

	[MyExport("Data/MovementFunction", Variant.Type.String, PropertyHint.Enum, "auto", enumType: typeof(MyMovementFunction))]
	public MyMovementFunction MovementFunction { get => State.MovementFunction; set => State.MovementFunction = value; }

	[MyExport("Data/MovementMode", Variant.Type.String, PropertyHint.Enum, "auto", enumType: typeof(MyMovementMode))]
	public MyMovementMode MovementMode { get => State.MovementMode; set => State.MovementMode = value; }

	#endregion

	public MyRigidKinematicBody()
	{
		State.Owner = this;
	}

	public override void _Ready()
	{
		base._Ready();
		MyPhysicalEngine.AddBody(this);
		GD.PrintS(MovementMode, MovementFunction);
	}

	public override object _Get(string property)
	{
		if (MyExportAttribute.Get(this, property, out object ret))
		{
			return ret;
		} else
		{
			return base._Get(property);
		}
	}

	public override bool _Set(string property, object value)
	{
		if (MyExportAttribute.Set(this, property, value))
		{
			return true;
		} else
		{
			return base._Set(property, value);
		}
	}

	public override Array _GetPropertyList()
	{
		return MyExportAttribute.GetPropertyList(this, base._GetPropertyList());
	}

	public virtual void AddCustomForces(MyRigidKinematicBodyState state, float delta)
	{
		state.FlushForces(delta);
	}

	public virtual void IntegrateForces(MyRigidKinematicBodyState state, float delta)
	{
		if (state.MovementMode != MyMovementMode.STATIC)
		{
			Transform = new Transform(Transform.basis.Rotate(state.AngularSpeed * delta), Transform.origin);
			if (state.MovementFunction == MyMovementFunction.COLLIDE_AND_STOP)
			{
				MoveAndCollide(state.LinearSpeed * delta, false);
			} else if (state.MovementFunction == MyMovementFunction.COLLIDE_AND_BOUNCE)
			{
				if (MoveAndCollide(state.LinearSpeed * delta, true) is KinematicCollision collision)
				{
					if (collision.Collider is MyRigidKinematicBody body)
					{
						MyRigidKinematicBodyState.Collide2RigidKine(state, body.State, collision.Position, collision.Normal, delta, 1);
					} else if (collision.Collider is RigidBody rigidBody)
					{
						MyRigidKinematicBodyState.CollideRigidKineRigid(state, rigidBody, collision.Position, collision.Normal, delta, 1);
					} else
					{
						MyRigidKinematicBodyState.CollideRigidKineUnknow(state, collision.Collider, collision.Position, collision.Normal, delta, 1);
					}
				}
			} else if (state.MovementFunction == MyMovementFunction.SLIDE)
			{
				MoveAndSlide(state.LinearSpeed);
			}
			state.DampSpeeds(delta);
		}
	}

	protected override void Dispose(bool disposing)
	{
		base.Dispose(disposing);
		if (disposing)
		{
			//State.Free();
		}
	}

}
