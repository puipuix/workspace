using Godot;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bricksivers.Game.Physics
{
	public class MyPhysicalEngine : Spatial
	{
		private static WeakReference<MyPhysicalEngine> _singleton = new WeakReference<MyPhysicalEngine>(null);

		private static HashSet<WeakReference<MyRigidKinematicBody>> _bodies = new HashSet<WeakReference<MyRigidKinematicBody>>();
		private static Queue<WeakReference<MyRigidKinematicBody>> _toRemove = new Queue<WeakReference<MyRigidKinematicBody>>();
		private static Queue<WeakReference<MyRigidKinematicBody>> _toAdd = new Queue<WeakReference<MyRigidKinematicBody>>();
		public override void _Ready()
		{
			base._Ready();
			_singleton.SetTarget(this);
		}

		public static void AddBody(MyRigidKinematicBody body)
		{
			_toAdd.Enqueue(new WeakReference<MyRigidKinematicBody>(body));
		}

		public override void _PhysicsProcess(float delta)
		{
			base._PhysicsProcess(delta);

			while (_toAdd.Count > 0)
			{
				_bodies.Add(_toAdd.Dequeue());
			}

			foreach (var weak in _bodies)
			{
				if (weak.TryGetTarget(out MyRigidKinematicBody kinematic) && IsInstanceValid(kinematic))
				{
					kinematic.AddCustomForces(kinematic.State, delta);
				} else
				{
					_toRemove.Enqueue(weak);
				}
			}

			while( _toRemove.Count > 0)
			{
				_bodies.Remove(_toRemove.Dequeue());
			}

			foreach (var weak in _bodies)
			{
				if (weak.TryGetTarget(out MyRigidKinematicBody kinematic) && IsInstanceValid(kinematic))
				{
					kinematic.IntegrateForces(kinematic.State, delta);
				}
			}
		}
	}
}
