using Bricksivers.Game.Utils;
using Bricksivers.Game.World;
using Bricksivers.Game.World.Generator;
using static Bricksivers.Game.Utils.MyConst;
using Godot;
using System;
using Bricksivers.Game.Utils.Shapes;

public class MySpaceBodyDisplay : StaticBody
{
	public MeshInstance VisualMesh { get; private set; }
	public CollisionShape CollisionShape { get; private set; }

	public MySpaceBodyGenerator Generator { get; set; }
	public PackedScene PackedChunkDisplay { get; set; }


	public MyChunkDisplay.MyChunkUpdateManager ChunkManager { get; set; }

	private MyChunkDisplay[] _chunks = new MyChunkDisplay[18];
	private Area[] _gravityFields = new Area[18];
	private MyGravityHandler[] _gravityHandlers = new MyGravityHandler[18];

	public void SetParams(MySpaceBodyGenerator generator, PackedScene packedChunkDisplay, MyChunkDisplay.MyChunkUpdateManager chunkManager = null)
	{
		Generator = generator;
		PackedChunkDisplay = packedChunkDisplay;
		if (chunkManager is null)
		{
			ChunkManager = new MyChunkDisplay.MyChunkUpdateManager();
			AddChild(ChunkManager);
		} else
		{
			ChunkManager = chunkManager;
		}

	}

	public override void _Ready()
	{
		if (Generator.SurfaceSize % SLOT_SIZE != 0)
		{
			throw new ArgumentException("Invalid surface size: surface (" + Generator.SurfaceSize + ") % " + SLOT_SIZE + " = " + Generator.SurfaceSize % MyConst.SLOT_SIZE + "!= 0");
		}
		CollisionShape = GetNode<CollisionShape>("Hitbox");
		VisualMesh = GetNode<MeshInstance>("VisualMesh");
		SpatialMaterial mat = new SpatialMaterial();
		VisualMesh.SetSurfaceMaterial(0, mat);
		CollisionShape.Scale = VisualMesh.Scale = Vector3.One * Generator.SurfaceSize * PIECE_SIZE_XZ;
		mat.AlbedoColor = Generator.Color.BaseColor;

		float shift = Generator.SurfaceSize * PIECE_SIZE_XZ * 0.5f;
		float dist_f = DISTANCE_FLAT * Generator.SurfaceSize * PIECE_SIZE_XZ;

		#region Face

		MakeChunk(EMySurfaceIndex.Top, new Vector3(-shift, dist_f, -shift), dist_f);
		MakeChunk(EMySurfaceIndex.Right, new Vector3(dist_f, shift, -shift), dist_f);
		MakeChunk(EMySurfaceIndex.Bottom, new Vector3(shift, -dist_f, -shift), dist_f);
		MakeChunk(EMySurfaceIndex.Left, new Vector3(-dist_f, -shift, -shift), dist_f);
		MakeChunk(EMySurfaceIndex.Front, new Vector3(-shift, shift, dist_f), dist_f);
		MakeChunk(EMySurfaceIndex.Back, new Vector3(-shift, -shift, -dist_f), dist_f);

		#endregion

		#region Z axis

		MakeChunk(EMySurfaceIndex.TopRight, new Vector3(shift, dist_f, -shift), dist_f);
		MakeChunk(EMySurfaceIndex.BottomRight, new Vector3(dist_f, -shift, -shift), dist_f);
		MakeChunk(EMySurfaceIndex.BottomLeft, new Vector3(-shift, -dist_f, -shift), dist_f);
		MakeChunk(EMySurfaceIndex.TopLeft, new Vector3(-dist_f, shift, -shift), dist_f);

		#endregion

		#region Y axis

		MakeChunk(EMySurfaceIndex.BackLeft, new Vector3(shift, -shift, -dist_f), dist_f);
		MakeChunk(EMySurfaceIndex.FrontLeft, new Vector3(dist_f, -shift, shift), dist_f);
		MakeChunk(EMySurfaceIndex.FrontRight, new Vector3(-shift, -shift, dist_f), dist_f);
		MakeChunk(EMySurfaceIndex.BackRight, new Vector3(-dist_f, -shift, -shift), dist_f);

		#endregion

		#region X axis

		MakeChunk(EMySurfaceIndex.BackTop, new Vector3(-shift, shift, -dist_f), dist_f);
		MakeChunk(EMySurfaceIndex.BackBottom, new Vector3(-shift, -dist_f, -shift), dist_f);
		MakeChunk(EMySurfaceIndex.FrontBottom, new Vector3(-shift, -shift, dist_f), dist_f);
		MakeChunk(EMySurfaceIndex.FrontTop, new Vector3(-shift, dist_f, shift), dist_f);

		#endregion

	}

	private class MyGravityHandler : Godot.Object
	{
		Area _area;

		public MyGravityHandler(Area area)
		{
			_area = area;
			area.Connect("body_entered", this, nameof(OnPlayerEnter));
			area.Connect("body_exited", this, nameof(OnPlayerExit));
		}

		private void OnPlayerEnter(Node body)
		{
			if(body is MyCharacter character)
			{
				GD.Print(_area.GravityVec);
				character.Gravity += _area.GravityVec * 9.8f;
			}
		}

		private void OnPlayerExit(Node body)
		{
			if (body is MyCharacter character)
			{
				character.Gravity -= _area.GravityVec * 9.8f;
			}
		}
	}

	private void MakeChunk(EMySurfaceIndex index, Vector3 translation, float dist_f)
	{

		MyChunkDisplay chunkDisplay = (MyChunkDisplay)PackedChunkDisplay.Instance();
		chunkDisplay.Rotation = index.Rotation;
		chunkDisplay.Translation = translation;
		chunkDisplay.SetParams(Generator, PackedChunkDisplay, ChunkManager, index, SLOT_SIZE, 3, null);
		AddChild(chunkDisplay);
		Area area = new Area();
		MyGravityHandler handler = new MyGravityHandler(area);
		area.Rotation = index.Rotation;
		area.Translation = index.Rotate(new Vector3(0, dist_f, 0));
		area.GravityPoint = false;
		area.GravityVec = -area.Translation.Normalized();
		area.SpaceOverride = Area.SpaceOverrideEnum.Disabled;
		area.SetCollisionLayerBit(0, false);
		area.SetCollisionLayerBit(COLLISION_LAYER_TRASH, true);
		area.SetCollisionMaskBit(0, false);
		area.SetCollisionMaskBit(COLLISION_LAYER_LOCAL_PLAYER, true);

		
		CollisionShape collision = new CollisionShape();
		float bot = Generator.SurfaceSize * PIECE_SIZE_XZ / 2f;
		float top = bot + Mathf.Tan(rad45 / 2) * Generator.SurfaceSize * PIECE_SIZE_XZ;
		collision.Shape = new MyTrapezeShape(new Vector3(top, Generator.SurfaceSize * PIECE_SIZE_XZ, top), new Vector3(bot, 0, bot));
		area.AddChild(collision);
		AddChild(area);

		_chunks[index] = chunkDisplay;
		_gravityFields[index] = area;
		_gravityHandlers[index] = handler;
	}

	protected override void Dispose(bool disposing)
	{
		base.Dispose(disposing);
		if (disposing)
		{
			for (int i = 0; i < _gravityHandlers.Length; i++)
			{
				_gravityHandlers[i].Free();
			}
		}
	}
}

