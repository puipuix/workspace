using Bricksivers.Game.Utils;
using Bricksivers.Game.World;
using Bricksivers.Game.World.Generator;
using Godot;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Threading;

public class MyChunkDisplay : StaticBody
{
	public class MyChunkUpdateManager : Node
	{
		public int ChunkUpdatePerTick { get; set; } = 10;
		public int DividePerTick { get; set; } = 5;

		public int Pending
		{
			get {
				int pending = 0;
				lock (locker)
				{
					pending = _chunkUpdates.Count;
				}
				return pending;
			}
		}

		private readonly object locker = new object();
		private Queue<(MyChunkDisplay child, float[] surface, float[] underground, float[] heightmap, MyVector2I size, MyVector2I min)> _chunkUpdates = new Queue<(MyChunkDisplay child, float[] surface, float[] underground, float[] heightmap, MyVector2I size, MyVector2I min)>();
		public MyChunkUpdateManager() { }

		public void AddToQueue(MyChunkDisplay child, float[] surface, float[] underground, float[] heightmap, MyVector2I size, MyVector2I min)
		{
			lock (locker)
			{
				_chunkUpdates.Enqueue((child, surface, underground, heightmap, size, min));
			}
		}

		public override void _Process(float delta)
		{
			base._Process(delta);
			lock (locker)
			{
				for (int i = 0; i < ChunkUpdatePerTick && !_chunkUpdates.IsEmptyCollection(); i++)
				{
					(MyChunkDisplay child, float[] surface, float[] underground, float[] heightmap, MyVector2I size, MyVector2I min) = _chunkUpdates.Dequeue();

					if (IsInstanceValid(child))
					{
						child.SetAsBulkArray(surface, underground, heightmap, size, min);
					}
				}
			}
		}
	}



	const float PLAYER_DETECTION_SIZE_FACTOR = 2f;
	const float PLAYER_DETECTION_HEIGHT_FACTOR = 1.5f;
	const float PLAYER_DETECTION_HEIGHT_PERCENT_ABOVEGROUND = 0.8f;

	public Area SubdivisionZone { get; private set; }
	public MultiMesh UnderGroundMesh { get; private set; }
	public MultiMesh SurfaceMesh { get; private set; }
	public CollisionShape HitBox { get; private set; }
	public bool IsDivided { get; private set; }

	public PackedScene InstanceGenerator { get; set; }
	public IMySurfaceGenerator Generator { get; set; }
	public MyChunkUpdateManager ChunkManager { get; set; }
	public int LOD { get; set; }
	public EMySurfaceIndex SurfaceIndex { get; set; }
	public MyVector2I Origin { get; set; }

	/// <summary>
	/// Number of LOD x piece
	/// </summary>
	public int ChunkSize { get; set; }
	public int SubDivision { get; set; }

	private int _childReady = -1;
	private MyChunkDisplay _parent;

	private MyChunkDisplay[] _childs;
	public MyChunkDisplay()
	{
	}

	public void SetParams(MySpaceBodyGenerator generator, PackedScene instanceGenerator, MyChunkUpdateManager chunkManager, EMySurfaceIndex surfaceIndex, int chunkSize = 20, int subDivision = 3, int? lod = null, MyVector2I? origin = null)
	{
		Generator = generator;
		InstanceGenerator = instanceGenerator;
		ChunkManager = chunkManager;
		SurfaceIndex = surfaceIndex;
		ChunkSize = chunkSize;
		SubDivision = subDivision;
		LOD = lod ?? LODToFit(generator.SurfaceSize, ChunkSize, SubDivision);
		Origin = origin ?? MyVector2I.Zero;
	}

	public static int LODToFit(int surfaceSize, int chunkSize, int subDivision)
	{
		int lod = 1;
		while (lod * chunkSize < surfaceSize)
		{
			lod *= subDivision;
		}
		return lod;
	}

	public void AutoLOD(int surfaceToFit)
	{
		LOD = LODToFit(surfaceToFit, ChunkSize, SubDivision);
	}

	private void SetFromParent(MyChunkDisplay parent, MyVector2I origin)
	{
		InstanceGenerator = parent.InstanceGenerator;
		ChunkSize = parent.ChunkSize;
		SubDivision = parent.SubDivision;
		SurfaceIndex = parent.SurfaceIndex;
		Generator = parent.Generator;
		Origin = origin;
		LOD = parent.LOD / parent.SubDivision;
		ChunkManager = parent.ChunkManager;
		_parent = parent;
	}

	public override void _Ready()
	{
		if (LOD != 1 && LOD % SubDivision != 0)
		{
			throw new ArgumentException(LOD + " isn't a multiple of " + SubDivision);
		}
		UnderGroundMesh = new MultiMesh {
			Mesh = GD.Load<Mesh>("res://Content/Models/Pieces/terrain_underground.obj"),
			ColorFormat = MultiMesh.ColorFormatEnum.Color8bit,
			TransformFormat = MultiMesh.TransformFormatEnum.Transform3d,
			CustomDataFormat = MultiMesh.CustomDataFormatEnum.None,
		};
		SurfaceMesh = new MultiMesh {
			Mesh = GD.Load<Mesh>("res://Content/Models/Pieces/p_1x1x1_dot.obj"),
			ColorFormat = MultiMesh.ColorFormatEnum.Color8bit,
			TransformFormat = MultiMesh.TransformFormatEnum.Transform3d,
			CustomDataFormat = MultiMesh.CustomDataFormatEnum.None,
		};
		if (UnderGroundMesh.Mesh.SurfaceGetMaterial(0) is SpatialMaterial mat)
		{
			mat.VertexColorUseAsAlbedo = true;
		}
		if (SurfaceMesh.Mesh.SurfaceGetMaterial(0) is SpatialMaterial mat2)
		{
			mat2.VertexColorUseAsAlbedo = true;
		}
		GetNode<MultiMeshInstance>("MultiMeshUnderground").Multimesh = UnderGroundMesh;
		GetNode<MultiMeshInstance>("MultiMeshSurface").Multimesh = SurfaceMesh;
		HitBox = GetNode<CollisionShape>("HitBox");
		SubdivisionZone = GetNode<Area>("SubdivisionZone");
		BoxShape boxShape = new BoxShape();
		SubdivisionZone.GetNode<CollisionShape>("CollisionShape").Shape = boxShape;
		float extentXZ = ChunkSize / 2 * PLAYER_DETECTION_SIZE_FACTOR * LOD;
		float _extentY = Math.Max(Generator.GetMaxHeight(), 1) * (float)Math.Log(LOD, SubDivision) * PLAYER_DETECTION_HEIGHT_FACTOR;
		boxShape.Extents = new Vector3(extentXZ, _extentY, extentXZ) * MyConst.PIECE_SIZE_XYZ;
		SubdivisionZone.Connect("body_entered", this, nameof(OnPlayerEnter));
		SubdivisionZone.Connect("body_exited", this, nameof(OnPlayerExit));

		SubdivisionZone.Translation = new Vector3(Origin.x + ChunkSize / 2 * LOD,
			_extentY * PLAYER_DETECTION_HEIGHT_PERCENT_ABOVEGROUND,
			Origin.y + ChunkSize / 2 * LOD) * MyConst.PIECE_SIZE_XYZ;

		HitBox.Shape = new HeightMapShape();
		float coef = MyConst.PIECE_SIZE_XZ * LOD * (ChunkSize + 1f) / ChunkSize;
		HitBox.Scale = new Vector3(coef, 1, coef);
		ThreadPool.QueueUserWorkItem(Load);
	}

	private void Load(object threadContext)
	{
		float[] fullHeightMap = new float[ChunkSize * ChunkSize];
		float[] surface = new float[fullHeightMap.Length * 13]; // 12 for transform 1 for color
		float[] underground = new float[fullHeightMap.Length * 13 * 2];
		MyVector2I min = MyVector2I.One * int.MaxValue, max = MyVector2I.One * int.MinValue;
		for (int x = 0; x < ChunkSize; x++)
		{
			for (int z = 0; z < ChunkSize; z++)
			{
				int index = MyMath.From2dTo1d(new MyVector2I(x, z), ChunkSize);
				MyVector2I coor = new MyVector2I(Origin.x + x * LOD, Origin.y + z * LOD);
				MyChunkBlockData data = Generator.GetData(SurfaceIndex, coor.x, coor.y);
				if (data.IsEmpty)
				{
					Transform tr = new Transform(new Basis().Scaled(Vector3.One * 0.01f), new Vector3(0, -1, 0));
					WriteInto(surface, index, tr, Colors.White);
					WriteInto(underground, index * 2, tr, Colors.White);
					WriteInto(underground, index * 2 + 1, tr, Colors.White);
					fullHeightMap[index] = -1;
				} else
				{
					min.x = Math.Min(x, min.x);
					min.y = Math.Min(z, min.y);
					max.x = Math.Max(x, max.x);
					max.y = Math.Max(z, max.y);
					Vector3 surfacePos = new Vector3(coor.x, data.HeightInt - LOD * 0.5f, coor.y) * MyConst.PIECE_SIZE_XYZ;
					Vector3 subsurfacePos = surfacePos - new Vector3(0, LOD * MyConst.PIECE_SIZE_Y / 2, 0);
					Vector3 undergroundPos = subsurfacePos - new Vector3(0, Math.Max(1, data.SubSurfaceHeight) * MyConst.PIECE_SIZE_Y, 0);
					WriteInto(surface, index, new Transform(Basis.Identity.Scaled(new Vector3(LOD, LOD, LOD)), surfacePos), data.SurfaceColor);
					WriteInto(underground, index * 2, new Transform(Basis.Identity.Scaled(new Vector3(LOD, Math.Max(1, data.SubSurfaceHeight), LOD)), subsurfacePos), data.SubSurfaceColor);
					WriteInto(underground, index * 2 + 1, new Transform(Basis.Identity.Scaled(new Vector3(LOD, Math.Max(1, data.HeightInt - data.SubSurfaceHeight - 1), LOD)), undergroundPos), data.UndergroundColor);
					fullHeightMap[index] = (data.HeightFloat + LOD * 0.5f) * MyConst.PIECE_SIZE_Y;
				}
			}
		}

		MyVector2I size;
		float[] heightMap;
		if (max.x >= 0)
		{
			size = max - min + MyVector2I.One;
			heightMap = new float[size.x * size.y];
			int i = -1, i2 = -1;
			for (i = 0; i < fullHeightMap.Length; i++)
			{
				if (fullHeightMap[i] >= 0)
				{
					i2 = MyMath.From2dTo1d(MyMath.From1dTo2d(i, ChunkSize), size.x);
					heightMap[i2] = fullHeightMap[i];
				}
			}
		} else
		{
			min.x = min.y = 0;
			size = MyVector2I.Zero;
			heightMap = new float[0];
		}



		ChunkManager.AddToQueue(this, surface, underground, heightMap, size, min);
	}

	private void WriteInto(float[] array, int index, Transform tr, Color c)
	{
		int i = 0;
		index *= 13;
		try
		{
			// X
			array[index + i++] = tr.basis[0].x;
			array[index + i++] = tr.basis[0].y;
			array[index + i++] = tr.basis[0].z;
			array[index + i++] = tr.origin.x;

			// Y
			array[index + i++] = tr.basis[1].x;
			array[index + i++] = tr.basis[1].y;
			array[index + i++] = tr.basis[1].z;
			array[index + i++] = tr.origin.y;

			// Z
			array[index + i++] = tr.basis[2].x;
			array[index + i++] = tr.basis[2].y;
			array[index + i++] = tr.basis[2].z;
			array[index + i++] = tr.origin.z;

			// Color
			array[index + i++] = BitConverter.ToSingle(MyTools.ToArray((byte)c.r8, (byte)c.g8, (byte)c.b8, (byte)c.a8), 0);
		} catch (Exception)
		{
			GD.PrintS(index, array.Length);
			throw;
		}

	}

	private void SetAsBulkArray(float[] surface, float[] underground, float[] heightMap, MyVector2I size, MyVector2I min)
	{
		UnderGroundMesh.InstanceCount = 2 * (SurfaceMesh.InstanceCount = ChunkSize * ChunkSize);
		SurfaceMesh.SetAsBulkArray(surface);
		UnderGroundMesh.SetAsBulkArray(underground);
		if (HitBox.Shape is HeightMapShape map)
		{
			map.MapDepth = size.x;
			map.MapWidth = size.y;
			map.MapData = heightMap;
			if (heightMap.Length > 0)
			{
				HitBox.Translation = (new Vector3(size.x * 0.5f + min.x - 0.5f, 0, size.y * 0.5f + min.y - 0.5f) * LOD + new Vector3(Origin.x, 0, Origin.y)) * MyConst.PIECE_SIZE_XZ;
			} else
			{
				HitBox.Disable();
			}
		}
		if (_parent is MyChunkDisplay parent)
		{
			parent._childReady++;
		}
	}
	private void OnPlayerEnter(object body)
	{
		SubDivide();
	}

	private void OnPlayerExit(object body)
	{
		UnDivide();
	}

	private void SubDivide()
	{
		if (!IsDivided && LOD > 1)
		{
			IsDivided = true;
			_childs = new MyChunkDisplay[SubDivision * SubDivision];
			_childReady = 0;
			int nextLOD = LOD / SubDivision;
			for (int i = 0; i < SubDivision; i++)
			{
				for (int j = 0; j < SubDivision; j++)
				{
					Vector2 origin = Origin + new Vector2(i, j) * nextLOD * ChunkSize;
					MyChunkDisplay child = (MyChunkDisplay)InstanceGenerator.Instance();
					child.SetFromParent(this, origin.ToMyVector2I());
					AddChild(child);
					_childs[MyMath.From2dTo1d(new MyVector2I(i, j), SubDivision)] = child;
				}
			}
		}
	}

	private void UnDivide()
	{
		if (LOD > 1 && IsDivided)
		{
			_childReady = -1;
			for (int i = 0; i < _childs.Length; i++)
			{
				_childs[i].Unload();
				_childs[i] = null;
			}
			_childs = null;
			IsDivided = false;
		}
	}

	public void Unload()
	{
		FadeOut();
		GetParentOrNull<Node>()?.CallDeferred(nameof(RemoveChild), this);
		QueueFree();
	}

	public void FadeOut()
	{
		SurfaceMesh.VisibleInstanceCount = 0;
		UnderGroundMesh.VisibleInstanceCount = 0;
		HitBox.Disable();
	}

	public void FadeIn()
	{
		SurfaceMesh.VisibleInstanceCount = -1;
		UnderGroundMesh.VisibleInstanceCount = -1;
		HitBox.Enable();
	}

	public override void _Process(float delta)
	{
		base._Process(delta);
		if (_childs == null)
		{
			if (SurfaceMesh.VisibleInstanceCount == 0)
			{
				FadeIn();
			}
		} else if (_childReady == _childs.Length)
		{
			if (SurfaceMesh.VisibleInstanceCount == -1)
			{
				FadeOut();
			}
		} else
		{
			if (SurfaceMesh.VisibleInstanceCount == 0)
			{
				FadeIn();
			}
		}
	}
}
