﻿using Bricksivers.Game.Utils;
using Godot;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Bricksivers.Game.World.Generator
{
    public struct MySpaceBodyColorData
    {
        public Color SurfaceAColor { get; set; }
        public Color SurfaceBColor { get; set; }
        public Color SubSurfaceColor { get; set; }
        public Color UndergroundColor { get; set; }

        public MySpaceBodyColorData(Color surfaceA, Color surfaceB, Color subSurface, Color underground)
        {
            SurfaceAColor = surfaceA;
            SurfaceBColor = surfaceB;
            SubSurfaceColor = subSurface;
            UndergroundColor = underground;
        }

        public static MySpaceBodyColorData Avg(MySpaceBodyColorData?[] datas, float[] coefs)
        {
            Color sa, sb, ss, ug;
            sa = sb = ss = ug = Colors.Black;
            for (int i = 0; i < datas.Length; i++)
            {
                if (datas[i] is MySpaceBodyColorData c)
                {
                    sa += c.SurfaceAColor * coefs[i];
                    sb += c.SurfaceBColor * coefs[i];
                    ss += c.SubSurfaceColor * coefs[i];
                    ug += c.UndergroundColor * coefs[i];
                }
            }
            return new MySpaceBodyColorData(sa, sb, ss, ug);
        }
    }
    public class MySpaceBodyColor
    {
        public Color BaseColor { get; }
        public Image SurfaceA { get; }
        public Image SurfaceB { get; }
        public Image SubSurface { get; }
        public Image Underground { get; }

        public string Name { get; }

        public float TemperatureMin { get; }
        public float TemperatureMax { get; }

        public float HumidityMin { get; }
        public float HumidityMax { get; }


        public MySpaceBodyColor(Color baseColor, Image surfaceA, Image surfaceB, Image subSurface, Image underground, string name = "Unknow", float temperatureMin = 0, float temperatureMax = 1, float humidityMin = 0, float humidityMax = 1)
        {
            BaseColor = baseColor;
            SurfaceA = surfaceA;
            SurfaceB = surfaceB;
            SubSurface = subSurface;
            Underground = underground;
            Name = name;
            TemperatureMin = temperatureMin;
            TemperatureMax = temperatureMax;
            HumidityMin = humidityMin;
            HumidityMax = humidityMax;
        }

        private Color GetColor(Image image, Vector2 pcTempHum)
        {
            MyVector2I size = image.GetSize().ToMyVector2I();
            Vector2 coef = size * pcTempHum; // tmp -> we save the float coordinate

            MyVector2I min = coef.ToMyVector2I(); // lower pixel
            MyVector2I max = new MyVector2I(Math.Min(min.x + 1, size.x - 1), Math.Min(min.y + 1, size.y - 1)); // upper pixel
            coef -= min; // linear interpolation from min to the float coordinate

            Color c0 = image.GetPixel(min.x, min.y).LinearInterpolate(image.GetPixel(max.x, min.y), coef.x);
            Color c1 = image.GetPixel(min.x, max.y).LinearInterpolate(image.GetPixel(max.x, max.y), coef.x);
            return c0.LinearInterpolate(c1, coef.y);
        }

        public MySpaceBodyColorData GetColors(float temperature, float humidity)
        {
            Vector2 distTh = new Vector2(TemperatureMax - TemperatureMin, HumidityMax - HumidityMin);
            Vector2 th = new Vector2(Mathf.Clamp(temperature - TemperatureMin, 0, distTh.x), Mathf.Clamp(humidity - HumidityMin, 0, distTh.y));
            Vector2 pcTempHum = new Vector2(th / distTh) * 0.999f; // prevent get x or y equals 1
            //GD.PrintS(distTh, temperature, th, pcTempHum);
            return new MySpaceBodyColorData(GetColor(SurfaceA, pcTempHum), GetColor(SurfaceB, pcTempHum), GetColor(SubSurface, pcTempHum), GetColor(Underground, pcTempHum));
        }

        public static Image Load(string name)
        {
            Image img = GD.Load<StreamTexture>("res://Content/Textures/SpaceBodyColors/" + name).GetData();
            img.Lock();
            return img;
        }

        public static readonly MySpaceBodyColor DEBUG = new MySpaceBodyColor(Colors.Magenta, Load("debug_ab.png"), Load("debug_ab.png"), Load("dirt.png"), Load("stone.png"), "DEBUG", 0, 1000);
        public static readonly MySpaceBodyColor ASTEROID = new MySpaceBodyColor(Colors.Gray, Load("asteroid.png"), Load("asteroid.png"), Load("asteroid.png"), Load("asteroid.png"), "Asteroid");
        public static readonly MySpaceBodyColor FLORAL_GREEN = new MySpaceBodyColor(Colors.DimGray, Load("floral_green_a.png"), Load("floral_green_b.png"), Load("floral_green_s.png"), Load("stone.png"), "Floral Green", MySpaceMath.ToKalvin(-20), MySpaceMath.ToKalvin(40));
        public static readonly MySpaceBodyColor BLUESTAR = new MySpaceBodyColor(new Color(0, 0, 0.3f), Load("blue.png"), Load("black.png"), Load("blue.png"), Load("blue.png"), "Blue Star");
    }
}
