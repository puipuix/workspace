﻿using Bricksivers.Game.Utils;
using Godot;
using Godot.Collections;
using static Bricksivers.Game.Utils.MyConst;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;
using Bricksivers.Game.Utils.Noises;

namespace Bricksivers.Game.World.Generator
{


    public class MySpaceBodyGenerator : IMySurfaceGenerator
    {
        const float SURFACE_FACTOR = 100f;
        public float DistanceFromBorderToFullHeightCoef { get; set; } = 0.25f;
        public float Gravity { get; set; } = 0f;
        public float Temperature { get; set; } = 0f;
        public MySpaceBodyColor Color { get; set; } = MySpaceBodyColor.DEBUG;
        public MyNoiseGetter SurfaceColorVariationNoise { get; set; } = new MyNoiseGetter(new MyConstantNoise() { Constant = 1f });
        public MyNoiseGetter HumidityNoise { get; set; } = new MyNoiseGetter(new MyConstantNoise() { Constant = 0.5f });
        public MyNoiseGetter TemperatureNoise { get; set; } = new MyNoiseGetter(new MyConstantNoise() { Constant = 0.5f });
        public MyNoiseGetter HillNoise { get; set; } = new MyNoiseGetter(new MyConstantNoise() { Constant = 1f }) { MinNoiseOutput = 0, MaxNoiseOutput = 100 };
        public MyNoiseGetter ContinentNoise { get; set; } = new MyNoiseGetter(new MyConstantNoise() { Constant = 1f }) { MinNoiseOutput = 0, MaxNoiseOutput = 100 };
        public MyNoiseGetter SubSurfaceHeightNoise { get; set; } = new MyNoiseGetter(new MyConstantNoise() { Constant = 1f }) { MinNoiseOutput = 0, MaxNoiseOutput = 9 };

        public int SurfaceSize { get; set; }


        private int _seed = 0;
        public int Seed
        {
            get => _seed;
            set {
                _seed = value;
                ContinentNoise.Noise.Seed = _seed;
                HillNoise.Noise.Seed = _seed + 7;
                SubSurfaceHeightNoise.Noise.Seed = _seed + 17;
                SurfaceColorVariationNoise.Noise.Seed = _seed + 47;
                HumidityNoise.Noise.Seed = _seed + 67;
                TemperatureNoise.Noise.Seed = _seed + 97;
            }
        }
        
        public MyChunkBlockData GetData(EMySurfaceIndex surfaceIndex, int x, int z)
        {
            if (x < 0 || z < 0 || x >= SurfaceSize || z >= SurfaceSize)
            {
                return MyChunkBlockData.EMPTY;
            } else
            {
                Vector3 pos = surfaceIndex.Rotate(new Vector3(x - SurfaceSize / 2, MyConst.DISTANCE_FLAT * SurfaceSize, z - SurfaceSize / 2));
                float humidity = HumidityNoise.Get3D(pos.x, pos.y, pos.z);
                float temperature = TemperatureNoise.Get3D(pos.x, pos.y, pos.z);

                float distance = Math.Min(SurfaceSize - Math.Abs(x * 2 - SurfaceSize), SurfaceSize - Math.Abs(2 * z - SurfaceSize));
                float maxDist = 2 * SurfaceSize * DistanceFromBorderToFullHeightCoef;
                float coef = distance < maxDist ? Mathf.Lerp(0, 1, distance / maxDist) : 1f;
                float height = (HillNoise.Get3D(pos.x, pos.y, pos.z) + ContinentNoise.Get3D(pos.x, pos.y, pos.z)) * coef;
                float subsurfaceHeight = SubSurfaceHeightNoise.Get3D(pos.x, pos.y, pos.z) * coef;

                MySpaceBodyColorData color = Color.GetColors(Math.Max(0, temperature - height * MyConst.TEMPERATURE_REDUCTION_PER_PIECE_ALTITUDE), humidity);
                float inter = SurfaceColorVariationNoise.Get3D(pos.x, pos.y, pos.z);
                Color colorAB = color.SurfaceAColor.LinearInterpolate(color.SurfaceBColor, inter);

                return new MyChunkBlockData(height, colorAB, color.SubSurfaceColor, color.UndergroundColor, (int)subsurfaceHeight);
            }
        }

        public float GetMaxHeight()
        {
            return HillNoise.MaxNoiseOutput + ContinentNoise.MaxNoiseOutput;
        }
    }

    
    public class MyBlueStarGenerator : MySpaceBodyGenerator
    {
        public MyBlueStarGenerator(int seed) : base()
        {
            DistanceFromBorderToFullHeightCoef = 0.5f;
            Color = MySpaceBodyColor.BLUESTAR;
            HillNoise = new MyNoiseGetter(new MyConstantNoise() { Constant = 0 }) {
                FractalType = MyNoiseGetter.EMyFractalType.None,
                MinNoiseOutput = 0,
                MaxNoiseOutput = 0,
                MinResultOutput = 0,
                MaxResultOutput = 0,
            };
            ContinentNoise = new MyNoiseGetter(new MyConstantNoise() { Constant = 1 }) {
                FractalType = MyNoiseGetter.EMyFractalType.None,
                MinNoiseOutput = 0,
                MaxNoiseOutput = SurfaceSize * DISTANCE_SPHERE_PER_FLAT_RATIO * PIECE_RATIO_HEIGHT_PER_WIDE,
                MinResultOutput = 0,
                MaxResultOutput = SurfaceSize * DISTANCE_SPHERE_PER_FLAT_RATIO * PIECE_RATIO_HEIGHT_PER_WIDE,
            };
            SubSurfaceHeightNoise = new MyNoiseGetter(new MyConstantNoise() { Constant = 0 }) {
                FractalType = MyNoiseGetter.EMyFractalType.None,
            };
            SurfaceColorVariationNoise = new MyNoiseGetter(new MyCellularNoise() { CellularReturnType = MyCellularNoise.EMyCellularReturnType.Distance, CellDiameter = 1000 }) {
                FractalType = MyNoiseGetter.EMyFractalType.None,
                MinNoiseOutput = -1f,
                MaxNoiseOutput = 1.0f,
                MinResultOutput = 0f,
                MaxResultOutput = 1.0f,
                Factor = .01f,
            };

        }
    }
}
