﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bricksivers.Game.World.Generator
{
    public interface IMySurfaceGenerator
    {
        MyChunkBlockData GetData(EMySurfaceIndex surface, int x, int z);

        float GetMaxHeight();
    }
}
