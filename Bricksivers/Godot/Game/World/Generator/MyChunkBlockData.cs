﻿using Godot;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bricksivers.Game.World.Generator
{
    public struct MyChunkBlockData
    {
        public bool IsEmpty { get; }
        public int HeightInt => (int)HeightFloat;
        public float HeightFloat{ get; }
        public Color SurfaceColor { get;}
        public Color SubSurfaceColor { get;}
        public Color UndergroundColor { get;}
        public int SubSurfaceHeight { get; }

        public MyChunkBlockData(float height, Color surfaceColor, Color subSurfaceColor, Color undergroundColor, int subSurfaceHeight, bool isEmpty = false)
        {
            IsEmpty = isEmpty;
            HeightFloat = height;
            SurfaceColor = surfaceColor;
            SubSurfaceColor = subSurfaceColor;
            UndergroundColor = undergroundColor;
            SubSurfaceHeight = subSurfaceHeight;
        }

        public static MyChunkBlockData EMPTY => new MyChunkBlockData(0, default, default, default, 0, true);
    }
}