﻿using Bricksivers.Game.Utils;
using static Bricksivers.Game.Utils.MyConst;
using Godot;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bricksivers.Game.World
{
    public class EMySurfaceIndex
    {
        public int Index { get; }
        public string Name { get; }
        public Vector3 Rotation { get; }

        public bool IsValid => MyMath.IsBetween(Index, -1, 18);

        private EMySurfaceIndex(int index, string name, Vector3 rotation)
        {
            Index = index;
            Name = name;
            Rotation = rotation;
        }

        public static bool operator ==(EMySurfaceIndex a, EMySurfaceIndex b)
        {
            return a.Index == b.Index;
        }

        public static bool operator !=(EMySurfaceIndex a, EMySurfaceIndex b)
        {
            return a.Index != b.Index;
        }

        public override bool Equals(object obj)
        {
            return obj is EMySurfaceIndex i && i == this;
        }

        public override int GetHashCode()
        {
            return Index;
        }

        public Vector3 Rotate(Vector3 vector)
        {
            return vector.Rotated(Vector3.Back, Rotation.z).Rotated(Vector3.Up, Rotation.y).Rotated(Vector3.Right, Rotation.x);
        }

        public static implicit operator int(EMySurfaceIndex index) => index.Index;
        public static implicit operator EMySurfaceIndex(int index) => index < 0 ? new EMySurfaceIndex(index, "Unknow", Vector3.Zero) : index > 17 ? new EMySurfaceIndex(index, "Unknow", Vector3.Zero) : _indexs[index];

        public static readonly EMySurfaceIndex Top = new EMySurfaceIndex(0, "Top", new Vector3());
        public static readonly EMySurfaceIndex Bottom = new EMySurfaceIndex(1, "Bottom", new Vector3(0, 0, rad180));
        public static readonly EMySurfaceIndex Left = new EMySurfaceIndex(2, "Left", new Vector3(0, 0, rad90));
        public static readonly EMySurfaceIndex Right = new EMySurfaceIndex(3, "Right", new Vector3(0, 0, -rad90));
        public static readonly EMySurfaceIndex Front = new EMySurfaceIndex(4, "Front", new Vector3(rad90, 0, 0));
        public static readonly EMySurfaceIndex Back = new EMySurfaceIndex(5, "Back", new Vector3(-rad90, 0, 0));


        public static readonly EMySurfaceIndex TopLeft = new EMySurfaceIndex(6, "TopLeft", new Vector3(0, 0, rad45));
        public static readonly EMySurfaceIndex BottomLeft = new EMySurfaceIndex(7, "BottomLeft", new Vector3(0, 0, -rad45 - rad180));
        public static readonly EMySurfaceIndex BottomRight = new EMySurfaceIndex(8, "BottomRight", new Vector3(0, 0, -rad45 - rad90));
        public static readonly EMySurfaceIndex TopRight = new EMySurfaceIndex(9, "TopRight", new Vector3(0, 0, -rad45));

        public static readonly EMySurfaceIndex BackLeft = new EMySurfaceIndex(10, "BackLeft", new Vector3(-rad90, 0, -rad45));
        public static readonly EMySurfaceIndex FrontLeft = new EMySurfaceIndex(11, "FrontLeft", new Vector3(-rad90, 0, -rad45 - rad90));
        public static readonly EMySurfaceIndex FrontRight = new EMySurfaceIndex(12, "FrontRight", new Vector3(-rad90, 0, -rad45 - rad180));
        public static readonly EMySurfaceIndex BackRight = new EMySurfaceIndex(13, "BackRight", new Vector3(-rad90, 0, rad45));

        public static readonly EMySurfaceIndex BackTop = new EMySurfaceIndex(14, "BackTop", new Vector3(-rad45, 0, 0));
        public static readonly EMySurfaceIndex FrontTop = new EMySurfaceIndex(15, "FrontTop", new Vector3(rad45, 0, 0));
        public static readonly EMySurfaceIndex FrontBottom = new EMySurfaceIndex(16, "FrontBottom", new Vector3(-rad45 - rad180, 0, 0));
        public static readonly EMySurfaceIndex BackBottom = new EMySurfaceIndex(17, "BackBottom", new Vector3(-rad45 - rad90, 0, 0));

        public static readonly EMySurfaceIndex Unknow = new EMySurfaceIndex(-1, "Unknow", Vector3.Zero);

        private static readonly EMySurfaceIndex[] _indexs = { Top, Bottom, Left, Right, Front, Back, TopLeft, BottomLeft, BottomRight, TopRight, BackLeft, FrontLeft, FrontRight, BackRight, BackTop, FrontTop, FrontBottom, BackBottom };
    }
}
