using Bricksivers.Game.Character;
using Bricksivers.Game.Utils;
using Godot;
using System;
using System.Runtime.CompilerServices;

public class MyCharacter : RigidBody, IMyCameraAttachableTo
{
	public const string ANIMATION_IDLE_HAND_EMPTY = "idle_hand_empty";
	public const string ANIMATION_WALK_HAND_EMPTY = "walk_hand_empty";
	public const string ANIMATION_SPRINT_HAND_EMPTY = "sprint_hand_empty";


	public AnimationNodeStateMachinePlayback AnimationStateMachine { get; private set; }

	public MeshInstance Head { get; private set; }
	public MeshInstance Pelvis { get; private set; }
	public MeshInstance Body { get; private set; }
	public MeshInstance ArmLeft { get; private set; }
	public MeshInstance ArmRight { get; private set; }
	public MeshInstance HandLeft { get; private set; }
	public MeshInstance HandRight { get; private set; }
	public CollisionShape HitBox { get; private set; }
	public MyCharacterLeg LegLeft { get; private set; }
	public MyCharacterLeg LegRight { get; private set; }
	public Spatial Model { get; private set; }
	public Spatial CameraAttachment { get; private set; }
	public RayCast CameraAttachmentRayCast { get; private set; }
	public RayCast OnGroundArea { get; private set; }
	public RayCast OnGroundArea2 { get; private set; }
	public Vector2 ViewRotation
	{
		get => new Vector2(CameraAttachment.Rotation.x, CameraAttachment.Rotation.y);
		set {
			Pelvis.Rotation = new Vector3(Mathf.Clamp(value.x, -MyConst.rad60, MyConst.rad60), 0, 0);
			Head.Rotation = new Vector3(0, Mathf.Clamp(value.y, -MyConst.rad60, MyConst.rad60), 0);
			CameraAttachment.Rotation = new Vector3(Pelvis.Rotation.x, Head.Rotation.y, 0);
		}
	}

	private IMyPlayer _player;
	public IMyPlayer Player
	{
		get => _player; set {
			_player = value;
			_player?.ApplyCollisionLayerTo(this);
		}
	}

	public Vector3 Gravity { get; set; }
	public string CurrentAnimation { get; set; } = ANIMATION_IDLE_HAND_EMPTY;

	private (Spatial attachmentPoint, Vector3 offset)[] _attachmentPoints;


	public int AttachmentPointCount => _attachmentPoints.Length;

	public override void _Ready()
	{
		AnimationStateMachine = GetNode<AnimationTree>("AnimationTree").Get("parameters/playback") as AnimationNodeStateMachinePlayback;
		AnimationStateMachine.Start(CurrentAnimation);
		HitBox = GetNode<CollisionShape>("CollisionShape");
		Model = HitBox.GetNode<Spatial>("model");
		Pelvis = HitBox.GetNode<MeshInstance>("model/pelvis");
		Body = Pelvis.GetNode<MeshInstance>("body");
		Head = Body.GetNode<MeshInstance>("head");
		ArmLeft = Body.GetNode<MeshInstance>("arm_left");
		ArmRight = Body.GetNode<MeshInstance>("arm_right");
		HandLeft = ArmLeft.GetNode<MeshInstance>("hand_left");
		HandRight = ArmRight.GetNode<MeshInstance>("hand_right");
		LegLeft = HitBox.GetNode<MyCharacterLeg>("model/leg_left");
		LegRight = HitBox.GetNode<MyCharacterLeg>("model/leg_right");
		OnGroundArea = HitBox.GetNode<RayCast>("model/on_ground_area");
		OnGroundArea2 = HitBox.GetNode<RayCast>("model/on_ground_area2");


		CameraAttachment = HitBox.GetNode<Spatial>("model/camera_attachment");
		CameraAttachmentRayCast = CameraAttachment.GetNode<RayCast>("camera_attachment_raycast");
		CameraAttachmentRayCast.AddException(HitBox);
		CameraAttachmentRayCast.CollideWithAreas = false;
		PhysicsMaterialOverride = new PhysicsMaterial() { Friction = 1 };

		HandLeftColor = HandRightColor = HeadColor = Colors.White;
		ArmLeftColor = ArmRightColor = ((BodyColor = Colors.ForestGreen * 0.6f) * 1.2f) * 0.8f;
		LegLeftColor = (LegRightColor = Colors.ForestGreen * 0.8f) * 0.8f;
		PelvisColor = Colors.Brown;
		HeadTexture = GD.Load<Texture>("res://Content/Textures/Models/Character/Head/default.png");

		SetPhysicsProcess(true);
		_attachmentPoints = new (Spatial, Vector3)[] { (CameraAttachment, new Vector3(0, 1, 0)), (CameraAttachment, new Vector3(0.5f, 1, 3)) };

	}

	public override void _Process(float delta)
	{
		AnimationStateMachine.Travel(CurrentAnimation);
		LegLeft.PelvisRotation = Pelvis.RotationDegrees.x;
		LegRight.PelvisRotation = Pelvis.RotationDegrees.x;
	}

	public override void _ExitTree()
	{
		base._ExitTree();
		AnimationStateMachine = null;
	}

	public override void _PhysicsProcess(float delta)
	{
		base._PhysicsProcess(delta);
		//Player?.ApplyPlayerInputsTo(this, null, delta);
	}

	public override void _IntegrateForces(PhysicsDirectBodyState state)
	{
		base._IntegrateForces(state);
		Player?.ApplyPlayerInputsTo(this, state, 0.016f);
	}

	public Vector3 GetBodyUpVector()
	{
		return HitBox.GetForwardVector().Normalized();
	}

	public Vector3 GetBodyForwardVector()
	{
		return HitBox.GetUpVector().Normalized();
	}

	public Vector3 GetBodyRightVector()
	{
		return HitBox.GetRightVector().Normalized();
	}

	public Vector3 GetViewForwardVector()
	{
		return GetBodyForwardVector().Rotated(GetBodyUpVector(), CameraAttachment.Rotation.y);
	}

	public Vector3 GetViewRightVector()
	{
		return GetBodyRightVector().Rotated(GetBodyUpVector(), CameraAttachment.Rotation.y);
	}

	public Vector3 GetViewUpVector()
	{
		return GetBodyUpVector();
	}

	public Spatial GetAttachmentPoint(int index)
	{
		return _attachmentPoints[index].attachmentPoint;
	}

	public Vector3 GetAttachmentPointOffset(int index)
	{
		return _attachmentPoints[index].offset;
	}

	public bool IsAttachmentPointAviable(int index)
	{
		switch (index)
		{
			case 0:
				return true;
			case 1:
				return !CameraAttachmentRayCast.IsColliding();
			default:
				return false;
		}
	}

	public void OnAttachmentPointSelected(int index)
	{
		Head.Visible = index != 0;
	}

	private void SetMeshColor(MeshInstance mesh, Color color)
	{
		for (int i = 0; i < mesh.GetSurfaceMaterialCount(); i++)
		{
			SpatialMaterial material = (SpatialMaterial)mesh.GetSurfaceMaterial(i);
			material.AlbedoColor = color;
		}
	}

	private void SetMeshTexture(MeshInstance mesh, Texture texture, int surface)
	{
		SpatialMaterial material = (SpatialMaterial)mesh.GetSurfaceMaterial(surface);
		material.AlbedoTexture = texture;
	}

	private Color GetMeshColor(MeshInstance mesh)
	{
		return ((SpatialMaterial)mesh.GetSurfaceMaterial(0)).AlbedoColor;
	}
	private Texture GetMeshTexture(MeshInstance mesh)
	{
		return ((SpatialMaterial)mesh.GetSurfaceMaterial(0)).AlbedoTexture;
	}


	public bool IsOnGround => OnGroundArea.IsColliding() || OnGroundArea2.IsColliding();


	public Color HeadColor
	{
		get => GetMeshColor(Head);
		set => SetMeshColor(Head, value);
	}

	public Color ArmLeftColor
	{
		get => GetMeshColor(ArmLeft);
		set => SetMeshColor(ArmLeft, value);
	}

	public Color ArmRightColor
	{
		get => GetMeshColor(ArmRight);
		set => SetMeshColor(ArmRight, value);
	}

	public Color HandLeftColor
	{
		get => GetMeshColor(HandLeft);
		set => SetMeshColor(HandLeft, value);
	}

	public Color HandRightColor
	{
		get => GetMeshColor(HandRight);
		set => SetMeshColor(HandRight, value);
	}

	public Color LegLeftColor
	{
		get => GetMeshColor(LegLeft);
		set => SetMeshColor(LegLeft, value);
	}

	public Color LegRightColor
	{
		get => GetMeshColor(LegRight);
		set => SetMeshColor(LegRight, value);
	}

	public Color PelvisColor
	{
		get => GetMeshColor(Pelvis);
		set => SetMeshColor(Pelvis, value);
	}

	public Color BodyColor
	{
		get => GetMeshColor(Body);
		set => SetMeshColor(Body, value);
	}

	public Texture BodyTexture
	{
		get => GetMeshTexture(Body);
		set => SetMeshTexture(Body, value, 0);
	}

	public Texture HeadTexture
	{
		get => GetMeshTexture(Head);
		set => SetMeshTexture(Head, value, 1);
	}
}
