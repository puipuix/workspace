﻿using Godot;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bricksivers.Game.Character
{
    public interface IMyCameraAttachableTo
    {
        int AttachmentPointCount { get; }
        Spatial GetAttachmentPoint(int index);
        Vector3 GetAttachmentPointOffset(int index);
        bool IsAttachmentPointAviable(int index);
        void OnAttachmentPointSelected(int index);
    }
}
