using Godot;
using System;

public class MyCharacterLeg : MeshInstance
{
	[Export]
	public float LegRotation { get; set; }
	[Export]
	public float PelvisRotation { get; set; }

	public override void _Process(float delta)
	{
		RotationDegrees = new Vector3(Mathf.Clamp(LegRotation, -60 + PelvisRotation, 90 + PelvisRotation), 0, 0);
	}
}
