using Bricksivers.Game.Character;
using Bricksivers.Game.Utils;
using Godot;
using System;

public class MyPlayer : Node, IMyPlayer
{
    private const float MOUSE_SENSIBILITY = 0.01f;
    private const float MOUSE_DEATH_ZONE = 1f;

    public const float WALK_MAX_SPEED = 5f;
    public const float WALK_ACCEL = 5f;

    public const float SPRINT_MULTIPLIER = 2;
    public const float FLY_MULTIPLIER = 20;

    public const float JUMP_FORCE = 5f;

    public bool IsFlying { get; set; }

    private Vector2 mouseInput;


    public override void _Input(InputEvent @event)
    {
        base._Input(@event);
        if (@event is InputEventMouseMotion motion)
        {
            mouseInput.x = Math.Abs(motion.Relative.x) > MOUSE_DEATH_ZONE ? motion.Relative.x * -MOUSE_SENSIBILITY : 0;
            mouseInput.y = Math.Abs(motion.Relative.y) > MOUSE_DEATH_ZONE ? motion.Relative.y * -MOUSE_SENSIBILITY : 0;
        }
    }

    public void ApplyPlayerInputsTo(MyCharacter character, PhysicsDirectBodyState state, float delta)
    {
        // Update isFlying when you press ui_fly
        if (Input.IsActionJustPressed("ui_fly"))
        {
            IsFlying = !IsFlying;
        }

        // Rotation input from mouse and action
        Vector3 rotationInputs = new Vector3(
            mouseInput.y,
            mouseInput.x,
            IsFlying ? (-Input.GetActionStrength("ui_lean_left") + Input.GetActionStrength("ui_lean_right")) * -MOUSE_SENSIBILITY : 0);

        // Movement inputs
        Vector3 moveInputs = new Vector3(
            Input.GetActionStrength("ui_right") - Input.GetActionStrength("ui_left"),
            Input.GetActionStrength("ui_jump") - Input.GetActionStrength("ui_crouch"),
            Input.GetActionStrength("ui_up") - Input.GetActionStrength("ui_down"));


        // Rotations

        // We rotate the head
        character.Head.RotateY(rotationInputs.y);
        // If the head is too angled, we move back the head to the max angle and we send the rest to the body rotation
        if (character.Head.Rotation.y > MyConst.rad60)
        {
            float diff = character.Head.Rotation.y - MyConst.rad60;
            character.Head.Rotation = new Vector3(0, MyConst.rad60, 0);
            rotationInputs.y = diff;
        } else if (character.Head.Rotation.y < -MyConst.rad60)
        {
            float diff = character.Head.Rotation.y + MyConst.rad60;
            character.Head.Rotation = new Vector3(0, -MyConst.rad60, 0);
            rotationInputs.y = diff;
        } else
        {
            // If the head is good, we slowly align the body and the head
            float rate = character.Head.Rotation.y / MyConst.rad60 * delta * ((moveInputs.z != 0 || moveInputs.x != 0) ? 4 : 1);
            character.Head.Rotation -= new Vector3(0, rate, 0);
            rotationInputs.y = rate;
        }

        //Movements

        //Check animation
        if (moveInputs.z == 0 && moveInputs.x == 0)
        {
            character.CurrentAnimation = MyCharacter.ANIMATION_IDLE_HAND_EMPTY;
        } else
        {
            character.CurrentAnimation = MyCharacter.ANIMATION_WALK_HAND_EMPTY;
        }

        // what is the current acceleration and speed depending on if we are walking, flying or sprinting
        float accel = WALK_ACCEL;
        float maxSpeed = WALK_MAX_SPEED;
        if (IsFlying)
        {
            accel *= FLY_MULTIPLIER;
            maxSpeed *= FLY_MULTIPLIER;
        }
        // Sprint only if we goes forward
        if (moveInputs.z > 0 && Input.IsActionPressed("ui_shift"))
        {
            accel *= SPRINT_MULTIPLIER;
            maxSpeed *= SPRINT_MULTIPLIER;
            character.CurrentAnimation = MyCharacter.ANIMATION_SPRINT_HAND_EMPTY;
        }

        character.AddCentralForce(character.Gravity / character.Mass);

        if (IsFlying)
        {
            // If the pelvis is rotated we move the body to keep the view where we were looking
            rotationInputs.x += character.Pelvis.Rotation.x;
            character.Pelvis.Rotation = character.CameraAttachment.Rotation = Vector3.Zero;

            // Movement where the player is looking
            Vector3 movement = character.GetViewRightVector() * moveInputs.x
            + character.GetBodyUpVector() * moveInputs.y
            + character.GetViewForwardVector() * -moveInputs.z;

            // We add a force to move
            character.AddCentralForce(movement.Normalized() * accel);
            // We rotate the hitbox and not the character because godot
            character.HitBox.Rotate(character.GetBodyRightVector(), rotationInputs.x);
            character.HitBox.Rotate(character.GetBodyUpVector(), rotationInputs.y);
            character.HitBox.Rotate(character.GetBodyForwardVector(), rotationInputs.z);
        } else
        {
            //We will align the character to the gravity
            Vector3 gravityUp = -character.Gravity;
            float length = gravityUp.LengthSquared();
            // if 0 -> no gravity
            if (length > 0.1f)
            {
                gravityUp /= Mathf.Sqrt(length);
                float angle = character.GetBodyUpVector().AngleTo(gravityUp);
                if (angle > Mathf.Deg2Rad(1))
                {
                    Vector3 axis = character.GetBodyUpVector().Cross(gravityUp);
                    character.HitBox.Rotate(axis.Normalized(), angle * delta);
                }
            }

            // If the raycast collide with the ground
            if (character.IsOnGround)
            {
                // Movement where the view looks but not up vector
                Vector3 movement = character.GetViewRightVector() * moveInputs.x
                + character.GetViewForwardVector() * -moveInputs.z;

                // Make a jump by applying a central inpulse in direction of the body up vector
                if (Input.IsActionJustPressed("ui_jump"))
                {
                    character.ApplyCentralImpulse(character.GetBodyUpVector() * JUMP_FORCE);
                }

                // If we don't want to move we enable friction to prevent sliding
                if (movement == Vector3.Zero)
                {
                    character.PhysicsMaterialOverride.Friction = 1;
                } else if (movement.x * character.LinearVelocity.x < -0.01f || movement.y * character.LinearVelocity.y < -0.01f || movement.z * character.LinearVelocity.z < -0.01f)
                {
                    character.PhysicsMaterialOverride.Friction = 1;
                } else
                {
                    // But if we want, we disable friction to make movement easier.
                    character.PhysicsMaterialOverride.Friction = 0;
                }

                // If the are not to fast we add the movement
                // We not directly set velocity to not disable external force.
                if (MyMath.IsBetween(character.LinearVelocity.x + movement.x * accel * delta, -maxSpeed, maxSpeed))
                {
                    character.AddCentralForce(new Vector3(movement.x * accel, 0, 0));
                }

                if (MyMath.IsBetween(character.LinearVelocity.y + movement.y * accel * delta, -maxSpeed, maxSpeed))
                {
                    character.AddCentralForce(new Vector3(0, movement.y * accel, 0));
                }

                if (MyMath.IsBetween(character.LinearVelocity.z + movement.z * accel * delta, -maxSpeed, maxSpeed))
                {
                    character.AddCentralForce(new Vector3(0, 0, movement.z * accel));
                }
            }

            // We rotate the body when we look left/right
            character.HitBox.Rotate(character.GetBodyUpVector(), rotationInputs.y);
            // We rotate the pelvis for up and down
            character.Pelvis.Rotation = new Vector3(Mathf.Clamp(character.Pelvis.Rotation.x + rotationInputs.x, -MyConst.rad60, MyConst.rad60), 0, 0);
        }
        // We set the camera rotation to the head (y) and the pelvis (x)
        character.CameraAttachment.Rotation = character.Head.Rotation + character.Pelvis.Rotation;

        // Reset mouse input to prevent infinite movement when the player don't use the mouse
        mouseInput = Vector2.Zero;

    }

    public void ApplyCollisionLayerTo(MyCharacter character)
    {
        character.Mode = RigidBody.ModeEnum.Character;
        GD.PrintS("alo", character.GetCollisionLayerBit(0));
        character.SetCollisionLayerBit(MyConst.COLLISION_LAYER_EXTERNAL_PLAYER, false);
        character.SetCollisionLayerBit(MyConst.COLLISION_LAYER_LOCAL_PLAYER, true);
    }
}
