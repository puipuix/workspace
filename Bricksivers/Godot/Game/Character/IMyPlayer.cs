﻿using Godot;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bricksivers.Game.Character
{
    public interface IMyPlayer
    {
        void ApplyPlayerInputsTo(MyCharacter character, PhysicsDirectBodyState state, float delta);
        void ApplyCollisionLayerTo(MyCharacter character);
    }
}
