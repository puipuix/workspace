﻿using Godot;
using Godot.Collections;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bricksivers.Game.Utils
{
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property, AllowMultiple = false)]
    sealed class MyExportAttribute : Attribute
    {
        static bool _warningPrinted = false;
        private bool _valid = true;
        public string Name { get; }
        public Variant.Type Type { get; }
        public PropertyHint Hint { get; }
        public string HintString { get; }
        public PropertyUsageFlags Usage { get; }

        public MyExportAttribute(string name, Variant.Type type, PropertyHint hint = PropertyHint.None, string hintString = "", PropertyUsageFlags usage = PropertyUsageFlags.Default, Type enumType = null)
        {
            Name = name;
            Hint = hint;
            if (hint == PropertyHint.Enum)
            {
                if (Engine.EditorHint && !_warningPrinted)
                {
                    _warningPrinted = true;
                    GD.PrintErr("Warning, enum isn't fully supported by the editor, get will return a string and set need have a string as parameter.");
                    GD.PrintErr("Parse the string to enum with Enum.Parse(string) and myEnumValue.ToString()");
                }
                Type = Variant.Type.String;
                if (enumType != null)
                {
                    StringBuilder b = new StringBuilder();
                    string[] names = Enum.GetNames(enumType);
                    for (int i = 0; i < names.Length; i++)
                    {
                        b.Append(names[i]);
                        if (i < names.Length - 1)
                        {
                            b.Append(",");
                        }
                    }
                    HintString = b.ToString();
                } else
                {
                    _valid = false;
                    HintString = "Error:,enumType is null";
                }
            } else
            {
                Type = type;
                HintString = hintString;
            }
            Usage = usage;
        }

        public Dictionary ToDictionary()
        {
            return new Dictionary() {
                ["name"] = Name,
                ["type"] = Type,
                ["hint"] = Hint,
                ["hint_string"] = HintString,
                ["usage"] = Usage,
            };
        }

        public static bool Get(Godot.Object obj, string propertyName, out object ret)
        {
            bool find = false;
            ret = null;
            foreach (var property in obj.GetType().GetProperties())
            {
                if (GetCustomAttribute(property, typeof(MyExportAttribute), true) is MyExportAttribute attribute)
                {
                    if (attribute.Name == propertyName)
                    {
                        find = true;
                        ret = property.GetValue(obj);
                        if (attribute.Hint == PropertyHint.Enum && attribute._valid)
                        {
                            ret = ret?.ToString();
                        }
                        break;
                    }
                }
            }

            if (!find)
            {
                foreach (var field in obj.GetType().GetFields())
                {
                    if (GetCustomAttribute(field, typeof(MyExportAttribute), true) is MyExportAttribute attribute)
                    {
                        if (attribute.Name == propertyName)
                        {
                            find = true;
                            ret = field.GetValue(obj);
                            if (attribute.Hint == PropertyHint.Enum && attribute._valid)
                            {
                                ret = ret?.ToString();
                            }
                            break;
                        }
                    }
                }
            }

            return find;
        }

        public static bool Set(Godot.Object obj, string propertyName, object value)
        {
            foreach (var property in obj.GetType().GetProperties())
            {
                if (GetCustomAttribute(property, typeof(MyExportAttribute), true) is MyExportAttribute attribute)
                {
                    if (attribute.Name == propertyName)
                    {
                        if (attribute.Hint == PropertyHint.Enum && value is string str && attribute._valid)
                        {
                            property.SetValue(obj, Enum.Parse(property.PropertyType, str));
                            return true;
                        } else
                        {
                            property.SetValue(obj, value);
                            return true;
                        }
                    }
                }
            }

            foreach (var field in obj.GetType().GetFields())
            {
                if (GetCustomAttribute(field, typeof(MyExportAttribute), true) is MyExportAttribute attribute)
                {
                    if (attribute.Hint == PropertyHint.Enum && value is string str && attribute._valid)
                    {
                        field.SetValue(obj, Enum.Parse(field.FieldType, str));
                        return true;
                    } else
                    {
                        field.SetValue(obj, value);
                        return true;
                    }
                }
            }

            return false;
        }

        public static Godot.Collections.Array GetPropertyList(Godot.Object obj, Godot.Collections.Array array = null)
        {
            array = array ?? new Godot.Collections.Array();
            foreach (var property in obj.GetType().GetProperties())
            {
                if (GetCustomAttribute(property, typeof(MyExportAttribute), true) is MyExportAttribute attribute)
                {
                    array.Add(attribute.ToDictionary());
                }
            }

            foreach (var field in obj.GetType().GetFields())
            {
                if (GetCustomAttribute(field, typeof(MyExportAttribute), true) is MyExportAttribute attribute)
                {
                    array.Add(attribute.ToDictionary());
                }
            }

            return array;
        }
    }
}
