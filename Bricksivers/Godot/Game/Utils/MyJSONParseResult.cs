﻿using Godot;
using Godot.Collections;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bricksivers.Game.Utils
{
    public class MyJSONParseResult
    {
        private Dictionary _object;

        public MyJSONParseResult(Dictionary obj)
        {
            _object = obj;
        }

        public static explicit operator MyJSONParseResult(Dictionary dic)
        {
            return new MyJSONParseResult(dic);
        }

        public override string ToString()
        {
            return _object.ToString();
        }

        public bool HasData(string name) => _object.Contains(name);

        public MyJSONParseResult GetObjectData(string name)
        {
            return new MyJSONParseResult((Dictionary)_object[name]);
        }

        private object GetDataForType<T>(string name)
        {
            if (typeof(T).Equals(typeof(Color)))
            {
                return GetColorData(name);
            } else
            {
                return _object[name];
            }
        }

        public bool TryGetData<T>(string name, out T value)
        {
            if (HasData(name))
            {
                value = (T)GetDataForType<T>(name);
                return true;
            } else
            {
                value = default;
                return false;
            }
        }

        public T GetDataOrDefault<T>(string name, T defaultValue)
        {
            if (HasData(name))
            {
                return (T)GetDataForType<T>(name);
            } else
            {
                return defaultValue;
            }
        }

        public float GetFloatData(string name)
        {
            return (float)_object[name];
        }

        public int GetIntData(string name)
        {
            return (int)GetFloatData(name);
        }

        public string GetStringData(string name)
        {
            return (string)_object[name];
        }

        public Color GetColorData(string name)
        {
            object data = _object[name];
            if (data is Dictionary dic)
            {
                var js = new MyJSONParseResult(dic);
                return new Color(js.GetDataOrDefault("r", 0f), js.GetDataOrDefault("g", 0f), js.GetDataOrDefault("b", 0f));
            }
            else if (data is string str)
            {
                return str.StartsWith("#") ? new Color(str) { a = 1 } : Color.ColorN(str, 1);
            }
            else
            {
                return (Color)data;
            }
        }
    }
}
