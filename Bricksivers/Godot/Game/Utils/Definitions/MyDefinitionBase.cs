﻿using Godot;
using GArray = Godot.Collections.Array;
using GDictionary = Godot.Collections.Dictionary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;
using System.Reflection;
using Bricksivers.Game.Utils.Json;

namespace Bricksivers.Game.Utils.Definitions
{
    public abstract class MyDefinitionBase : IComparable<MyDefinitionBase>
    {
        protected const string UNKNOW = "Unknow";

        public static Dictionary<string, Func<Type>> DefinitionFactories = new Dictionary<string, Func<Type>>();

        public static Dictionary<string, MyDefinitionBase> Definitions = new Dictionary<string, MyDefinitionBase>();

        public static bool LoadDefinition(JObject obj, out MyDefinitionBase definition)
        {
            definition = null;
            if (obj.TryGetValue("Class", out JToken token))
            {
                if (token.Type == JTokenType.String)
                {
                    string defClass = (string)((JValue)token).Value;
                    if (DefinitionFactories.TryGetValue(defClass, out var func))
                    {
                        try
                        {
                            definition = (MyDefinitionBase)obj.ToObject(func(), MyJsonConst.DEFAULT_SERIALIZER);
                            if (definition.Checkup())
                            {
                                MyDebug.PrintLine(definition.ClassDefinitionAsKey + " loaded.");
                                return true;
                            }
                            else
                            {
                                return false;
                            }
                        }
                        catch (Exception e)
                        {
                            MyDebug.PrintErr(e);
                            return false;
                        }
                    }
                    else
                    {
                        MyDebug.PrintErr("No definition factory as been found for the class '" + defClass + "'.");
                        return false;
                    }
                }
                else
                {
                    MyDebug.PrintErr("Class isn't a string but is a '" + token.Type + "'.");
                    return false;
                }
            }
            else
            {
                MyDebug.PrintErr("No class data has been found.");
                return false;
            }
        }

        public static T GetDefinition<T>(string defId) where T : MyDefinitionBase
        {
            return (T)Definitions[defId];
        }

        private static void LoadFromFolder(string folder, ref int loaded)
        {
            Directory dir = new Directory();
            File file = new File();

            dir.Open(folder);
            dir.ListDirBegin(true, true);
            string name;
            while ((name = dir.GetNext()) != "")
            {
                if (dir.CurrentIsDir())
                {
                    LoadFromFolder(folder + name + "/", ref loaded);
                }
                else
                {
                    if (name.Contains(".def"))
                    {
                        string path = folder + name;
                        MyDebug.PrintLine(string.Concat("Reading '", path, "'..."));
                        MyDebug.AddTab();
                        Error err = file.Open(path, File.ModeFlags.Read);
                        if (err == Error.Ok)
                        {
                            string text = file.GetAsText();
                            text = MyTools.RemoveJsonComment(text);
                            try
                            {
                                JToken token = JToken.Parse(text);
                                if (token is JArray array)
                                {
                                    for (int i = 0; i < array.Count; i++)
                                    {
                                        if (array[i] is JObject obj)
                                        {
                                            if (LoadDefinition(obj, out MyDefinitionBase def))
                                            {
                                                Definitions.Add(def.ClassDefinitionAsKey, def);
                                                def.Init();
                                                loaded++;
                                            }
                                        }
                                    }
                                }
                                else if (token is JObject obj)
                                {
                                    if (LoadDefinition(obj, out MyDefinitionBase def))
                                    {
                                        Definitions.Add(def.ClassDefinitionAsKey, def);
                                        def.Init();
                                        loaded++;
                                    }
                                }
                            }
                            catch (Exception e)
                            {
                                MyDebug.PrintErr(e);
                            }
                        }
                        else
                        {
                            MyDebug.PrintErr(err);
                        }
                        MyDebug.RemoveTab();
                        file.Close();
                    }
                    else
                    {
                        MyDebug.PrintLine("Skiping '" + name + "'.");
                    }
                }
            }
        }

        public static void LoadDefinitions()
        {
            DefinitionFactories.Add("SpaceBodyGenerator", () => typeof(MySpaceBodyGeneratorDefinition));

            MyDebug.PrintLine("Loading definitions from '" + MyPathConst.DEFINITION_FOLDER + "'...");
            MyDebug.AddTab();
            int loaded = 0;
            LoadFromFolder(MyPathConst.DEFINITION_FOLDER, ref loaded);
            MyDebug.PrintLine(loaded + " definition loaded.");
            MyDebug.ClearTab();
        }

        public string Class { get; set; } = UNKNOW;
        public string Id { get; set; } = UNKNOW;
        public string SubId { get; set; } = "";

        [JsonIgnore]
        public string ClassDefinitionAsKey => string.Concat(Class, "/", Id, "/", SubId);

        [JsonIgnore]
        public string DefinitionAsKey => string.Concat(Id, "/", SubId);

        /// <summary>
        /// Return true if the value is set.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="value"></param>
        /// <param name="unSetValue"></param>
        /// <returns></returns>
        protected bool AssertSet(string name, object value, object unSetValue = null)
        {
            if (value == unSetValue || (value is object && value.Equals(unSetValue)))
            {
                MyDebug.PrintErr("Error '" + name + "' isn't set");
                return false;
            }
            else
            {
                return true;
            }
        }

        public virtual bool Checkup()
        {
            return AssertSet(nameof(Class), Class, UNKNOW) && AssertSet(nameof(Id), Id, UNKNOW);
        }

        public virtual void Init() { }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this, MyJsonConst.DEFAULT_SETTING);
        }

        public int CompareTo(MyDefinitionBase other)
        {
            return ClassDefinitionAsKey.CompareTo(other.ClassDefinitionAsKey);
        }
    }

    public abstract class MyDefinitionBase<TResult> : MyDefinitionBase
    {

        public abstract TResult CreateNew();
    }
}
