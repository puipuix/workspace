﻿using Bricksivers.Game.Utils.Noises;
using Bricksivers.Game.World.Generator;
using Godot;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bricksivers.Game.Utils.Definitions
{
    public class MySpaceBodyGeneratorDefinition : MyDefinitionBase<MySpaceBodyGenerator>
    {
        public enum EMySpaceBodyType
        {
            /// <summary>
            /// Main body of a system
            /// </summary>
            Main,

            /// <summary>
            /// Located between the <see cref="Main"/> body and the <see cref="HabitableZone"/>
            /// </summary>
            InnerZone,

            /// <summary>
            /// Can be located around the <see cref="HabitableZone"/> 
            /// </summary>
            InnerBelt,

            /// <summary>
            /// In the middle of the system
            /// </summary>
            HabitableZone,

            /// <summary>
            /// After the <see cref="HabitableZone"/>
            /// </summary>
            OutterZone,

            /// <summary>
            /// After the <see cref="OutterZone"/>
            /// </summary>
            OutterBelt
        }

        public class MySpaceBodyNoiseDefinition
        {
            public string NoiseName { get; set; } = "Constant";

            public float Constant { get; set; } = 1;

            public byte Offset { get; set; } = 0;
            public MyPermutationNoise.Interp Interpolation { get; set; } = MyPermutationNoise.Interp.Linear;

            public float CellDiameter { get; set; } = 10;
            public MyCellularNoise.EMyCellularReturnType CellularReturnType { get; set; } = MyCellularNoise.EMyCellularReturnType.Distance;

            public bool IsFractal { get; set; } = false;
            public int Octave { get; set; } = 4;
            public float Gain { get; set; } = 0.5f;
            public float Lacunarity { get; set; } = 2f;
            public float Stepness { get; set; } = 2f;
            public float Factor { get; set; } = 1f;
            public float MaxNoise { get; set; } = 1;
            public float MinNoise { get; set; } = -1;
            public float MaxResult { get; set; } = 1;
            public float MinResult { get; set; } = -1;
            public float MaxNoiseAndResult { set { MaxNoise = MaxResult = value; } }
            public float MinNoiseAndResult { set { MinNoise = MinResult = value; } }

            private MyNoise GetNoise()
            {
                switch (NoiseName)
                {
                    case "Value":
                        return new MyValueNoise() { Offset = Offset, InterpolationMode = Interpolation };
                    case "Random":
                        return new MyRandomNoise();
                    case "Cellular":
                        return new MyCellularNoise() { CellDiameter = CellDiameter, CellularReturnType = CellularReturnType, };
                    case "White":
                        return new MyWhiteNoise();
                    case "Simplex":
                        return new MySimplexNoise() { Offset = Offset, InterpolationMode = Interpolation };
                    case "Perlin":
                        return new MyPerlinNoise() { Offset = Offset, InterpolationMode = Interpolation };

                    default:
                        return new MyConstantNoise() { Constant = Constant };
                }
            }

            public MyNoiseGetter GetNoiseGetter()
            {
                return new MyNoiseGetter(GetNoise())
                {
                    Factor = Factor,
                    FractalType = IsFractal ? MyNoiseGetter.EMyFractalType.Billow : MyNoiseGetter.EMyFractalType.None,
                    Gain = Gain,
                    Lacunarity = Lacunarity,
                    OctaveCount = Octave,
                    Steepness = Stepness,
                    MaxNoiseOutput = MaxNoise,
                    MinNoiseOutput = MinNoise,
                    MaxResultOutput = MaxResult,
                    MinResultOutput = MinResult,
                };
            }
        }


        private static List<MySpaceBodyGeneratorDefinition>[] _definitions;
        static MySpaceBodyGeneratorDefinition()
        {
            _definitions = new List<MySpaceBodyGeneratorDefinition>[Enum.GetValues(typeof(EMySpaceBodyType)).Length];
            for (int i = 0; i < _definitions.Length; i++)
            {
                _definitions[i] = new List<MySpaceBodyGeneratorDefinition>();
            }
        }

        public static MySpaceBodyGeneratorDefinition GetRandomDefinition(Random r, EMySpaceBodyType type)
        {
            return _definitions[(int)type][r.Next(0, _definitions[(int)type].Count)];
        }

        public float DistanceFromBorderToFullHeightCoef { get; set; } = 0.1f;
        public Color BaseColor { get; set; } = Colors.Black;
        public string SurfaceA { get; set; } = null;
        public string SurfaceB { get; set; } = null;
        public string SubSurface { get; set; } = null;
        public string Underground { get; set; } = null;
        public float TemperatureMin { get; set; } = 0;
        public float TemperatureMax { get; set; } = 2000;
        public float HumididyMin { get; set; } = 0;
        public float HumididyMax { get; set; } = 1;

        public EMySpaceBodyType[] Locations { get; set; } = new EMySpaceBodyType[] { };
        public float Density { get; set; } = 1f;
        public float SurfaceSizeMin { get; set; } = -1f;
        public float SurfaceSizeMax { get; set; } = -1f;

        public MySpaceBodyNoiseDefinition ContinentNoise { get; set; } = new MySpaceBodyNoiseDefinition()
        {
            MinNoiseAndResult = 10,
            MaxNoiseAndResult = 20,
        };
        public MySpaceBodyNoiseDefinition HillNoise { get; set; } = new MySpaceBodyNoiseDefinition()
        {
            MinNoiseAndResult = 0,
            MaxNoiseAndResult = 10,
        };
        public MySpaceBodyNoiseDefinition SubSurfaceNoise { get; set; } = new MySpaceBodyNoiseDefinition()
        {
            MinNoiseAndResult = 1,
            MaxNoiseAndResult = 9,
        };
        public MySpaceBodyNoiseDefinition TemperatureNoise { get; set; } = new MySpaceBodyNoiseDefinition()
        {
            Constant = 0.5f
        };
        public MySpaceBodyNoiseDefinition HumidityNoise { get; set; } = new MySpaceBodyNoiseDefinition()
        {
            Constant = 0.5f
        };
        public MySpaceBodyNoiseDefinition ColorNoise { get; set; } = new MySpaceBodyNoiseDefinition()
        {
            MinNoiseAndResult = 0,
            MaxNoiseAndResult = 1,
        };

        public override bool Checkup()
        {
            return base.Checkup() && AssertSet(nameof(SurfaceA), SurfaceA) && AssertSet(nameof(SurfaceB), SurfaceB)
                && AssertSet(nameof(SubSurface), SubSurface) && AssertSet(nameof(Underground), Underground)
                && AssertSet(nameof(SurfaceSizeMin), SurfaceSizeMin, -1f) && AssertSet(nameof(SurfaceSizeMax), SurfaceSizeMax, -1f);
        }

        public override void Init()
        {
            base.Init();
            foreach (var item in Locations)
            {
                _definitions[(int)item].Add(this);
                _definitions[(int)item].Sort();
            }
        }

        public override MySpaceBodyGenerator CreateNew()
        {
            return new MySpaceBodyGenerator()
            {
                DistanceFromBorderToFullHeightCoef = DistanceFromBorderToFullHeightCoef,
                Color = new MySpaceBodyColor(BaseColor, MySpaceBodyColor.Load(SurfaceA), MySpaceBodyColor.Load(SurfaceB), MySpaceBodyColor.Load(SubSurface), MySpaceBodyColor.Load(Underground), DefinitionAsKey, TemperatureMin, TemperatureMax, HumididyMin, HumididyMax),
                ContinentNoise = ContinentNoise.GetNoiseGetter(),
                HillNoise = HillNoise.GetNoiseGetter(),
                SubSurfaceHeightNoise = SubSurfaceNoise.GetNoiseGetter(),
                HumidityNoise = HumidityNoise.GetNoiseGetter(),
                TemperatureNoise = TemperatureNoise.GetNoiseGetter(),
                SurfaceColorVariationNoise = ColorNoise.GetNoiseGetter(),
            };
        }

    }
}
