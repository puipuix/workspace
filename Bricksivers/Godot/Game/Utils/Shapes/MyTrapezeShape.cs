﻿using Godot;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bricksivers.Game.Utils.Shapes
{
    public class MyTrapezeShape : ConvexPolygonShape
    {
        private Vector3 _topExtend;
        private Vector3 _bottomExtend;

        /// <summary>
        /// XZ -> Extend XZ (points: -x, x)
        /// Y -> Height from 0 (>0) (point: y)
        /// </summary>
        public Vector3 TopExtend
        {
            get => _topExtend;
            set { _topExtend = value; UpdateShape(); }
        }

        /// <summary>
        /// XZ -> Extend XZ (points: -x, x)
        /// Y -> Depth from 0 (>0) (point: -y)
        /// </summary>
        public Vector3 BottomExtend
        {
            get => _bottomExtend;
            set { _bottomExtend = value; UpdateShape(); }
        }

        public MyTrapezeShape()
        {
            _topExtend = new Vector3(0.5f, 0.5f, 0.5f);
            _bottomExtend = new Vector3(1, 0.5f, 1);
            UpdateShape();
        }

        public MyTrapezeShape(Vector3 topExtend, Vector3 bottomExtend)
        {
            _topExtend = topExtend;
            _bottomExtend = bottomExtend;
            UpdateShape();
        }

        private void UpdateShape()
        {
            Vector3[] array = new Vector3[8];
            
            // Top
            array[0] = new Vector3(-_topExtend.x, _topExtend.y, -_topExtend.z);
            array[1] = new Vector3(_topExtend.x, _topExtend.y, -_topExtend.z);
            array[2] = new Vector3(_topExtend.x, _topExtend.y, _topExtend.z);
            array[3] = new Vector3(-_topExtend.x, _topExtend.y, _topExtend.z);

            // Bottom
            array[4] = new Vector3(-_bottomExtend.x, -_bottomExtend.y, -_bottomExtend.z);
            array[5] = new Vector3(_bottomExtend.x, -_bottomExtend.y, -_bottomExtend.z);
            array[6] = new Vector3(_bottomExtend.x, -_bottomExtend.y, _bottomExtend.z);
            array[7] = new Vector3(-_bottomExtend.x, -_bottomExtend.y, _bottomExtend.z);
            

            Points = array;
        }
    }
}
