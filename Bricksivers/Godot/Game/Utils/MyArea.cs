using Bricksivers.Game.Utils;
using Godot;
using System;

public class MyArea : Area
{
	private int _objectCount = 0;

	public bool IsColliding => _objectCount > 0;

	public override void _Ready()
	{
		Connect("body_entered", this, nameof(OnBodyEnter));
		Connect("body_exited", this, nameof(OnBodyExit));
		
	}

	protected virtual void OnBodyEnter(Node body)
	{
		GD.Print("in");
		GD.Print(body);
		_objectCount++;
	}

	protected virtual void OnBodyExit(Node body)
	{
		GD.Print("out");
		_objectCount--;
	}
}
