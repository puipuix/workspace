﻿namespace Bricksivers.Game.Utils.Noises
{
    // Original c++ code link:
    // https://github.com/thomasp85/ambient
    //
    // Modified and adapter for Bricksivers by Alexis Vernes
    //
    // Original license:
    //
    // MIT License
    //
    // Copyright(c) 2017 Jordan Peck
    //
    // Permission is hereby granted, free of charge, to any person obtaining a copy
    // of this software and associated documentation files(the "Software"), to deal
    // in the Software without restriction, including without limitation the rights
    // to use, copy, modify, merge, publish, distribute, sublicense, and / or sell
    // copies of the Software, and to permit persons to whom the Software is
    // furnished to do so, subject to the following conditions :
    //
    // The above copyright notice and this permission notice shall be included in all
    // copies or substantial portions of the Software.
    //
    // THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    // IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    // FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
    // AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    // LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    // OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    // SOFTWARE.
    //
    // The developer's email is jorzixdan.me2@gzixmail.com (for great email, take
    // off every 'zix'.)
    //
    public class MyWhiteNoise : MyNoise
    {
        public override bool Can1D => true;
        public override bool Can2D => true;
        public override bool Can3D => true;
        public override bool Can4D => true;

        public MyWhiteNoise() : base() { }

        private float ValCoord1D(int seed, int x)
        {
            int n = seed;
            n ^= X_PRIME * x;

            return (n * n * n * 60493) / 2147483648f;
        }
        private float ValCoord2D(int seed, int x, int y)
        {
            int n = seed;
            n ^= X_PRIME * x;
            n ^= Y_PRIME * y;

            return (n * n * n * 60493) / 2147483648f;
        }
        private float ValCoord3D(int seed, int x, int y, int z)
        {
            int n = seed;
            n ^= X_PRIME * x;
            n ^= Y_PRIME * y;
            n ^= Z_PRIME * z;

            return (n * n * n * 60493) / 2147483648f;
        }
        private float ValCoord4D(int seed, int x, int y, int z, int w)
        {
            int n = seed;
            n ^= X_PRIME * x;
            n ^= Y_PRIME * y;
            n ^= Z_PRIME * z;
            n ^= W_PRIME * w;

            return (n * n * n * 60493) / 2147483648f;
        }

        public override float Get4D(float x, float y, float z, float w, float factor = 1)
        {
            return ValCoord4D(Seed,
                (int)((x * factor) % int.MaxValue),
                (int)((y * factor) % int.MaxValue),
                (int)((z * factor) % int.MaxValue),
                (int)((w * factor) % int.MaxValue));
        }

        public override float Get3D(float x, float y, float z, float factor = 1)
        {
            return ValCoord3D(Seed,
                (int)((x * factor) % int.MaxValue),
                (int)((y * factor) % int.MaxValue),
                (int)((z * factor) % int.MaxValue));
        }

        public override float Get2D(float x, float y, float factor = 1)
        {
            return ValCoord2D(Seed,
                (int)((x * factor) % int.MaxValue),
                (int)((y * factor) % int.MaxValue));
        }

        public override float Get1D(float x, float factor = 1)
        {
            return ValCoord1D(Seed,
                (int)((x * factor) % int.MaxValue));
        }

        public override MyNoise Copy()
        {
            return new MyWhiteNoise();
        }
    }
}