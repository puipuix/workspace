﻿namespace Bricksivers.Game.Utils.Noises
{
    public class MyConstantNoise : MyNoise
    {
        public override bool Can1D => true;
        public override bool Can2D => true;
        public override bool Can3D => true;
        public override bool Can4D => true;

        public float Constant { get; set; }

        public MyConstantNoise() : base()
        {

        }

        public override MyNoise Copy()
        {
            return new MyConstantNoise() { Constant = Constant };
        }

        public override float Get1D(float x, float factor = 1)
        {
            return Constant;
        }

        public override float Get2D(float x, float y, float factor = 1)
        {
            return Constant;
        }

        public override float Get3D(float x, float y, float z, float factor = 1)
        {
            return Constant;
        }

        public override float Get4D(float x, float y, float z, float w, float factor = 1)
        {
            return Constant;
        }
    }
}