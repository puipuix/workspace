﻿using Godot;

namespace Bricksivers.Game.Utils.Noises
{
    public class MyNoiseGetter
    {
        public enum EMyFractalType
        {
            /// <summary>
            /// The noise can generate negative value.
            /// </summary>
            FBM,
            /// <summary>
            /// The noise can't  generate negative value.
            /// </summary>
            Billow,
            /// <summary>
            /// No fractal will be used.
            /// </summary>
            None
        }

        public MyNoise Noise { get; set; }

        /// <summary>
        /// Type of fractal to use
        /// </summary>
        public EMyFractalType FractalType { get; set; } = EMyFractalType.None;

        /// <summary>
        /// Number of noise generation
        /// </summary>
        public int OctaveCount { get; set; } = 1;

        /// <summary>
        /// Will be multipled by x, y, z and w before sending to the noise.
        /// </summary>
        public float Factor { get; set; } = 1f;

        /// <summary>
        /// Will multiply the factor each octave.
        /// </summary>
        public float Lacunarity { get; set; } = 2f;

        /// <summary>
        /// Will be multiplied to himself and to the noise result each octave.
        /// </summary>
        public float Gain { get; set; } = 0.5f;

        /// <summary>
        /// Power of the noise generation. e.g. : <code>Mathf.Pow(Get2D(x, y, factor), steepness)</code>
        /// </summary>
        public float Steepness { get; set; } = 2f;

        /// <summary>
        /// Minimum value the Noise can output
        /// </summary>
        public float MinNoiseOutput { get; set; } = -1f;

        /// <summary>
        /// Maximum value the Noise can output
        /// </summary>
        public float MaxNoiseOutput { get; set; } = 1f;

        /// <summary>
        /// Get_D will return the maximum value between <see cref="MinNoiseOutput"/> and the noise return value.
        /// </summary>
        public float MinResultOutput { get; set; } = -1f;

        /// <summary>
        /// Get_D will return the minimum value between <see cref="MaxResultOutput"/> and the noise return value.
        /// </summary>
        public float MaxResultOutput { get; set; } = 1f;

        public MyNoiseGetter(MyNoise noise)
        {
            Noise = noise;
        }

        public float Get1D(float x)
        {
            switch (FractalType)
            {
                case EMyFractalType.FBM:
                    return Mathf.Clamp(MyNoise.GetInRangeFromNoiseResult(Noise.GetFractalFBM1D(x, OctaveCount, Factor, Lacunarity, Gain, Steepness), MinNoiseOutput, MaxNoiseOutput), MinResultOutput, MaxResultOutput);
                case EMyFractalType.Billow:
                    return Mathf.Clamp(Mathf.Lerp(MinNoiseOutput, MaxNoiseOutput, Noise.GetFractalBillow1D(x, OctaveCount, Factor, Lacunarity, Gain, Steepness)), MinResultOutput, MaxResultOutput);
                default:
                    return Mathf.Clamp(MyNoise.GetInRangeFromNoiseResult(Noise.Get1D(x, Factor), MinNoiseOutput, MaxNoiseOutput), MinResultOutput, MaxResultOutput);
            }
        }

        public float Get2D(float x, float y)
        {
            switch (FractalType)
            {
                case EMyFractalType.FBM:
                    return Mathf.Clamp(MyNoise.GetInRangeFromNoiseResult(Noise.GetFractalFBM2D(x, y, OctaveCount, Factor, Lacunarity, Gain, Steepness), MinNoiseOutput, MaxNoiseOutput), MinResultOutput, MaxResultOutput);
                case EMyFractalType.Billow:
                    return Mathf.Clamp(Mathf.Lerp(MinNoiseOutput, MaxNoiseOutput, Noise.GetFractalBillow2D(x, y, OctaveCount, Factor, Lacunarity, Gain, Steepness)), MinResultOutput, MaxResultOutput);
                default:
                    return Mathf.Clamp(MyNoise.GetInRangeFromNoiseResult(Noise.Get2D(x, y, Factor), MinNoiseOutput, MaxNoiseOutput), MinResultOutput, MaxResultOutput);
            }
        }

        public float Get3D(float x, float y, float z)
        {
            switch (FractalType)
            {
                case EMyFractalType.FBM:
                    return Mathf.Clamp(MyNoise.GetInRangeFromNoiseResult(Noise.GetFractalFBM3D(x, y, z, OctaveCount, Factor, Lacunarity, Gain, Steepness), MinNoiseOutput, MaxNoiseOutput), MinResultOutput, MaxResultOutput);
                case EMyFractalType.Billow:
                    return Mathf.Clamp(Mathf.Lerp(MinNoiseOutput, MaxNoiseOutput, Noise.GetFractalBillow3D(x, y, z, OctaveCount, Factor, Lacunarity, Gain, Steepness)), MinResultOutput, MaxResultOutput);
                default:
                    return Mathf.Clamp(MyNoise.GetInRangeFromNoiseResult(Noise.Get3D(x, y, z, Factor), MinNoiseOutput, MaxNoiseOutput), MinResultOutput, MaxResultOutput);
            }
        }

        public float Get4D(float x, float y, float z, float w)
        {
            switch (FractalType)
            {
                case EMyFractalType.FBM:
                    return Mathf.Clamp(MyNoise.GetInRangeFromNoiseResult(Noise.GetFractalFBM4D(x, y, z, w, OctaveCount, Factor, Lacunarity, Gain, Steepness), MinNoiseOutput, MaxNoiseOutput), MinResultOutput, MaxResultOutput);
                case EMyFractalType.Billow:
                    return Mathf.Clamp(Mathf.Lerp(MinNoiseOutput, MaxNoiseOutput, Noise.GetFractalBillow4D(x, y, z, w, OctaveCount, Factor, Lacunarity, Gain, Steepness)), MinResultOutput, MaxResultOutput);
                default:
                    return Mathf.Clamp(MyNoise.GetInRangeFromNoiseResult(Noise.Get4D(x, y, z, w, Factor), MinNoiseOutput, MaxNoiseOutput), MinResultOutput, MaxResultOutput);
            }
        }
    }
}