﻿using Godot;

namespace Bricksivers.Game.Utils.Noises
{
    // Original c++ code link:
    // https://github.com/thomasp85/ambient
    //
    // Modified and adapter for Bricksivers by Alexis Vernes
    //
    // Original license:
    //
    // MIT License
    //
    // Copyright(c) 2017 Jordan Peck
    //
    // Permission is hereby granted, free of charge, to any person obtaining a copy
    // of this software and associated documentation files(the "Software"), to deal
    // in the Software without restriction, including without limitation the rights
    // to use, copy, modify, merge, publish, distribute, sublicense, and / or sell
    // copies of the Software, and to permit persons to whom the Software is
    // furnished to do so, subject to the following conditions :
    //
    // The above copyright notice and this permission notice shall be included in all
    // copies or substantial portions of the Software.
    //
    // THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    // IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    // FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
    // AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    // LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    // OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    // SOFTWARE.
    //
    // The developer's email is jorzixdan.me2@gzixmail.com (for great email, take
    // off every 'zix'.)
    //
    public class MySimplexNoise : MyPermutationNoise
    {
        protected static readonly float F1 = (Mathf.Sqrt2 - 1) / 1f;
        protected static readonly float G1 = (2.0f - Mathf.Sqrt2) / 2f;

        protected static readonly float F2 = (Mathf.Sqrt(3) - 1) / 2f;
        protected static readonly float G2 = (3.0f - Mathf.Sqrt(3)) / 6f;

        protected static readonly float F3 = (Mathf.Sqrt(4) - 1) / 3f;
        protected static readonly float G3 = (4 - Mathf.Sqrt(4)) / 12f;

        protected static readonly float F4 = (Mathf.Sqrt(5) - 1) / 4f;
        protected static readonly float G4 = (5 - Mathf.Sqrt(5)) / 20f;

        public override bool Can1D => true;
        public override bool Can2D => true;
        public override bool Can3D => true;
        public override bool Can4D => true;
        public MySimplexNoise() : base()
        {

        }

        public override float Get4D(float x, float y, float z, float w, float factor = 1)
        {
            x *= factor;
            y *= factor;
            z *= factor;
            w *= factor;
            float n0, n1, n2, n3, n4;
            float t = (x + y + z + w) * F4;
            int i = MyMath.FastFloor(x + t);
            int j = MyMath.FastFloor(y + t);
            int k = MyMath.FastFloor(z + t);
            int l = MyMath.FastFloor(w + t);
            t = (i + j + k + l) * G4;
            float X0 = i - t;
            float Y0 = j - t;
            float Z0 = k - t;
            float W0 = l - t;
            float x0 = x - X0;
            float y0 = y - Y0;
            float z0 = z - Z0;
            float w0 = w - W0;

            int rankx = 0;
            int ranky = 0;
            int rankz = 0;
            int rankw = 0;

            if (x0 > y0)
                rankx++;
            else
                ranky++;
            if (x0 > z0)
                rankx++;
            else
                rankz++;
            if (x0 > w0)
                rankx++;
            else
                rankw++;
            if (y0 > z0)
                ranky++;
            else
                rankz++;
            if (y0 > w0)
                ranky++;
            else
                rankw++;
            if (z0 > w0)
                rankz++;
            else
                rankw++;

            int i1 = rankx >= 3 ? 1 : 0;
            int j1 = ranky >= 3 ? 1 : 0;
            int k1 = rankz >= 3 ? 1 : 0;
            int l1 = rankw >= 3 ? 1 : 0;

            int i2 = rankx >= 2 ? 1 : 0;
            int j2 = ranky >= 2 ? 1 : 0;
            int k2 = rankz >= 2 ? 1 : 0;
            int l2 = rankw >= 2 ? 1 : 0;

            int i3 = rankx >= 1 ? 1 : 0;
            int j3 = ranky >= 1 ? 1 : 0;
            int k3 = rankz >= 1 ? 1 : 0;
            int l3 = rankw >= 1 ? 1 : 0;

            float x1 = x0 - i1 + G4;
            float y1 = y0 - j1 + G4;
            float z1 = z0 - k1 + G4;
            float w1 = w0 - l1 + G4;
            float x2 = x0 - i2 + 2 * G4;
            float y2 = y0 - j2 + 2 * G4;
            float z2 = z0 - k2 + 2 * G4;
            float w2 = w0 - l2 + 2 * G4;
            float x3 = x0 - i3 + 3 * G4;
            float y3 = y0 - j3 + 3 * G4;
            float z3 = z0 - k3 + 3 * G4;
            float w3 = w0 - l3 + 3 * G4;
            float x4 = x0 - 1 + 4 * G4;
            float y4 = y0 - 1 + 4 * G4;
            float z4 = z0 - 1 + 4 * G4;
            float w4 = w0 - 1 + 4 * G4;

            t = 0.6f - x0 * x0 - y0 * y0 - z0 * z0 - w0 * w0;
            if (t < 0)
                n0 = 0;
            else
            {
                t *= t;
                n0 = t * t * GradCoord4D(i, j, k, l, x0, y0, z0, w0);
            }
            t = 0.6f - x1 * x1 - y1 * y1 - z1 * z1 - w1 * w1;
            if (t < 0)
                n1 = 0;
            else
            {
                t *= t;
                n1 = t * t * GradCoord4D(i + i1, j + j1, k + k1, l + l1, x1, y1, z1, w1);
            }
            t = 0.6f - x2 * x2 - y2 * y2 - z2 * z2 - w2 * w2;
            if (t < 0)
                n2 = 0;
            else
            {
                t *= t;
                n2 = t * t * GradCoord4D(i + i2, j + j2, k + k2, l + l2, x2, y2, z2, w2);
            }
            t = 0.6f - x3 * x3 - y3 * y3 - z3 * z3 - w3 * w3;
            if (t < 0)
                n3 = 0;
            else
            {
                t *= t;
                n3 = t * t * GradCoord4D(i + i3, j + j3, k + k3, l + l3, x3, y3, z3, w3);
            }
            t = 0.6f - x4 * x4 - y4 * y4 - z4 * z4 - w4 * w4;
            if (t < 0)
                n4 = 0;
            else
            {
                t *= t;
                n4 = t * t * GradCoord4D(i + 1, j + 1, k + 1, l + 1, x4, y4, z4, w4);
            }

            return 27 * (n0 + n1 + n2 + n3 + n4);
        }

        public override float Get3D(float x, float y, float z, float factor = 1)
        {
            x *= factor;
            y *= factor;
            z *= factor;
            float t = (x + y + z) * F3;
            int i = MyMath.FastFloor(x + t);
            int j = MyMath.FastFloor(y + t);
            int k = MyMath.FastFloor(z + t);

            t = (i + j + k) * G3;
            float X0 = i - t;
            float Y0 = j - t;
            float Z0 = k - t;

            float x0 = x - X0;
            float y0 = y - Y0;
            float z0 = z - Z0;

            int i1, j1, k1;
            int i2, j2, k2;

            if (x0 >= y0)
            {
                if (y0 >= z0)
                {
                    i1 = 1;
                    j1 = 0;
                    k1 = 0;
                    i2 = 1;
                    j2 = 1;
                    k2 = 0;
                } else if (x0 >= z0)
                {
                    i1 = 1;
                    j1 = 0;
                    k1 = 0;
                    i2 = 1;
                    j2 = 0;
                    k2 = 1;
                } else // x0 < z0
                {
                    i1 = 0;
                    j1 = 0;
                    k1 = 1;
                    i2 = 1;
                    j2 = 0;
                    k2 = 1;
                }
            } else // x0 < y0
            {
                if (y0 < z0)
                {
                    i1 = 0;
                    j1 = 0;
                    k1 = 1;
                    i2 = 0;
                    j2 = 1;
                    k2 = 1;
                } else if (x0 < z0)
                {
                    i1 = 0;
                    j1 = 1;
                    k1 = 0;
                    i2 = 0;
                    j2 = 1;
                    k2 = 1;
                } else // x0 >= z0
                {
                    i1 = 0;
                    j1 = 1;
                    k1 = 0;
                    i2 = 1;
                    j2 = 1;
                    k2 = 0;
                }
            }

            float x1 = x0 - i1 + G3;
            float y1 = y0 - j1 + G3;
            float z1 = z0 - k1 + G3;
            float x2 = x0 - i2 + 2 * G3;
            float y2 = y0 - j2 + 2 * G3;
            float z2 = z0 - k2 + 2 * G3;
            float x3 = x0 - 1 + 3 * G3;
            float y3 = y0 - 1 + 3 * G3;
            float z3 = z0 - 1 + 3 * G3;

            float n0, n1, n2, n3;

            t = 0.6f - x0 * x0 - y0 * y0 - z0 * z0;
            if (t < 0)
                n0 = 0;
            else
            {
                t *= t;
                n0 = t * t * GradCoord3D(i, j, k, x0, y0, z0);
            }

            t = 0.6f - x1 * x1 - y1 * y1 - z1 * z1;
            if (t < 0)
                n1 = 0;
            else
            {
                t *= t;
                n1 = t * t * GradCoord3D(i + i1, j + j1, k + k1, x1, y1, z1);
            }

            t = 0.6f - x2 * x2 - y2 * y2 - z2 * z2;
            if (t < 0)
                n2 = 0;
            else
            {
                t *= t;
                n2 = t * t * GradCoord3D(i + i2, j + j2, k + k2, x2, y2, z2);
            }

            t = 0.6f - x3 * x3 - y3 * y3 - z3 * z3;
            if (t < 0)
                n3 = 0;
            else
            {
                t *= t;
                n3 = t * t * GradCoord3D(i + 1, j + 1, k + 1, x3, y3, z3);
            }

            return 32 * (n0 + n1 + n2 + n3);
        }

        public override float Get2D(float x, float y, float factor = 1)
        {
            x *= factor;
            y *= factor;
            float t = (x + y) * F2;
            int i = MyMath.FastFloor(x + t);
            int j = MyMath.FastFloor(y + t);

            t = (i + j) * G2;
            float X0 = i - t;
            float Y0 = j - t;

            float x0 = x - X0;
            float y0 = y - Y0;

            int i1, j1;
            if (x0 > y0)
            {
                i1 = 1;
                j1 = 0;
            } else
            {
                i1 = 0;
                j1 = 1;
            }

            float x1 = x0 - (float)i1 + G2;
            float y1 = y0 - (float)j1 + G2;
            float x2 = x0 - 1 + 2 * G2;
            float y2 = y0 - 1 + 2 * G2;

            float n0, n1, n2;

            t = 0.5f - x0 * x0 - y0 * y0;
            if (t < 0)
                n0 = 0;
            else
            {
                t *= t;
                n0 = t * t * GradCoord2D(i, j, x0, y0);
            }

            t = 0.5f - x1 * x1 - y1 * y1;
            if (t < 0)
                n1 = 0;
            else
            {
                t *= t;
                n1 = t * t * GradCoord2D(i + i1, j + j1, x1, y1);
            }

            t = 0.5f - x2 * x2 - y2 * y2;
            if (t < 0)
                n2 = 0;
            else
            {
                t *= t;
                n2 = t * t * GradCoord2D(i + 1, j + 1, x2, y2);
            }

            return 70 * (n0 + n1 + n2);
        }

        public override float Get1D(float x, float factor = 1)
        {
            x *= factor;
            float t = x * F1;
            int i = MyMath.FastFloor(x + t);

            t = i * G1;
            float X0 = i - t;

            float x0 = x - X0;

            int i1 = 0;

            float x1 = x0 - i1 + G1;

            float n0, n1;

            t = 0.5f - x0 * x0;
            if (t < 0)
                n0 = 0;
            else
            {
                t *= t;
                n0 = t * t * GradCoord1D(i, x0);
            }

            t = 0.5f - x1 * x1;
            if (t < 0)
                n1 = 0;
            else
            {
                t *= t;
                n1 = t * t * GradCoord1D(i + i1, x1);
            }

            return 85 * (n0 + n1);
        }

        public override MyNoise Copy()
        {
            return new MySimplexNoise() { InterpolationMode = InterpolationMode, Offset = Offset };
        }
    }
}