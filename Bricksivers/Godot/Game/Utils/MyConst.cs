using Godot;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bricksivers.Game.Utils
{
    public static class MyConst
    {
        public const float rad180 = Mathf.Pi;
        public const float rad90 = Mathf.Pi / 2;
        public const float rad60 = Mathf.Pi / 3;
        public const float rad45 = Mathf.Pi / 4;

        public const float PIECE_SIZE_XZ = 0.4f, PIECE_SIZE_Y = 0.15f;
        public static readonly Vector3 PIECE_SIZE_XYZ = new Vector3(PIECE_SIZE_XZ, PIECE_SIZE_Y, PIECE_SIZE_XZ);
        public const float PIECE_RATIO_HEIGHT_PER_WIDE = PIECE_SIZE_Y / PIECE_SIZE_XZ;

        public const int COLLISION_LAYER_LOCAL_PLAYER = 9;
        public const int COLLISION_LAYER_EXTERNAL_PLAYER = 10;
        public const int COLLISION_LAYER_TRASH = 19;
        public const int COLLISION_LAYER_WORLD_CHUNK = 18;

        /// <summary>
        /// Maximum height of a hill
        /// </summary>
        public const float HILL_MAX_HEIGHT = 8000f;
        public const float HILL_HEIGHT_SURFACE_FACTOR = 0.1f;

        /// <summary>
        /// Maximum height of a hill
        /// </summary>
        public const float CONTINENT_MAX_HEIGHT = 100f;
        public const float CONTINENT_HEIGHT_SURFACE_FACTOR = 0.5f;

        public const float RIGID_MONTAIN_TEMPERATURE = 1300.0f;
        public const float LAVA_TEMPERATURE = 1800.0f;

        /// <summary>
        /// Lenght in pieces for a slot
        /// </summary>
        public const int SLOT_SIZE = 20;

        /// <summary>
        /// Distance of the center of the surface from the center if the surface size equals 1m
        /// </summary>
        public const float DISTANCE_FLAT = 1.20711f;

        /// <summary>
        /// Distance of a corner from the center if the surface size equals 1m
        /// </summary>
        public const float DISTANCE_ANGLED = 0.85355f;

        /// <summary>
        /// If multiplied by surface size, give the hill height to make a sphere
        /// </summary>
        public const float DISTANCE_SPHERE = 1.31f;

        /// <summary>
        /// If multiplied by surface size, give the hill height to make a sphere
        /// </summary>
        public const float DISTANCE_SPHERE_PER_FLAT_RATIO = DISTANCE_SPHERE / DISTANCE_FLAT;

        public static readonly float TEMPERATURE_REDUCTION_PER_PIECE_ALTITUDE = MySpaceMath.ToKalvin(100) / (10000 / PIECE_SIZE_Y);
    }
}
