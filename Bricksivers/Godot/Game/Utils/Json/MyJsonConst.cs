﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bricksivers.Game.Utils.Json
{
    public static class MyJsonConst
    {
        public static readonly JsonSerializerSettings DEFAULT_SETTING = new JsonSerializerSettings()
        {
            Formatting = Formatting.Indented,
            Converters = new List<JsonConverter>() { new MyJsonColorConverter() },

        };

        public static readonly JsonSerializer DEFAULT_SERIALIZER = new JsonSerializer()
        {
            Formatting = Formatting.Indented,
        };

        static MyJsonConst()
        {
            DEFAULT_SERIALIZER.Converters.Add(new MyJsonColorConverter());
        }
    }
}
