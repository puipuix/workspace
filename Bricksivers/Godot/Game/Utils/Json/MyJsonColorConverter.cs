﻿using Godot;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bricksivers.Game.Utils.Json
{
    public class MyJsonColorConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return objectType.Equals(typeof(Color));
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.StartObject)
            {
                Color color = new Color();
                while (reader.TokenType != JsonToken.EndObject)
                {
                    string key = reader.ReadAsString();
                    switch (key)
                    {
                        case "r": color.r = (float)reader.ReadAsDecimal().Value; break;
                        case "g": color.g = (float)reader.ReadAsDecimal().Value; break;
                        case "b": color.b = (float)reader.ReadAsDecimal().Value; break;
                        case "a": color.a = (float)reader.ReadAsDecimal().Value; break;
                        default: reader.Read(); break;
                    }
                }
                return color;
            }
            else if (reader.TokenType == JsonToken.String)
            {
                string value = (string)reader.Value;
                return value.StartsWith("#") ? new Color(value) : Color.ColorN(value);
            }
            else
            {
                return existingValue;
            }
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            writer.WriteValue("#" + ((Color)value).ToHtml());
        }
    }
}
