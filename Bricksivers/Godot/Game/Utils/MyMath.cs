﻿using Godot;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Bricksivers.Game.Utils
{
    public static class MyMath
    {

        public static void NormalizedAndLength(this Vector3 vector, out float length, out Vector3 normalized)
        {
            length = vector.Length();
            normalized = vector / Mathf.Sqrt(length);
        }
        public static void DistanceAndDirectionTo(this Vector3 from, Vector3 target, out float distance, out Vector3 normalized)
        {
            Vector3 vector3 = target - from;
            vector3.NormalizedAndLength(out distance, out normalized);
        }

        public static float Sum(this Vector2 vector)
        {
            return vector.x + vector.y;
        }

        public static float Sum(this Vector3 vector)
        {
            return vector.x + vector.y + vector.z;
        }

        public static Vector3 Clamp(this Vector3 vector, Vector3 min, Vector3 max)
        {
            return new Vector3(Mathf.Clamp(vector.x, min.x, max.x), Mathf.Clamp(vector.y, min.y, max.y), Mathf.Clamp(vector.z, min.z, max.z));
        }

        public static int From2dTo1d(MyVector2I vector, int xMaxExclude)
        {
            return vector.x + vector.y * xMaxExclude;
        }

        public static MyVector2I From1dTo2d(int x, int xMaxExclude)
        {
            return new MyVector2I(x % xMaxExclude, x / xMaxExclude);
        }

        public static float Min(this Vector2 vector)
        {
            return Math.Min(vector.x, vector.x);
        }
        public static float Max(this Vector2 vector)
        {
            return Math.Max(vector.x, vector.y);
        }

        public static float Avg(this Vector2 vec)
        {
            return vec.Sum() / 2.0f;
        }

        public static Vector2 Max(Vector2 a, Vector2 b)
        {
            return new Vector2(Math.Max(a.x, b.x), Math.Max(a.y, b.y));
        }
        public static Vector2 Min(Vector2 a, Vector2 b)
        {
            return new Vector2(Math.Min(a.x, b.x), Math.Min(a.y, b.y));
        }

        public static float Min(this Vector3 vector)
        {
            return Math.Min(Math.Min(vector.x, vector.y), vector.z);
        }
        public static float Max(this Vector3 vector)
        {
            return Math.Max(Math.Max(vector.x, vector.y), vector.z);
        }
        public static float Avg(this Vector3 vec)
        {
            return vec.Sum() / 3.0f;
        }
        public static Vector3 Max(Vector3 a, Vector3 b)
        {
            return new Vector3(Math.Max(a.x, b.x), Math.Max(a.y, b.y), Math.Max(a.z, b.z));
        }
        public static Vector3 Min(Vector3 a, Vector3 b)
        {
            return new Vector3(Math.Min(a.x, b.x), Math.Min(a.y, b.y), Math.Min(a.z, b.z));
        }

        public static double Clamp(double min, double value, double max)
        {
            return Math.Max(min, Math.Min(value, max));
        }
        public static long Clamp(long min, long value, long max)
        {
            return Math.Max(min, Math.Min(value, max));
        }


        /// <summary>
        /// Faster than <see cref="Math.Floor(double)"/>
        /// </summary>
        /// <param name="my"></param>
        /// <param name="v"></param>
        /// <returns></returns>
        public static long FastFloor(double v)
        {
            return (v >= 0) ? ((long)v) : (((long)v) - 1);
        }
        /// <summary>
        /// Faster than <see cref="Math.Floor(double)"/>
        /// </summary>
        /// <param name="my"></param>
        /// <param name="v"></param>
        /// <returns></returns>
        public static int FastFloor(float v)
        {
            return (v >= 0) ? ((int)v) : (((int)v) - 1);
        }

        public static double Lerp(double a, double b, double coef)
        {
            return a + (b - a) * coef;
        }

        public static int Lerp(int a, int b, float coef)
        {
            return a + (int)((b - a) * coef);
        }

        public static long Lerp(long a, long b, float coef)
        {
            return a + (long)((b - a) * coef);
        }

        public static Vector2 Average<T>(this IEnumerable<T> enumerable, Func<T, Vector2> selector)
        {
            Vector2 sum = Vector2.Zero;
            int count = 0;
            foreach (Vector2 v in enumerable.Select(selector))
            {
                sum += v;
                count++;
            }
            return sum / count;
        }
        public static Vector3 Average<T>(this IEnumerable<T> enumerable, Func<T, Vector3> selector)
        {
            Vector3 sum = Vector3.Zero;
            int count = 0;
            foreach (Vector3 v in enumerable.Select(selector))
            {
                sum += v;
                count++;
            }
            return sum / count;
        }

        public static Vector2 Average<T>(this IEnumerable<T> enumerable, Func<T, (Vector2, float)> selector)
        {
            Vector2 sum = Vector2.Zero;
            float count = 0;
            foreach ((Vector2 v, float w) in enumerable.Select(selector))
            {
                sum += v * w;
                count += w;
            }
            return sum / count;
        }
        public static Vector3 Average<T>(this IEnumerable<T> enumerable, Func<T, (Vector3, float)> selector)
        {
            Vector3 sum = Vector3.Zero;
            float count = 0;
            foreach ((Vector3 v, float w) in enumerable.Select(selector))
            {
                sum += v * w;
                count += w;
            }
            return sum / count;
        }

        public static Vector3 Rotate(this Vector3 vector, Vector3 xyz)
        {
            return vector.Rotated(Vector3.Back, xyz.z).Rotated(Vector3.Up, xyz.y).Rotated(Vector3.Right, xyz.x);
        }

        public static Basis Rotate(this Basis basis, Vector3 xyz)
        {
            return basis.Rotated(Vector3.Back, xyz.z).Rotated(Vector3.Up, xyz.y).Rotated(Vector3.Right, xyz.x);
        }

        public static int FastRound(float f) { return (f >= 0) ? (int)(f + 0.5f) : (int)(f - 0.5f); }
        public static float InterpHermiteFunc(float t) { return t * t * (3 - 2 * t); }
        public static float InterpQuinticFunc(float t) { return t * t * t * (t * (t * 6 - 15) + 10); }
        public static float CubicLerp(float a, float b, float c, float d, float t)
        {
            float p = (d - c) - (a - b);
            return t * t * t * p + t * t * ((a - b) - p) + t * (c - a) + b;
        }

        public static float Pow(float frequency, float v)
        {
            return (float)Math.Pow(frequency, v);
        }

        public static MyVector2I ToMyVector2I(this Vector2 vector)
        {
            return new MyVector2I((int)vector.x, (int)vector.y);
        }

        public static bool IsBetween(this float f, float min, float max)
        {
            return min < f && f < max;
        }

        public static bool IsBetween(this float f, float min, float max, bool minExclusive = false, bool maxExclusive = true)
        {
            return (minExclusive ? min < f : min <= f) && (maxExclusive ? f < max : f <= max);
        }
    }
}
