﻿using Godot;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;

namespace Bricksivers.Game.Utils
{
    public struct MyVector2I
    {
        public int x, y;

        public MyVector2I(int xy)
        {
            x = xy;
            y = xy;
        }

        public MyVector2I(int x, int y)
        {
            this.x = x;
            this.y = y;
        }

        public MyVector2I Distances(MyVector2I other)
        {
            return new MyVector2I(Math.Abs(x - other.x), Math.Abs(y - other.y));
        }

        public MyVector2I Abs()
        {
            return new MyVector2I(Math.Abs(x), Math.Abs(y));
        }

        public override string ToString()
        {
            return string.Concat("{ ", x, ", ", y, " }");
        }

        public int GetMin() => Math.Min(x, y);
        public int GetMax() => Math.Max(x, y);

        public static MyVector2I operator /(MyVector2I a, MyVector2I b)
        {
            return new MyVector2I(a.x / b.x, a.y / b.y);
        }
        public static MyVector2I operator /(MyVector2I a, int b)
        {
            return new MyVector2I(a.x / b, a.y / b);
        }
        public static MyVector2I operator %(MyVector2I a, MyVector2I b)
        {
            return new MyVector2I(a.x % b.x, a.y % b.y);
        }
        public static MyVector2I operator %(MyVector2I a, int b)
        {
            return new MyVector2I(a.x % b, a.y % b);
        }

        public static MyVector2I operator *(MyVector2I a, MyVector2I b)
        {
            return new MyVector2I(a.x * b.x, a.y * b.y);
        }
        public static MyVector2I operator *(MyVector2I a, int b)
        {
            return new MyVector2I(a.x * b, a.y * b);
        }
        public static MyVector2I operator *(int b, MyVector2I a)
        {
            return new MyVector2I(a.x * b, a.y * b);
        }

        public static MyVector2I operator +(MyVector2I a, MyVector2I b)
        {
            return new MyVector2I(a.x + b.x, a.y + b.y);
        }
        public static MyVector2I operator -(MyVector2I a, MyVector2I b)
        {
            return new MyVector2I(a.x - b.x, a.y - b.y);
        }

        public static implicit operator Vector2(MyVector2I vector)
        {
            return new Vector2(vector.x, vector.y);
        }

        public static MyVector2I One => new MyVector2I(1);
        public static MyVector2I NegOne => new MyVector2I(-1);
        public static MyVector2I Zero => new MyVector2I(0);
    }
}
