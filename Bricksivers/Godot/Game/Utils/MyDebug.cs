﻿using Godot;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection.Emit;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Bricksivers.Game.Utils
{
    public static class MyDebug
    {
        public const int MaxExceptionStackTrace = 20;

        private static string _tabs = "";

        public static void ClearTab()
        {
            _tabs = "";
        }

        public static void AddTab()
        {
            _tabs += "\t";
        }

        public static void RemoveTab()
        {
            if (_tabs.Length > 0)
            {
                _tabs = _tabs.Remove(0, 1);
            }
        }

        public static object SafeToString(this object obj)
        {
            return obj ?? "Null";
        }

        public static object[] ObjOrNullStr(params object[] objs)
        {
            for (int i = 0; i < objs.Length; i++)
            {
                objs[i] = SafeToString(objs[i]);
            }
            return objs;
        }

        private static string GetLocation(string filePath, int line)
        {
            string[] cut = filePath.Split('\\', '.');
            string result = cut[cut.Length - 2];
            if (result.Length > 17)
            {
                result = result.Substring(0, 14) + "...";
            }
            string number = line.ToString();
            return string.Concat(" (", result, ":", number, ")", new string(Enumerable.Repeat(' ', 20 - result.Length - number.Length).ToArray()));
        }

        private static string FixToStringSize(int number, int size)
        {
            string str = number.ToString();
            str += new string(Enumerable.Repeat(' ', size - str.Length).ToArray());
            return str;
        }

        private static string GetInfo(string memberName, int sourceLineNumber)
        {
            return string.Concat(DateTime.Now, " - Thread: ", 
                FixToStringSize(System.Threading.Thread.CurrentThread.ManagedThreadId, 3), 
                GetLocation(memberName, sourceLineNumber)," -> ", _tabs);
        }

        private static string GetExceptionMessage(Exception e)
        {
            return string.Concat("Caused by:\n'", e.GetType().FullName, "': ", e.Message, "\n", e.StackTrace, "\n");
        }
        
        public static void PrintLine(object what, [CallerFilePath] string memberName = "", [CallerLineNumber] int sourceLineNumber = -1)
        {
            GD.Print(GetInfo(memberName, sourceLineNumber) + SafeToString(what));
        }

        public static void PrintErr(object what, [CallerFilePath] string memberName = "", [CallerLineNumber] int sourceLineNumber = -1)
        {
            GD.PrintErr(GetInfo(memberName, sourceLineNumber) + SafeToString(what));
        }

        public static void PrintErr(Exception e, [CallerFilePath] string memberName = "", [CallerLineNumber] int sourceLineNumber = -1)
        {
            StringBuilder builder = new StringBuilder();
            Exception inner = e;
            string tab = _tabs;
            int count = 0;
            while (!(inner is null) && count < MaxExceptionStackTrace)
            {
                builder.Append(GetExceptionMessage(inner).Replace("\n", '\n' + tab));
                tab += '\t';
                inner = inner.InnerException;
                if (++count >= MaxExceptionStackTrace)
                {
                    builder.Append("...");
                }
            }
            PrintErr(builder.ToString(), memberName, sourceLineNumber);
        }

        public static void PrintLineSafe(params object[] what)
        {
            GD.Print(ObjOrNullStr(what));
        }
    }
}
