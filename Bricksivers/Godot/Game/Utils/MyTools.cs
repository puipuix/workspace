﻿using Godot;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Bricksivers.Game.Utils
{
    public static class MyTools
    {
        public static Vector3 ToLocalUnRotated(this Spatial spatial, Vector3 globalPoint) => globalPoint - spatial.GlobalTransform.origin;
        public static T GetProperty<T>(this Godot.Object godotObject, string property)
        {
            return (T)godotObject.Get(property);
        }

        public static Vector3 GetWorldLocation(this Spatial spatial)
        {
            return spatial.GlobalTransform.origin;
        }

        public static object[] ToArray(params object[] what)
        {
            return what;
        }

        public static T[] ToArray<T>(params T[] what)
        {
            return what;
        }

        public static Vector3 GetUpVector(this Spatial spatial)
        {
            return spatial.GlobalTransform.basis.y;
        }
        public static Vector3 GetRightVector(this Spatial spatial)
        {
            return spatial.GlobalTransform.basis.x;
        }
        public static Vector3 GetForwardVector(this Spatial spatial)
        {
            return -spatial.GlobalTransform.basis.z;
        }

        /// <summary>
        /// Random Singleton
        /// </summary>
        public static Random RNG { get; } = new Random();


        /// <summary>
        /// Find the type in all assembly using the name.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static Type FindType(string name)
        {
            foreach (Assembly assembly in AppDomain.CurrentDomain.GetAssemblies())
            {
                foreach (Type type in assembly.GetTypes())
                {
                    if (type.Name == name)
                    {
                        return type;
                    }
                }
            }
            return null;
        }


        /// <summary>
        /// Try to invoke and EventHandler, return false if it was null.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="handler"></param>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        public static bool SafeInvoke<T>(this EventHandler<T> handler, object sender, T args) where T : EventArgs
        {
            if (handler != null)
            {
                handler.Invoke(sender, args);
                return true;
            }
            return false;
        }

        /// <summary>
        /// Try to invoke and EventHandler if the expression is true, return false if the EventHandler was null or if expression was false.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="expression"></param>
        /// <param name="handler"></param>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        public static bool SafeInvokeIf<T>(this EventHandler<T> handler, bool expression, object sender, T args) where T : EventArgs
        {
            if (expression)
            {
                return SafeInvoke(handler, sender, args);
            } else
            {
                return false;
            }
        }

        /// <summary>
        /// Try to invoke and EventHandler, return false if it was null.
        /// </summary>
        /// <param name="handler"></param>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        public static bool SafeInvoke(this EventHandler handler, object sender, EventArgs args = null)
        {
            if (handler != null)
            {
                handler.Invoke(sender, args ?? EventArgs.Empty);
                return true;
            }
            return false;
        }

        /// <summary>
        /// Try to invoke and EventHandler if the expression is true, return false if the EventHandler was null or if expression was false.
        /// </summary>
        /// <param name="expression"></param>
        /// <param name="handler"></param>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        public static bool SafeInvokeIf(this EventHandler handler, bool expression, object sender, EventArgs args = null)
        {
            if (expression)
            {
                return SafeInvoke(handler, sender, args ?? EventArgs.Empty);
            } else
            {
                return false;
            }
        }

        /// <summary>
        /// Return of the collection is empty
        /// </summary>
        /// <param name="enumerable"></param>
        /// <returns></returns>
        public static bool IsEmptyEnumerable<T>(this IEnumerable<T> enumerable)
        {
            bool empty = true;
            foreach (var item in enumerable)
            {
                empty = false;
                break;
            }
            return empty;
        }

        /// <summary>
        /// Return of the collection is empty
        /// </summary>
        /// <param name="enumerable"></param>
        /// <returns></returns>
        public static bool IsEmptyCollection<T>(this ICollection<T> collection)
        {
            return collection.Count == 0;
        }

        /// <summary>
        /// Return of the collection is empty
        /// </summary>
        /// <param name="enumerable"></param>
        /// <returns></returns>
        public static bool IsEmptyCollection(this ICollection collection)
        {
            return collection.Count == 0;
        }

        /// <summary>
        /// Return of the collection is empty
        /// </summary>
        /// <param name="enumerable"></param>
        /// <returns></returns>
        public static T FirstOrDefault<T>(this IEnumerable<T> enumerable, T defaultValue)
        {
            foreach (var item in enumerable)
            {
                defaultValue = item;
                break;
            }
            return defaultValue;
        }

        public static T FindMax<T>(this IEnumerable<T> enumerable, Comparison<T> comparison)
        {
            bool start = true;
            T max = default;
            foreach (var item in enumerable)
            {
                if (start)
                {
                    max = item;
                } else
                {
                    if (comparison(item, max) > 0)
                    {
                        max = item;
                    }
                }
            }

            return max;
        }
        public static T FindMin<T>(this IEnumerable<T> enumerable, Comparison<T> comparison)
        {
            bool start = true;
            T max = default;
            foreach (var item in enumerable)
            {
                if (start)
                {
                    max = item;
                } else
                {
                    if (comparison(item, max) < 0)
                    {
                        max = item;
                    }
                }
            }

            return max;
        }
        public static T[] Sorted<T>(this IEnumerable<T> enumerable, Comparison<T> comparison)
        {
            T[] array = enumerable.ToArray();
            Array.Sort(array, comparison);
            return array;
        }
        public static void ForEach<T>(this IEnumerable<T> enumerable, Action<T> action)
        {
            foreach (var item in enumerable)
            {
                action.Invoke(item);
            }
        }
        public static void For<T>(this IEnumerable<T> enumerable, Action<T, int> action)
        {
            int i = 0;
            foreach (var item in enumerable)
            {
                action.Invoke(item, i++);
            }
        }
        public static string ToString<T>(this IEnumerable<T> enumerable, Func<T, string> toStringFunc)
        {
            StringBuilder builder = new StringBuilder(enumerable.GetType().ToString()).Append("{ ");
            foreach (var item in enumerable)
            {
                builder.Append(toStringFunc.Invoke(item)).Append(", ");
            }
            builder.Append(" }");
            return builder.ToString();
        }

        /// <summary>
        /// Return false if one element return true
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="enumerable"></param>
        /// <param name="predicate"></param>
        /// <returns></returns>
        public static bool None<T>(this IEnumerable<T> enumerable, Func<T, bool> predicate)
        {
            bool none = true;
            foreach (var item in enumerable)
            {
                if (predicate(item))
                {
                    none = false;
                    break;
                }
            }
            return none;
        }

        public static float NextFloat(this Random r)
        {
            return (float)r.NextDouble();
        }

        public static bool NextBool(this Random r, double trueChance = 0.5)
        {
            return r.NextDouble() < trueChance;
        }

        public static Vector2 NextVector2(this Random r)
        {
            return new Vector2(r.NextFloat(), r.NextFloat());
        }

        public static Vector3 NextVector3(this Random r)
        {
            return new Vector3(r.NextFloat(), r.NextFloat(), r.NextFloat());
        }

        public static MyVector2I NextMyVector2I(this Random r, MyVector2I? min = null, MyVector2I? max = null)
        {
            return new MyVector2I(r.Next(min?.x ?? 0, max?.x ?? int.MaxValue), r.Next(min?.x ?? 0, max?.x ?? int.MaxValue));
        }

        public static Color NextColor(this Random r)
        {
            return new Color(r.NextFloat(), r.NextFloat(), r.NextFloat());
        }

        public static void AddIfAbscent<TKey, TValue>(this Dictionary<TKey, TValue> dictionary, TKey key, TValue value)
        {
            if (!dictionary.ContainsKey(key))
            {
                dictionary.Add(key, value);
            }
        }
        public static TValue GetOrAdd<TKey, TValue>(this Dictionary<TKey, TValue> dictionary, TKey key, TValue defaultValue)
        {
            if (dictionary.TryGetValue(key, out TValue val))
            {
                return val;
            } else
            {
                dictionary.Add(key, defaultValue);
                return defaultValue;
            }
        }
        public static TValue GetOrAdd<TKey, TValue>(this Dictionary<TKey, TValue> dictionary, TKey key, Func<TValue> generator)
        {
            if (dictionary.TryGetValue(key, out TValue val))
            {
                return val;
            } else
            {
                val = generator();
                dictionary.Add(key, val);
                return val;
            }
        }

        public static void AddRange<T>(this ICollection<T> collection, IEnumerable<T> enumerable)
        {
            foreach (var v in enumerable)
            {
                collection.Add(v);
            }
        }

        public static void RemoveIf<T>(this ICollection<T> collection, Func<T, bool> selector)
        {
            var array = collection.Where(selector).ToArray();
            foreach (var v in array)
            {
                collection.Remove(v);
            }
        }

        public static TResult CustomCast<T, TResult>(this T v, Func<T, TResult> caster)
        {
            return caster(v);
        }



        public static Vector3 ToVector3(this Color color)
        {
            return new Vector3(color.r, color.g, color.b);
        }

        public static void Disable(this CollisionShape collision)
        {
            collision.Disabled = true;
            collision.Hide();
        }

        public static void Enable(this CollisionShape collision)
        {
            collision.Disabled = false;
            collision.Show();
        }


        public static int ToScan(this KeyList key) => (int)key;

        public static string RemoveJsonComment(string json)
        {
            RegEx reg = new RegEx();
            reg.Compile("(\\/\\/.*)|(\\/\\*[\\s\\S]*\\*\\/)");
            var array = reg.SearchAll(json);
            for(int i = 0; i < array.Count; i++)
            {
                RegExMatch match = array[i] as RegExMatch;
                json = json.Replace(match.GetString(), "");
            }
            return json;
        }
    }
}
