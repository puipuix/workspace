using Bricksivers.Game.Character;
using Godot;
using System;

public class MyAttachableCamera : Camera
{
	private Spatial _currentAttachmentPoint;
	private IMyCameraAttachableTo _myAttachableTo;
	public IMyCameraAttachableTo MyAttachableTo
	{
		get => _myAttachableTo;
		set {
			_myAttachableTo = value;
			_realPointIndex = -1;
			CurrentAttachmentIndex = 0;
		}
	}

	private int _realPointIndex;
	public int CurrentAttachmentIndex { get; private set; }

	public override void _Ready()
	{
		base._Ready();
		_currentAttachmentPoint = GetParentOrNull<Spatial>();
	}


	private void SwitchPoint(Spatial next, Vector3 offset, int index)
	{
		_currentAttachmentPoint?.RemoveChild(this);
		_currentAttachmentPoint = next;
		_currentAttachmentPoint?.AddChild(this);
		MyAttachableTo?.OnAttachmentPointSelected(index);
		Translation = offset;
		_realPointIndex = index;
	}

	public override void _Process(float delta)
	{
		base._Process(delta);
		if (MyAttachableTo is IMyCameraAttachableTo && MyAttachableTo.AttachmentPointCount > 0)
		{
			if (Input.IsActionJustPressed("ui_view_mode"))
			{
				CurrentAttachmentIndex = (CurrentAttachmentIndex + 1) % MyAttachableTo.AttachmentPointCount;
			}

			bool isValid = MyAttachableTo.IsAttachmentPointAviable(CurrentAttachmentIndex);
			if (!isValid && _realPointIndex != 0)
			{
				SwitchPoint(MyAttachableTo.GetAttachmentPoint(0), MyAttachableTo.GetAttachmentPointOffset(0), 0);
			} else if (isValid && _realPointIndex != CurrentAttachmentIndex)
			{
				SwitchPoint(MyAttachableTo.GetAttachmentPoint(CurrentAttachmentIndex), MyAttachableTo.GetAttachmentPointOffset(CurrentAttachmentIndex), CurrentAttachmentIndex);
			}
		} else if (_currentAttachmentPoint is Spatial)
		{
			SwitchPoint(null, Vector3.Zero, -1);
		}
	}
}
