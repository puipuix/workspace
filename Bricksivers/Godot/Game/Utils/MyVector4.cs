﻿using Godot;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bricksivers.Game.Utils
{
    public struct MyVector4
    {
        public float x, y, z, w;

        public MyVector4(int xyzw)
        {
            x = xyzw;
            y = xyzw;
            z = xyzw;
            w = xyzw;
        }

        public MyVector4(float x, float y, float z, float w)
        {
            this.x = x;
            this.y = y;
            this.z = z;
            this.w = w;
        }

        public static MyVector4 operator /(MyVector4 a, MyVector4 b)
        {
            return new MyVector4(a.x / b.x, a.y / b.y, a.z / b.z, a.w / b.w);
        }
        public static MyVector4 operator /(MyVector4 a, float b)
        {
            return new MyVector4(a.x / b, a.y / b, a.z / b, a.w / b);
        }
        public static MyVector4 operator %(MyVector4 a, MyVector4 b)
        {
            return new MyVector4(a.x % b.x, a.y % b.y, a.z % b.z, a.w % b.w);
        }
        public static MyVector4 operator %(MyVector4 a, float b)
        {
            return new MyVector4(a.x % b, a.y % b, a.z % b, a.w % b);
        }

        public static MyVector4 operator *(MyVector4 a, MyVector4 b)
        {
            return new MyVector4(a.x * b.x, a.y * b.y, a.z * b.z, a.w * b.w);
        }
        public static MyVector4 operator *(MyVector4 a, float b)
        {
            return new MyVector4(a.x * b, a.y * b, a.z * b, a.w * b);
        }
        public static MyVector4 operator *(float b, MyVector4 a)
        {
            return new MyVector4(a.x * b, a.y * b, a.z * b, a.w * b);
        }


        public static MyVector4 operator +(MyVector4 a, MyVector4 b)
        {
            return new MyVector4(a.x + b.x, a.y + b.y, a.z + b.z, a.w + b.w);
        }
        public static MyVector4 operator -(MyVector4 a, MyVector4 b)
        {
            return new MyVector4(a.x - b.x, a.y - b.y, a.z - b.z, a.w - b.w);
        }

        public MyVector4 Round()
        {
            return new MyVector4(MyMath.FastRound(x), MyMath.FastRound(y), MyMath.FastRound(z), MyMath.FastRound(w));
        }

        public float LengthSquared()
        {
            return x * x + y * y + z * z + w * w;
        }

        public float DistanceSquaredTo(MyVector4 neiPoint)
        {
            return (neiPoint - this).LengthSquared();
        }

        public float Length()
        {
            return Mathf.Sqrt(x * x + y * y + z * z + w * w);
        }

        public float Distance(MyVector4 neiPoint)
        {
            return (neiPoint - this).Length();
        }

        public static MyVector4 One => new MyVector4(1);
        public static MyVector4 NegOne => new MyVector4(-1);
        public static MyVector4 Zero => new MyVector4(0);
    }
}
