﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bricksivers.Game.Utils
{
    public static class MySpaceMath
    {
        public static float ToKalvin(float celsius) => celsius + 273.15f;
        public static float ToCelsius(float kalvin) => kalvin - 273.15f;
    }
}
