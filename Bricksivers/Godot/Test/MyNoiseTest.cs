using Bricksivers.Game.Utils;
using Bricksivers.Game.Utils.Noises;
using Bricksivers.Game.World.Generator;
using Godot;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bricksivers.Test
{
	public static class MyNoiseTest
	{

		/// <summary>
		/// Print the min and max value that can the noise return using random coordinant between 0 and maxValue
		/// </summary>
		/// <param name="noise"></param>
		/// <param name="iterations"></param>
		/// <param name="maxValueInput"></param>
		/// <param name="minValueInput"></param>
		public static void DebugMinMax4D(MyNoiseGetter noise, float maxValueInput = 1000, float minValueInput = -1000, int iterations = 1000)
		{
			float diff = maxValueInput - minValueInput;
			float min = float.PositiveInfinity, max = float.NegativeInfinity, avg = 0;
			Random r = new Random();
			for (int i = 0; i < iterations; i++)
			{
				float v = noise.Get4D(minValueInput + r.NextFloat() * diff, minValueInput + r.NextFloat() * diff, minValueInput + r.NextFloat() * diff, minValueInput + r.NextFloat() * diff);
				min = Math.Min(min, v);
				max = Math.Max(max, v);
				avg = (avg * i + v) / (i + 1);
			}
			GD.Print(noise.GetType().Name, " 4D in [", min, ";", max, "] avg: ", avg);
		}

		/// <summary>
		/// Print the min and max value that can the noise return using random coordinant between 0 and maxValue
		/// </summary>
		/// <param name="noise"></param>
		/// <param name="iterations"></param>
		/// <param name="maxValueInput"></param>
		/// <param name="minValueInput"></param>
		public static void DebugMinMax3D(MyNoiseGetter noise, float maxValueInput = 1000, float minValueInput = -1000, int iterations = 1000)
		{
			float diff = maxValueInput - minValueInput;
			float min = float.PositiveInfinity, max = float.NegativeInfinity, avg = 0;
			Random r = new Random();
			for (int i = 0; i < iterations; i++)
			{
				float v = noise.Get3D(minValueInput + r.NextFloat() * diff, minValueInput + r.NextFloat() * diff, minValueInput + r.NextFloat() * diff);
				min = Math.Min(min, v);
				max = Math.Max(max, v);
				avg = (avg * i + v) / (i + 1);
			}
			GD.Print(noise.GetType().Name, " 3D in [", min, ";", max, "] avg: ", avg);
		}

		/// <summary>
		/// Print the min and max value that can the noise return using random coordinant between 0 and maxValue
		/// </summary>
		/// <param name="noise"></param>
		/// <param name="iterations"></param>
		/// <param name="maxValueInput"></param>
		/// <param name="minValueInput"></param>
		public static void DebugMinMax2D(MyNoiseGetter noise, float maxValueInput = 1000, float minValueInput = -1000, int iterations = 1000)
		{
			float diff = maxValueInput - minValueInput;
			float min = float.PositiveInfinity, max = float.NegativeInfinity, avg = 0;
			Random r = new Random();
			for (int i = 0; i < iterations; i++)
			{
				float v = noise.Get2D(minValueInput + r.NextFloat() * diff, minValueInput + r.NextFloat() * diff);
				min = Math.Min(min, v);
				max = Math.Max(max, v);
				avg = (avg * i + v) / (i + 1);
			}
			GD.Print(noise.GetType().Name, " 2D in [", min, ";", max, "] avg: ", avg);
		}

		/// <summary>
		/// Print the min and max value that can the noise return using random coordinant between 0 and maxValue
		/// </summary>
		/// <param name="noise"></param>
		/// <param name="iterations"></param>
		/// <param name="maxValueInput"></param>
		/// <param name="minValueInput"></param>
		public static void DebugMinMax1D(MyNoiseGetter noise, float maxValueInput = 1000, float minValueInput = -1000, int iterations = 1000)
		{
			float diff = maxValueInput - minValueInput;
			float min = float.PositiveInfinity, max = float.NegativeInfinity, avg = 0;
			Random r = new Random();
			for (int i = 0; i < iterations; i++)
			{
				float v = noise.Get1D(minValueInput + r.NextFloat() * diff);
				min = Math.Min(min, v);
				max = Math.Max(max, v);
				avg = (avg * i + v) / (i + 1);
			}
			GD.Print(noise.GetType().Name, " 1D in [", min, ";", max, "] avg: ", avg);
		}

		public static void FullTestMinMax()
		{
			int seed = MyTools.RNG.Next();
			MyNoiseGetter noise = new MyNoiseGetter(new MyPerlinNoise() { Seed = seed });
			DebugMinMax1D(noise);
			DebugMinMax2D(noise);
			DebugMinMax3D(noise);
			DebugMinMax4D(noise);

			noise = new MyNoiseGetter(new MySimplexNoise() { Seed = seed });
			DebugMinMax1D(noise);
			DebugMinMax2D(noise);
			DebugMinMax3D(noise);
			DebugMinMax4D(noise);

			noise = new MyNoiseGetter(new MyWhiteNoise() { Seed = seed });
			DebugMinMax1D(noise);
			DebugMinMax2D(noise);
			DebugMinMax3D(noise);
			DebugMinMax4D(noise);

			noise = new MyNoiseGetter(new MyRandomNoise() { Seed = seed });
			DebugMinMax1D(noise);
			DebugMinMax2D(noise);
			DebugMinMax3D(noise);
			DebugMinMax4D(noise);

			noise = new MyNoiseGetter(new MyValueNoise() { Seed = seed });
			DebugMinMax1D(noise);
			DebugMinMax2D(noise);
			DebugMinMax3D(noise);
			DebugMinMax4D(noise);

			noise = new MyNoiseGetter(new MyPerlinNoise() { Seed = seed }) { FractalType = MyNoiseGetter.EMyFractalType.Billow, OctaveCount = 4 };
			DebugMinMax3D(noise);

			noise = new MyNoiseGetter(new MyPerlinNoise() { Seed = seed }) { FractalType = MyNoiseGetter.EMyFractalType.FBM, OctaveCount = 4 };
			DebugMinMax3D(noise);
		}
	}
}
