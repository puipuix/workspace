using Bricksivers.Game.Utils;
using Bricksivers.Game.Utils.Definitions;
using Bricksivers.Game.Utils.Json;
using Bricksivers.Game.World.Generator;
using Bricksivers.Test;
using Godot;
using Godot.Collections;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;

public class MyTestScene : Spatial
{
	MyChunkDisplay.MyChunkUpdateManager m;
	Label l;
	public override void _Ready()
	{
		MyDefinitionBase.LoadDefinitions();
		MyDebug.PrintLine("test");
		Input.SetMouseMode(Input.MouseMode.Captured);
		MyCharacter character = GetNode<MyCharacter>("MyCharacter");
		l = GetNode<Label>("Label");
		MyPlayer player = new MyPlayer();
		character.Player = player;
		player.IsFlying = true;
		AddChild(player);
		character.Rotation = Vector3.Zero;
		int size = MyConst.SLOT_SIZE * 2000;
		MySpaceBodyGeneratorDefinition def = MyDefinitionBase.GetDefinition<MySpaceBodyGeneratorDefinition>("SpaceBodyGenerator/SpaceBody/FloralGreen");
		MyDebug.PrintLine(def);
		MySpaceBodyGenerator generator = def.CreateNew();
		generator.Seed = 10;
		generator.Temperature = MySpaceMath.ToKalvin(20);
		generator.SurfaceSize = size;

		PackedScene scene = GD.Load<PackedScene>("res://Game/World/MyChunkDisplay.tscn");

		MySpaceBodyDisplay display = (MySpaceBodyDisplay)GD.Load<PackedScene>("res://Game/World/MySpaceBodyDisplay.tscn").Instance();
		display.SetParams(generator, scene);
		AddChild(display);
		m = display.ChunkManager;

		character.Translation = new Vector3(0, size * 0.8f, size * 0.8f);
		/*MyChunkDisplay chunkDisplay = (MyChunkDisplay)scene.Instance();
		chunkDisplay.SetParams(generator, scene, new MyChunkDisplay.MyChunkUpdateManager(), 0);
		m = chunkDisplay.ChunkManager;
		AddChild(chunkDisplay);
		AddChild(m);
		*/
		MyAttachableCamera camera = GetNode<MyAttachableCamera>("MyAttachableCamera");
		camera.MyAttachableTo = character;

	}

	public override void _Process(float delta)
	{
		base._Process(delta);
		l.Text = m.Pending + "Chunks";
	}
}
