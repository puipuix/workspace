﻿using Godot;
using GodotUtils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Px.Game
{
    public struct MyPiece
    {
        private readonly bool[,] _tile;

        private int _rotated;
        public MyPiece Rotated => EMyPiece.GetFromId(_rotated);

        public int CenterXShift { get; }
        public int CenterYShift { get; }

        public int SizeX => _tile.GetLength(0);
        public int SizeY => _tile.GetLength(1);

        /// <summary>
        /// WARNING TILE int[Y, X]
        /// </summary>
        /// <param name="tile"></param>
        /// <param name="rotated"></param>
        /// <param name="centerXshift"></param>
        /// <param name="centerYshift"></param>
        public MyPiece(int[,] tile, int rotated, int centerXshift = 0, int centerYshift = 0)
        {
            CenterXShift = centerXshift;
            CenterYShift = centerYshift;

            _rotated = rotated;
            _tile = new bool[tile.GetLength(1), tile.GetLength(0)];
            for (int x = 0; x < SizeX; x++)
            {
                for (int y = 0; y < SizeY; y++)
                {
                    // WARNING X AND Y ARE INVERTED
                    _tile[x, y] = tile[y, x] != 0;
                }
            }
        }

        public bool CanMoveTo(int i, int j)
        {
            return MyMath.IsBetween(i, 0, 9 - SizeX) && MyMath.IsBetween(j, 0, 9 - SizeY);
        }

        public bool CanPlaceOn(int i, int j, MyGrid grid)
        {
            bool ret = true;
            for (int x = 0; ret && x < SizeX; x++)
            {
                for (int y = 0; ret && y < SizeY; y++)
                {
                    ret = !_tile[x, y] || grid.IsAlive(i + x, j + y);
                }
            }
            return ret;
        }

        public void PlaceOn(int i, int j, MyGrid grid, int currentValue)
        {
            for (int x = 0; x < SizeX; x++)
            {
                for (int y = 0; y < SizeY; y++)
                {
                    if (_tile[x, y])
                    {
                        grid[x + i, y + j] -= currentValue;
                    }
                }
            }
        }

        public void EnterGrid(ref int i, ref int j, MyPiece old)
        {
            i = Mathf.Clamp(i + old.CenterXShift - CenterXShift, 0, 8 - SizeX);
            j = Mathf.Clamp(j + old.CenterYShift - CenterYShift, 0, 8 - SizeY);
        }

        public void Draw(int i, int j, MyGrid grid, int currentValue, Image img)
        {
            for (int x = 0; x < SizeX; x++)
            {
                for (int y = 0; y < SizeY; y++)
                {
                    if (_tile[x, y])
                    {
                        int xi = x + i;
                        int yj = y + j;
                        Color color;
                        if (grid.IsAlive(xi, yj))
                        {
                            if (grid.WillBeAlive(xi, yj, currentValue))
                            {
                                color = Colors.Green;
                            }
                            else
                            {
                                color = Colors.Cyan;
                            }
                        }
                        else
                        {
                            color = Colors.Blue;
                        }
                        float nextValue = grid[xi, yj] - currentValue;
                        float rate = 1.0f - Mathf.Clamp(nextValue / MyGrid.MaxValue, 0, 1);
                        img.SetPixel(xi, yj, color.Darkened(rate * 2.0f / 3.0f));
                    }
                }
            }
        }

    }

    public static class EMyPiece
    {
        public const int P2x2 = 0;
        public const int P1x4 = 1, P4x1 = 2;
        public const int PL1 = 3, PL2 = 4, PL3 = 5, PL4 = 6;
        public const int PLInv1 = 7, PLInv2 = 8, PLInv3 = 9, PLInv4 = 10;
        public const int PHP1 = 11, PHP2 = 12, PHP3 = 13, PHP4 = 14;
        public const int PS1 = 15, PS2 = 16;
        public const int PSInv1 = 17, PSInv2 = 18;

        /// <summary>
        /// WARNING X AND Y REVERSED, THE ARRAY HERE LOOK LIKE IN GAME BUT X AND Y HAVE TO BE INVERSED
        /// </summary>
        private static readonly MyPiece[] _allPieces =
        {
            new MyPiece(new int[,] {
                {1,1},
                {1,1}}, P2x2),

            // 1 X 4
            new MyPiece(new int[,] {
                {1,1,1,1}}, P4x1, 1, 0),
            new MyPiece(new int[,] {
                { 1 },
                { 1 },
                { 1 },
                { 1 }}, P1x4, 0, 1),

            // L
            new MyPiece(new int[,] {
                { 1 , 0 , 0 },
                { 1 , 1 , 1 }}, PL2, 0, 1),
            new MyPiece(new int[,] {
                { 1 , 1 },
                { 1 , 0 },
                { 1 , 0 },}, PL3, 0, 0),
            new MyPiece(new int[,] {
                { 1 , 1 , 1},
                { 0 , 0 , 1 }}, PL4, 2, 0),
            new MyPiece(new int[,] {
                { 0 , 1 },
                { 0 , 1 },
                { 1 , 1 },}, PL1, 1, 2),

            // L INV
            new MyPiece(new int[,] {
                { 1 , 1 , 1 },
                { 1 , 0 , 0 }}, PLInv2, 0, 0),
            new MyPiece(new int[,] {
                { 1 , 1 },
                { 0 , 1 },
                { 0 , 1 },}, PLInv3, 1, 0),
            new MyPiece(new int[,] {
                { 0 , 0 , 1},
                { 1 , 1 , 1 }}, PLInv4, 2, 1),
            new MyPiece(new int[,] {
                { 1 , 0 },
                { 1 , 0 },
                { 1 , 1 },}, PLInv1, 0, 2),

             // HALF PLUS
            new MyPiece(new int[,] {
                { 1 , 1 , 1 },
                { 0 , 1 , 0 }}, PHP2, 1, 0),
            new MyPiece(new int[,] {
                { 0 , 1 },
                { 1 , 1 },
                { 0 , 1 },}, PHP3, 1, 1),
            new MyPiece(new int[,] {
                { 0 , 1 , 0},
                { 1 , 1 , 1 }}, PHP4, 1, 1),
            new MyPiece(new int[,] {
                { 1 , 0 },
                { 1 , 1 },
                { 1 , 0 },}, PHP1, 0, 1),

            // STAIR
            new MyPiece(new int[,] {
                { 1 , 1 , 0 },
                { 0 , 1 , 1 }}, PS2, 1, 0),
            new MyPiece(new int[,] {
                { 0 , 1 },
                { 1 , 1 },
                { 1 , 0 },}, PS1, 1, 1),

            // STAIR INV
            new MyPiece(new int[,] {
                { 1 , 1 , 0 },
                { 0 , 1 , 1 }}, PS2, 1, 0),
            new MyPiece(new int[,] {
                { 0 , 1 },
                { 1 , 1 },
                { 1 , 0 },}, PS1, 1, 1),
        };

        private static MyPiece[] _basePieces =
        {
            _allPieces[P2x2],
            _allPieces[P1x4],
            _allPieces[PL1],
            _allPieces[PLInv1],
            _allPieces[PHP1],
            _allPieces[PS1],
            _allPieces[PSInv1],
        };

        public static MyPiece GetFromId(int id)
        {
            return _allPieces[id];
        }

        public static MyPiece GetRandom()
        {
            MyPiece tmp = MyTools.RNG.NextElement(_basePieces);
            int rot = MyTools.RNG.Next(4);
            while (rot > 0)
            {
                tmp = tmp.Rotated;
                rot--;
            }
            return tmp;
        }
    }
}
