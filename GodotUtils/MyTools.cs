﻿using Godot;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace GodotUtils
{
    public static class MyTools
    {
        private class MyToolsSingleton : Godot.Object
        {
            public bool IsNotNullAndInstanceValid(Godot.Object obj)
            {
                return obj != null && IsInstanceValid(obj);
            }
        }

        private static readonly MyToolsSingleton _singleton = new MyToolsSingleton();

        #region Rpc

        public static void Rpc(this Node node, Action method)
        {
            node.Rpc(method.Method.Name);
        }

        public static void Rpc<T>(this Node node, Action<T> method, T arg)
        {
            node.Rpc(method.Method.Name, arg);
        }
        public static void Rpc<T, U>(this Node node, Action<T, U> method, T arg1, U arg2)
        {
            node.Rpc(method.Method.Name, arg1, arg2);
        }

        public static void Rpc<T, U, V>(this Node node, Action<T, U, V> method, T arg1, U arg2, V arg3)
        {
            node.Rpc(method.Method.Name, arg1, arg2, arg3);
        }

        public static void Rpc<T, U, V, W>(this Node node, Action<T, U, V, W> method, T arg1, U arg2, V arg3, W arg4)
        {
            node.Rpc(method.Method.Name, arg1, arg2, arg3, arg4);
        }

        public static void Rpc<T, U, V, W, X>(this Node node, Action<T, U, V, W, X> method, T arg1, U arg2, V arg3, W arg4, X arg5)
        {
            node.Rpc(method.Method.Name, arg1, arg2, arg3, arg4, arg5);
        }

        public static void RpcId(this Node node, int peerId, Action method)
        {
            node.RpcId(peerId, method.Method.Name, new object[0]);
        }

        public static void RpcId<T>(this Node node, int peerId, Action<T> method, T arg)
        {
            node.RpcId(peerId, method.Method.Name, arg);
        }

        public static void RpcId<T, U>(this Node node, int peerId, Action<T, U> method, T arg1, U arg2)
        {
            node.RpcId(peerId, method.Method.Name, arg1, arg2);
        }

        public static void RpcId<T, U, V>(this Node node, int peerId, Action<T, U, V> method, T arg1, U arg2, V arg3)
        {
            node.RpcId(peerId, method.Method.Name, arg1, arg2, arg3);
        }

        public static void RpcId<T, U, V, W>(this Node node, int peerId, Action<T, U, V, W> method, T arg1, U arg2, V arg3, W arg4)
        {
            node.RpcId(peerId, method.Method.Name, arg1, arg2, arg3, arg4);
        }

        public static void RpcId<T, U, V, W, X>(this Node node, int peerId, Action<T, U, V, W, X> method, T arg1, U arg2, V arg3, W arg4, X arg5)
        {
            node.RpcId(peerId, method.Method.Name, arg1, arg2, arg3, arg4, arg5);
        }

        #endregion

        #region Connect


        public static Error Connect(this Godot.Object obj, string signal, Godot.Object target, Action method, Godot.Collections.Array binds = null, uint flags = 0)
        {
            return obj.Connect(signal, target, method.Method.Name, binds, flags);
        }

        public static Error Connect<T>(this Godot.Object obj, string signal, Godot.Object target, Action<T> method, Godot.Collections.Array binds = null, uint flags = 0)
        {
            return obj.Connect(signal, target, method.Method.Name, binds, flags);
        }

        public static Error Connect<T, U>(this Godot.Object obj, string signal, Godot.Object target, Action<T, U> method, Godot.Collections.Array binds = null, uint flags = 0)
        {
            return obj.Connect(signal, target, method.Method.Name, binds, flags);
        }

        public static Error Connect<T, U, V>(this Godot.Object obj, string signal, Godot.Object target, Action<T, U, V> method, Godot.Collections.Array binds = null, uint flags = 0)
        {
            return obj.Connect(signal, target, method.Method.Name, binds, flags);
        }

        public static Error Connect<T, U, V, W>(this Godot.Object obj, string signal, Godot.Object target, Action<T, U, V, W> method, Godot.Collections.Array binds = null, uint flags = 0)
        {
            return obj.Connect(signal, target, method.Method.Name, binds, flags);
        }

        public static Error Connect<T, U, V, W, X>(this Godot.Object obj, string signal, Godot.Object target, Action<T, U, V, W, X> method, Godot.Collections.Array binds = null, uint flags = 0)
        {
            return obj.Connect(signal, target, method.Method.Name, binds, flags);
        }


        public static Error Connect(this Godot.Object obj, string signal, Godot.Object target, Action method, params object[] binds)
        {
            return obj.Connect(signal, target, method.Method.Name, new Godot.Collections.Array(binds));
        }

        public static Error Connect<T>(this Godot.Object obj, string signal, Godot.Object target, Action<T> method, params object[] binds)
        {
            return obj.Connect(signal, target, method.Method.Name, new Godot.Collections.Array(binds));
        }

        public static Error Connect<T, U>(this Godot.Object obj, string signal, Godot.Object target, Action<T, U> method, params object[] binds)
        {
            return obj.Connect(signal, target, method.Method.Name, new Godot.Collections.Array(binds));
        }

        public static Error Connect<T, U, V>(this Godot.Object obj, string signal, Godot.Object target, Action<T, U, V> method, params object[] binds)
        {
            return obj.Connect(signal, target, method.Method.Name, new Godot.Collections.Array(binds));
        }

        public static Error Connect<T, U, V, W>(this Godot.Object obj, string signal, Godot.Object target, Action<T, U, V, W> method, params object[] binds)
        {
            return obj.Connect(signal, target, method.Method.Name, new Godot.Collections.Array(binds));
        }

        public static Error Connect<T, U, V, W, X>(this Godot.Object obj, string signal, Godot.Object target, Action<T, U, V, W, X> method, params object[] binds)
        {
            return obj.Connect(signal, target, method.Method.Name, new Godot.Collections.Array(binds));
        }


        public static void Disconnect(this Godot.Object obj, string signal, Godot.Object target, Action method)
        {
            obj.Disconnect(signal, target, method.Method.Name);
        }

        public static void Disconnect<T>(this Godot.Object obj, string signal, Godot.Object target, Action<T> method)
        {
            obj.Disconnect(signal, target, method.Method.Name);
        }

        public static void Disconnect<T, U>(this Godot.Object obj, string signal, Godot.Object target, Action<T, U> method)
        {
            obj.Disconnect(signal, target, method.Method.Name);
        }

        public static void Disconnect<T, U, V>(this Godot.Object obj, string signal, Godot.Object target, Action<T, U, V> method)
        {
            obj.Disconnect(signal, target, method.Method.Name);
        }

        public static void Disconnect<T, U, V, W>(this Godot.Object obj, string signal, Godot.Object target, Action<T, U, V, W> method)
        {
            obj.Disconnect(signal, target, method.Method.Name);
        }

        public static void Disconnect<T, U, V, W, X>(this Godot.Object obj, string signal, Godot.Object target, Action<T, U, V, W, X> method)
        {
            obj.Disconnect(signal, target, method.Method.Name);
        }


        public static bool IsConnected(this Godot.Object obj, string signal, Godot.Object target, Action method)
        {
            return obj.IsConnected(signal, target, method.Method.Name);
        }

        public static bool IsConnected<T>(this Godot.Object obj, string signal, Godot.Object target, Action<T> method)
        {
            return obj.IsConnected(signal, target, method.Method.Name);
        }

        public static bool IsConnected<T, U>(this Godot.Object obj, string signal, Godot.Object target, Action<T, U> method)
        {
            return obj.IsConnected(signal, target, method.Method.Name);
        }

        public static bool IsConnected<T, U, V>(this Godot.Object obj, string signal, Godot.Object target, Action<T, U, V> method)
        {
            return obj.IsConnected(signal, target, method.Method.Name);
        }

        public static bool IsConnected<T, U, V, W>(this Godot.Object obj, string signal, Godot.Object target, Action<T, U, V, W> method)
        {
            return obj.IsConnected(signal, target, method.Method.Name);
        }

        public static bool IsConnected<T, U, V, W, X>(this Godot.Object obj, string signal, Godot.Object target, Action<T, U, V, W, X> method)
        {
            return obj.IsConnected(signal, target, method.Method.Name);
        }

        public static Error ConnectAction(this Godot.Object obj, string signal, Action method, bool oneShot = true, Godot.Collections.Array binds = null, uint flags = 0)
        {
            return obj.Connect(signal, new MyGodotAction(method, oneShot), "Invoke", binds, flags);
        }

        public static Error ConnectAction<T>(this Godot.Object obj, string signal, Action<T> method, bool oneShot = true, Godot.Collections.Array binds = null, uint flags = 0)
        {
            return obj.Connect(signal, new MyGodotAction<T>(method, oneShot), "Invoke", binds, flags);
        }

        public static Error ConnectAction<T, U>(this Godot.Object obj, string signal, Action<T, U> method, bool oneShot = true, Godot.Collections.Array binds = null, uint flags = 0)
        {
            return obj.Connect(signal, new MyGodotAction<T, U>(method, oneShot), "Invoke", binds, flags);
        }

        public static Error ConnectAction<T, U, V>(this Godot.Object obj, string signal, Action<T, U, V> method, bool oneShot = true, Godot.Collections.Array binds = null, uint flags = 0)
        {
            return obj.Connect(signal, new MyGodotAction<T, U, V>(method, oneShot), "Invoke", binds, flags);
        }

        public static Error ConnectAction<T, U, V, W>(this Godot.Object obj, string signal, Action<T, U, V, W> method, bool oneShot = true, Godot.Collections.Array binds = null, uint flags = 0)
        {
            return obj.Connect(signal, new MyGodotAction<T, U, V, W>(method, oneShot), "Invoke", binds, flags);
        }

        public static Error ConnectAction<T, U, V, W, X>(this Godot.Object obj, string signal, Action<T, U, V, W, X> method, bool oneShot = true, Godot.Collections.Array binds = null, uint flags = 0)
        {
            return obj.Connect(signal, new MyGodotAction<T, U, V, W, X>(method, oneShot), "Invoke", binds, flags);
        }


        #endregion

        #region CallDefered

        public static void CallDeferred(this Godot.Object obj, Action method)
        {
            obj.CallDeferred(method.Method.Name);
        }

        public static void CallDeferred<T>(this Godot.Object obj, Action<T> method, T arg1)
        {
            obj.CallDeferred(method.Method.Name, arg1);
        }

        public static void CallDeferred<T, U>(this Godot.Object obj, Action<T, U> method, T arg1, U arg2)
        {
            obj.CallDeferred(method.Method.Name, arg1, arg2);
        }

        public static void CallDeferred<T, U, V>(this Godot.Object obj, Action<T, U, V> method, T arg1, U arg2, V arg3)
        {
            obj.CallDeferred(method.Method.Name, arg1, arg2, arg3);
        }

        public static void CallDeferred<T, U, V, W>(this Godot.Object obj, Action<T, U, V, W> method, T arg1, U arg2, V arg3, W arg4)
        {
            obj.CallDeferred(method.Method.Name, arg1, arg2, arg3, arg4);
        }

        public static void CallDeferred<T, U, V, W, X>(this Godot.Object obj, Action<T, U, V, W, X> method, T arg1, U arg2, V arg3, W arg4, X arg5)
        {
            obj.CallDeferred(method.Method.Name, arg1, arg2, arg3, arg4, arg5);
        }


        public static void CallDeferred<R>(this Godot.Object obj, Func<R> method)
        {
            obj.CallDeferred(method.Method.Name);
        }

        public static void CallDeferred<T, R>(this Godot.Object obj, Func<T, R> method, T arg1)
        {
            obj.CallDeferred(method.Method.Name, arg1);
        }

        public static void CallDeferred<T, U, R>(this Godot.Object obj, Func<T, U, R> method, T arg1, U arg2)
        {
            obj.CallDeferred(method.Method.Name, arg1, arg2);
        }

        public static void CallDeferred<T, U, V, R>(this Godot.Object obj, Func<T, U, V, R> method, T arg1, U arg2, V arg3)
        {
            obj.CallDeferred(method.Method.Name, arg1, arg2, arg3);
        }

        public static void CallDeferred<T, U, V, W, R>(this Godot.Object obj, Func<T, U, V, W, R> method, T arg1, U arg2, V arg3, W arg4)
        {
            obj.CallDeferred(method.Method.Name, arg1, arg2, arg3, arg4);
        }

        public static void CallDeferred<T, U, V, W, X, R>(this Godot.Object obj, Func<T, U, V, W, X, R> method, T arg1, U arg2, V arg3, W arg4, X arg5)
        {
            obj.CallDeferred(method.Method.Name, arg1, arg2, arg3, arg4, arg5);
        }

        #endregion

        #region Node Navigation

        public static bool HasParent(this Node node)
        {
            return node.GetParentOrNull<Node>() != null;
        }

        /// <summary>
        /// <para>Navigate from parent to parent to find the first ancestor of type <paramref name="ancestorType"/></para>
        /// <para>Exemple:<br>Viewport v -> StaticBody body -> CollisionShape shape -> Sprite sp</br><br>sp.<see cref="GetAncestorsOrNull{StaticBody}()"/> will return body.</br></para>
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="node"></param>
        /// <returns></returns>
        public static Node GetAncestorOrNull(this Node node, Type ancestorType)
        {
            Node obj = null;
            Node parent = node.GetParent();
            while (!(parent is Viewport) && obj is null)
            {
                if (ancestorType.IsInstanceOfType(parent))
                {
                    obj = parent;
                }
                else
                {
                    parent = parent.GetParent();
                }
            }
            return obj;
        }

        /// <summary>
        /// <para>Navigate from parent to parent to find the first ancestor of type <paramref name="ancestorType"/></para>
        /// <para>Exemple:<br>Viewport v -> StaticBody body -> CollisionShape shape -> Sprite sp</br><br>sp.<see cref="GetAncestorsOrNull{StaticBody}()"/> will return body.</br></para>
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="node"></param>
        /// <returns></returns>
        public static Node GetAncestor(this Node node, Type ancestorType)
        {
            return GetAncestorOrNull(node, ancestorType) ?? throw new KeyNotFoundException("No ancestor of type '" + ancestorType.Name + "' has been found.");
        }

        /// <summary>
        /// <para>Navigate from parent to parent to find the first ancestor of type <typeparamref name="T"/></para>
        /// <para>Exemple:<br>Viewport v -> StaticBody body -> CollisionShape shape -> Sprite sp</br><br>sp.<see cref="GetAncestorsOrNull{StaticBody}()"/> will return body.</br></para>
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="node"></param>
        /// <returns></returns>
        public static T GetAncestorOrNull<T>(this Node node) where T : Node
        {
            return (T)GetAncestorOrNull(node, typeof(T));
        }

        /// <summary>
        /// <para>Navigate from parent to parent to find the first ancestor of type <typeparamref name="T"/></para>
        /// <para>Exemple:<br>Viewport v -> StaticBody body -> CollisionShape shape -> Sprite sp</br><br>sp.<see cref="GetAncestorsOrNull{StaticBody}()"/> will return body.</br></para>
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="node"></param>
        /// <returns></returns>
        public static T GetAncestor<T>(this Node node) where T : Node
        {
            return (T)GetAncestor(node, typeof(T));
        }

        /// <summary>
        /// <para>Remove all children of this node.</para>
        /// <para>Use <paramref name="free"/> to free the nodes </para>
        /// </summary>
        /// <param name="node"></param>
        /// <param name="free"></param>
        public static void RemoveAllChildren(this Node node, Action<Node> free = null)
        {
            foreach (Node item in node.GetChildren())
            {
                node.RemoveChild(item);
                free?.Invoke(item);
            }
        }

        /// <summary>
        /// <para>Navigate from child to child's child to find the first descendant of type <typeparamref name="T"/> with the name <paramref name="name"/>.</para>
        /// <para>Exemple:
        /// <br>Viewport v -> StaticBody body -> CollisionShape shape -> Sprite sp</br>
        /// <br>body.<see cref="GetDescendantOrNull{Sprite}('sp')"/> will return sp.</br></para>
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="node"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public static T GetDescendantOrNull<T>(this Node node, string name) where T : Node
        {
            var pending = new Queue(node.GetChildren());
            while (!pending.IsEmptyCollection())
            {
                Node n = (Node)pending.Dequeue();
                if (n is T t && t.Name == name)
                {
                    pending.Clear();
                    return t;
                }
                else
                {
                    pending.EnqueueRange(n.GetChildren());
                }
            }
            pending.Clear();
            return null;
        }

        /// <summary>
        /// <para>Navigate from child to child's child to find the first descendant of type <typeparamref name="T"/> with the name <paramref name="name"/>.</para>
        /// <para>Exemple:
        /// <br>Viewport v -> StaticBody body -> CollisionShape shape -> Sprite sp</br>
        /// <br>body.<see cref="GetDescendantOrNull{Sprite}('sp')"/> will return sp.</br></para>
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="node"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public static T GetDescendant<T>(this Node node, string name) where T : Node
        {
            return GetDescendantOrNull<T>(node, name) ?? throw new KeyNotFoundException("No node of type '" + typeof(T).Name + "' with name " + name + " has been found.");
        }

        /// <summary>
        /// <para>Navigate from child to child's child to find the first descendant with the name <paramref name="name"/>.</para>
        /// <para>Exemple:
        /// <br>Viewport v -> StaticBody body -> CollisionShape shape -> Sprite sp</br>
        /// <br>body.<see cref="GetDescendantOrNull{Sprite}('sp')"/> will return sp.</br></para>
        /// </summary>
        /// <param name="node"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public static Node GetDescendantOrNull(this Node node, string name)
        {
            return GetDescendantOrNull<Node>(node, name);
        }

        /// <summary>
        /// <para>Navigate from child to child's child to find the first descendant with the name <paramref name="name"/>.</para>
        /// <para>Exemple:
        /// <br>Viewport v -> StaticBody body -> CollisionShape shape -> Sprite sp</br>
        /// <br>body.<see cref="GetDescendantOrNull{Sprite}('sp')"/> will return sp.</br></para>
        /// </summary>
        /// <param name="node"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public static Node GetDescendant(this Node node, string name)
        {
            return GetDescendantOrNull(node, name) ?? throw new KeyNotFoundException("No node with name '" + name + "' has been found.");
        }

        #endregion

        #region Spatial

        public static Vector3 ToLocalUnRotated(this Spatial spatial, Vector3 globalPoint) => globalPoint - spatial.GlobalTransform.origin;

        public static Vector3 GetGlobalLocation(this Spatial spatial)
        {
            return spatial.GlobalTransform.origin;
        }

        public static Vector3 GetUpVector(this Spatial spatial)
        {
            return spatial.GlobalTransform.basis.y;
        }
        public static Vector3 GetRightVector(this Spatial spatial)
        {
            return spatial.GlobalTransform.basis.x;
        }
        public static Vector3 GetForwardVector(this Spatial spatial)
        {
            return -spatial.GlobalTransform.basis.z;
        }

        #endregion

        #region Params
        public static object[] ToArray(params object[] what)
        {
            return what;
        }

        public static T[] ToArray<T>(params T[] what)
        {
            return what;
        }

        #endregion

        #region EventHandler


        /// <summary>
        /// Try to invoke and EventHandler, return false if it was null.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="handler"></param>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        public static bool SafeInvoke<T>(this EventHandler<T> handler, object sender, T args) where T : EventArgs
        {
            if (handler != null)
            {
                handler.Invoke(sender, args);
                return true;
            }
            return false;
        }


        /// <summary>
        /// Try to invoke and EventHandler if the expression is true, return false if the EventHandler was null or if expression was false.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="expression"></param>
        /// <param name="handler"></param>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        public static bool SafeInvokeIf<T>(this EventHandler<T> handler, bool expression, object sender, T args) where T : EventArgs
        {
            if (expression)
            {
                return SafeInvoke(handler, sender, args);
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Try to invoke and EventHandler, return false if it was null.
        /// </summary>
        /// <param name="handler"></param>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        public static bool SafeInvoke(this EventHandler handler, object sender, EventArgs args = null)
        {
            if (handler != null)
            {
                handler.Invoke(sender, args ?? EventArgs.Empty);
                return true;
            }
            return false;
        }

        /// <summary>
        /// Try to invoke and EventHandler if the expression is true, return false if the EventHandler was null or if expression was false.
        /// </summary>
        /// <param name="expression"></param>
        /// <param name="handler"></param>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        public static bool SafeInvokeIf(this EventHandler handler, bool expression, object sender, EventArgs args = null)
        {
            if (expression)
            {
                return SafeInvoke(handler, sender, args ?? EventArgs.Empty);
            }
            else
            {
                return false;
            }
        }

        #endregion

        #region IsEmpty

        /// <summary>
        /// Return of the collection is empty
        /// </summary>
        /// <param name="enumerable"></param>
        /// <returns></returns>
        public static bool IsEmptyEnumerable<T>(this IEnumerable<T> enumerable)
        {
            bool empty = true;
            foreach (var item in enumerable)
            {
                empty = false;
                break;
            }
            return empty;
        }

        /// <summary>
        /// Return of the collection is empty
        /// </summary>
        /// <param name="enumerable"></param>
        /// <returns></returns>
        public static bool IsEmpty<T>(this ICollection<T> collection)
        {
            return collection.Count == 0;
        }

        /// <summary>
        /// Return of the collection is empty
        /// </summary>
        /// <param name="enumerable"></param>
        /// <returns></returns>
        public static bool IsEmptyCollection(this ICollection collection)
        {
            return collection.Count == 0;
        }

        #endregion

        #region Enumerable Collection

        public static int DimensionCount(this Array array)
        {
            return array.Length > 0 ? array.Length / array.GetLength(0) : 0;
        }

        public static T[] From2dTo1d<T>(this T[,] array)
        {
            var ret = new T[array.Length];
            for (int i = 0; i < ret.Length; i++)
            {
                (int x, int y) = MyMath.From1dTo2d(i, array.GetLength(0));
                ret[i] = array[x, y];
            }
            return ret;
        }

        public static T[,] From1dTo2d<T>(this T[] array, int iMax)
        {
            var ret = new T[iMax, array.Length / iMax];
            for (int i = 0; i < array.Length; i++)
            {
                (int x, int y) = MyMath.From1dTo2d(i, iMax);
                ret[x, y] = array[i];
            }
            return ret;
        }



        public static void Clear<T>(this T[] array) where T : class
        {
            for (int i = 0; i < array.Length; i++)
            {
                array[i] = null;
            }
        }

        public static IEnumerable<T> SelectRange<T>(this IEnumerable<T> enumerable, int min, int max)
        {
            return enumerable.Skip(min).Take(max - min);
        }

        /// <summary>
        /// Return <paramref name="defaultValue"/> if <paramref name="enumerable"/> is empty.
        /// </summary>
        /// <param name="enumerable"></param>
        /// <returns></returns>
        public static T FirstOrCustomValue<T>(this IEnumerable<T> enumerable, T defaultValue)
        {
            foreach (var item in enumerable)
            {
                defaultValue = item;
                break;
            }
            return defaultValue;
        }

        public static T FindMax<T>(this IEnumerable<T> enumerable, Comparison<T> comparison)
        {
            bool start = true;
            T max = default;
            foreach (var item in enumerable)
            {
                if (start)
                {
                    max = item;
                    start = false;
                }
                else
                {
                    if (comparison(item, max) > 0)
                    {
                        max = item;
                    }
                }
            }

            return max;
        }
        public static T FindMin<T>(this IEnumerable<T> enumerable, Comparison<T> comparison)
        {
            bool start = true;
            T min = default;
            foreach (var item in enumerable)
            {
                if (start)
                {
                    min = item;
                    start = false;
                }
                else
                {
                    if (comparison(item, min) < 0)
                    {
                        min = item;
                    }
                }
            }

            return min;
        }
        public static T[] Sorted<T>(this IEnumerable<T> enumerable, Comparison<T> comparison)
        {
            T[] array = enumerable.ToArray();
            Array.Sort(array, comparison);
            return array;
        }
        public static void ForEach<T>(this IEnumerable<T> enumerable, Action<T> action)
        {
            foreach (var item in enumerable)
            {
                action.Invoke(item);
            }
        }
        public static void For<T>(this IEnumerable<T> enumerable, Action<T, int> action)
        {
            int i = 0;
            foreach (var item in enumerable)
            {
                action.Invoke(item, i++);
            }
        }
        public static string ToString<T>(this IEnumerable<T> enumerable, Func<T, string> toStringFunc)
        {
            StringBuilder builder = new StringBuilder(enumerable.GetType().ToString()).Append("{ ");
            foreach (var item in enumerable)
            {
                builder.Append(toStringFunc.Invoke(item)).Append(", ");
            }
            builder.Append(" }");
            return builder.ToString();
        }

        /// <summary>
        /// Return false if one element return true.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="enumerable"></param>
        /// <param name="predicate"></param>
        /// <returns></returns>
        public static bool None<T>(this IEnumerable<T> enumerable, Func<T, bool> predicate)
        {
            bool none = true;
            foreach (var item in enumerable)
            {
                if (predicate(item))
                {
                    none = false;
                    break;
                }
            }
            return none;
        }

        public static void AddRange<T>(this ICollection<T> collection, IEnumerable<T> enumerable)
        {
            foreach (var v in enumerable)
            {
                collection.Add(v);
            }
        }

        public static void EnqueueRange<T>(this Queue<T> queue, IEnumerable<T> enumerable)
        {
            foreach (var v in enumerable)
            {
                queue.Enqueue(v);
            }
        }

        public static void EnqueueRange(this Queue queue, IEnumerable enumerable)
        {
            foreach (var v in enumerable)
            {
                queue.Enqueue(v);
            }
        }

        public static void RemoveIf<T>(this ICollection<T> collection, Func<T, bool> selector)
        {
            var array = collection.Where(selector).ToArray();
            foreach (var v in array)
            {
                collection.Remove(v);
            }
        }


        #endregion

        #region Fill

        public static void Fill<T>(this T[] array, T element, int startIndex = 0, int endIndex = int.MaxValue)
        {
            for (int i = startIndex; i < endIndex && i < array.Length; i++)
            {
                array[i] = element;
            }
        }

        public static void Fill<T>(this List<T> array, T element, int startIndex = 0, int endIndex = int.MaxValue)
        {
            for (int i = startIndex; i < endIndex && i < array.Count; i++)
            {
                array[i] = element;
            }
        }

        public static void Fill(this Godot.Collections.Array array, object element, int startIndex = 0, int endIndex = int.MaxValue)
        {
            for (int i = startIndex; i < endIndex && i < array.Count; i++)
            {
                array[i] = element;
            }
        }

        public static void Fill<T>(this Godot.Collections.Array<T> array, T element, int startIndex = 0, int endIndex = int.MaxValue)
        {
            for (int i = startIndex; i < endIndex && i < array.Count; i++)
            {
                array[i] = element;
            }
        }

        #endregion

        #region Random

        public static T NextElement<T>(this Random rng, params T[] array)
        {
            return array[rng.Next(array.Length)];
        }

        public static T NextElement<T>(this Random rng, List<T> list)
        {
            return list[rng.Next(list.Count)];
        }

        public static T[] Shuffle<T>(this Random rng, params T[] array)
        {
            int n = array.Length;
            while (n > 1)
            {
                int k = rng.Next(n--);
                T temp = array[n];
                array[n] = array[k];
                array[k] = temp;
            }
            return array;
        }

        public static List<T> Shuffle<T>(this Random rng, List<T> list)
        {
            int n = list.Count;
            while (n > 1)
            {
                int k = rng.Next(n--);
                T temp = list[n];
                list[n] = list[k];
                list[k] = temp;
            }
            return list;
        }

        /// <summary>
        /// Random Singleton
        /// </summary>
        public static Random RNG { get; } = new Random();

        public static float NextFloat(this Random r)
        {
            return (float)r.NextDouble();
        }

        public static float NextFloat(this Random r, float min, float max)
        {
            return min + r.NextFloat() * (max - min);
        }

        public static float NextFloat(this Random r, float max)
        {
            return r.NextFloat() * max;
        }

        public static bool NextBool(this Random r, double trueChance = 0.5)
        {
            return r.NextDouble() < trueChance;
        }

        public static Vector2 NextVector2(this Random r)
        {
            return new Vector2(r.NextFloat(), r.NextFloat());
        }

        public static Vector3 NextVector3(this Random r)
        {
            return new Vector3(r.NextFloat(), r.NextFloat(), r.NextFloat());
        }

        public static MyVector2I NextMyVector2I(this Random r, MyVector2I? min = null, MyVector2I? max = null)
        {
            return new MyVector2I(r.Next(min?.x ?? 0, max?.x ?? int.MaxValue), r.Next(min?.x ?? 0, max?.x ?? int.MaxValue));
        }

        public static Color NextColor(this Random r)
        {
            return new Color(r.NextFloat(), r.NextFloat(), r.NextFloat());
        }

        public static char NextChar(this Random r, char[] allowed = null)
        {
            return allowed is null ? (char)r.Next(char.MinValue, char.MaxValue) : r.NextElement(allowed);
        }

        public static string NextString(this Random r, int length = 16, char[] allowed = null)
        {
            var array = new char[length];
            for (int i = 0; i < array.Length; i++)
            {
                array[i] = r.NextChar(allowed);
            }
            return new string(array);
        }

        #endregion

        #region Dictionary

        public static void AddIfAbscent<TKey, TValue>(this Dictionary<TKey, TValue> dictionary, TKey key, TValue value)
        {
            if (!dictionary.ContainsKey(key))
            {
                dictionary.Add(key, value);
            }
        }

        public static TValue GetOrDefault<TKey, TValue>(this Dictionary<TKey, TValue> dictionary, TKey key, TValue defaultValue)
        {
            if (dictionary.TryGetValue(key, out TValue val))
            {
                return val;
            }
            else
            {
                return defaultValue;
            }
        }


        public static TValue GetOrDefault<TKey, TValue>(this Dictionary<TKey, TValue> dictionary, TKey key, Func<TValue> generator)
        {
            if (dictionary.TryGetValue(key, out TValue val))
            {
                return val;
            }
            else
            {
                return generator();
            }
        }


        public static TValue GetOrAdd<TKey, TValue>(this Dictionary<TKey, TValue> dictionary, TKey key, TValue defaultValue)
        {
            if (dictionary.TryGetValue(key, out TValue val))
            {
                return val;
            }
            else
            {
                dictionary.Add(key, defaultValue);
                return defaultValue;
            }
        }
        public static TValue GetOrAdd<TKey, TValue>(this Dictionary<TKey, TValue> dictionary, TKey key, Func<TValue> generator)
        {
            if (dictionary.TryGetValue(key, out TValue val))
            {
                return val;
            }
            else
            {
                val = generator();
                dictionary.Add(key, val);
                return val;
            }
        }

        #endregion

        #region Godot

        /// <summary>
        /// Return if the object is not null and is instance valid.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static bool IsAccessible(this Godot.Object obj)
        {
            return _singleton.IsNotNullAndInstanceValid(obj);
        }

        public static T GetValue<T>(this ConfigFile config, string section, string key, T defaultValue)
        {
            return (T)config.GetValue(section, key, defaultValue);
        }

        public static T GetValue<T>(this ConfigFile config, string section, string key, Func<T> defaultValueGetter)
        {
            return config.HasSectionKey(section, key) ? (T)config.GetValue(section, key) : defaultValueGetter.Invoke();
        }

        public static Vector3 ToVector3(this Color color)
        {
            return new Vector3(color.r, color.g, color.b);
        }

        public static int ToScan(this KeyList key) => (int)key;

        public static T Get<T>(this Godot.Object godotObject, string property)
        {
            return (T)godotObject.Get(property);
        }
        #endregion

        #region Collision

        public static void Disable(this CollisionShape collision)
        {
            collision.Disabled = true;
            collision.Hide();
        }

        public static void Enable(this CollisionShape collision)
        {
            collision.Disabled = false;
            collision.Show();
        }

        #endregion

        #region Other

        /// <summary>
        /// Find the type in all assembly using the name.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static Type FindType(string name)
        {
            foreach (Assembly assembly in AppDomain.CurrentDomain.GetAssemblies())
            {
                foreach (Type type in assembly.GetTypes())
                {
                    if (type.Name == name)
                    {
                        return type;
                    }
                }
            }
            return null;
        }

        public static string RemoveJsonComment(string json)
        {
            RegEx reg = new RegEx();
            reg.Compile("(\\/\\/.*)|(\\/\\*[\\s\\S]*\\*\\/)");
            var array = reg.SearchAll(json);
            for (int i = 0; i < array.Count; i++)
            {
                RegExMatch match = array[i] as RegExMatch;
                json = json.Replace(match.GetString(), "");
            }
            return json;
        }

        #endregion
    }
}
