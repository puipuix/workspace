﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GodotUtils.Json
{
    public static class MyJsonConst
    {
        public static JsonConverter[] Converters => new JsonConverter[] { new MyJsonColorConverter() };

        public static readonly JsonSerializerSettings DEFAULT_SETTING = new JsonSerializerSettings()
        {
            Formatting = Formatting.Indented,
            Converters = Converters
        };

        public static readonly JsonSerializer DEFAULT_SERIALIZER = new JsonSerializer()
        {
            Formatting = Formatting.Indented,
        };

        static MyJsonConst()
        {
            DEFAULT_SERIALIZER.Converters.Add(new MyJsonColorConverter());
        }
    }
}
