﻿using Godot;
using Godot.Collections;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Array = Godot.Collections.Array;
using GodotUtils;
using System.Reflection;

namespace GodotUtils
{
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property, AllowMultiple = false)]
    public sealed class MyExportAttribute : Attribute
    {
        static bool _warningPrinted = false;
        private static System.Collections.Generic.Dictionary<Type, Variant.Type> _typeTovariant = new System.Collections.Generic.Dictionary<Type, Variant.Type>()
        {
            [typeof(bool)] = Variant.Type.Bool,
            [typeof(int)] = Variant.Type.Int,
            [typeof(float)] = Variant.Type.Real,
            [typeof(string)] = Variant.Type.String,
            [typeof(Vector2)] = Variant.Type.Vector2,
            [typeof(Rect2)] = Variant.Type.Rect2,
            [typeof(Vector3)] = Variant.Type.Vector3,
            [typeof(Transform2D)] = Variant.Type.Transform2d,
            [typeof(Plane)] = Variant.Type.Plane,
            [typeof(Quat)] = Variant.Type.Quat,
            [typeof(AABB)] = Variant.Type.Aabb,
            [typeof(Basis)] = Variant.Type.Basis,
            [typeof(Transform)] = Variant.Type.Transform,
            [typeof(Color)] = Variant.Type.Color,
            [typeof(NodePath)] = Variant.Type.NodePath,
            [typeof(RID)] = Variant.Type.Rid,
            [typeof(Godot.Object)] = Variant.Type.Object,
            [typeof(Dictionary)] = Variant.Type.Dictionary,
            [typeof(Array)] = Variant.Type.Array,
            [typeof(byte[])] = Variant.Type.RawArray,
            [typeof(int[])] = Variant.Type.IntArray,
            [typeof(float[])] = Variant.Type.RealArray,
            [typeof(string[])] = Variant.Type.StringArray,
            [typeof(Vector2[])] = Variant.Type.Vector2Array,
            [typeof(Vector3[])] = Variant.Type.Vector3Array,
            [typeof(Color[])] = Variant.Type.ColorArray,
            [typeof(Array<byte>)] = Variant.Type.RawArray,
            [typeof(Array<int>)] = Variant.Type.IntArray,
            [typeof(Array<float>)] = Variant.Type.RealArray,
            [typeof(Array<string>)] = Variant.Type.StringArray,
            [typeof(Array<Vector2>)] = Variant.Type.Vector2Array,
            [typeof(Array<Vector3>)] = Variant.Type.Vector3Array,
            [typeof(Array<Color>)] = Variant.Type.ColorArray,
        };

        private string SubMenu { get; }
        public PropertyHint Hint { get; }
        public string HintString { get; }
        public PropertyUsageFlags Usage { get; }

        public MyExportAttribute(string subMenu = "", PropertyHint hint = PropertyHint.None, string hintString = "", PropertyUsageFlags usage = PropertyUsageFlags.Default, Type enumType = null)
        {
            SubMenu = subMenu;
            Hint = hint;
            if (hint == PropertyHint.Enum && (enumType?.IsEnum ?? false))
            {
                if (Engine.EditorHint && !_warningPrinted)
                {
                    _warningPrinted = true;
                    GD.PrintErr("Warning, enum isn't fully supported by the editor, get will return a string and set need have a string as parameter.");
                    GD.PrintErr("Parse the string to enum with Enum.Parse(string) and myEnumValue.ToString()");
                }

                StringBuilder b = new StringBuilder();
                string[] names = Enum.GetNames(enumType);
                for (int i = 0; i < names.Length; i++)
                {
                    b.Append(names[i]);
                    if (i < names.Length - 1)
                    {
                        b.Append(",");
                    }
                }
                HintString = b.ToString();
            }
            else
            {
                HintString = hintString;
            }
            Usage = usage;
        }

        private string PropertyName(string name)
        {
            return SubMenu + "/" + name;
        }

        private Variant.Type FindType(Type t)
        {
            if (t.IsEnum)
            {
                return Variant.Type.String;
            }
            else
            {
                return _typeTovariant.GetOrDefault(t, Variant.Type.Nil);
            }
        }

        public Dictionary ToDictionary(string name, Type t)
        {
            return new Dictionary()
            {
                ["name"] = PropertyName(name),
                ["type"] = FindType(t),
                ["hint"] = Hint,
                ["hint_string"] = HintString,
                ["usage"] = Usage,
            };
        }

        private static bool Get<T>(string propertyName, out object ret, IEnumerable<T> members, Func<T, Type> getMemberType, Func<T, object> getValue) where T : MemberInfo
        {
            bool find = false;
            ret = null;
            foreach (var member in members)
            {
                if (GetCustomAttribute(member, typeof(MyExportAttribute), true) is MyExportAttribute attribute)
                {
                    if (attribute.PropertyName(member.Name) == propertyName)
                    {
                        find = true;
                        ret = getValue(member);
                        if (getMemberType(member).IsEnum)
                        {
                            ret = ret?.ToString();
                        }
                    }
                }
            }
            return find;
        }

        public static bool Get(Godot.Object obj, string propertyName, out object ret)
        {
            return Get(propertyName, out ret, obj.GetType().GetProperties(), p => p.PropertyType, p => p.GetValue(obj))
                ? true
                : Get(propertyName, out ret, obj.GetType().GetFields(), f => f.FieldType, f => f.GetValue(obj));
        }

        private static bool Set<T>(string propertyName, object value, IEnumerable<T> members, Func<T, Type> getMemberType, Action<T, object> setValue) where T : MemberInfo
        {
            bool find = false;
            foreach (var member in members)
            {
                if (GetCustomAttribute(member, typeof(MyExportAttribute), true) is MyExportAttribute attribute)
                {
                    if (attribute.PropertyName(member.Name) == propertyName)
                    {
                        find = true;
                        if (getMemberType(member).IsEnum)
                        {
                            setValue(member, Enum.Parse(getMemberType(member), value.ToString()));
                        }
                        else
                        {
                            setValue(member, value);
                        }
                    }
                }
            }
            return find;
        }

        public static bool Set(Godot.Object obj, string propertyName, object value)
        {
            return Set(propertyName, value, obj.GetType().GetProperties(), p => p.PropertyType, (p, v) => p.SetValue(obj, v))
                ? true
                : Set(propertyName, value, obj.GetType().GetFields(), f => f.FieldType, (f, v) => f.SetValue(obj, v));
        }

        private static void GetPropertyList<T>(Array array, IEnumerable<T> infos, Func<T, Type> getMemberType) where T : MemberInfo
        {
            foreach (var member in infos)
            {
                if (GetCustomAttribute(member, typeof(MyExportAttribute), true) is MyExportAttribute attribute)
                {
                    array.Add(attribute.ToDictionary(member.Name, getMemberType(member)));
                }
            }
        }

        public static Array GetPropertyList(Godot.Object obj, Array array = null)
        {
            array = array ?? new Array();
            GetPropertyList(array, obj.GetType().GetProperties(), p => p.PropertyType);
            GetPropertyList(array, obj.GetType().GetFields(), f => f.FieldType);
            return array;
        }
    }
}
