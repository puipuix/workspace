﻿using Godot;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace GodotUtils
{
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property, Inherited = true, AllowMultiple = false)]
    public sealed class MyOnReadyAttribute : Attribute
    {
        public EMyNodeResearchMode ResearchMode { get; }
        public string ResearchValue { get; }
        public bool Nullable { get; }

        public MyOnReadyAttribute(string researchValue, EMyNodeResearchMode researchMode = EMyNodeResearchMode.Path, bool nullable = false)
        {
            ResearchMode = researchMode;
            ResearchValue = researchValue;
            Nullable = false;
        }

        private static void Research<T>(Node target, IEnumerable<T> memberInfos, Func<T, Type> memberTypeGetter, Action<T, Node> valueSetter) where T : MemberInfo
        {
            foreach (var (member, atr) in memberInfos
                .Select(f => (f, (MyOnReadyAttribute)GetCustomAttribute(f, typeof(MyOnReadyAttribute))))
                .Where(t => t.Item2 != null))
            {
                try
                {
                    Node n;
                    switch (atr.ResearchMode)
                    {
                        case EMyNodeResearchMode.Descendant:
                            n = atr.Nullable ? target.GetDescendantOrNull(atr.ResearchValue) : target.GetDescendant(atr.ResearchValue);
                            break;
                        case EMyNodeResearchMode.Path:
                            n = atr.Nullable ? target.GetNodeOrNull(atr.ResearchValue) : target.GetNode(atr.ResearchValue);
                            break;
                        case EMyNodeResearchMode.Parent:
                            n = atr.Nullable ? target.GetParentOrNull<Node>() : target.GetParent();
                            break;
                        case EMyNodeResearchMode.Tree:
                            n = atr.Nullable ? target.GetViewport().GetDescendantOrNull(atr.ResearchValue) : target.GetViewport().GetDescendant(atr.ResearchValue);
                            break;
                        case EMyNodeResearchMode.Ancestor:
                            n = atr.Nullable ? target.GetAncestorOrNull(memberTypeGetter(member)) : target.GetAncestor(memberTypeGetter(member));
                            break;
                        default:
                            throw new NotImplementedException("'" + atr.ResearchMode + "' isn't supported.");
                    }
                    valueSetter(member, n);
                }
                catch (Exception e)
                {
                    throw new Exception("Error setting: '" + member?.Name.SafeToString() + "' with parameter: " + atr.SafeToString(), e);
                }
            }
        }

        public override string ToString()
        {
            return string.Concat("{ Mode: ", ResearchMode, ", Value: '", ResearchValue, "', Nullable: ", Nullable, " }");
        }

        public static void SetUp(Node target)
        {
            var flag = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.DeclaredOnly;
            Research(target, target.GetType().GetFields(flag), f => f.FieldType, (f, n) => f.SetValue(target, n));
            Research(target, target.GetType().GetProperties(flag), f => f.PropertyType, (f, n) => f.SetValue(target, n));
        }
    }

    public enum EMyNodeResearchMode {
        /// <summary>
        /// Navigate from childs to child's childs. <see cref="MyOnReadyAttribute.ResearchValue"/> is the name of the node.
        /// </summary>
        Descendant,
        /// <summary>
        /// Navigate using a path. <see cref="MyOnReadyAttribute.ResearchValue"/> is the path to the node.
        /// </summary>
        Path,
        /// <summary>
        /// Return the parent. <see cref="MyOnReadyAttribute.ResearchValue"/> isn't used.
        /// </summary>
        Parent,
        /// <summary>
        /// Navigate from parent to parent's parent.  <see cref="MyOnReadyAttribute.ResearchValue"/> isn't used.
        /// </summary>
        Ancestor, 
        /// <summary>
        /// Same as <see cref="Descendant"/> but start at the tree's root.
        /// </summary>
        Tree }
}
