﻿using Godot;
using Newtonsoft.Json.Schema;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;

namespace GodotUtils
{
    public interface IMyRPCData
    {
        int GetRPCLength();
        int ToRPCValue(byte[] buffer, int index);
        int Construct(byte[] buffer, int index);
    }

    public delegate int UnStoreFunc<T>(out T value, byte[] buffer, int index);

    public delegate int StoreFunc<T>(T value, byte[] buffer, int index);

    public delegate int BufferLengthFunc<T>(T value);

    public static class MyRPCConvert
    {
        public static byte[] ToRPCValue(this IMyRPCData data)
        {
            var buffer = new byte[data.GetRPCLength()];
            data.ToRPCValue(buffer, 0);
            return buffer;
        }

        private static Exception GetExceptionMessage(Exception e, byte[] buffer, int index, int required)
        {
            if (e is OutOfMemoryException || e is IndexOutOfRangeException)
            {
                return new OutOfMemoryException("The buffer was to short: " + buffer.Length + ", Required: " + (required < 0 ? "Unknow" : (required + index).ToString()) + ".", e);
            }
            else
            {
                return e;
            }
        }

        #region Store
        public static int Store(sbyte value, byte[] buffer, int index)
        {
            try
            {
                buffer[index] = (byte)value;
                return index + 1;
            }
            catch (Exception e)
            {
                throw GetExceptionMessage(e, buffer, index, 1);
            }
        }

        public static int Store(byte value, byte[] buffer, int index)
        {
            try
            {
                buffer[index] = value;
                return index + 1;
            }
            catch (Exception e)
            {
                throw GetExceptionMessage(e, buffer, index, 1);
            }
        }
        public static int Store(bool value, byte[] buffer, int index)
        {
            try
            {
                buffer[index] = value ? (byte)1 : (byte)0;
                return index + 1;
            }
            catch (Exception e)
            {
                throw GetExceptionMessage(e, buffer, index, 1);
            }
        }

        public static int Store(short value, byte[] buffer, int index)
        {
            try
            {
                buffer[index] = (byte)(value >> 8);
                buffer[index + 1] = (byte)value;
                return index + 2;
            }
            catch (Exception e)
            {
                throw GetExceptionMessage(e, buffer, index, 2);
            }
        }

        public static int Store(char value, byte[] buffer, int index)
        {
            try
            {
                return Store((short)value, buffer, index);
            }
            catch (Exception e)
            {
                throw GetExceptionMessage(e, buffer, index, 2);
            }
        }

        public static int Store(int value, byte[] buffer, int index)
        {
            try
            {
                buffer[index] = (byte)(value >> 24);
                buffer[index + 1] = (byte)(value >> 16);
                buffer[index + 2] = (byte)(value >> 8);
                buffer[index + 3] = (byte)value;
                return index + 4;
            }
            catch (Exception e)
            {
                throw GetExceptionMessage(e, buffer, index, 4);
            }
        }

        public static int Store(long value, byte[] buffer, int index)
        {
            try
            {
                buffer[index] = (byte)(value >> 56);
                buffer[index + 1] = (byte)(value >> 48);
                buffer[index + 2] = (byte)(value >> 40);
                buffer[index + 3] = (byte)(value >> 32);
                buffer[index + 4] = (byte)(value >> 24);
                buffer[index + 5] = (byte)(value >> 16);
                buffer[index + 6] = (byte)(value >> 8);
                buffer[index + 7] = (byte)value;
                return index + 8;
            }
            catch (Exception e)
            {
                throw GetExceptionMessage(e, buffer, index, 8);
            }
        }

        public static int Store(float value, byte[] buffer, int index)
        {
            try
            {
                byte[] bin = BitConverter.GetBytes(value);
                bin.CopyTo(buffer, index);
                return index + bin.Length;
            }
            catch (Exception e)
            {
                throw GetExceptionMessage(e, buffer, index, 4);
            }
        }

        public static int Store(double value, byte[] buffer, int index)
        {
            try
            {
                byte[] bin = BitConverter.GetBytes(value);
                bin.CopyTo(buffer, index);
                return index + bin.Length;
            }
            catch (Exception e)
            {
                throw GetExceptionMessage(e, buffer, index, 8);
            }
        }

        public static int Store(Vector2 value, byte[] buffer, int index)
        {
            try
            {
                int i = Store(value.x, buffer, index);
                i = Store(value.y, buffer, i);
                return i;
            }
            catch (Exception e)
            {
                throw GetExceptionMessage(e, buffer, index, 8);
            }
        }

        public static int Store(Vector3 value, byte[] buffer, int index)
        {
            try
            {
                int i = Store(value.x, buffer, index);
                i = Store(value.y, buffer, i);
                i = Store(value.z, buffer, i);
                return i;
            }
            catch (Exception e)
            {
                throw GetExceptionMessage(e, buffer, index, 12);
            }
        }

        public static int Store(Color value, byte[] buffer, int index)
        {
            try
            {
                int i = Store(value.r, buffer, index);
                i = Store(value.g, buffer, i);
                i = Store(value.b, buffer, i);
                i = Store(value.a, buffer, i);
                return i;
            }
            catch (Exception e)
            {
                throw GetExceptionMessage(e, buffer, index, 16);
            }
        }
        public static int Store(Transform value, byte[] buffer, int index)
        {
            try
            {
                int i = Store(value.basis.Column0, buffer, index);
                i = Store(value.basis.Column1, buffer, i);
                i = Store(value.basis.Column2, buffer, i);
                i = Store(value.origin, buffer, i);
                return i;
            }
            catch (Exception e)
            {
                throw GetExceptionMessage(e, buffer, index, 48);
            }
        }
        public static int Store(Transform2D value, byte[] buffer, int index)
        {
            try
            {
                int i = Store(value.Rotation, buffer, index);
                i = Store(value.origin, buffer, i);
                return i;
            }
            catch (Exception e)
            {
                throw GetExceptionMessage(e, buffer, index, 12);
            }
        }
        public static int Store(string value, byte[] buffer, int index)
        {
            try
            {
                int i = Store(value.Length, buffer, index);
                for (int j = 0; j < value.Length; j++)
                {
                    i = Store(value[j], buffer, i);
                }
                return i;
            }
            catch (Exception e)
            {
                throw GetExceptionMessage(e, buffer, index, value.Length * 2 + 4);
            }
        }
        public static int Store<T>(T value, byte[] buffer, int index) where T : IMyRPCData
        {
            try
            {
                return value.ToRPCValue(buffer, index);
            }
            catch (Exception e)
            {
                throw GetExceptionMessage(e, buffer, index, value.GetRPCLength());
            }
        }

        public static int StoreNullable<T>(T? value, StoreFunc<T> objectToBytes, byte[] buffer, int index) where T : struct
        {
            try
            {
                int i = Store(value.HasValue, buffer, index);
                if (value.HasValue)
                {
                    i = objectToBytes(value.Value, buffer, i);
                }
                return i;
            }
            catch (Exception e)
            {
                throw GetExceptionMessage(e, buffer, index, -1);
            }
        }

        public static int StoreNullable<T>(T value, StoreFunc<T> objectToBytes, byte[] buffer, int index) where T : class
        {
            try
            {
                int i = Store(value != null, buffer, index);
                if (value != null)
                {
                    i = objectToBytes(value, buffer, i);
                }
                return i;
            }
            catch (Exception e)
            {
                throw GetExceptionMessage(e, buffer, index, -1);
            }
        }

        public static int StoreNullable<T>(T? value, byte[] buffer, int index) where T : struct, IMyRPCData
        {
            try
            {
                int i = Store(value.HasValue, buffer, index);
                if (value.HasValue)
                {
                    i = value.Value.ToRPCValue(buffer, i);
                }
                return i;
            }
            catch (Exception e)
            {
                throw GetExceptionMessage(e, buffer, index, GetNullableBufferLength(value));
            }
        }

        public static int StoreNullable<T>(T value, byte[] buffer, int index) where T : class, IMyRPCData
        {
            try
            {
                int i = Store(value != null, buffer, index);
                if (value != null)
                {
                    i = value.ToRPCValue(buffer, i);
                }
                return i;
            }
            catch (Exception e)
            {
                throw GetExceptionMessage(e, buffer, index, GetNullableBufferLength(value));
            }
        }

        public static int Store<T>(T[] array, StoreFunc<T> objectToBytes, byte[] buffer, int index)
        {
            try
            {
                int i = Store(array.Length, buffer, index);
                for (int j = 0; j < array.Length; j++)
                {
                    i = objectToBytes(array[j], buffer, i);
                }
                return i;
            }
            catch (Exception e)
            {
                throw GetExceptionMessage(e, buffer, index, -1);
            }
        }

        public static int Store<T>(T[] array, byte[] buffer, int index) where T : IMyRPCData
        {
            try
            {
                int i = Store(array.Length, buffer, index);
                for (int j = 0; j < array.Length; j++)
                {
                    i = array[j].ToRPCValue(buffer, i);
                }
                return i;
            }
            catch (Exception e)
            {
                throw GetExceptionMessage(e, buffer, index, -1);
            }
        }

        #endregion

        #region UnStore
        public static int UnStore(out byte value, byte[] buffer, int index)
        {
            try
            {
                value = buffer[index];
                return index + 1;
            }
            catch (Exception e)
            {
                throw GetExceptionMessage(e, buffer, index, 1);
            }
        }

        public static int UnStore(out sbyte value, byte[] buffer, int index)
        {
            try
            {
                value = (sbyte)buffer[index];
                return index + 1;
            }
            catch (Exception e)
            {
                throw GetExceptionMessage(e, buffer, index, 1);
            }
        }

        public static int UnStore(out bool value, byte[] buffer, int index)
        {
            try
            {
                value = (buffer[index] & 1) == 1;
                return index + 1;
            }
            catch (Exception e)
            {
                throw GetExceptionMessage(e, buffer, index, 1);
            }
        }

        public static int UnStore(out short value, byte[] buffer, int index)
        {
            try
            {
                value = (short)((buffer[index] << 8) | (buffer[index + 1]));
                return index + 2;
            }
            catch (Exception e)
            {
                throw GetExceptionMessage(e, buffer, index, 2);
            }
        }

        public static int UnStore(out char value, byte[] buffer, int index)
        {
            try
            {
                int i = UnStore(out short v, buffer, index);
                value = (char)v;
                return i;
            }
            catch (Exception e)
            {
                throw GetExceptionMessage(e, buffer, index, 2);
            }
        }

        public static int UnStore(out int value, byte[] buffer, int index)
        {
            try
            {
                value = (buffer[index] << 24) | (buffer[index + 1] << 16) | (buffer[index + 2] << 8) | (buffer[index + 3]);
                return index + 4;
            }
            catch (Exception e)
            {
                throw GetExceptionMessage(e, buffer, index, 4);
            }
        }

        public static int UnStore(out long value, byte[] buffer, int index)
        {
            try
            {
                long first = (buffer[index] << 24) | (buffer[index + 1] << 16) | (buffer[index + 2] << 8) | (buffer[index + 3]);
                long second = (buffer[index + 4] << 24) | (buffer[index + 5] << 16) | (buffer[index + 6] << 8) | (buffer[index + 7]);
                value = (first << 32) | second;
                return index + 8;
            }
            catch (Exception e)
            {
                throw GetExceptionMessage(e, buffer, index, 8);
            }
        }

        public static int UnStore(out string value, byte[] buffer, int index)
        {
            try
            {
                int i = UnStore(out int length, buffer, index);
                char[] str = new char[length];
                for (int j = 0; j < length; j++)
                {
                    i = UnStore(out char c, buffer, i);
                    str[j] = c;
                }
                value = new string(str);
                return i;
            }
            catch (Exception e)
            {
                throw GetExceptionMessage(e, buffer, index, -1);
            }
        }

        public static int UnStore(out float value, byte[] buffer, int index)
        {
            try
            {
                value = BitConverter.ToSingle(buffer, index);
                return index + 4;
            }
            catch (Exception e)
            {
                throw GetExceptionMessage(e, buffer, index, 4);
            }
        }

        public static int UnStore(out double value, byte[] buffer, int index)
        {
            try
            {
                value = BitConverter.ToDouble(buffer, index);
                return index + 8;
            }
            catch (Exception e)
            {
                throw GetExceptionMessage(e, buffer, index, 8);
            }
        }

        public static int UnStore(out Vector2 value, byte[] buffer, int index)
        {
            try
            {
                int i = UnStore(out float x, buffer, index);
                i = UnStore(out float y, buffer, i);
                value = new Vector2(x, y);
                return i;
            }
            catch (Exception e)
            {
                throw GetExceptionMessage(e, buffer, index, 8);
            }
        }

        public static int UnStore(out Vector3 value, byte[] buffer, int index)
        {
            try
            {
                int i = UnStore(out float x, buffer, index);
                i = UnStore(out float y, buffer, i);
                i = UnStore(out float z, buffer, i);
                value = new Vector3(x, y, z);
                return i;
            }
            catch (Exception e)
            {
                throw GetExceptionMessage(e, buffer, index, 12);
            }
        }


        public static int UnStore(out Color value, byte[] buffer, int index)
        {
            try
            {
                int i = UnStore(out float r, buffer, index);
                i = UnStore(out float g, buffer, i);
                i = UnStore(out float b, buffer, i);
                i = UnStore(out float a, buffer, i);
                value = new Color(r, g, b, a);
                return i;
            }
            catch (Exception e)
            {
                throw GetExceptionMessage(e, buffer, index, 16);
            }
        }
        public static int UnStore(out Transform value, byte[] buffer, int index)
        {
            try
            {
                int i = UnStore(out Vector3 column0, buffer, index);
                i = UnStore(out Vector3 column1, buffer, i);
                i = UnStore(out Vector3 column2, buffer, i);
                i = UnStore(out Vector3 origin, buffer, i);
                value = new Transform(new Basis(column0, column1, column2), origin);
                return i;
            }
            catch (Exception e)
            {
                throw GetExceptionMessage(e, buffer, index, 48);
            }
        }
        public static int UnStore(out Transform2D value, byte[] buffer, int index)
        {
            try
            {
                int i = UnStore(out float rot, buffer, index);
                i = UnStore(out Vector2 origin, buffer, i);
                value = new Transform2D(rot, origin);
                return i;
            }
            catch (Exception e)
            {
                throw GetExceptionMessage(e, buffer, index, 12);
            }
        }

        public static int UnStore<T>(out T value, byte[] buffer, int index) where T : IMyRPCData, new()
        {
            try
            {
                value = new T();
                return value.Construct(buffer, index);
            }
            catch (Exception e)
            {
                throw GetExceptionMessage(e, buffer, index, -1);
            }
        }

        public static int UnStoreNullable<T>(out T? value, UnStoreFunc<T> bytesToObject, byte[] buffer, int index) where T : struct
        {
            try
            {
                int i = UnStore(out bool hasValue, buffer, index);
                if (hasValue)
                {
                    i = bytesToObject(out T val, buffer, i);
                    value = val;
                }
                else
                {
                    value = null;
                }
                return i;
            }
            catch (Exception e)
            {
                throw GetExceptionMessage(e, buffer, index, -1);
            }
        }

        public static int UnStoreNullable<T>(out T value, UnStoreFunc<T> bytesToObject, byte[] buffer, int index) where T : class
        {
            try
            {
                int i = UnStore(out bool hasValue, buffer, index);
                if (hasValue)
                {
                    i = bytesToObject(out value, buffer, i);
                }
                else
                {
                    value = null;
                }
                return i;
            }
            catch (Exception e)
            {
                throw GetExceptionMessage(e, buffer, index, -1);
            }
        }


        public static int UnStoreNullable<T>(out T? value, byte[] buffer, int index) where T : struct, IMyRPCData
        {
            try
            {
                int i = UnStore(out bool hasValue, buffer, index);
                if (hasValue)
                {
                    T val = new T();
                    i = val.Construct(buffer, i);
                    value = val;
                }
                else
                {
                    value = null;
                }
                return i;
            }
            catch (Exception e)
            {
                throw GetExceptionMessage(e, buffer, index, -1);
            }
        }

        public static int UnStoreNullable<T>(out T value, byte[] buffer, int index) where T : class, IMyRPCData, new()
        {
            try
            {
                int i = UnStore(out bool hasValue, buffer, index);
                if (hasValue)
                {
                    value = new T();
                    i = value.Construct(buffer, i);
                }
                else
                {
                    value = null;
                }
                return i;
            }
            catch (Exception e)
            {
                throw GetExceptionMessage(e, buffer, index, -1);
            }
        }


        public static int UnStore<T>(out T[] array, UnStoreFunc<T> bytesToObject, byte[] buffer, int index)
        {
            try
            {
                int i = UnStore(out int size, buffer, index);
                array = new T[size];
                for (int j = 0; j < size; j++)
                {
                    i = bytesToObject(out T value, buffer, i);
                    array[j] = value;
                }
                return i;
            }
            catch (Exception e)
            {
                throw GetExceptionMessage(e, buffer, index, -1);
            }
        }


        public static int UnStore<T>(out T[] array, byte[] buffer, int index) where T : IMyRPCData, new()
        {
            try
            {
                int i = UnStore(out int size, buffer, index);
                array = new T[size];
                for (int j = 0; j < size; j++)
                {
                    T value = new T();
                    i = value.Construct(buffer, i);
                    array[j] = value;
                }
                return index;
            }
            catch (Exception e)
            {
                throw GetExceptionMessage(e, buffer, index, -1);
            }
        }


        #endregion

        public static int GetBufferLength(IEnumerable values)
        {
            int count = 0;
            foreach (var o in values)
            {
                if (o is null)
                {
                    throw new NullReferenceException("Unnable to find the type's size of a null value.");
                }
                else if (o is long || o is double)
                {
                    count += 8;
                }
                else if (o is int | o is float)
                {
                    count += 4;
                }
                else if (o is byte || o is sbyte || o is bool)
                {
                    count += 1;
                }
                else if (o is char || o is short)
                {
                    count += 2;
                }
                else if (o is string str)
                {
                    count += 4 + str.Length * 2;
                }
                else if (o is IMyRPCData data)
                {
                    count += data.GetRPCLength();
                }
                else if (o is Vector2)
                {
                    count += 8;
                }
                else if (o is Vector3)
                {
                    count += 12;
                }
                else if (o is Transform)
                {
                    count += 48;
                }
                else if (o is Transform2D)
                {
                    count += 12;
                }
                else if (o is Color)
                {
                    count += 16;
                }
                else if (o is IEnumerable)
                {
                    count += GetArrayBufferLength((o as IEnumerable));
                }
                else
                {
                    throw new NotImplementedException("The type '" + o.GetType().FullName + "' isn't implemented.");
                }
            }
            return count;
        }

        public static int GetBufferLength(params object[] values)
        {
            return GetBufferLength((IEnumerable)values);
        }

        public static int GetArrayBufferLength(IEnumerable array)
        {
            return 4 + GetBufferLength(array);
        }

        public static int GetArrayBufferLength<T>(T[] array, BufferLengthFunc<T> getLength)
        {
            int count = 4;
            for (int i = 0; i < array.Length; i++)
            {
                count += getLength(array[i]);
            }
            return count;
        }

        public static int GetNullableBufferLength<T>(T? value) where T : struct, IMyRPCData
        {
            return 1 + (value?.GetRPCLength() ?? 0);
        }

        public static int GetNullableBufferLength<T>(T value) where T : class, IMyRPCData
        {
            return 1 + (value?.GetRPCLength() ?? 0);
        }

        public static int GetNullableBufferLength<T>(T? value, BufferLengthFunc<T> getLength) where T : struct
        {
            return 1 + (value.HasValue ? getLength(value.Value) : 0);
        }

        public static int GetNullableBufferLength<T>(T value, BufferLengthFunc<T> getLength) where T : class
        {
            return 1 + (value != null ? getLength(value) : 0);
        }

        [Obsolete]
        public static int GetNullableLength<T>(T? value, BufferLengthFunc<T> getLength) where T : struct
        {
            return GetNullableBufferLength(value, getLength);
        }

        [Obsolete]
        public static int GetNullableLength<T>(T value, BufferLengthFunc<T> getLength) where T : class
        {
            return GetNullableBufferLength(value, getLength);
        }

    }
}
