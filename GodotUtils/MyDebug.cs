﻿using Godot;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Reflection.Emit;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace GodotUtils
{
    public static class MyDebug
    {
        public const int MaxExceptionStackTrace = 20;
        public static int SourceFileSizeLimit { get; set; } = 10;
        public static int CustomTextSizeLimit { get; set; } = 10;
        public static bool PrintSourceFileAndLine { get; set; } = true;
        public static bool PrintCustomText { get; set; } = false;

        private static readonly StringBuilder _print = new StringBuilder();
        private static readonly StringBuilder _error = new StringBuilder();
        private static readonly StringBuilder _warning = new StringBuilder();
        private static int _tabs = 0;

        #region ToString
        public static object SafeToString(this object obj)
        {
            return obj ?? "Null";
        }

        public static object[] ObjOrNullStr(params object[] objs)
        {
            for (int i = 0; i < objs.Length; i++)
            {
                objs[i] = SafeToString(objs[i]);
            }
            return objs;
        }

        #endregion

        #region Tab
        public static void ClearTab()
        {
            _tabs = 0;
        }

        public static void AddTab()
        {
            _tabs++;
        }

        public static void RemoveTab()
        {
            if (_tabs > 0)
            {
                _tabs--;
            }
        }

        private static void PrintTabs(StringBuilder builder)
        {
            for (int i = 0; i < _tabs; i++)
            {
                builder.Append('\t');
            }
        }

        #endregion

        #region Flush
        private static void FlushPrint()
        {
            GD.Print(_print.ToString());
            _print.Clear();
        }

        private static void FlushError()
        {
            GD.PushError(_error.ToString());
            _error.Clear();
        }

        private static void FlushWarning()
        {
            GD.PushWarning(_warning.ToString());
            _warning.Clear();
        }

        #endregion

        #region Info

        private static void PrintInfo(StringBuilder builder, string customText, string memberName, int sourceLineNumber)
        {
            builder.Append(DateTime.Now.ToString("HH:mm:ss.fff")).Append(" - Thread: ");
            string str = System.Threading.Thread.CurrentThread.ManagedThreadId.ToString();
            for (int i = str.Length; i < 3; i++)
            {
                builder.Append(' ');
            }
            builder.Append(str);

            if (PrintSourceFileAndLine)
            {
                string[] cut = memberName.Split('\\', '.');
                string result = cut[cut.Length - 2];
                if (result.Length > SourceFileSizeLimit)
                {
                    result = result.Substring(0, SourceFileSizeLimit - 3) + "...";
                }
                string number = sourceLineNumber.ToString();
                int j = 0;
                for (int i = result.Length + number.Length; i < SourceFileSizeLimit + 3; i += 2)
                {
                    j++;
                    builder.Append(' ');
                }
                builder.Append(" (").Append(result).Append(":").Append(number).Append(")");
                for (int i = j + result.Length + number.Length; i < SourceFileSizeLimit + 3; i++)
                {
                    builder.Append(' ');
                }
            }
            if (PrintCustomText)
            {
                if (customText.Length > CustomTextSizeLimit)
                {
                    customText = customText.Substring(0, CustomTextSizeLimit - 3) + "...";
                }
                builder.Append("[");
                int j = 0;
                for (int i = customText.Length; i < CustomTextSizeLimit; i += 2)
                {
                    j++;
                    builder.Append(' ');
                }
                builder.Append(customText);
                for (int i = j + customText.Length; i < CustomTextSizeLimit; i++)
                {
                    builder.Append(' ');
                }
                builder.Append("]");
            }
            builder.Append(" -> ");
            PrintTabs(builder);
        }

        public static void PrintInfo(string customText = "", [CallerFilePath] string memberName = "", [CallerLineNumber] int sourceLineNumber = -1)
        {
            PrintInfo(_print, customText, memberName, sourceLineNumber);
            FlushPrint();
        }

        public static void PrintErrorInfo(string customText = "", [CallerFilePath] string memberName = "", [CallerLineNumber] int sourceLineNumber = -1)
        {
            PrintInfo(_error, customText, memberName, sourceLineNumber);
            FlushError();
        }

        public static void PrintWarningInfo(string customText = "", [CallerFilePath] string memberName = "", [CallerLineNumber] int sourceLineNumber = -1)
        {
            PrintInfo(_warning, customText, memberName, sourceLineNumber);
            FlushWarning();
        }

        #endregion

        public static void Print(object what, string customText = "", [CallerFilePath] string memberName = "", [CallerLineNumber] int sourceLineNumber = -1)
        {
            PrintInfo(_print, customText, memberName, sourceLineNumber);
            _print.Append(SafeToString(what));
            FlushPrint();
        }

        [Obsolete]
        public static void PrintLine(object what, string customText = "", [CallerFilePath] string memberName = "", [CallerLineNumber] int sourceLineNumber = -1)
        {
            Print(what, customText, memberName, sourceLineNumber);
        }

        public static void PrintErr(object what, string customText = "", [CallerFilePath] string memberName = "", [CallerLineNumber] int sourceLineNumber = -1)
        {
            PrintInfo(_error, customText, memberName, sourceLineNumber);
            _error.Append(SafeToString(what));
            FlushError();
        }

        public static void PrintErr(Exception e, string customText = "", [CallerFilePath] string memberName = "", [CallerLineNumber] int sourceLineNumber = -1)
        {
            PrintInfo(_error, customText, memberName, sourceLineNumber);
            _error.Append("An exception has been throwned:").AppendLine();
            Exception inner = e;
            int tab = _tabs;
            _tabs = 0;
            int count = 0;
            while (!(inner is null) && count < MaxExceptionStackTrace)
            {
                PrintTabs(_error);
                _error.Append("  ");
                if (count > 0)
                {
                    _error.Append("Caused by: '");
                }
                _error.Append(inner.GetType().FullName).Append("': ").Append(inner.Message).AppendLine();
                _tabs++;
                PrintTabs(_error);
                _error.Append(inner.StackTrace).AppendLine();
                inner = inner.InnerException;
                if (++count >= MaxExceptionStackTrace)
                {
                    PrintTabs(_error);
                    _error.Append("[...]").AppendLine();
                }
            }
            _tabs = tab;
            FlushError();
        }

        public static void PrintWarning(object what, string customText = "", [CallerFilePath] string memberName = "", [CallerLineNumber] int sourceLineNumber = -1)
        {
            PrintInfo(_warning, customText, memberName, sourceLineNumber);
            _warning.Append(SafeToString(what));
            FlushWarning();
        }
    }
}
