﻿using Godot;

namespace GodotUtils.Noises
{
    // Original c++ code link:
    // https://github.com/thomasp85/ambient
    //
    // Modified and adapter for Bricksivers by Alexis Vernes
    //
    // Original license:
    //
    // MIT License
    //
    // Copyright(c) 2017 Jordan Peck
    //
    // Permission is hereby granted, free of charge, to any person obtaining a copy
    // of this software and associated documentation files(the "Software"), to deal
    // in the Software without restriction, including without limitation the rights
    // to use, copy, modify, merge, publish, distribute, sublicense, and / or sell
    // copies of the Software, and to permit persons to whom the Software is
    // furnished to do so, subject to the following conditions :
    //
    // The above copyright notice and this permission notice shall be included in all
    // copies or substantial portions of the Software.
    //
    // THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    // IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    // FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
    // AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    // LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    // OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    // SOFTWARE.
    //
    // The developer's email is jorzixdan.me2@gzixmail.com (for great email, take
    // off every 'zix'.)
    //
    public class MyPerlinNoise : MyPermutationNoise
    {
        public override bool Can1D => true;
        public override bool Can2D => true;
        public override bool Can3D => true;
        public override bool Can4D => true;

        public MyPerlinNoise() : base()
        {

        }

        public override float Get4D(float x, float y, float z, float w, float factor = 1)
        {
            x *= factor;
            y *= factor;
            z *= factor;
            w *= factor;
            int x0 = MyMath.FastFloor(x);
            int y0 = MyMath.FastFloor(y);
            int z0 = MyMath.FastFloor(z);
            int w0 = MyMath.FastFloor(w);
            int x1 = x0 + 1;
            int y1 = y0 + 1;
            int z1 = z0 + 1;
            int w1 = w0 + 1;

            float xs, ys, zs, ws;
            switch (InterpolationMode)
            {
                case Interp.Hermite:
                    xs = MyMath.InterpHermiteFunc(x - x0);
                    ys = MyMath.InterpHermiteFunc(y - y0);
                    zs = MyMath.InterpHermiteFunc(z - z0);
                    ws = MyMath.InterpHermiteFunc(w - w0);
                    break;
                case Interp.Quintic:
                    xs = MyMath.InterpQuinticFunc(x - x0);
                    ys = MyMath.InterpQuinticFunc(y - y0);
                    zs = MyMath.InterpQuinticFunc(z - z0);
                    ws = MyMath.InterpQuinticFunc(w - w0);
                    break;
                default:
                    xs = x - x0;
                    ys = y - y0;
                    zs = z - z0;
                    ws = w - w0;
                    break;
            }

            float xd0 = x - x0;
            float yd0 = y - y0;
            float zd0 = z - z0;
            float wd0 = w - w0;
            float xd1 = xd0 - 1;
            float yd1 = yd0 - 1;
            float zd1 = zd0 - 1;
            float wd1 = wd0 - 1;

            float xf000 = Mathf.Lerp(GradCoord4D(x0, y0, z0, w0, xd0, yd0, zd0, wd0), GradCoord4D(x1, y0, z0, w0, xd1, yd0, zd0, wd0), xs);
            float xf100 = Mathf.Lerp(GradCoord4D(x0, y1, z0, w0, xd0, yd1, zd0, wd0), GradCoord4D(x1, y1, z0, w0, xd1, yd1, zd0, wd0), xs);
            float xf010 = Mathf.Lerp(GradCoord4D(x0, y0, z1, w0, xd0, yd0, zd1, wd0), GradCoord4D(x1, y0, z1, w0, xd1, yd0, zd1, wd0), xs);
            float xf110 = Mathf.Lerp(GradCoord4D(x0, y1, z1, w0, xd0, yd1, zd1, wd0), GradCoord4D(x1, y1, z1, w0, xd1, yd1, zd1, wd0), xs);
            float xf001 = Mathf.Lerp(GradCoord4D(x0, y0, z0, w1, xd0, yd0, zd0, wd1), GradCoord4D(x1, y0, z0, w1, xd1, yd0, zd0, wd1), xs);
            float xf101 = Mathf.Lerp(GradCoord4D(x0, y1, z0, w1, xd0, yd1, zd0, wd1), GradCoord4D(x1, y1, z0, w1, xd1, yd1, zd0, wd1), xs);
            float xf011 = Mathf.Lerp(GradCoord4D(x0, y0, z1, w1, xd0, yd0, zd1, wd1), GradCoord4D(x1, y0, z1, w1, xd1, yd0, zd1, wd1), xs);
            float xf111 = Mathf.Lerp(GradCoord4D(x0, y1, z1, w1, xd0, yd1, zd1, wd1), GradCoord4D(x1, y1, z1, w1, xd1, yd1, zd1, wd1), xs);


            float yf00 = Mathf.Lerp(xf000, xf100, ys);
            float yf10 = Mathf.Lerp(xf010, xf110, ys);
            float yf01 = Mathf.Lerp(xf001, xf101, ys);
            float yf11 = Mathf.Lerp(xf011, xf111, ys);

            float zf0 = Mathf.Lerp(yf00, yf10, zs);
            float zf1 = Mathf.Lerp(yf01, yf11, zs);

            return Mathf.Lerp(zf0, zf1, ws);
        }
        public override float Get3D(float x, float y, float z, float factor = 1)
        {
            x *= factor;
            y *= factor;
            z *= factor;
            int x0 = MyMath.FastFloor(x);
            int y0 = MyMath.FastFloor(y);
            int z0 = MyMath.FastFloor(z);
            int x1 = x0 + 1;
            int y1 = y0 + 1;
            int z1 = z0 + 1;

            float xs, ys, zs;
            switch (InterpolationMode)
            {
                case Interp.Hermite:
                    xs = MyMath.InterpHermiteFunc(x - x0);
                    ys = MyMath.InterpHermiteFunc(y - y0);
                    zs = MyMath.InterpHermiteFunc(z - z0);
                    break;
                case Interp.Quintic:
                    xs = MyMath.InterpQuinticFunc(x - x0);
                    ys = MyMath.InterpQuinticFunc(y - y0);
                    zs = MyMath.InterpQuinticFunc(z - z0);
                    break;
                default:
                    xs = x - x0;
                    ys = y - y0;
                    zs = z - z0;
                    break;
            }

            float xd0 = x - x0;
            float yd0 = y - y0;
            float zd0 = z - z0;
            float xd1 = xd0 - 1;
            float yd1 = yd0 - 1;
            float zd1 = zd0 - 1;

            float xf00 = Mathf.Lerp(GradCoord3D(x0, y0, z0, xd0, yd0, zd0), GradCoord3D(x1, y0, z0, xd1, yd0, zd0), xs);
            float xf10 = Mathf.Lerp(GradCoord3D(x0, y1, z0, xd0, yd1, zd0), GradCoord3D(x1, y1, z0, xd1, yd1, zd0), xs);
            float xf01 = Mathf.Lerp(GradCoord3D(x0, y0, z1, xd0, yd0, zd1), GradCoord3D(x1, y0, z1, xd1, yd0, zd1), xs);
            float xf11 = Mathf.Lerp(GradCoord3D(x0, y1, z1, xd0, yd1, zd1), GradCoord3D(x1, y1, z1, xd1, yd1, zd1), xs);

            float yf0 = Mathf.Lerp(xf00, xf10, ys);
            float yf1 = Mathf.Lerp(xf01, xf11, ys);

            return Mathf.Lerp(yf0, yf1, zs);
        }
        public override float Get2D(float x, float y, float factor = 1)
        {
            x *= factor;
            y *= factor;
            int x0 = MyMath.FastFloor(x);
            int y0 = MyMath.FastFloor(y);
            int x1 = x0 + 1;
            int y1 = y0 + 1;

            float xs, ys;
            switch (InterpolationMode)
            {
                case Interp.Hermite:
                    xs = MyMath.InterpHermiteFunc(x - x0);
                    ys = MyMath.InterpHermiteFunc(y - y0);
                    break;
                case Interp.Quintic:
                    xs = MyMath.InterpQuinticFunc(x - x0);
                    ys = MyMath.InterpQuinticFunc(y - y0);
                    break;
                default:
                    xs = x - x0;
                    ys = y - y0;
                    break;
            }

            float xd0 = x - x0;
            float yd0 = y - y0;
            float xd1 = xd0 - 1;
            float yd1 = yd0 - 1;

            float xf0 = Mathf.Lerp(GradCoord2D(x0, y0, xd0, yd0), GradCoord2D(x1, y0, xd1, yd0), xs);
            float xf1 = Mathf.Lerp(GradCoord2D(x0, y1, xd0, yd1), GradCoord2D(x1, y1, xd1, yd1), xs);

            return Mathf.Lerp(xf0, xf1, ys);
        }
        public override float Get1D(float x, float factor = 1)
        {
            x *= factor;
            int x0 = MyMath.FastFloor(x);
            int x1 = x0 + 1;

            float xs;
            switch (InterpolationMode)
            {
                case Interp.Hermite:
                    xs = MyMath.InterpHermiteFunc(x - x0);
                    break;
                case Interp.Quintic:
                    xs = MyMath.InterpQuinticFunc(x - x0);
                    break;
                default:
                    xs = x - x0;
                    break;
            }

            float xd0 = x - x0;
            float xd1 = xd0 - 1;

            return Mathf.Lerp(GradCoord1D(x0, xd0), GradCoord1D(x1, xd1), xs);
        }

        public override MyNoise Copy()
        {
            return new MyPerlinNoise() { InterpolationMode = InterpolationMode, Offset = Offset };
        }
    }
}