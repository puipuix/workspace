﻿using GodotUtils;
using Godot;
using System.Diagnostics.Contracts;

namespace GodotUtils.Noises
{
    public abstract class MyNoise
    {
        protected const int X_PRIME = 1619;
        protected const int Y_PRIME = 31337;
        protected const int Z_PRIME = 6971;
        protected const int W_PRIME = 1013;


        /// <summary>
        /// The noise seed
        /// </summary>
        public virtual int Seed { get; set; }

        public abstract bool Can1D { get; }
        public abstract bool Can2D { get; }
        public abstract bool Can3D { get; }
        public abstract bool Can4D { get; }


        public MyNoise()
        {
            Seed = 0;
        }

        /// <summary>
        /// Get a value between [-1,1] using fractal
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="z"></param>
        /// <param name="w"></param>
        /// <param name="octaves">Number of layer</param>
        /// <param name="factor">default factor</param>
        /// <param name="lacunarity">multiply the factor each layer</param>
        /// <param name="gain">multiply the amplitude each layer</param>
        /// <returns></returns>
        public virtual float GetFractalFBM4D(float x, float y, float z, float w, int octaves, float factor = 1, float lacunarity = 2, float gain = 0.5f, float steepness = 1)
        {
            float sum = Mathf.Pow(Get4D(x, y, z, w, factor), steepness);
            float max = 1;
            float amp = 1;
            int i = 0;

            while (++i < octaves)
            {
                factor *= lacunarity;
                amp *= gain;
                sum += Mathf.Pow(Get4D(x, y, z, w, factor), steepness) * amp;
                max += amp;
            }

            return sum / max;
        }

        /// <summary>
        /// Get a value between [0,1] using fractal
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="z"></param>
        /// <param name="w"></param>
        /// <param name="octaves">Number of layer</param>
        /// <param name="factor">default factor</param>
        /// <param name="lacunarity">multiply the factor each layer</param>
        /// <param name="gain">multiply the amplitude each layer</param>
        /// <returns></returns>
        public virtual float GetFractalBillow4D(float x, float y, float z, float w, int octaves, float factor = 1, float lacunarity = 2, float gain = 0.5f, float steepness = 1)
        {
            float sum = Mathf.Pow(1 - GetPercentFromNoiseResult(Get4D(x, y, z, w, factor)), steepness);
            float max = 1;
            float amp = 1;
            int i = 0;

            while (++i < octaves)
            {
                factor *= lacunarity;
                amp *= gain;
                sum += Mathf.Pow(1 - GetPercentFromNoiseResult(Get4D(x, y, z, w, factor)), steepness) * amp;
                max += amp;
            }

            return sum / max;
        }

        /// <summary>
        /// Get a value between [-1,1] using fractal
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="z"></param>
        /// <param name="octaves">Number of layer</param>
        /// <param name="factor">default factor</param>
        /// <param name="lacunarity">multiply the factor each layer</param>
        /// <param name="gain">multiply the amplitude each layer</param>
        /// <returns></returns>
        public virtual float GetFractalFBM3D(float x, float y, float z, int octaves, float factor = 1, float lacunarity = 2, float gain = 0.5f, float steepness = 1)
        {
            float sum = Mathf.Pow(Get3D(x, y, z, factor), steepness);
            float max = 1;
            float amp = 1;
            int i = 0;

            while (++i < octaves)
            {
                factor *= lacunarity;
                amp *= gain;
                sum += Mathf.Pow(Get3D(x, y, z, factor), steepness) * amp;
                max += amp;
            }

            return sum / max;
        }

        /// <summary>
        /// Get a value between [0,1] using fractal
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="z"></param>
        /// <param name="octaves">Number of layer</param>
        /// <param name="factor">default factor</param>
        /// <param name="lacunarity">multiply the factor each layer</param>
        /// <param name="gain">multiply the amplitude each layer</param>
        /// <returns></returns>
        public virtual float GetFractalBillow3D(float x, float y, float z, int octaves, float factor = 1, float lacunarity = 2, float gain = 0.5f, float steepness = 1)
        {
            float sum = Mathf.Pow(1 - GetPercentFromNoiseResult(Get3D(x, y, z, factor)), steepness);
            float max = 1;
            float amp = 1;
            int i = 0;

            while (++i < octaves)
            {
                factor *= lacunarity;
                amp *= gain;
                sum += Mathf.Pow(1 - GetPercentFromNoiseResult(Get3D(x, y, z, factor)), steepness) * amp;
                max += amp;
            }

            return sum / max;
        }


        /// <summary>
        /// Get a value between [-1,1] using fractal
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="octaves">Number of layer</param>
        /// <param name="factor">default factor</param>
        /// <param name="lacunarity">multiply the factor each layer</param>
        /// <param name="gain">multiply the amplitude each layer</param>
        /// <returns></returns>
        public virtual float GetFractalFBM2D(float x, float y, int octaves, float factor = 1, float lacunarity = 2, float gain = 0.5f, float steepness = 1)
        {
            float sum = Mathf.Pow(Get2D(x, y, factor), steepness);
            float max = 1;
            float amp = 1;
            int i = 0;

            while (++i < octaves)
            {
                factor *= lacunarity;
                amp *= gain;
                sum += Mathf.Pow(Get2D(x, y, factor), steepness) * amp;
                max += amp;
            }

            return sum / max;
        }

        /// <summary>
        /// Get a value between [0,1] using fractal
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="octaves">Number of layer</param>
        /// <param name="factor">default factor</param>
        /// <param name="lacunarity">multiply the factor each layer</param>
        /// <param name="gain">multiply the amplitude each layer</param>
        /// <returns></returns>
        public virtual float GetFractalBillow2D(float x, float y, int octaves, float factor = 1, float lacunarity = 2, float gain = 0.5f, float steepness = 1)
        {
            float sum = Mathf.Pow(1 - GetPercentFromNoiseResult(Get2D(x, y, factor)), steepness);
            float max = 1;
            float amp = 1;
            int i = 0;

            while (++i < octaves)
            {
                factor *= lacunarity;
                amp *= gain;
                sum += Mathf.Pow(1 - GetPercentFromNoiseResult(Get2D(x, y, factor)), steepness) * amp;
                max += amp;
            }

            return sum / max;
        }

        /// <summary>
        /// Get a value between [-1,1] using fractal
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="octaves">Number of layer</param>
        /// <param name="factor">default factor</param>
        /// <param name="lacunarity">multiply the factor each layer</param>
        /// <param name="gain">multiply the amplitude each layer</param>
        /// <returns></returns>
        public virtual float GetFractalFBM1D(float x, int octaves, float factor = 1, float lacunarity = 2, float gain = 0.5f, float steepness = 1)
        {
            float sum = Mathf.Pow(Get1D(x, factor), steepness);
            float max = 1;
            float amp = 1;
            int i = 0;

            while (++i < octaves)
            {
                factor *= lacunarity;
                amp *= gain;
                sum += Mathf.Pow(Get1D(x, factor), steepness) * amp;
                max += amp;
            }

            return sum / max;
        }

        /// <summary>
        /// Get a value between [0,1] using fractal
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="octaves">Number of layer</param>
        /// <param name="factor">default factor</param>
        /// <param name="lacunarity">multiply the factor each layer</param>
        /// <param name="gain">multiply the amplitude each layer</param>
        /// <returns></returns>
        public virtual float GetFractalBillow1D(float x, int octaves, float factor = 1, float lacunarity = 2, float gain = 0.5f, float steepness = 1)
        {
            float sum = Mathf.Pow(1 - GetPercentFromNoiseResult(Get1D(x, factor)), steepness);
            float max = 1;
            float amp = 1;
            int i = 0;

            while (++i < octaves)
            {
                factor *= lacunarity;
                amp *= gain;
                sum += Mathf.Pow(1 - GetPercentFromNoiseResult(Get1D(x, factor)), steepness) * amp;
                max += amp;
            }

            return sum / max;
        }

        /// <summary>
        /// Return a value between [-1;1]
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="z"></param>
        /// <param name="w"></param>
        /// <returns></returns>
        public abstract float Get4D(float x, float y, float z, float w, float factor = 1);

        /// <summary>
        /// Return a value between [-1;1]
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="z"></param>
        /// <returns></returns>
        public abstract float Get3D(float x, float y, float z, float factor = 1);

        /// <summary>
        /// Return a value between [-1;1]
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public abstract float Get2D(float x, float y, float factor = 1);

        /// <summary>
        /// Return a value between [-1;1]
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public abstract float Get1D(float x, float factor = 1);

        public abstract MyNoise Copy();

        /// <summary>
        /// Transform a noise value ([-1;1]) to a percent value ([0;1])
        /// </summary>
        /// <param name="v"></param>
        /// <returns></returns>
        public static float GetPercentFromNoiseResult(float v)
        {
            return (v + 1) * 0.5f;
        }

        public static bool GetBoolFromNoiseResult(float noise, float trueProbability)
        {
            return GetPercentFromNoiseResult(noise) <= trueProbability;
        }

        /// <summary>
        /// Transform a noise value ([-1;1]) to set it in range between [min;max]
        /// </summary>
        /// <param name="v"></param>
        /// <param name="min"></param>
        /// <param name="max"></param>
        /// <returns></returns>
        public static float GetInRangeFromNoiseResult(float v, float min, float max)
        {
            return Mathf.Lerp(min, max, GetPercentFromNoiseResult(v));
        }

        /// <summary>
        /// Transform a noise value ([-1;1]) to a color
        /// </summary>
        /// <param name="v"></param>
        /// <returns></returns>
        public static Color NoiseToColor(float v)
        {
            float col = GetPercentFromNoiseResult(v) * 3;
            return new Color(0 <= col && col <= 1 ? col : 0, 1 <= col && col <= 2 ? col - 1 : 0, 2 <= col && col <= 3 ? col - 2 : 0);
        }

    }
}