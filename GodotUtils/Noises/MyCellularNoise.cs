﻿using Godot;
using Microsoft.Win32;
using System;

namespace GodotUtils.Noises
{
    public class MyCellularNoise : MyNoise
    {
        public enum EMyCellularReturnType { Distance, Value }
        public override bool Can1D => true;
        public override bool Can2D => true;
        public override bool Can3D => true;
        public override bool Can4D => true;

        public MyNoise CellPositionNoise { get; set; } = new MyRandomNoise();
        public MyNoise CellValueNoise { get; set; } = new MyRandomNoise();
        public EMyCellularReturnType CellularReturnType { get; set; } = EMyCellularReturnType.Value;
        public float CellDiameter { get; set; } = 10f;

        private float _offx, _offy, _offz, _offw;

        public override int Seed
        {
            get => CellPositionNoise.Seed;
            set {
                CellPositionNoise.Seed = value;
                CellValueNoise.Seed = value;
                Random r = new Random(Seed);
                _offx = r.NextFloat() * 100000f;
                _offy = r.NextFloat() * 100000f;
                _offz = r.NextFloat() * 100000f;
                _offw = r.NextFloat() * 100000f;
            }
        }

        // use a random noise by default
        public MyCellularNoise() : base()
        {

        }

        private void FindCell4D(MyVector4 vector, out MyVector4 position, out float distance)
        {
            // point in cell coor
            MyVector4 pointPosition = vector / CellDiameter;
            // cell coor
            MyVector4 cellPosition = pointPosition.Round();

            distance = float.PositiveInfinity;
            position = new MyVector4();

            for (float ix = -0.5f; ix <= 0.6f; ix++)
            {
                for (float iy = -0.5f; iy <= 0.6f; iy++)
                {
                    for (float iz = -0.5f; iz <= 0.6f; iz++)
                    {
                        for (float iw = -0.5f; iw <= 0.6f; iw++)
                        {
                            // point of the cell
                            MyVector4 neighbour = cellPosition + new MyVector4(ix, iy, iz, iw);
                            MyVector4 neiPoint = neighbour + new MyVector4(
                                    CellPositionNoise.Get4D(_offx + neighbour.x, _offx + neighbour.y, _offx + neighbour.z, _offx + neighbour.w) / 2.0f,
                                    CellPositionNoise.Get4D(_offy + neighbour.x, _offy + neighbour.y, _offy + neighbour.z, _offy + neighbour.w) / 2.0f,
                                    CellPositionNoise.Get4D(_offz + neighbour.x, _offz + neighbour.y, _offz + neighbour.z, _offz + neighbour.w) / 2.0f,
                                    CellPositionNoise.Get4D(_offw + neighbour.x, _offw + neighbour.y, _offw + neighbour.z, _offw + neighbour.w) / 2.0f
                                    );
                            float distance2 = pointPosition.DistanceSquaredTo(neiPoint);
                            if (distance2 < distance)
                            {
                                distance = distance2;
                                position = neighbour;
                            }
                        }
                    }
                }
            }

            distance = Mathf.Sqrt(distance);
        }

        private void FindCell3D(Vector3 vector, out Vector3 position, out float distance)
        {
            // point in cell coor
            Vector3 pointPosition = vector / CellDiameter;
            // cell coor
            Vector3 cellPosition = pointPosition.Round();

            distance = float.PositiveInfinity;
            position = new Vector3();

            for (float ix = -0.5f; ix <= 0.6f; ix++)
            {
                for (float iy = -0.5f; iy <= 0.6f; iy++)
                {
                    for (float iz = -0.5f; iz <= 0.6f; iz++)
                    {
                        // point of the cell
                        Vector3 neighbour = cellPosition + new Vector3(ix, iy, iz);
                        Vector3 neiPoint = neighbour + new Vector3(
                                CellPositionNoise.Get3D(_offx + neighbour.x, _offx + neighbour.y, _offx + neighbour.z) / 2.0f,
                                CellPositionNoise.Get3D(_offy + neighbour.x, _offy + neighbour.y, _offy + neighbour.z) / 2.0f,
                                CellPositionNoise.Get3D(_offz + neighbour.x, _offz + neighbour.y, _offz + neighbour.z) / 2.0f);
                        float distance2 = pointPosition.DistanceSquaredTo(neiPoint);
                        if (distance2 < distance)
                        {
                            distance = distance2;
                            position = neighbour;
                        }
                    }
                }
            }

            distance = Mathf.Sqrt(distance);
        }

        private void FindCell2D(Vector2 vector, out Vector2 position, out float distance)
        {
            // point in cell coor
            Vector2 pointPosition = vector / CellDiameter;
            // cell coor
            Vector2 cellPosition = pointPosition.Round();

            distance = float.PositiveInfinity;
            position = new Vector2();

            for (float ix = -0.5f; ix <= 0.6f; ix++)
            {
                for (float iy = -0.5f; iy <= 0.6f; iy++)
                {
                    // point of the cell
                    Vector2 neighbour = cellPosition + new Vector2(ix, iy);
                    Vector2 neiPoint = neighbour + new Vector2(
                            CellPositionNoise.Get2D(_offx + neighbour.x, _offx + neighbour.y) / 2.0f,
                            CellPositionNoise.Get2D(_offy + neighbour.x, _offy + neighbour.y) / 2.0f);
                    float distance2 = pointPosition.DistanceSquaredTo(neiPoint);
                    if (distance2 < distance)
                    {
                        distance = distance2;
                        position = neighbour;
                    }
                }
            }

            distance = Mathf.Sqrt(distance);
        }

        private void FindCell1D(float vector, out float position, out float distance)
        {
            // point in cell coor
            float pointPosition = vector / CellDiameter;
            // cell coor
            float cellPosition = Mathf.Round(pointPosition);

            distance = float.PositiveInfinity;
            position = 0f;

            for (float ix = -0.5f; ix <= 0.6f; ix++)
            {
                // point of the cell
                float neighbour = cellPosition + ix;
                float neiPoint = neighbour + CellPositionNoise.Get1D(_offx + neighbour) / 2.0f;
                float distance2 = Mathf.Abs(pointPosition - neiPoint);
                if (distance2 < distance)
                {
                    distance = distance2;
                    position = neighbour;
                }
            }

            distance = Mathf.Sqrt(distance);
        }



        public override MyNoise Copy()
        {
            return new MyCellularNoise() {
                CellPositionNoise = CellPositionNoise.Copy(),
                CellValueNoise = CellValueNoise.Copy(),
            };
        }

        public override float Get1D(float x, float factor = 1)
        {
            FindCell1D(x  * factor, out float pos, out float dist);

            if (CellularReturnType == EMyCellularReturnType.Value)
            {
                return CellValueNoise.Get1D(pos);
            } else
            {
                return dist;
            }
        }

        public override float Get2D(float x, float y, float factor = 1)
        {
            FindCell2D(new Vector2(x, y) * factor, out Vector2 pos, out float dist);

            if (CellularReturnType == EMyCellularReturnType.Value)
            {
                return CellValueNoise.Get2D(pos.x, pos.y);
            } else
            {
                return dist;
            }
        }

        public override float Get3D(float x, float y, float z, float factor = 1)
        {
            FindCell3D(new Vector3(x, y, z) * factor, out Vector3 pos, out float dist);

            if (CellularReturnType == EMyCellularReturnType.Value)
            {
                return CellValueNoise.Get3D(pos.x, pos.y, pos.z);
            } else
            {
                return dist;
            }
        }

        public override float Get4D(float x, float y, float z, float w, float factor = 1)
        {
            FindCell4D(new MyVector4(x, y, z, w) * factor, out MyVector4 pos, out float dist);

            if (CellularReturnType == EMyCellularReturnType.Value)
            {
                return CellValueNoise.Get4D(pos.x, pos.y, pos.z, pos.w);
            } else
            {
                return dist;
            }
        }
    }
}