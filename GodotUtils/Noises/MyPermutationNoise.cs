﻿using System;

namespace GodotUtils.Noises
{
    // Original c++ code link:
    // https://github.com/thomasp85/ambient
    //
    // Modified and adapter for Bricksivers by Alexis Vernes
    //
    // Original license:
    //
    // MIT License
    //
    // Copyright(c) 2017 Jordan Peck
    //
    // Permission is hereby granted, free of charge, to any person obtaining a copy
    // of this software and associated documentation files(the "Software"), to deal
    // in the Software without restriction, including without limitation the rights
    // to use, copy, modify, merge, publish, distribute, sublicense, and / or sell
    // copies of the Software, and to permit persons to whom the Software is
    // furnished to do so, subject to the following conditions :
    //
    // The above copyright notice and this permission notice shall be included in all
    // copies or substantial portions of the Software.
    //
    // THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    // IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    // FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
    // AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    // LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    // OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    // SOFTWARE.
    //
    // The developer's email is jorzixdan.me2@gzixmail.com (for great email, take
    // off every 'zix'.)
    //
    public abstract class MyPermutationNoise : MyNoise
    {
        protected static readonly float[] GRAD_X =
            {
                1, -1, 1, -1,
                1, -1, 1, -1,
                0, 0, 0, 0
            };
        protected static readonly float[] GRAD_Y =
            {
                1, 1, -1, -1,
                0, 0, 0, 0,
                1, -1, 1, -1
            };
        protected static readonly float[] GRAD_Z =
            {
                 0, 0, 0, 0,
                 1, 1, -1, -1,
                 1, 1, -1, -1
            };
        protected static readonly float[] GRAD_4D =
            {
                  0,1,1,1,0,1,1,-1,0,1,-1,1,0,1,-1,-1,
                  0,-1,1,1,0,-1,1,-1,0,-1,-1,1,0,-1,-1,-1,
                  1,0,1,1,1,0,1,-1,1,0,-1,1,1,0,-1,-1,
                  -1,0,1,1,-1,0,1,-1,-1,0,-1,1,-1,0,-1,-1,
                  1,1,0,1,1,1,0,-1,1,-1,0,1,1,-1,0,-1,
                  -1,1,0,1,-1,1,0,-1,-1,-1,0,1,-1,-1,0,-1,
                  1,1,1,0,1,1,-1,0,1,-1,1,0,1,-1,-1,0,
                  -1,1,1,0,-1,1,-1,0,-1,-1,1,0,-1,-1,-1,0
             };
        protected static readonly float[] VAL_LUT =
            {
                  0.3490196078f, 0.4352941176f, -0.4509803922f, 0.6392156863f, 0.5843137255f, -0.1215686275f, 0.7176470588f, -0.1058823529f, 0.3960784314f, 0.0431372549f, -0.03529411765f, 0.3176470588f, 0.7254901961f, 0.137254902f, 0.8588235294f, -0.8196078431f,
                  -0.7960784314f, -0.3333333333f, -0.6705882353f, -0.3882352941f, 0.262745098f, 0.3254901961f, -0.6470588235f, -0.9215686275f, -0.5294117647f, 0.5294117647f, -0.4666666667f, 0.8117647059f, 0.3803921569f, 0.662745098f, 0.03529411765f, -0.6156862745f,
                  -0.01960784314f, -0.3568627451f, -0.09019607843f, 0.7490196078f, 0.8352941176f, -0.4039215686f, -0.7490196078f, 0.9529411765f, -0.0431372549f, -0.9294117647f, -0.6549019608f, 0.9215686275f, -0.06666666667f, -0.4431372549f, 0.4117647059f, -0.4196078431f,
                  -0.7176470588f, -0.8117647059f, -0.2549019608f, 0.4901960784f, 0.9137254902f, 0.7882352941f, -1.0f, -0.4745098039f, 0.7960784314f, 0.8509803922f, -0.6784313725f, 0.4588235294f, 1.0f, -0.1843137255f, 0.4509803922f, 0.1450980392f,
                  -0.231372549f, -0.968627451f, -0.8588235294f, 0.4274509804f, 0.003921568627f, -0.003921568627f, 0.2156862745f, 0.5058823529f, 0.7647058824f, 0.2078431373f, -0.5921568627f, 0.5764705882f, -0.1921568627f, -0.937254902f, 0.08235294118f, -0.08235294118f,
                  0.9058823529f, 0.8274509804f, 0.02745098039f, -0.168627451f, -0.7803921569f, 0.1137254902f, -0.9450980392f, 0.2f, 0.01960784314f, 0.5607843137f, 0.2705882353f, 0.4431372549f, -0.9607843137f, 0.6156862745f, 0.9294117647f, -0.07450980392f,
                  0.3098039216f, 0.9921568627f, -0.9137254902f, -0.2941176471f, -0.3411764706f, -0.6235294118f, -0.7647058824f, -0.8901960784f, 0.05882352941f, 0.2392156863f, 0.7333333333f, 0.6549019608f, 0.2470588235f, 0.231372549f, -0.3960784314f, -0.05098039216f,
                  -0.2235294118f, -0.3725490196f, 0.6235294118f, 0.7019607843f, -0.8274509804f, 0.4196078431f, 0.07450980392f, 0.8666666667f, -0.537254902f, -0.5058823529f, -0.8039215686f, 0.09019607843f, -0.4823529412f, 0.6705882353f, -0.7882352941f, 0.09803921569f,
                  -0.6078431373f, 0.8039215686f, -0.6f, -0.3254901961f, -0.4117647059f, -0.01176470588f, 0.4823529412f, 0.168627451f, 0.8745098039f, -0.3647058824f, -0.1607843137f, 0.568627451f, -0.9921568627f, 0.9450980392f, 0.5137254902f, 0.01176470588f,
                  -0.1450980392f, -0.5529411765f, -0.5764705882f, -0.1137254902f, 0.5215686275f, 0.1607843137f, 0.3725490196f, -0.2f, -0.7254901961f, 0.631372549f, 0.7098039216f, -0.568627451f, 0.1294117647f, -0.3098039216f, 0.7411764706f, -0.8509803922f,
                  0.2549019608f, -0.6392156863f, -0.5607843137f, -0.3176470588f, 0.937254902f, 0.9843137255f, 0.5921568627f, 0.6941176471f, 0.2862745098f, -0.5215686275f, 0.1764705882f, 0.537254902f, -0.4901960784f, -0.4588235294f, -0.2078431373f, -0.2156862745f,
                  0.7725490196f, 0.3647058824f, -0.2392156863f, 0.2784313725f, -0.8823529412f, 0.8980392157f, 0.1215686275f, 0.1058823529f, -0.8745098039f, -0.9843137255f, -0.7019607843f, 0.9607843137f, 0.2941176471f, 0.3411764706f, 0.1529411765f, 0.06666666667f,
                  -0.9764705882f, 0.3019607843f, 0.6470588235f, -0.5843137255f, 0.05098039216f, -0.5137254902f, -0.137254902f, 0.3882352941f, -0.262745098f, -0.3019607843f, -0.1764705882f, -0.7568627451f, 0.1843137255f, -0.5450980392f, -0.4980392157f, -0.2784313725f,
                  -0.9529411765f, -0.09803921569f, 0.8901960784f, -0.2862745098f, -0.3803921569f, 0.5529411765f, 0.7803921569f, -0.8352941176f, 0.6862745098f, 0.7568627451f, 0.4980392157f, -0.6862745098f, -0.8980392157f, -0.7725490196f, -0.7098039216f, -0.2470588235f,
                  -0.9058823529f, 0.9764705882f, 0.1921568627f, 0.8431372549f, -0.05882352941f, 0.3568627451f, 0.6078431373f, 0.5450980392f, 0.4039215686f, -0.7333333333f, -0.4274509804f, 0.6f, 0.6784313725f, -0.631372549f, -0.02745098039f, -0.1294117647f,
                  0.3333333333f, -0.8431372549f, 0.2235294118f, -0.3490196078f, -0.6941176471f, 0.8823529412f, 0.4745098039f, 0.4666666667f, -0.7411764706f, -0.2705882353f, 0.968627451f, 0.8196078431f, -0.662745098f, -0.4352941176f, -0.8666666667f, -0.1529411765f,
             };

        protected byte[] m_perm = new byte[512];
        protected byte[] m_perm12 = new byte[512];
        protected int m_seed;

        public enum Interp { Linear, Hermite, Quintic };

        public Interp InterpolationMode { get; set; } = Interp.Linear;

        public byte Offset { get; set; }

        public override int Seed
        {
            get => m_seed;
            set {
                m_seed = value;

                Random r = new Random(m_seed);

                for (int i = 0; i < 256; i++)
                    m_perm[i] = (byte)i;

                for (int j = 0; j < 256; j++)
                {
                    int k = (r.Next() % (256 - j)) + j;
                    byte l = m_perm[j];
                    m_perm[j] = m_perm[j + 256] = m_perm[k];
                    m_perm[k] = l;
                    m_perm12[j] = m_perm12[j + 256] = (byte)(m_perm[j] % 12);
                }
            }
        }

        protected MyPermutationNoise() : base() { }

        protected byte Index1D_12(int x)
        {
            return m_perm12[(x & 0xff) + Offset];
        }
        protected byte Index2D_12(int x, int y)
        {
            return m_perm12[(x & 0xff) + m_perm[(y & 0xff) + Offset]];
        }
        protected byte Index3D_12(int x, int y, int z)
        {
            return m_perm12[(x & 0xff) + m_perm[(y & 0xff) + m_perm[(z & 0xff) + Offset]]];
        }
        protected byte Index4D_32(int x, int y, int z, int w)
        {
            return (byte)(m_perm[(x & 0xff) + m_perm[(y & 0xff) + m_perm[(z & 0xff) + m_perm[(w & 0xff) + Offset]]]] & 31);
        }

        protected byte Index1D_256(int x)
        {
            return m_perm[(x & 0xff) + Offset];
        }
        protected byte Index2D_256(int x, int y)
        {
            return m_perm[(x & 0xff) + m_perm[(y & 0xff) + Offset]];
        }
        protected byte Index3D_256(int x, int y, int z)
        {
            return m_perm[(x & 0xff) + m_perm[(y & 0xff) + m_perm[(z & 0xff) + Offset]]];
        }
        protected byte Index4D_256(int x, int y, int z, int w)
        {
            return m_perm[(x & 0xff) + m_perm[(y & 0xff) + m_perm[(z & 0xff) + m_perm[(w & 0xff) + Offset]]]];
        }

        protected float ValCoord1DFast(int x)
        {
            return VAL_LUT[Index1D_256(x)];
        }
        protected float ValCoord2DFast(int x, int y)
        {
            return VAL_LUT[Index2D_256(x, y)];
        }
        protected float ValCoord3DFast(int x, int y, int z)
        {
            return VAL_LUT[Index3D_256(x, y, z)];
        }
        protected float ValCoord4DFast(int x, int y, int z, int w)
        {
            return VAL_LUT[Index4D_256(x, y, z, w)];
        }

        protected float GradCoord1D(int x, float xd)
        {
            byte lutPos = Index1D_12(x);

            return xd * GRAD_X[lutPos];
        }
        protected float GradCoord2D(int x, int y, float xd, float yd)
        {
            byte lutPos = Index2D_12(x, y);

            return xd * GRAD_X[lutPos] + yd * GRAD_Y[lutPos];
        }
        protected float GradCoord3D(int x, int y, int z, float xd, float yd, float zd)
        {
            byte lutPos = Index3D_12(x, y, z);

            return xd * GRAD_X[lutPos] + yd * GRAD_Y[lutPos] + zd * GRAD_Z[lutPos];
        }
        protected float GradCoord4D(int x, int y, int z, int w, float xd, float yd, float zd, float wd)
        {
            byte lutPos = (byte)(Index4D_32(x, y, z, w) << 2);

            return xd * GRAD_4D[lutPos] + yd * GRAD_4D[lutPos + 1] + zd * GRAD_4D[lutPos + 2] + wd * GRAD_4D[lutPos + 3];
        }
    }
}