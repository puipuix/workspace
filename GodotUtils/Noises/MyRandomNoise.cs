﻿namespace GodotUtils.Noises
{
    public class MyRandomNoise : MyNoise
    {
        public override bool Can1D => true;
        public override bool Can2D => true;
        public override bool Can3D => true;
        public override bool Can4D => true;

        public MyRandomNoise() : base()
        {
        }

        public override float Get1D(float x, float factor = 1)
        {
            x *= factor;

            long seed = (long)(x) * X_PRIME;
            seed ^= 0xE56FAA12;
            seed += Seed * 3433;
            seed ^= 0xa7b2c49a;
            if (seed < 0)
            {
                seed = -seed;
            }

            return ((seed % 2001) - 1000) / 1000.0f;
        }

        public override float Get2D(float x, float y, float factor = 1)
        {
            x *= factor;
            y *= factor;

            long seed = (long)(x) * X_PRIME;
            seed ^= 0xE56FAA12;
            seed += (long)(y) * Y_PRIME;
            seed ^= 0x69628a2d;
            seed += Seed * 3433;
            seed ^= 0xa7b2c49a;
            if (seed < 0)
            {
                seed = -seed;
            }

            return ((seed % 2001) - 1000) / 1000.0f;
        }

        public override float Get3D(float x, float y, float z, float factor = 1)
        {
            x *= factor;
            y *= factor;
            z *= factor;

            long seed = (long)(x) * X_PRIME;
            seed ^= 0xE56FAA12;
            seed += (long)(y) * Y_PRIME;
            seed ^= 0x69628a2d;
            seed += (long)(z) * Z_PRIME;
            seed ^= 0xA65BE11;
            seed += Seed * 3433;
            seed ^= 0xa7b2c49a;
            if (seed < 0)
            {
                seed = -seed;
            }

            return ((seed % 2001) - 1000) / 1000.0f;
        }

        public override float Get4D(float x, float y, float z, float w, float factor = 1)
        {
            x *= factor;
            y *= factor;
            z *= factor;
            w *= factor;

            long seed = (long)(x) * X_PRIME;
            seed ^= 0xE56FAA12;
            seed += (long)(y) * Y_PRIME;
            seed ^= 0x69628a2d;
            seed += (long)(z) * Z_PRIME;
            seed ^= 0xA65BE11;
            seed += (long)(w) * W_PRIME;
            seed ^= 0xf5b23bde;
            seed += Seed * 3433;
            seed ^= 0xa7b2c49a;
            if (seed < 0)
            {
                seed = -seed;
            }

            return ((seed % 2001) - 1000) / 1000.0f;
        }

        public override MyNoise Copy()
        {
            return new MyRandomNoise();
        }
    }
}