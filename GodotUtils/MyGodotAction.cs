﻿using Godot;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GodotUtils
{
    public abstract class MyGodotActionBase : Godot.Object
    {
        public bool OneShot { get; set; }

        protected MyGodotActionBase(bool oneShot)
        {
            OneShot = oneShot;
        }

        protected virtual void LookFree()
        {
            if (OneShot)
            {
                CallDeferred("free");
            }
        }
    }

    public class MyGodotAction : MyGodotActionBase
    {
        public Action Action { get; set; }

        public MyGodotAction(Action action, bool oneShot) : base(oneShot)
        {
            Action = action;
        }

        public void Invoke()
        {
            Action.Invoke();
            base.LookFree();
        }
    }

    public class MyGodotAction<T1> : MyGodotActionBase
    {
        public Action<T1> Action { get; set; }

        public MyGodotAction(Action<T1> action, bool oneShot) : base(oneShot)
        {
            Action = action;
        }

        public void Invoke(T1 arg1)
        {
            Action.Invoke(arg1); base.LookFree();
        }

    }

    public class MyGodotAction<T1, T2> : MyGodotActionBase
    {
        public Action<T1, T2> Action { get; set; }

        public MyGodotAction(Action<T1, T2> action, bool oneShot) : base(oneShot)
        {
            Action = action;
        }

        public void Invoke(T1 arg1, T2 arg2)
        {
            Action.Invoke(arg1, arg2); base.LookFree();
        }

    }

    public class MyGodotAction<T1, T2, T3> : MyGodotActionBase
    {
        public Action<T1, T2, T3> Action { get; set; }

        public MyGodotAction(Action<T1, T2, T3> action, bool oneShot) : base(oneShot)
        {
            Action = action;
        }

        public void Invoke(T1 arg1, T2 arg2, T3 arg3)
        {
            Action.Invoke(arg1, arg2, arg3); base.LookFree();
        }
    }

    public class MyGodotAction<T1, T2, T3, T4> : MyGodotActionBase
    {
        public Action<T1, T2, T3, T4> Action { get; set; }

        public MyGodotAction(Action<T1, T2, T3, T4> action, bool oneShot) : base(oneShot)
        {
            Action = action;
        }

        public void Invoke(T1 arg1, T2 arg2, T3 arg3, T4 arg4)
        {
            Action.Invoke(arg1, arg2, arg3, arg4); base.LookFree();
        }
    }

    public class MyGodotAction<T1, T2, T3, T4, T5> : MyGodotActionBase
    {
        public Action<T1, T2, T3, T4, T5> Action { get; set; }

        public MyGodotAction(Action<T1, T2, T3, T4, T5> action, bool oneShot) : base(oneShot)
        {
            Action = action;
        }

        public void Invoke(T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5)
        {
            Action.Invoke(arg1, arg2, arg3, arg4, arg5); base.LookFree();
        }
    }
}
