﻿using Godot;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace GodotUtils
{
    public interface IMyDeferredOperation
    {
        /// <summary>
        /// Execute and return if we can execute the next operation.
        /// </summary>
        /// <returns></returns>
        bool Execute();
    }

    public class MyDeferredCalls : Godot.Object
    {
        private readonly Queue<IMyDeferredOperation> _operations = new Queue<IMyDeferredOperation>();

        private readonly float _delay;
        private readonly SceneTree _tree;
        private SceneTreeTimer _timer;

        public MyDeferredCalls(SceneTree tree, float delay, params IMyDeferredOperation[] operations)
        {
            _tree = tree;
            _delay = delay;
            for (int i = 0; i < operations.Length; i++)
            {
                PushCustom(operations[i]);
            }
        }

        public MyDeferredCalls PushCustom(IMyDeferredOperation operation)
        {
            _operations.Enqueue(operation);
            return this;
        }

        public MyDeferredCalls PushSingle(Action action)
        {
            _operations.Enqueue(new MyDeferredSingleOperation(action));
            return this;
        }

        public MyDeferredCalls PushWhile(Func<bool> whileFunc)
        {
            return PushCustom(new MyDeferredWhileOperation(whileFunc));
        }

        public MyDeferredCalls PushFor(int start, Func<int, bool> continueFunc, Func<int, int> incrementFunc, Action<int> action)
        {
            return PushCustom(new MyDeferredForOperation(start, continueFunc, incrementFunc, action));
        }

        public void Start()
        {
            _timer = _tree.CreateTimer(_delay); 
            _timer.Connect("timeout", this, Run);
        }
        
        public void Stop()
        {
            if (_timer.IsAccessible() && _timer.IsConnected("timeout", this, Run))
            {
                _timer.Disconnect("timeout", this, Run);
            }
        }

        public void Run()
        {
            if (!_operations.IsEmptyCollection())
            {
                var op = _operations.Peek();
                if (op.Execute())
                {
                    _operations.Dequeue();
                }

                if (_operations.IsEmptyCollection())
                {
                    CallDeferred("free");
                }
                else
                {
                    Start();
                }
            }
            else
            {
                CallDeferred("free");
            }
        }
    }

    public class MyDeferredSingleOperation : IMyDeferredOperation
    {
        public Action Action { get; }

        public MyDeferredSingleOperation(Action action)
        {
            Action = action;
        }

        public bool Execute()
        {
            Action();
            return true;
        }
    }

    public class MyDeferredWhileOperation : IMyDeferredOperation
    {
        public Func<bool> Func { get; }

        public MyDeferredWhileOperation(Func<bool> whileFunc)
        {
            Func = whileFunc;
        }

        public bool Execute()
        {
            return !Func();
        }
    }

    public class MyDeferredForOperation : IMyDeferredOperation
    {
        public Action<int> Action { get; }
        public int I { get; private set; }
        public Func<int, int> Increment { get; }
        public Func<int, bool> Continue { get; }

        public MyDeferredForOperation(int start, Func<int, bool> continueFunc, Func<int, int> incrementFunc, Action<int> action)
        {
            Action = action;
            I = start;
            Increment = incrementFunc;
            Continue = continueFunc;
        }

        public bool Execute()
        {
            if (Continue(I))
            {
                Action(I);
                I = Increment(I);
                return false;
            } else
            {
                return true;
            }
        }
    }
}
