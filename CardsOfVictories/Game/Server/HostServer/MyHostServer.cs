using CardsOfVictories.Game.DataBase;
using CardsOfVictories.Game.Server;
using Godot;
using GodotUtils;
using System;
using System.Collections.Generic;
using GArray = Godot.Collections.Array;
using System.Diagnostics;
using System.IO.Pipes;
using System.Linq;
using System.Net.NetworkInformation;
using CardsOfVictories.Game.NetworkAPI;

public class MyHostServer : MyServerBase, IMyHostServer
{
    private static readonly char[] _aviableCharForPassWord = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ".ToArray();

    private Dictionary<MyServerClientInfo, MySelectedDeck> _waitForMatchMaking = new Dictionary<MyServerClientInfo, MySelectedDeck>();

    public void StartHostServer()
    {
        StartServer(55000, 2048);
        var timer = new Timer();
        AddChild(timer);
        timer.WaitTime = 3;
        timer.Connect("timeout", this, MatchMaking);
        timer.Start();
    }

    private void MatchMaking()
    {
        var waiting = _waitForMatchMaking.Sorted((a, b) => a.Value.BR.CompareTo(b.Value.BR));
        for (int i = 0; i < waiting.Length - 1; i++)
        {
            var p1 = waiting[i];
            var p2 = waiting[i + 1];
            if (MyMath.Distance(p1.Value.BR, p2.Value.BR) <= 1)
            {
                var prop = IPGlobalProperties.GetIPGlobalProperties();
                var set = new HashSet<int>(prop.GetActiveTcpConnections()
                        .Where(c => c.State != TcpState.Closed)
                        .Select(c => c.LocalEndPoint)
                    .Concat(prop.GetActiveTcpListeners())
                    .Concat(prop.GetActiveUdpListeners())
                    .Select(inet => inet.Port));
                int selectedPort = -1;
                for (int port = 1024; selectedPort == -1 && port < ushort.MaxValue; port++)
                {
                    if (!set.Contains(port))
                    {
                        selectedPort = port;
                    }
                }
                if (selectedPort != -1)
                {
                    var sessioId = CreateToken();
                    MyDebug.Print("Creating session '" + sessioId + "' for player " + p1.Value.PlayerId + " and " + p2.Value.PlayerId + "...");
                    MyDebug.AddTab();
                    _waitForMatchMaking.Remove(p1.Key);
                    _waitForMatchMaking.Remove(p2.Key);
                    var session = MyServerSession.Instance();
                    AddChild(session);
                    session.CreateSession(sessioId, selectedPort, new MySelectedDeck[] { p1.Value, p2.Value });
                    this.RpcId(p1.Key.PeerId, ConnectToSession, sessioId, selectedPort);
                    this.RpcId(p2.Key.PeerId, ConnectToSession, sessioId, selectedPort);
                    var kick = GetTree().CreateTimer(3);
                    kick.ConnectAction("timeout", () => Kick(p1.Key.PeerId, "Redirection"));
                    kick.ConnectAction("timeout", () => Kick(p2.Key.PeerId, "Redirection"));
                    i++; 
                    MyDebug.RemoveTab();
                }
            }
        }
    }

    protected override void OnClientDisconnected(MyServerClientInfo client)
    {
        base.OnClientDisconnected(client);
        _waitForMatchMaking.Remove(client);
    }

    /// <summary>
    /// Add the peer to the queue
    /// </summary>
    [Master]
    public void RegisterToMatchMaking()
    {
        if (TryGetClientSenderId(out var client))
        {
            MyDebug.Print("Adding " + client + " to matchmaking...");
            _waitForMatchMaking.Add(client, MySelectedDeck.GetSelectedDeck(client.DBID));
        }
    }

    /// <summary>
    /// No Effect, only for clients
    /// </summary>
    /// <param name="port"></param>
    [Puppet]
    public void ConnectToSession(string sessionId, int port) { }

    public static string CreateToken()
    {
        return MyTools.RNG.NextString(24, _aviableCharForPassWord);
    }
}
