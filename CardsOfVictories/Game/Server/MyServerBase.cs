﻿using CardsOfVictories.Game.DataBase;
using CardsOfVictories.Game.NetworkAPI;
using Godot;
using GodotUtils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GArray = Godot.Collections.Array;

namespace CardsOfVictories.Game.Server
{
    public abstract class MyServerBase : Node, IMyServerBase
    {
        public NetworkedMultiplayerENet Network => (NetworkedMultiplayerENet)CustomMultiplayer.NetworkPeer;

        private Dictionary<int, SceneTreeTimer> _awaitForIdentification = new Dictionary<int, SceneTreeTimer>();

        protected Dictionary<int, MyServerClientInfo> _clients = new Dictionary<int, MyServerClientInfo>();

        public void StartServer(int port, int maxPlayers)
        {
            MyDebug.Print("Starting Server on port " + port + "...");
            CustomMultiplayer = new MultiplayerAPI();
            CustomMultiplayer.Connect<int>("network_peer_connected", this, OnPeerConnected);
            CustomMultiplayer.Connect<int>("network_peer_disconnected", this, OnPeerDisconnected);
            var network = new NetworkedMultiplayerENet();
            network.ServerRelay = false;
            var err = network.CreateServer(port, maxPlayers);
            if (err != Error.Ok)
            {
                MyDebug.PrintErr("\t" + err + "!");
                CustomMultiplayer.Disconnect<int>("network_peer_connected", this, OnPeerConnected);
                CustomMultiplayer.Disconnect<int>("network_peer_disconnected", this, OnPeerDisconnected);
                CustomMultiplayer = null;
            }
            else
            {
                CustomMultiplayer.SetRootNode(this);
                CustomMultiplayer.NetworkPeer = network;
                MyDebug.Print("\tDone.");
            }
        }

        public void StopServer()
        {
            MyDebug.Print("Stoping Server...");
            Network.CloseConnection();
            CustomMultiplayer.NetworkPeer = null;
            CustomMultiplayer = null;
            _clients.Clear();
            foreach (var timer in _awaitForIdentification.Values)
            {
                timer.Disconnect<int, string>("timeout", this, Kick);
            }
            _awaitForIdentification.Clear();
            _clients = null;
            _awaitForIdentification = null;
        }

        protected void Kick(int peer, string why)
        {
            if (CustomMultiplayer.GetNetworkConnectedPeers().Contains(peer))
            {
                MyDebug.Print("Kicking " + peer + "... (" + why + ")");
                Network.DisconnectPeer(peer);
            }
        }
        protected virtual void OnPeerConnected(int peer)
        {
            MyDebug.Print("New peer: " + peer);
            var timer = GetTree().CreateTimer(3);
            timer.Connect<int, string>("timeout", this, Kick, peer, "Identification timeout");
            _awaitForIdentification.Add(peer, timer);
        }


        [Master]
        public void IndentifyPeer(string token)
        {
            int id = MyDBClient.GetDBIdFromToken(token);
            int peer = CustomMultiplayer.GetRpcSenderId();
            MyDebug.Print("Peer indentification... (id:" + (id == 0 ? "?" : id.ToString()) + ", peer:" + peer + ")");
            if (id != 0 && peer != 0 && _awaitForIdentification.TryGetValue(peer, out var timer))
            {
                var client = new MyServerClientInfo(peer, id);
                _clients.Add(peer, client);
                timer.Disconnect<int, string>("timeout", this, Kick);
                _awaitForIdentification.Remove(peer);
                this.RpcId(peer, IsTokenValidated, true);
                OnClientIndentified(client);
            }
            else
            {
                MyDebug.PrintWarning("\tUnknow token, peer or not await for identification. Disconnecting client.");
                if (peer != 0)
                {
                    this.RpcId(peer, IsTokenValidated, false);
                }
                Kick(peer, "Identification error");
            }
        }

        /// <summary>
        /// No action only for client.
        /// </summary>
        /// <param name="valid"></param>
        [Puppet]
        public void IsTokenValidated(bool valid) { }

        public override void _Process(float delta)
        {
            base._Process(delta);
            CustomMultiplayer?.Poll();
        }

        protected virtual void OnPeerDisconnected(int peer)
        {
            if (_awaitForIdentification.TryGetValue(peer, out var timer))
            {
                if (timer.IsConnected<int, string>("timeout", this, Kick))
                {
                    timer.Disconnect<int, string>("timeout", this, Kick);
                }
                _awaitForIdentification.Remove(peer);
            }
            if (_clients.TryGetValue(peer, out var client))
            {
                _clients.Remove(peer);
                OnClientDisconnected(client);
            }
        }

        protected bool TryGetClientSenderId(out MyServerClientInfo client)
        {
            client = null;
            int current = CustomMultiplayer.GetRpcSenderId();
            return current != 0 && _clients.TryGetValue(current, out client);
        }

        protected virtual void OnClientIndentified(MyServerClientInfo client) { }
        protected virtual void OnClientDisconnected(MyServerClientInfo client) { }


    }
}
