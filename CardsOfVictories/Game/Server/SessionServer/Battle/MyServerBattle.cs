using Godot;
using GodotUtils;
using System;

public class MyServerBattle : Node
{
    [MyOnReady("Terrain")]
    public MyServerTerrain Terrain { get; private set; }

    [MyOnReady("PlayerBase1")]
    public MyServerPlayerBase PlayerBase1 { get; private set; }

    [MyOnReady("PlayerBase2")]
    public MyServerPlayerBase PlayerBase2 { get; private set; }
}
