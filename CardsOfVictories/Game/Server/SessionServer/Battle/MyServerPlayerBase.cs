using Godot;
using GodotUtils;
using System;

public class MyServerPlayerBase : Node
{
    [MyOnReady("Cemetery")]
    public MyServerCemetery Cemetery { get; private set; }
    
    [MyOnReady("Deck")]
    public MyServerDeck Deck { get; private set; }
    
    [MyOnReady("Hand")]
    public MyServerHand Hand { get; private set; }

    public override void _Ready()
    {
        base._Ready();
        MyOnReadyAttribute.SetUp(this);
    }
}
