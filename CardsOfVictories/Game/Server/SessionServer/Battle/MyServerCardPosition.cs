﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CardsOfVictories.Game.Server.Battle
{
    public abstract class MyServerCardPosition
    {
        public abstract bool SetCard(MyServerCard card, MyServerBattle battle);
        public abstract MyServerCard GetCard(MyServerBattle battle);
        public abstract bool TryGetCard(out MyServerCard card, MyServerBattle battle);
    }

    public class MyServerTerrainCardPosition : MyServerCardPosition
    {
        public int I { get; }
        public int J { get; }

        public MyServerTerrainCardPosition(int i, int j)
        {
            I = i;
            J = j;
        }

        public override MyServerCard GetCard(MyServerBattle battle)
        {
            return battle.Terrain[I, J];
        }

        public override bool SetCard(MyServerCard card, MyServerBattle battle)
        {
            return battle.Terrain.AddCard(I, J, card);
        }

        public override bool TryGetCard(out MyServerCard card, MyServerBattle battle)
        {
            return battle.Terrain.TryGetCard(I, J, out card);
        }
    }

    public abstract class MyServerPlayerBaseCardPosition : MyServerCardPosition
    {
        public int PlayerIndex { get; }

        protected MyServerPlayerBaseCardPosition(int playerIndex)
        {
            PlayerIndex = playerIndex;
        }

        protected MyServerPlayerBase GetBase(MyServerBattle battle)
        {
            switch (PlayerIndex)
            {
                case 1: return battle.PlayerBase1;
                case 2: return battle.PlayerBase2;
                default:
                    throw new IndexOutOfRangeException("PlayerIndex=" + PlayerIndex);
            }
        }
    }

    public class MyServerHandCardPosition : MyServerPlayerBaseCardPosition
    {
        public string CardName { get; }
        public MyServerHandCardPosition(int playerIndex, string cardName) : base(playerIndex)
        {
            CardName = cardName;
        }

        public override MyServerCard GetCard(MyServerBattle battle)
        {
            return GetBase(battle).Hand.GetCard(CardName);
        }

        public override bool SetCard(MyServerCard card, MyServerBattle battle)
        {
            GetBase(battle).Hand.AddCard(card);
            return true;
        }

        public override bool TryGetCard(out MyServerCard card, MyServerBattle battle)
        {
            return GetBase(battle).Hand.TryGetCard(CardName, out card);
        }
    }

    public class MyServerDeckCardPosition : MyServerPlayerBaseCardPosition
    {
        public int CardIndex { get; }
        public MyServerDeckCardPosition(int playerIndex, int cardIndex) : base(playerIndex)
        {
            CardIndex = cardIndex;
        }

        public override MyServerCard GetCard(MyServerBattle battle)
        {
            return GetBase(battle).Deck.Seek(CardIndex);
        }

        public override bool SetCard(MyServerCard card, MyServerBattle battle)
        {
            GetBase(battle).Deck.Push(card, CardIndex);
            return true;
        }

        public override bool TryGetCard(out MyServerCard card, MyServerBattle battle)
        {
            var deck = GetBase(battle).Deck;
            if (0 <= CardIndex && deck.GetChildCount() < CardIndex)
            {
                card = deck.Seek(CardIndex);
                return true;
            } else
            {
                card = null;
                return false;
            }
        }
    }

    public class MyServerCemeteryCardPosition : MyServerPlayerBaseCardPosition
    {
        public int CardIndex { get; }
        public MyServerCemeteryCardPosition(int playerIndex, int cardIndex) : base(playerIndex)
        {
            CardIndex = cardIndex;
        }

        public override MyServerCard GetCard(MyServerBattle battle)
        {
            return GetBase(battle).Cemetery.Seek(CardIndex);
        }

        public override bool SetCard(MyServerCard card, MyServerBattle battle)
        {
            // TODO

            return false;
        }

        public override bool TryGetCard(out MyServerCard card, MyServerBattle battle)
        {
            var cemetery = GetBase(battle).Cemetery;
            if (0 <= CardIndex && cemetery.GetChildCount() < CardIndex)
            {
                card = cemetery.Seek(CardIndex);
                return true;
            }
            else
            {
                card = null;
                return false;
            }
        }
    }
}
