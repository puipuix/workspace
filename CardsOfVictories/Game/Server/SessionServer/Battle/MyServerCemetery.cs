using CardsOfVictories.Game.Server.Battle;
using Godot;
using GodotUtils;
using System;
using System.Collections.Generic;
using System.Linq;

public class MyServerCemetery : MyServerCardCollectionBase
{
    public void Push(MyServerCard card)
    {
        AddCard(card);
        MoveChild(card, 0);
    }

    public override void PushAll(IEnumerable<MyServerCard> cards)
    {
        cards.Reverse().ForEach(AddCard);
    }
}
