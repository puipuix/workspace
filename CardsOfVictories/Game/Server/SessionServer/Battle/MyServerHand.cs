using Godot;
using System;
using System.Collections.Generic;
using System.Linq;

public class MyServerHand : Node
{
    public IEnumerable<MyServerCard> Cards => GetChildren().OfType<MyServerCard>();

    public void AddCard(MyServerCard card)
    {
        card.GetParent()?.RemoveChild(card);
        AddChild(card);
    }

    public bool RemoveCard(MyServerCard card)
    {
        return RemoveCard(card.Name);
    }

    public bool RemoveCard(string name)
    {
        if (TryGetCard(name, out var card))
        {
            RemoveChild(card);
            return true;
        } else
        {
            return false;
        }
    }

    public MyServerCard GetCard(string name)
    {
        return GetNodeOrNull<MyServerCard>(name);
    }

    public bool TryGetCard(string name, out MyServerCard card)
    {
        card = GetNodeOrNull<MyServerCard>(name);
        return card != null;
    }
}
