using Godot;
using System;
using System.Collections.Generic;
using System.Linq;
using GodotUtils;
using CardsOfVictories.Game.Server.Battle;

public class MyServerDeck : MyServerCardCollectionBase
{
    public void PushLast(MyServerCard card)
    {
        AddCard(card);
    }

    public void Push(MyServerCard card, int idx)
    {
        AddCard(card);
        MoveChild(card, idx);
    }

    public void PushFirst(MyServerCard card)
    {
        Push(card, 0);
    }

    public void PushRandom(MyServerCard card, int idxMin = 0, int idxMax = -1)
    {
        idxMax = idxMax < 0 ? GetChildCount() : idxMax;
        Push(card, MyTools.RNG.Next(idxMin, idxMax));
    }

    public override void PushAll(IEnumerable<MyServerCard> cards)
    {
        cards.ForEach(AddCard);
    }
}
