using Godot;
using System;

public class MyServerTerrain : Node
{
    private MyServerCard[,] _cards;

    public MyServerCard this[int i, int j]
    {
        get => _cards[i, j];
    }

    public bool AddCard(int i, int j, MyServerCard card)
    {
        if (IsCellFree(i, j))
        {
            _cards[i, j] = card;
            card.GetParentOrNull<Node>()?.RemoveChild(card);
            AddChild(card);
            return true;
        }
        else
        {
            return false;
        }
    }

    public bool RemoveCard(int i, int j)
    {
        if (TryGetCard(i, j, out var card))
        {
            RemoveChild(card);
            _cards[i, j] = null;
            return true;
        } else
        {
            return false;
        }
    }

    public bool TryGetCard(int i, int j, out MyServerCard card)
    {
        if (IsCellFree(i, j))
        {
            card = null;
            return false;
        }
        else
        {
            card = _cards[i, j];
            return true;
        }
    }

    public bool IsCellFree(int i, int j)
    {
        return this[i, j] == null;
    }
}
