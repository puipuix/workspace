﻿using Godot;
using GodotUtils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CardsOfVictories.Game.Server.Battle
{
    public abstract class MyServerCardCollectionBase : Node
    {
        public IEnumerable<MyServerCard> Cards => GetChildren().OfType<MyServerCard>();

        public MyServerCard Seek(int idx = 0) => GetChild<MyServerCard>(idx);

        public MyServerCard Pop(int idx = 0)
        {
            var card = Seek(idx);
            RemoveChild(card);
            return card;
        }

        public MyServerCard PopRandom(int idxMin = 0, int idxMax = -1)
        {
            idxMax = idxMax < 0 ? GetChildCount() : idxMax;
            return Pop(MyTools.RNG.Next(idxMin, idxMax));
        }

        protected void AddCard(MyServerCard card)
        {
            card.GetParentOrNull<Node>()?.RemoveChild(card);
            AddChild(card);
        }

        public abstract void PushAll(IEnumerable<MyServerCard> cards);

        public void PushAll(params MyServerCard[] cards)
        {
            PushAll((IEnumerable<MyServerCard>)cards);
        }
    }
}
