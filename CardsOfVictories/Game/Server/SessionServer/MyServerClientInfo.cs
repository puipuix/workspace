﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CardsOfVictories.Game.Server
{
    public class MyServerClientInfo
    {
        public int DBID { get; }
        public int PeerId { get; private set; }

        public MyServerClientInfo(int peerId, int dBID = -1)
        {
            DBID = dBID;
            PeerId = peerId;
        }

        public override string ToString()
        {
            return DBID + " (Peer:" + PeerId + ")";
        }
    }
}
