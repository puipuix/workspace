using Godot;
using System;
using System.Collections.Generic;

public class MyServerCard : Node
{
    private static Dictionary<int, Func<MyServerCard>> _generators = new Dictionary<int, Func<MyServerCard>>();

    public static MyServerCard Instance(int id)
    {
        return _generators[id]();
    }
}
