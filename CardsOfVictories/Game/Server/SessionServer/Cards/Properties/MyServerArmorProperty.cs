using Godot;
using System;

public class MyServerArmorProperty : MyServerProperty
{
    public virtual int Absorb(string damateType, int damage)
    {
        return Math.Max(0, damage - Value);
    }
}
