using Godot;
using System;
using GodotUtils;
public class MyServerProperty : Node, IMyAPIProperty
{
    [Export]
    public bool Public { get; set; } = false;

    [Export]
    public int Base { get; private set; }

    [MyOnReady("", EMyNodeResearchMode.Ancestor)]
    public MyServerCard Card { get; private set; }

    private int _bonus, _malus, _current;

    [Puppet]
    public int Value { get => Math.Max(_current + _bonus - _malus, 0); set { } }

    public override void _Ready()
    {
        base._Ready();
        MyOnReadyAttribute.SetUp(this);
    }

    private void FlushValue()
    {
        Rset(nameof(Value), Value);
    }

    public void AddBonus(int bonus)
    {
        _bonus += bonus;
        FlushValue();
    }

    public void RemoveBonus(int bonus)
    {
        _bonus = Math.Max(0, _bonus - bonus);
        FlushValue();
    }

    public void AddMalus(int malus)
    {
        _malus += malus;
        FlushValue();
    }

    public void RemoveMalus(int malus)
    {
        _malus = Math.Max(0, _malus - malus);
        FlushValue();
    }

    public void Recover(int amount)
    {
        _current = Math.Min(Base, _current + amount);
        FlushValue();
    }

    public void Reduce(int amount)
    {
        _current = Math.Max(0, _current - amount);
        FlushValue();
    }
}
