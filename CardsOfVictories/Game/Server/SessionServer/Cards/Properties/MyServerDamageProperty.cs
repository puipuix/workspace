using Godot;
using System;

public class MyServerDamageProperty : MyServerProperty
{
    [Export]
    public string DamageType { get; private set; } = "";
}
