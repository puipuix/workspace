using CardsOfVictories.Game;
using CardsOfVictories.Game.DataBase;
using CardsOfVictories.Game.NetworkAPI;
using CardsOfVictories.Game.Server;
using Godot;
using GodotUtils;
using System;
using System.Collections.Generic;

public class MyServerSession : MyServerBase, IMySession
{
    private static PackedScene _packedScene = GD.Load<PackedScene>("res://Game/Server/SessionServer/MyServerSession.tscn");
    public static MyServerSession Instance()
    {
        return (MyServerSession)_packedScene.Instance();
    }

    [MyOnReady("MySessionInfo")]
    public MySessionInfo SessionInfo { get; private set; }
    
    public override void _Ready()
    {
        base._Ready();
        MyOnReadyAttribute.SetUp(this);
    }

    public void CreateSession(string sessionId, int port, MySelectedDeck[] players)
    {
        Name = sessionId;
        SessionInfo.Initialize(players);
        StartServer(port, 32);
    }

    [PuppetSync]
    public void EndSession()
    {
        MyDebug.Print("Ending the session...");
        StopServer();
    }

    [Master]
    public void ConnectAsPlayer()
    {
        if (TryGetClientSenderId(out var client))
        {
            if (SessionInfo.AddPlayer(client))
            {
                MyDebug.Print("New player: " + client);
                if (SessionInfo.IsReadyToStart)
                {
                    this.Rpc(EndSession);
                } else
                {
                    MyDebug.Print("Waiting more player...");
                }
            }
            else
            {
                Kick(client.PeerId, "Connection as player refused");
            }
        }
    }

    [Master]
    public void ConnectAsSpectator(int player)
    {
        if (TryGetClientSenderId(out var client))
        {
            MyDebug.Print("New spectator: " + client);
            if (!SessionInfo.AddSpectator(client, player))
            {
                Kick(client.PeerId, "Connection as spectator refused");
            }
        }
    }
}
