﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CardsOfVictories.Game.NetworkAPI
{
    public interface IMyServerBase
    {
        /// <summary>
        /// Server
        /// </summary>
        /// <param name="token"></param>
        void IndentifyPeer(string token);

        /// <summary>
        /// Client
        /// </summary>
        /// <param name="valid"></param>
        void IsTokenValidated(bool valid);
    }
}
