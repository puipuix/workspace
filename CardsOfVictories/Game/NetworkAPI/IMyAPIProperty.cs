using Godot;
using System;

public interface IMyAPIProperty
{
    int Value { get; set; }
}
