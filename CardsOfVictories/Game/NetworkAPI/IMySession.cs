﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CardsOfVictories.Game.NetworkAPI
{
    public interface IMySession
    {
        /// <summary>
        /// Server
        /// </summary>
        void ConnectAsPlayer();

        /// <summary>
        /// Server
        /// </summary>
        /// <param name="player"></param>
        void ConnectAsSpectator(int player);

        /// <summary>
        /// Common
        /// </summary>
        void EndSession();
    }
}
