using CardsOfVictories.Game.DataBase;
using CardsOfVictories.Game.Server;
using Godot;
using GodotUtils;
using System;
using System.Collections.Generic;
using System.Linq;

public class MySessionInfo : Node
{
    MySelectedDeck[] _decks;
    MyServerClientInfo[] _players;
    List<MyServerClientInfo>[] _playersSpectators;
    List<MyServerClientInfo> _globalSpectators = new List<MyServerClientInfo>();

    public MyServerClientInfo GetPlayer(int playerId) => _players[playerId - 1];
    public List<MyServerClientInfo> GetSpectators(int playerId) => _playersSpectators[playerId - 1];
    public List<MyServerClientInfo> GetGlobalSpectators() => _globalSpectators;
    public string SessionId => GetParent().Name;

    public bool IsReadyToStart => _players.None(client => client == null);

    public void Initialize(params MySelectedDeck[] players)
    {
        _decks = players;
        _players = new MyServerClientInfo[_decks.Length];
        _playersSpectators = new List<MyServerClientInfo>[players.Length];
        for (int i = 0; i < players.Length; i++)
        {
            _playersSpectators[i] = new List<MyServerClientInfo>();
        }
    }

    /// <summary>
    /// Link a player to a deck. Return if the client is a player
    /// </summary>
    /// <param name="client"></param>
    /// <returns></returns>
    public bool AddPlayer(MyServerClientInfo client)
    {
        bool found = false;
        for (int i = 0; !found && i < _decks.Length; i++)
        {
            if (_decks[i].PlayerId == client.DBID)
            {
                _players[i] = client;
                found = true;
            }
        }
        return found;
    }

    public bool AddSpectator(MyServerClientInfo client, int player)
    {
        if (player > 0 && player <= _playersSpectators.Length)
        {
            GetSpectators(player).Add(client);
            return true;
        } else
        {
            return false;
        }
    }
}
