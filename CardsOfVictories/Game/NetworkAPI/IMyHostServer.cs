﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CardsOfVictories.Game.NetworkAPI
{
    public interface IMyHostServer : IMyServerBase
    {
        /// <summary>
        /// Host Server
        /// </summary>
        void RegisterToMatchMaking();

        /// <summary>
        /// Client
        /// </summary>
        void ConnectToSession(string sessionId, int port);
    }
}
