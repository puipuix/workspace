using Godot;
using GodotUtils;
using System;

public class MyHostingTest : Node2D
{
    [MyOnReady("Server/MyHost")]
    private MyHostServer _server = null;

    [MyOnReady("Client/MyHost")]
    private MyClientHost _client1 = null;

    [MyOnReady("Client2/MyHost")]
    private MyClientHost _client2 = null;

    [MyOnReady("CanvasLayer/VBoxContainer/ButtonServer")]
    private Button _startServer = null;

    [MyOnReady("CanvasLayer/VBoxContainer/ButtonClient")]
    private Button _startClient = null;
    [MyOnReady("CanvasLayer/VBoxContainer/ButtonClient2")]
    private Button _startClient2 = null; 
    
    [MyOnReady("CanvasLayer/VBoxContainer/ButtonMMClient")]
    private Button _mMClient = null;
    [MyOnReady("CanvasLayer/VBoxContainer/ButtonMMClient2")]
    private Button _mMClient2 = null;
    public override void _Ready()
    {
        MyOnReadyAttribute.SetUp(this);
        _startServer.Connect("pressed", this, StartServer);
        _startClient.Connect("pressed", this, StartClient);
        _startClient2.Connect("pressed", this, StartClient2);
        _mMClient.Connect("pressed", this, MMClient);
        _mMClient2.Connect("pressed", this, MMClient2);
    }

    private void StartServer()
    {
        _server.StartHostServer();
    }
    private void StartClient()
    {
        _client1.ConnectToHostServer("localhost", 55000, MyHostServer.CreateToken());
    }
    private void StartClient2()
    {
        _client2.ConnectToHostServer("localhost", 55000, MyHostServer.CreateToken());
    }
    private void MMClient()
    {
        _client1.RegisterToMatchMaking();
    }
    private void MMClient2()
    {
        _client2.RegisterToMatchMaking();
    }

    //  // Called every frame. 'delta' is the elapsed time since the previous frame.
    //  public override void _Process(float delta)
    //  {
    //      
    //  }
}
