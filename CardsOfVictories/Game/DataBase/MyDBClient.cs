﻿using GodotUtils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CardsOfVictories.Game.DataBase
{
    public static class MyDBClient
    {
        /// <summary>
        /// TEST ONLY, NEED TO LOOK AT THE DB
        /// </summary>
        private static Dictionary<string, int> _idFromToken = new Dictionary<string, int>();
        public static int GetDBIdFromToken(string token)
        {
            if (!_idFromToken.TryGetValue(token, out int id))
            {
                id = MyTools.RNG.Next(1, int.MaxValue);
                _idFromToken.Add(token, id);
            }
            return id;
        }
    }
}
