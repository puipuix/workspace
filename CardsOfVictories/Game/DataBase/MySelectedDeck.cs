﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CardsOfVictories.Game.DataBase
{
    public class MySelectedDeck
    {
        public int PlayerId { get; }
        public IEnumerable<int> Cards { get; }
        public int BR { get; }

        public MySelectedDeck(IEnumerable<int> cards, int bR, int playerId)
        {
            PlayerId = playerId;
            Cards = cards;
            BR = bR;
        }

        public static MySelectedDeck GetSelectedDeck(int dbid)
        {
            return new MySelectedDeck(new int[0], 0, dbid);
        }
    }
}
