﻿using CardsOfVictories.Game.NetworkAPI;
using Godot;
using GodotUtils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CardsOfVictories.Game.Client
{
    public abstract class MyClientBase : Node2D, IMyServerBase
    {
        public NetworkedMultiplayerENet Network => (NetworkedMultiplayerENet)CustomMultiplayer.NetworkPeer;
        public string Token { get; protected set; }

        protected string _serverIp;
        protected int _serverPort;

        public void ConnectToServer(string ip, int port)
        {
            MyDebug.Print("Connecting to server '" + ip + ":" + port + "'...");
            _serverIp = ip;
            _serverPort = port; 
            CustomMultiplayer = new MultiplayerAPI();
            CustomMultiplayer.Connect("connected_to_server", this, OnConnected);
            CustomMultiplayer.Connect("connection_failed", this, OnConnectionFailed);
            CustomMultiplayer.Connect("server_disconnected", this, OnDisconnected);
            var network = new NetworkedMultiplayerENet();
            var err = network.CreateClient(ip, port);
            if (err != Error.Ok)
            {
                MyDebug.PrintErr("\t" + err + "!");
                CustomMultiplayer.Disconnect("connected_to_server", this, OnConnected);
                CustomMultiplayer.Disconnect("connection_failed", this, OnConnectionFailed);
                CustomMultiplayer.Disconnect("server_disconnected", this, OnDisconnected);
                CustomMultiplayer = null;
            }
            else
            {
                CustomMultiplayer.SetRootNode(this);
                CustomMultiplayer.NetworkPeer = network;
                MyDebug.Print("\tDone.");
            }
        }

        private void OnConnected()
        {
            MyDebug.Print("Connected to server.");
            GetTree().CreateTimer(1).Connect<string>("timeout", this, IndentifyPeer, Token);
        }

        [Puppet]
        public void IsTokenValidated(bool valid)
        {
            if (valid)
            {
                OnTokenValidated();
            } else
            {
                OnConnectionFailed();
            }
        }

        protected virtual void OnTokenValidated() {
            MyDebug.Print("Connection validated.");
        }
        protected virtual void OnConnectionFailed() {
            MyDebug.Print("Connection failed.");
        }
        protected virtual void OnDisconnected() {
            MyDebug.Print("Disconnected.");
        }


        public void DisconnectFromServer()
        {
            MyDebug.Print("Disconnecting...");
            Network.CloseConnection(); 
            CustomMultiplayer.NetworkPeer = null;
            CustomMultiplayer = null;
        }

        /// <summary>
        /// No direct effect, redirect call to the server.
        /// </summary>
        /// <param name="token"></param>
        [Master]
        public void IndentifyPeer(string token)
        {
            MyDebug.Print("Sending token...");
            this.RpcId(1, IndentifyPeer, token);
        }

        public override void _Process(float delta)
        {
            base._Process(delta);
            CustomMultiplayer?.Poll();
        }
    }
}
