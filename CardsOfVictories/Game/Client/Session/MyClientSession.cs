using CardsOfVictories.Game.Client;
using CardsOfVictories.Game.NetworkAPI;
using Godot;
using GodotUtils;
using System;

public class MyClientSession : MyClientBase, IMySession
{
    private static PackedScene _packedScene = GD.Load<PackedScene>("res://Game/Client/Session/MyClientSession.tscn");

    private int _spectating;


    public static MyClientSession Instance()
    {
        return (MyClientSession)_packedScene.Instance();
    }

    public void JoinSession(string ip, int port, string sessionId, string token, int spectatingPlayer = 0)
    {
        _spectating = spectatingPlayer;
        Token = token;
        Name = sessionId;
        ConnectToServer(ip, port);
    }

    protected override void OnTokenValidated()
    {
        base.OnTokenValidated();
        if (_spectating > 0)
        {
            ConnectAsSpectator(_spectating);
        }
        else
        {
            ConnectAsPlayer();
        }
    }

    [Master]
    public void ConnectAsPlayer()
    {
        MyDebug.Print("Connecting as player...");
        this.RpcId(1, ConnectAsPlayer);
    }

    [Master]
    public void ConnectAsSpectator(int player)
    {
        MyDebug.Print("Connecting as spectator of player " + player + "...");
        this.RpcId(1, ConnectAsSpectator, player);
    }
    
    [PuppetSync]
    public void EndSession()
    {
        MyDebug.Print("Session is ended.");
    }
}
