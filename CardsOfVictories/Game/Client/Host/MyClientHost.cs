using CardsOfVictories.Game.NetworkAPI;
using Godot;
using System;
using CardsOfVictories.Game.Client;
using GodotUtils;

public class MyClientHost : MyClientBase, IMyHostServer
{
    public override void _Ready()
    {
        base._Ready();
    }

    public void ConnectToHostServer(string ip, int port, string token)
    {
        Token = token;
        ConnectToServer(ip, port);
    }
    /// <summary>
    /// No direct effect, redirect call to the server.
    /// </summary>
    [Master]
    public void RegisterToMatchMaking()
    {
        MyDebug.Print("Register to match making...");
        this.RpcId(1, RegisterToMatchMaking);
    }

    [Puppet]
    public void ConnectToSession(string sessionId, int port)
    {
        MyDebug.Print("Connecting to session '" + sessionId + "' at " + _serverIp + ":" + port + "...");
        var session = MyClientSession.Instance();
        GetParent().AddChild(session);
        GetParent().RemoveChild(this);
        session.JoinSession(_serverIp, port, sessionId, Token);
        DisconnectFromServer();
    }
}
